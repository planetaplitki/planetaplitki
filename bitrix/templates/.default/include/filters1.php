<form class="form-inline header-filter hidden-xs" role="form">
    <div class="row">
        <div class="col-sm-4 col-md-2">
            <label class="sr-only" for="height"></label>
            <input type="text" class="filters" id="height" placeholder="Высота">

        </div>
        <div class="col-sm-4 col-md-2">
            <label class="sr-only" for="width"></label>
            <input type="text" class="filters" id="width" placeholder="Ширины">
        </div>
        <div class="col-sm-4 col-md-2">
            <select class="filters select" id="select_type1" name="type1">
                <option value="1">Матовая</option>
                <option value="2">Не матовая</option>
            </select>
        </div>
        <div class="col-sm-4 col-md-2">
            <button type="button" class="btn slider-buttom">Цена</button>
<!--            <label class="sr-only" for="price"></label>-->
<!--            <input type="text" class="filters" id="price" placeholder="Цена">-->

            <div class="arrow_box">
                <div class="range">
                    <div id="slider-range">
                        <input type="text" class="ui-slider-handle ui-state-default ui-corner-all " id="min_cost">
                        <input type="text" class="ui-slider-handle ui-state-default ui-corner-all " id="max_cost">
                    </div>
                </div>

                <hr class="arrow-line">
                <button type="button" class="btn link-buttom">применить</button>
            </div>


        </div>

        <div class="col-sm-4 col-md-2">
            <select class="filters select" id="select2_type2" name="type2">
                <option value="1">Напольная</option>
                <option value="2">Не напольная</option>
            </select>
        </div>
        <div class="col-sm-4 col-md-2">
            <div class="text-box"><a href="#">Расширенный поиск</a></div>
        </div>
    </div>
    <div class="row">
        <div class="col-sm-4 col-md-2">
            <select class="filters select" id="country" name="country">
                <option value="1">Россия</option>
                <option value="2">Испания</option>
            </select>
        </div>
        <div class="col-sm-4 col-md-2">
            <select class="filters select" id="brend" name="brend">
                <option value="1">Бренд 1</option>
                <option value="2">Бренд 2</option>
            </select>
        </div>
        <div class="col-sm-4 col-md-2">
            <select class="filters select" id="collection" name="collection">
                <option value="1">Коллекция 1</option>
                <option value="2">Коллекция 2</option>
            </select>
        </div>
        <div class="col-sm-4 col-md-2">
            <select class="filters select" id="topics" name="topics">
                <option value="1">Тематика 1</option>
                <option value="2">Тематика 2</option>
            </select>
        </div>
        <div class="col-sm-4 col-md-2">
            <select class="filters select" id="color" name="color">
                <option value="1">Цвет 1</option>
                <option value="2">Цвет 2</option>
            </select>
        </div>
        <div class="col-sm-4 col-md-2">
            <button type="reset" class="btn reset-buttom">Сбросить фильтр</button>
        </div>
    </div>
</form>