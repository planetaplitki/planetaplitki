<a data-toggle="collapse" href="#menu-filtr">
    <div class="hidden-sm hidden-md hidden-lg chouse-box">
<!--        <div class="text">уточнить<br>выбор</div>-->
        <div class="gear"></div>
    </div>
</a>
<div class="collapse menu-col-mob" id="menu-filtr">
    <div>
        <form class="form-inline header-filter" role="form">
            <ul class="menu-top-box-mob">
                <li>
                    <a data-toggle="collapse" data-target="#toggleDemo1" data-parent="#sidenav1">
                        Габариты
                        <div class="col-mob">
                            <span class="point"></span>
                            <div class="text2">ПОКАЗАТЬ</div>
                        </div>
                    </a>
                    <div class="collapse" id="toggleDemo1">
                        <div class="row obj50">
                            <div class="col-xs-6">
                                <label class="sr-only" for="height"></label>
                                <input type="text" class="filters mnone" id="height" placeholder="Высота">
                            </div>
                            <div class="col-xs-6">
                                <label class="sr-only" for="width"></label>
                                <input type="text" class="filters mnone" id="width" placeholder="Ширины">
                            </div>
                        </div>
                    </div>
                </li>
                <li>
                    <a data-toggle="collapse" data-target="#toggleDemo2" data-parent="#sidenav2">
                        Цена
                        <div class="col-mob">
                            <span class="point"></span>
                            <div class="text2">ПОКАЗАТЬ</div>
                        </div>
                    </a>
                    <div class="collapse" id="toggleDemo2">
                        <div class="row obj50">
                            <div class="col-xs-6">
                                <label class="sr-only" for="price"></label>
                                <input type="text" class="filters" id="price" placeholder="От 0 руб.">
                            </div>
                            <div class="col-xs-6">
                                <label class="sr-only" for="width"></label>
                                <input type="text" class="filters mnone" id="width" placeholder="До 100 000 руб.">
                            </div>
                        </div>
                    </div>
                </li>
                <li>
                    <a data-toggle="collapse" data-target="#toggleDemo3" data-parent="#sidenav3">
                        Тип
                        <div class="col-mob">
                            <span class="point"></span>
                            <div class="text2">ПОКАЗАТЬ</div>
                        </div>
                    </a>
                    <div class="collapse" id="toggleDemo3">
                        <div class="row obj50">
                            <div class="col-xs-6">
                                <select class="filters select" id="type1" name="type1">
                                    <option>Матовая</option>
                                    <option>Не матовая</option>
                                </select>
                            </div>
                            <div class="col-xs-6">
                                <select class="filters select" id="type2" name="type2">
                                    <option>Напольная</option>
                                    <option>Не напольная</option>
                                </select>
                            </div>
                        </div>
                    </div>
                </li>
                <li>
                    <a data-toggle="collapse" data-target="#toggleDemo4" data-parent="#sidenav4">
                        Производитель
                        <div class="col-mob">
                            <span class="point"></span>
                            <div class="text2">ПОКАЗАТЬ</div>
                        </div>
                    </a>
                    <div class="collapse" id="toggleDemo4">
                        <div class="row obj50">
                            <div class="col-xs-6">
                                <select class="filters select" id="country" name="country">
                                    <option>Россия</option>
                                    <option>Испания</option>
                                </select>
                            </div>
                            <div class="col-xs-6">
                                <select class="filters select" id="brend" name="brend">
                                    <option>Бренд 1</option>
                                    <option>Бренд 2</option>
                                </select>
                            </div>
                        </div>
                    </div>
                </li>
                <li>
                    <a data-toggle="collapse" data-target="#toggleDemo5" data-parent="#sidenav5">
                        Коллекция \ тематика
                        <div class="col-mob">
                            <span class="point"></span>
                            <div class="text2">ПОКАЗАТЬ</div>
                        </div>
                    </a>
                    <div class="collapse" id="toggleDemo5">
                        <div class="row obj50">
                            <div class="col-xs-6">
                                <select class="filters select" id="collection" name="collection">
                                    <option>Коллекция 1</option>
                                    <option>Коллекция 2</option>
                                </select>
                            </div>
                            <div class="col-xs-6">
                                <select class="filters select" id="topics" name="topics">
                                    <option>Тематика 1</option>
                                    <option>Тематика 2</option>
                                </select>
                            </div>
                        </div>
                    </div>
                </li>
            </ul>
            <div class="buttons">
                <button type="button" class="btn link-buttom">ПЕРЕЙТИ В КАТАЛОГ</button>
                <button type="button" class="btn link-up">НАВЕРХ<span class="icon-up"></span></button>
            </div>
        </form>
    </div>
</div>