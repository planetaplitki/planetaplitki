<p><a href="http://<?$_SERVER['HTTP_HOST']?>/"><b>Главная</b></a></p>

<?$APPLICATION->IncludeComponent("bitrix:menu", "sitemap", array(
    "ROOT_MENU_TYPE" => "topright",
    "MAX_LEVEL" => "1",
    "CHILD_MENU_TYPE" => "left",
    "USE_EXT" => "Y",
    "MENU_CACHE_TYPE" => "A",
    "MENU_CACHE_TIME" => "36000000",
    "MENU_CACHE_USE_GROUPS" => "Y",
    "MENU_CACHE_GET_VARS" => ""
),
    false,
    array(
        "ACTIVE_COMPONENT" => "Y"
    )
);?>
<?$APPLICATION->IncludeComponent("bitrix:menu", "sitemap", array(
    "ROOT_MENU_TYPE" => "topsimple",
    "MAX_LEVEL" => "1",
    "CHILD_MENU_TYPE" => "left",
    "USE_EXT" => "Y",
    "MENU_CACHE_TYPE" => "A",
    "MENU_CACHE_TIME" => "36000000",
    "MENU_CACHE_USE_GROUPS" => "Y",
    "MENU_CACHE_GET_VARS" => ""
),
    false,
    array(
        "ACTIVE_COMPONENT" => "Y"
    )
);?>
<p><a href="/catalog/"><b>Каталог продукции</b></a></p>
<?$APPLICATION->IncludeComponent(
    "bitrix:catalog.section.list",
    "sitemap",
    array(
        "IBLOCK_TYPE" => "1c_catalog",
        "IBLOCK_ID" => "34",
        "SECTION_ID" => "0",
        "SECTION_CODE" => "",
        "COUNT_ELEMENTS" => "N",
        "TOP_DEPTH" => "3",
        "SECTION_FIELDS" => array(
            0 => "NAME",
            1 => "",
        ),
        "SECTION_USER_FIELDS" => array(
            0 => "",
            1 => "",
        ),
        "SECTION_URL" => "/catalog/#SECTION_CODE_PATH#/",
        "CACHE_TYPE" => "A",
        "CACHE_TIME" => "3600",
        "CACHE_GROUPS" => "Y",
        "ADD_SECTIONS_CHAIN" => "Y",
        "COMPONENT_TEMPLATE" => "sitemap"
    ),
    false,
    array(
        "ACTIVE_COMPONENT" => "Y"
    )
);?>