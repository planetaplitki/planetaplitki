<div class="row obj hidden-sm hidden-md hidden-lg">
    <div class="col-xs-12">
        <p class="chouse-text">Подходят: <span>502</span> моделей</p>
    </div>
</div>
<div class="row">
    <div class="col-xs-6 col-sm-4">
        <div class="label-text2">Статус</div>
        <select class="filters2" id="status" name="status">
            <option>status 1</option>
            <option>status 2</option>
        </select>
    </div>
    <div class="col-xs-6 col-sm-4">
        <div class="label-text2">Сортировать по</div>
        <select class="filters2" id="sort" name="sort">
            <option>sort 1</option>
            <option>sort 2</option>
        </select>
    </div>
    <div class="col-sm-4 text-right hidden-xs">
        <div class="label-text">Показывать по:</div>
        <div class="list-block">
            <ul class="list-block-item">
                <li class="active"><a href="#">20</a></li>
                <li><a href="#">40</a></li>
                <li><a href="#">80</a></li>
                <li><a href="#">Все</a></li>
            </ul>
        </div>
    </div>
</div>