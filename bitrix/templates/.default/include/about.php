<h1><span style="font-size: 15pt;">ПЛАНЕТА ПЛИТКИ - КЕРАМИЧЕСКАЯ ПЛИТКА ОТ ЛУЧШИХ ЕВРОПЕЙСКИХ ФАБРИК&nbsp;&nbsp;</span></h1>
<p style="text-align: justify;">
 <span style="line-height: 1.2em; text-align: justify;">Вас приветствует интернет-магазин керамической плитки в Москве “Планета Плитки”. Начиная с 1995 года компания является ответственным и надежным поставщиком оригинальной продукции от лучших европейских фабрик. Уже более 20 лет философия индивидуального подхода к каждому клиенту и профессиональное отношение даже к самым мелким деталям позволяют нам гарантировать всем покупателям и партнерам наивысший уровень обслуживания.</span>
</p>
<p style="text-align: justify;">
	 Купить плитку в Москве в сети магазинов “Планета Плитки” — значит сделать выбор в пользу:
</p>
<ul style="text-align: justify;" type="square">
	<li> <em>высококлассных изделий ведущих мастеров Испании и Италии;</em></li>
	<li><em> качества, соответствующего самым строгим международным стандартам;</em></li>
	<li><em> экологичной и долговечной продукции;</em></li>
	<li><em> оптимальной цены на всем рынке России;</em></li>
	<li><em> признанных экспертов с безупречной репутацией.</em></li>
</ul>
<h2><span style="font-size: 15pt;">Интернет-магазин PlanetaPlitki.ru: почему стоит сделать покупку в нашей компании?</span></h2>
<div class="why_do_we_buy">
	<table width="100%" cellpadding="2" cellspacing="1">
	<tbody>
	<tr>
		<td>
			 &nbsp; <img width="50" alt="ic-1.png" src="/upload/medialibrary/f4c/ic_1.png" height="50" title="ic-1.png">
		</td>
		<td>
			 Мы импортируем плитку с 1995 года и являемся официальными дистрибьюторами ряда испанских и итальянских фабрик. <br>
			 Качество и оригинальность товара гарантированны.
		</td>
	</tr>
	<tr>
		<td>
			 &nbsp;<img width="50" alt="ic-2.png" src="/upload/medialibrary/bd7/ic_2.png" height="50" title="ic-2.png">
		</td>
		<td>
			 Более 100 эксклюзивных коллекций!
		</td>
	</tr>
	<tr>
		<td>
			 &nbsp;<img width="50" alt="ic-3.png" src="/upload/medialibrary/cfc/ic_3.png" height="50" title="ic-3.png">
		</td>
		<td>
			 Гибкая система скидок, различные акции для экономии ваших средств;
		</td>
	</tr>
	<tr>
		<td>
			 &nbsp;<img width="50" alt="ic-4.png" src="/upload/medialibrary/bba/ic_4.png" height="50" title="ic-4.png">
		</td>
		<td>
			 Онлайн-оплата банковской картой не выходя из дома;
		</td>
	</tr>
	</tbody>
	</table>
</div>
<div class="why_do_we_buy">
	<table width="100%" cellpadding="2" cellspacing="1">
	<tbody>
	<tr>
		<td>
			 &nbsp;<img width="50" alt="ic-5.png" src="/upload/medialibrary/2a6/ic_5.png" height="50" title="ic-5.png">
		</td>
		<td>
			 Большой выбор керамической плитки, керамогранита и других отделочных материалов в каталоге по актуальным ценам;
		</td>
	</tr>
	<tr>
		<td>
			 &nbsp;<img width="50" alt="ic-6.png" src="/upload/medialibrary/ceb/ic_6.png" height="50" title="ic-6.png">
		</td>
		<td>
			 Последние коллекции испанской, итальянской и португальской плитки, представленные на мировых выставках;
		</td>
	</tr>
	<tr>
		<td>
			 &nbsp;<img width="50" alt="ic-7.png" src="/upload/medialibrary/25a/ic_7.png" height="50" title="ic-7.png">
		</td>
		<td>
			 Сеть магазинов в Москве, салон-магазин на Нахимовском пр-те 50 площадью 310 кв.м. Самое большое&nbsp;количество&nbsp;образцов! Здесь Вам помогут сделать правильный выбор!
		</td>
	</tr>
	<tr>
		<td>
			 &nbsp;<img width="50" alt="ic-8.png" src="/upload/medialibrary/490/ic_8.png" height="50" title="ic-8.png">
		</td>
		<td>
			 Доставка заказа по Москве и МО.
		</td>
	</tr>
	</tbody>
	</table>
</div>
<div class="clear">
</div>
 <?/*<table width="100%" cellpadding="10" cellspacing="1">
<tbody>
<tr>
	<td>
		 &nbsp;<img width="56" alt="ic-1.png" src="/upload/medialibrary/31a/ic_1.png" height="56" title="ic-1.png">
	</td>
	<td>
		 Большой выбор керамической плитки, керамогранита и других отделочных материалов в каталоге по актуальным ценам;
	</td>
	<td>
		 &nbsp;
	</td>
	<td>
		 &nbsp;<img width="56" alt="ic-4.png" src="/upload/medialibrary/202/ic_4.png" height="56" title="ic-4.png">
	</td>
	<td>
		 Гибкая система скидок, различные акции для экономии ваших средств;
	</td>
</tr>
<tr>
	<td>
		 &nbsp;<img width="56" alt="ic-2.png" src="/upload/medialibrary/9d6/ic_2.png" height="56" title="ic-2.png">
	</td>
	<td>
		 Оформление заказа на сайте с возможностью онлайн-оплаты банковской картой не выходя из дома;
	</td>
	
	<td>
		 &nbsp; &nbsp;&nbsp;<img width="56" alt="ic-5.png" src="/upload/medialibrary/ba4/ic_5.png" height="56" title="ic-5.png">
	</td>
	<td>
		 Сеть магазинов в Москве, салон-магазин на Нахимовском пр-те, 50 с самым большим количеством образцов (площадь салона 310 кв.м.). <br>
		 Здесь Вам помогут сделать правильный выбор!
	</td>
</tr>
<tr>
	<td>
		 &nbsp;<img width="56" alt="ic-3.png" src="/upload/medialibrary/8d9/ic_3.png" height="56" title="ic-3.png">
	</td>
	<td>
		 Доставка заказа по Москве и МО;
	</td>
	<td>
		 &nbsp;
	</td>
	<td>
		 &nbsp; &nbsp; &nbsp;<img width="56" alt="ic-6.png" src="/upload/medialibrary/c6f/ic_6.png" height="56" title="ic-6.png">
	</td>
	<td>
		 Мы импортируем плитку с 1995 года и являемся официальными дистрибьюторами ряда испанских и итальянских фабрик. <br>
		 Качество и оригинальность товара гарантированно.
	</td>
</tr>
</tbody>
</table>
*/?>
<h2 style="text-align: justify;"><span style="font-size: 15pt;">Самый большой выбор плитки в Москве</span></h2>
<div style="line-height: 1.4em;">
	<p style="text-align: justify;">
		 Мы рады предложить покупателям различные варианты керамической плитки - от стандартной для дома до элитных моделей, идеально подходящих престижным отелям и ресторанам. У нас Вы можете купить кафель и керамогранит для объектов с большой проходимостью, например, магазинов и торговых центров. Эксклюзивные модели керамогранита испанских производителей <strong><strong>CERACASA</strong>, </strong><strong>DURSTONE, AZULEV, VENUS, </strong><strong><strong>GAYA FORES</strong>, GRESART, ROCERSA, PAMESA, ROCA</strong>, обладают привлекательным внешним видом, а также высокими показателями износоустойчивости.
	</p>
	<div style="line-height: 1.4em;">
		<p style="text-align: justify;">
 <span style="line-height: 1.4em;">Сегодня сеть наших магазинов - одна из крупнейших в городе&nbsp;Москва. Также </span><strong style="line-height: 1.4em;">наши дилеры работают во Владивостоке, Омске, Вологде, Уфе, Воронеже, Екатеринбурге</strong><span style="line-height: 1.4em;">. Динамичное развитие компании говорит о том, что керамическая плитка от компании «Планета Плитки» действительно пользуется огромной популярностью.&nbsp;</span>Своим клиентам <strong>«Планета Плитки»</strong> готова предложить коллекции крупнейших испанских производителей: <strong>APE,</strong><strong> CAS, AZULEV, LATINA, ROCA, IBERO, VENUS, </strong><strong>CERACASA</strong><strong>, EL MOLINO, MAINZU</strong><strong>, PAMESA, ROCERSA, GAYA FORES, GRESART</strong> и других фабрик. А так же таких крупных итальянских производителей как <strong>SERENISSIMA</strong>, <strong>ARIANA, ALTA CERAMICA, DOMUS LINEA, ELIOS CERAMICA</strong>.&nbsp;
		</p>
	</div>
</div>
 <br>