<?
$sections = getWholesaleSectionsID();
$countries = $sections['COUNTRIES'];
$brands = $sections['BRANDS'];
$collections = $sections['COLLECTIONS'];
?>

<div class="main opt_page">

    <div class="block1 opt">
        <?$APPLICATION->IncludeComponent(
            "bitrix:news.list",
            "m_mainPageLinks",
            array(
                "IBLOCK_TYPE" => "data",
                "IBLOCK_ID" => "36",
                "NEWS_COUNT" => "8",
                "SORT_BY1" => "SORT",
                "SORT_ORDER1" => "ASC",
                "SORT_BY2" => "ID",
                "SORT_ORDER2" => "ASC",
                "FILTER_NAME" => "",
                "FIELD_CODE" => array(
                    0 => "ID",
                    1 => "NAME",
                    2 => "PREVIEW_PICTURE",
                    3 => "DATE_ACTIVE_FROM",
                    4 => "",
                ),
                "PROPERTY_CODE" => array(
                    0 => "CLASS",
                    1 => "LINK",
                    2 => "PICTURE",
                ),
                "CHECK_DATES" => "Y",
                "DETAIL_URL" => "",
                "AJAX_MODE" => "N",
                "AJAX_OPTION_JUMP" => "N",
                "AJAX_OPTION_STYLE" => "N",
                "AJAX_OPTION_HISTORY" => "N",
                "CACHE_TYPE" => "A",
                "CACHE_TIME" => "36000000",
                "CACHE_FILTER" => "Y",
                "CACHE_GROUPS" => "Y",
                "PREVIEW_TRUNCATE_LEN" => "150",
                "ACTIVE_DATE_FORMAT" => "d.m.Y",
                "SET_TITLE" => "N",
                "SET_STATUS_404" => "N",
                "INCLUDE_IBLOCK_INTO_CHAIN" => "N",
                "ADD_SECTIONS_CHAIN" => "N",
                "HIDE_LINK_WHEN_NO_DETAIL" => "N",
                "PARENT_SECTION" => "",
                "PARENT_SECTION_CODE" => "",
                "INCLUDE_SUBSECTIONS" => "Y",
                "PAGER_TEMPLATE" => "",
                "DISPLAY_TOP_PAGER" => "N",
                "DISPLAY_BOTTOM_PAGER" => "N",
                "PAGER_TITLE" => "Новости",
                "PAGER_SHOW_ALWAYS" => "N",
                "PAGER_DESC_NUMBERING" => "N",
                "PAGER_DESC_NUMBERING_CACHE_TIME" => "36000",
                "PAGER_SHOW_ALL" => "N",
                "DISPLAY_DATE" => "N",
                "DISPLAY_NAME" => "N",
                "DISPLAY_PICTURE" => "N",
                "DISPLAY_PREVIEW_TEXT" => "N",
                "AJAX_OPTION_ADDITIONAL" => "",
                "SET_BROWSER_TITLE" => "N",
                "SET_META_KEYWORDS" => "N",
                "SET_META_DESCRIPTION" => "N"
            ),
            false
        );?>
    </div>

    <div class="container">
        <div class="row">
            <div class="col-sm-3">
                <div class="box-inner-country">
                    <div class="tab-country">
                        Страны
                    </div>
                    <div class="country-list">
                        <p>Все страны</p>
                        <?
                        $GLOBALS['arrSectionsFilter'] = array();
                        $GLOBALS['arrSectionsFilter']['ID'] = $countries;
                        ?>
                        <?$APPLICATION->IncludeComponent(
                            "up:iblock.section.list",
                            "wholesale.mainpage.list",
                            array(
                                "ID" => "0",
                                "SHOW_PARENT_NAME" => "Y",
                                "IBLOCK_TYPE" => "1c_catalog",
                                "IBLOCK_ID" => 34,
                                "SECTION_ID" => $arResult['VARIABLES']['SECTION_ID'],
                                "SECTION_CODE" => "",
                                "SECTION_URL" => "",
                                "COUNT_ELEMENTS" => "Y",
                                "TOP_DEPTH" => "3",
                                "SECTION_FIELDS" => array(
                                    0 => "ID",
                                    1 => "NAME",
                                    2 => "PICTURE",
                                    3 => "SECTION_PAGE_URL",
                                    4 => "DEPTH_LEVEL",
                                    5 => "IBLOCK_SECTION_ID",
                                ),
                                "SECTION_USER_FIELDS" => array(
                                    0 => "UF_MIN_PRICE",
                                    1 => "UF_NEW_COLLECTION",
                                    2 => "UF_EXCLUSIVE",
                                    3 => "UF_SALE",
                                ),
                                "ADD_SECTIONS_CHAIN" => "Y",
                                "CACHE_TYPE" => "A",
                                "CACHE_TIME" => "36000000",
                                "CACHE_NOTES" => "",
                                "CACHE_GROUPS" => "Y",
                                "COMPONENT_TEMPLATE" => "collections.list",
                                "FILTER_NAME" => "arrSectionsFilter",
                                "SORT_FIELD" => $sortField,
                                "SORT_ORDER" => $sortOrder,
                                "SORT_FIELD2" => "id",
                                "SORT_ORDER2" => "desc",
                                "URL_404" => "/404.php",
                                "ELEMENT_CONTROLS" => "N",
                                "AJAX_MODE" => "N",
                                "AJAX_OPTION_JUMP" => "N",
                                "AJAX_OPTION_STYLE" => "N",
                                "AJAX_OPTION_HISTORY" => "N",
                                "AJAX_OPTION_ADDITIONAL" => "",
                                "CACHE_FILTER" => "Y",
                                "PAGER_TEMPLATE" => "modern",
                                "DISPLAY_TOP_PAGER" => "N",
                                "DISPLAY_BOTTOM_PAGER" => "Y",
                                "PAGER_TITLE" => "",
                                "PAGER_SHOW_ALWAYS" => "N",
                                "PAGER_DESC_NUMBERING" => "N",
                                "PAGER_DESC_NUMBERING_CACHE_TIME" => "36000",
                                "PAGE_ELEMENT_COUNT" => 1000,
                                "PAGER_SHOW_ALL" => "Y"
                            ),
                            false
                        );?>
                    </div>
                </div>
            </div>
            <div class="col-sm-9">
                <ul class="nav nav-tabs" role="tablist">
                    <li class="active"><a href="#proizvod" role="tab" data-toggle="tab">Производители</a></li>
                    <li><a href="#oformlenie" role="tab" data-toggle="tab">Коллекции</a></li>
                </ul>

                <div class="tab-content">
                    <div class="tab-pane fade in active" id="proizvod">
                        <div class="box-inner">
                            <?
                            $GLOBALS['arrSectionsFilter'] = array();
                            $GLOBALS['arrSectionsFilter']['ID'] = $brands;
                            ?>
                            <?$APPLICATION->IncludeComponent(
                                "up:iblock.section.list",
                                "wholesale.mainpage.list",
                                array(
                                    "ID" => "0",
                                    "SHOW_PARENT_NAME" => "Y",
                                    "IBLOCK_TYPE" => "1c_catalog",
                                    "IBLOCK_ID" => 34,
                                    "SECTION_ID" => $arResult['VARIABLES']['SECTION_ID'],
                                    "SECTION_CODE" => "",
                                    "SECTION_URL" => "",
                                    "COUNT_ELEMENTS" => "Y",
                                    "TOP_DEPTH" => "3",
                                    "SECTION_FIELDS" => array(
                                        0 => "ID",
                                        1 => "NAME",
                                        2 => "PICTURE",
                                        3 => "SECTION_PAGE_URL",
                                        4 => "DEPTH_LEVEL",
                                        5 => "IBLOCK_SECTION_ID",
                                    ),
                                    "SECTION_USER_FIELDS" => array(
                                        0 => "UF_MIN_PRICE",
                                        1 => "UF_NEW_COLLECTION",
                                        2 => "UF_EXCLUSIVE",
                                        3 => "UF_SALE",
                                    ),
                                    "ADD_SECTIONS_CHAIN" => "Y",
                                    "CACHE_TYPE" => "A",
                                    "CACHE_TIME" => "36000000",
                                    "CACHE_NOTES" => "",
                                    "CACHE_GROUPS" => "Y",
                                    "COMPONENT_TEMPLATE" => "collections.list",
                                    "FILTER_NAME" => "arrSectionsFilter",
                                    "SORT_FIELD" => $sortField,
                                    "SORT_ORDER" => $sortOrder,
                                    "SORT_FIELD2" => "id",
                                    "SORT_ORDER2" => "desc",
                                    "URL_404" => "/404.php",
                                    "ELEMENT_CONTROLS" => "N",
                                    "AJAX_MODE" => "N",
                                    "AJAX_OPTION_JUMP" => "N",
                                    "AJAX_OPTION_STYLE" => "N",
                                    "AJAX_OPTION_HISTORY" => "N",
                                    "AJAX_OPTION_ADDITIONAL" => "",
                                    "CACHE_FILTER" => "Y",
                                    "PAGER_TEMPLATE" => "modern",
                                    "DISPLAY_TOP_PAGER" => "N",
                                    "DISPLAY_BOTTOM_PAGER" => "Y",
                                    "PAGER_TITLE" => "",
                                    "PAGER_SHOW_ALWAYS" => "N",
                                    "PAGER_DESC_NUMBERING" => "N",
                                    "PAGER_DESC_NUMBERING_CACHE_TIME" => "36000",
                                    "PAGE_ELEMENT_COUNT" => 1000,
                                    "PAGER_SHOW_ALL" => "Y"
                                ),
                                false
                            );?>
                        </div>
                    </div>
                    <div class="tab-pane fade" id="oformlenie">
                        <div class="box-inner">
                            <?
                            $GLOBALS['arrSectionsFilter'] = array();
                            $GLOBALS['arrSectionsFilter']['ID'] = $collections;
                            ?>
                            <?$APPLICATION->IncludeComponent(
                                "up:iblock.section.list",
                                "wholesale.mainpage.list",
                                array(
                                    "ID" => "0",
                                    "SHOW_PARENT_NAME" => "Y",
                                    "IBLOCK_TYPE" => "1c_catalog",
                                    "IBLOCK_ID" => 34,
                                    "SECTION_ID" => $arResult['VARIABLES']['SECTION_ID'],
                                    "SECTION_CODE" => "",
                                    "SECTION_URL" => "",
                                    "COUNT_ELEMENTS" => "Y",
                                    "TOP_DEPTH" => "3",
                                    "SECTION_FIELDS" => array(
                                        0 => "ID",
                                        1 => "NAME",
                                        2 => "PICTURE",
                                        3 => "SECTION_PAGE_URL",
                                        4 => "DEPTH_LEVEL",
                                        5 => "IBLOCK_SECTION_ID",
                                    ),
                                    "SECTION_USER_FIELDS" => array(
                                        0 => "UF_MIN_PRICE",
                                        1 => "UF_NEW_COLLECTION",
                                        2 => "UF_EXCLUSIVE",
                                        3 => "UF_SALE",
                                    ),
                                    "ADD_SECTIONS_CHAIN" => "Y",
                                    "CACHE_TYPE" => "A",
                                    "CACHE_TIME" => "36000000",
                                    "CACHE_NOTES" => "",
                                    "CACHE_GROUPS" => "Y",
                                    "COMPONENT_TEMPLATE" => "collections.list",
                                    "FILTER_NAME" => "arrSectionsFilter",
                                    "SORT_FIELD" => $sortField,
                                    "SORT_ORDER" => $sortOrder,
                                    "SORT_FIELD2" => "id",
                                    "SORT_ORDER2" => "desc",
                                    "URL_404" => "/404.php",
                                    "ELEMENT_CONTROLS" => "N",
                                    "AJAX_MODE" => "N",
                                    "AJAX_OPTION_JUMP" => "N",
                                    "AJAX_OPTION_STYLE" => "N",
                                    "AJAX_OPTION_HISTORY" => "N",
                                    "AJAX_OPTION_ADDITIONAL" => "",
                                    "CACHE_FILTER" => "Y",
                                    "PAGER_TEMPLATE" => "modern",
                                    "DISPLAY_TOP_PAGER" => "N",
                                    "DISPLAY_BOTTOM_PAGER" => "Y",
                                    "PAGER_TITLE" => "",
                                    "PAGER_SHOW_ALWAYS" => "N",
                                    "PAGER_DESC_NUMBERING" => "N",
                                    "PAGER_DESC_NUMBERING_CACHE_TIME" => "36000",
                                    "PAGE_ELEMENT_COUNT" => 1000,
                                    "PAGER_SHOW_ALL" => "Y"
                                ),
                                false
                            );?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="about">
    <div class="container">
        <?$APPLICATION->IncludeComponent("bitrix:main.include", ".default", array(
            "AREA_FILE_SHOW" => "file",
            "PATH" => SITE_DEFAULT_TEMPLATE_PATH."include/about.php",
            "EDIT_TEMPLATE" => ""
            ),false
            );?>
    </div>
</div>