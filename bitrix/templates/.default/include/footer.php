<footer>
    <div class="footer-top hidden-xs">
        <div class="container">
            <div class="menu-centr">
                <div class="menu">
                    <ul>
                        <li><a href="#" rel="nofollow">О компании</a></li>
                        <li><a href="#" rel="nofollow">Каталог</a></li>
                        <li><a href="#" rel="nofollow">Каталог дилерский</a></li>
                        <li><a href="#" rel="nofollow">Розничным покупателям</a></li>
                        <li><a href="#" rel="nofollow">Дизайнерам интерьеров</a></li>
                        <li><a href="#" rel="nofollow">Оплата и доставка</a></li>
                        <li><a href="#" rel="nofollow">Контакты</a></li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
    <div class="footer-middle hidden-xs">
        <div class="container border">
            <div class="row">
                <div class="col-sm-6 hidden-xs">
                    <p class="footer-text">Керамическая плитка из Испании, продажа оптом со склада, розничный магазин в Москве - Нахимовский проспект, д.50.</p>
                    <a href="#"><p class="link">Карта проезда</p></a>
                    <a href="#"><img src="<?php echo BASE_URL; ?>img/footer/vk_icon.png"></a>
                    <a href="#"><img src="<?php echo BASE_URL; ?>img/footer/fb_icon.png"></a>
                </div>
                <div class="col-sm-3 hidden-xs">
 					<a href="/contacts/"><p class="footer-text">Адреса салонов</p></a>
					<p class="footer-text">Салон-магазин</p>
                    <p class="footer-text-phone">+7 (495) 646-16-90</p>
                    <p class="footer-text">Отдел по работе с дизайнерами</p>
                    <p class="footer-text-phone">+7 (495) 646-16-90, доб.956</p>
                </div>
                <div class="col-sm-3 text-right hidden-xs">
                    <button class="btn footer-button" data-toggle="modal" data-target="#Modal-help">Помощь специалиста</button>
                    <div class="modal fade" id="Modal-help" tabindex="-1" role="dialog" aria-labelledby="ModalLabel" aria-hidden="true">
                        <div class="modal-dialog">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                                    <h4 class="modal-title" id="myModalLabel">Помощь специалиста</h4>
                                </div>
                                <div class="modal-body">
                                    <form class="form-inline header-filter" role="form">
                                        <div class="row">
                                            <div class="col-sm-3 modal-text text-right">Назначение <span>*</span></div>
                                            <div class="col-sm-7 text-left">
                                                <select class="filters select" id="ModalInputAppointment1" name="ModalInputPlace">
                                                    <option value="1">стена</option>
                                                    <option value="2">пол</option>
                                                </select>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-sm-3 modal-text text-right"></div>
                                            <div class="col-sm-7 text-left">
                                                <select class="filters select" id="ModalInputAppointment2" name="ModalInputPlace">
                                                    <option value="1">ванная</option>
                                                    <option value="2">кухня</option>
                                                    <option value="3">коридор</option>
                                                    <option value="4">гостиная</option>
                                                    <option value="5">балкон</option>
                                                </select>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-sm-3 modal-text text-right">Материал <span>*</span></div>
                                            <div class="col-sm-7 text-left">
                                                <select class="filters select" id="ModalInputMaterial" name="ModalInputPlace">
                                                    <option value="1">керамическая плитка</option>
                                                    <option value="2">керамогранит</option>
                                                    <option value="3">ступени</option>
                                                    <option value="4">мозаика</option>
                                                </select>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-sm-3 modal-text text-right">Цена до... <span>*</span></div>
                                            <div class="col-sm-7"><input type="text" class="filters" id="ModalInputPrice"></div>
                                        </div>
                                        <div class="row">
                                            <div class="col-sm-3 modal-text text-right">Цветовая гамма<span>*</span></div>
                                            <div class="col-sm-7"><input type="text" class="filters" id="ModalInputColor"></div>
                                        </div>
                                        <div class="row">
                                            <div class="col-sm-3 modal-text text-right">Комментарий <span>*</span></div>
                                            <div class="col-sm-7"><textarea class="filters textarea-fleld"></textarea></div>
                                        </div>
                                        <div class="row">
                                            <div class="col-sm-3 modal-text text-right">E-mail</div>
                                            <div class="col-sm-7"><input type="text" class="filters" id="ModalInputEmail"></div>
                                        </div>
                                        <div class="row">
                                            <div class="col-sm-3 modal-text text-right">Телефон</div>
                                            <div class="col-sm-7"><input type="text" class="filters" id="ModalInputPhone"></div>
                                        </div>

                                        <div class="row">
                                            <div class="col-sm-7 col-sm-offset-3"><button type="button" class="btn modal-buttom-form">Записаться</button></div>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>



                    <button class="btn footer-button" data-toggle="modal" data-target="#footer2">ЗАКАЗАТЬ ЗВОНОК</button>
                    <div class="modal fade" id="footer2" tabindex="-1" role="dialog" aria-labelledby="ModalLabel" aria-hidden="true">
                        <div class="modal-dialog">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                                    <h4 class="modal-title" id="myModalLabel">Заказать звонок</h4>
                                </div>
                                <div class="modal-body">
                                    <form class="form-inline header-filter" role="form">
                                        <div class="row">
                                            <div class="col-sm-3 modal-text text-right">Имя <span>*</span></div>
                                            <div class="col-sm-7"><input type="text" class="filters" id="ModalInputName"></div>
                                        </div>
                                        <div class="row">
                                            <div class="col-sm-3 modal-text text-right">Телефон <span>*</span></div>
                                            <div class="col-sm-7"><input type="text" class="filters" id="ModalInputPhone"></div>
                                        </div>
                                        <div class="row">
                                            <div class="col-sm-3 modal-text text-right">Удобное время <span>*</span></div>
                                            <div class="col-sm-2 text-left">
                                                <select class="filters select" id="ModalInputTime" name="ModalInputTime">
                                                    <option value="1">11:00</option>
                                                    <option value="2">12:00</option>
                                                </select>
                                            </div>
                                            <div class="col-sm-5">
                                                <div class="input-group date">
                                                    <input type="text" class="filters clear"><span class="input-group-addon"><i class="icon-cal"></i></span>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-sm-7 col-sm-offset-3 text-left"><button type="button" class="btn modal-buttom-form">Записаться</button></div>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="buttons">
                        <button type="button" class="btn footer-button">ЗВОНОК С САЙТА</button>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="footer-buttom hidden-xs">
        <div class="container">
            <div class="pole1"><p>© 2002-2014 «Планета Плитки»</p></div>
            <div class="pole2 hidden-xs"><p>Сайт сделан <a href="#"></a></p></div>
        </div>
    </div>
    <div class="footer-xs text-center hidden-sm hidden-md hidden-lg">
        <div class="container">
            <p class="footer-text">Салон-магазин</p>
            <p class="footer-text-phone">+7 (495) 646-16-90</p>
            <p class="footer-text">Отдел по работе с дизайнерами</p>
            <p class="footer-text-phone">+7 (495) 646-16-90, доб.956</p>
            <hr class="footer-line">
            <p class="footer-text">Керамическая плитка из Испании, продажа оптом со склада, розничный магазин в Москве - Нахимовский проспект, д.50.</p>
            <a href="#"><p class="link">Карта проезда</p></a>
            <hr class="footer-line">
            <div class="pole1"><p>© 1995-2016 «Планета Плитки»</p></div>
        </div>

    </div>




</footer>


<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>
<script src="<?php echo BASE_URL; ?>js/bootstrap.min.js"></script>
<script src="<?php echo BASE_URL; ?>js/jquery.jcarousel.min.js"></script>
<script src="<?php echo BASE_URL; ?>js/carousel-swipe.js"></script>
<script src="<?php echo BASE_URL; ?>js/jquery.imgCenter.js"></script>
<script src="<?php echo BASE_URL; ?>js/fotorama.js"></script>
<script src="<?php echo BASE_URL; ?>js/bootstrap-datepicker.js"></script>
<script src="<?php echo BASE_URL; ?>js/bootstrap-datepicker.ru.js"></script>
<script src="<?php echo BASE_URL; ?>js/jquery.selectbox-0.2.js"></script>
<script src="<?php echo BASE_URL; ?>js/jquery-ui.js"></script>


<script>
    $( ".slider-buttom" ).click(function() {
        $( ".arrow_box" ).toggle();
    });
</script>


<script>
    $(function() {
        $( "#slider-range" ).slider({
            range: true,
            min: 0,
            max: 500,
            values: [ 0, 500 ],
            slide: function( event, ui ) {
                $("input#min_cost").val($("#slider-range").slider("values",0));
                $("input#max_cost").val($("#slider-range").slider("values",1));
            }
        });
        $("input#min_cost").val($("#slider-range").slider("values",0));
        $("input#max_cost").val($("#slider-range").slider("values",1));
    });
</script>


<script>
    $('.input-group.date').datepicker({
        language: "ru",
        autoclose: true
    });
</script>

<script>
    $(document).ready(function() {
        $("#carousel_1").carousel();
    });
</script>

<script>
    $(document).ready( function() {
        $('#banner1').carousel({
            interval:   4000
        });

        var clickEvent = false;
        $('#banner1').on('click', '.nav a', function() {
            clickEvent = true;
            $('.nav li').removeClass('active');
            $(".buttom-banner").hide();
            $(this).parent().addClass('active');
            $(this).find(".buttom-banner").show();
        }).on('slid.bs.carousel', function(e) {
                if(!clickEvent) {
                    var count = $('.nav').children().length -1;
                    var current = $('.nav li.active');
                    current.removeClass('active').next().addClass('active');
                    var id = parseInt(current.data('slide-to'));
                    if(count == id) {
                        $('.nav li').first().addClass('active');
                    }
                }
                clickEvent = false;
            });
    });
</script>

<script>
    $(document).ready(function() {
        $('#carousel_1').carousel({
            interval: 10000
        });

        $('#carousel_2').carousel({
            interval: 10000
        })
    });
</script>

<script>
    $(document).ready(function() {

        $( ".tooltip-info" ).hide();
        $( ".info" ).hover(
            function() {
                $( ".tooltip-info" ).show();
            }, function() {
                $( ".tooltip-info" ).hide();
            }
        );
    });
</script>

<script>
    $(function(){
        $( window ).resize(function() {
            var mnfpict = $( ".pict_mnf" ).width();
            $('.pict_mnf').css('height', mnfpict);
        });

        var mnfpict = $( ".pict_mnf" ).width();
        $('.pict_mnf').css('height', mnfpict);

    });

    $(function(){
        $(window).load(function(){
            $(".pict_mnf img").imgCenter();
        });
        $(".pict_mnf img").imgCenter();
    });
</script>

<script>
    $(function(){
        $( window ).resize(function() {
            if($('body').width()>974) {
                var box3 = $( ".header-top-table2 .image-pict img" ).height();
                $('.header-top-table2 .box').css('height', box3);
            }
            else if ($('body').width()>750) {
                var box = $( ".header-top-table2 .image-pict img" ).height();
                $('.header-top-table2 .box').css('height', box/2);
            } else {
                var box2 = $( ".header-top-table2 .image-pict img" ).height();
                $('.header-top-table2 .box').css('height', box2/4);
            }
        });
        if($('body').width()>974) {
            var box3 = $( ".header-top-table2 .image-pict img" ).height();
            $('.header-top-table2 .box').css('height', box3);
        }
        else if ($('body').width()>750) {
            var box = $( ".header-top-table2 .image-pict img" ).height();
            $('.header-top-table2 .box').css('height', box/2);
        } else {
            var box2 = $( ".header-top-table2 .image-pict img" ).height();
            $('.header-top-table2 .box').css('height', box2/4);
        }
    });
</script>

<script>
    $(function(){
        $( window ).resize(function() {
            if($('body').width()>974) {
                var box3 = $( ".tab2 .image-pict img" ).height();
                $('.tab2 .box').css('height', box3);
            }
            else if ($('body').width()>750) {
                var box = $( ".tab2 .image-pict img" ).height();
                $('.tab2 .box').css('height', box/2);
            } else {
                var box2 = $( ".tab2 .image-pict img" ).height();
                $('.tab2 .box').css('height', box2/4);
            }
        });
        if($('body').width()>974) {
            var box3 = $( ".tab2 .image-pict img" ).height();
            $('.tab2 .box').css('height', box3);
        }
        else if ($('body').width()>750) {
            var box = $( ".tab2 .image-pict img" ).height();
            $('.tab2 .box').css('height', box/2);
        } else {
            var box2 = $( ".tab2 .image-pict img" ).height();
            $('.tab2 .box').css('height', box2/4);
        }
    });
</script>

<script type="text/javascript">
    $(function () {
        $(".select").selectbox();
        $(".filters2").selectbox({
            classHolder: 'sbHolder2'
        });
    });
</script>

<script>
    $(document).ready( function() {
        $('.fotorama').on('fotorama:showend ', function(){
            $("#openModal>div img").remove();
            $("#openModal>div").append('<img src="'+$('.fotorama__active.fotorama__stage__frame img').prop('src')+'"/>');
        });
    });
</script>


</body>
</html>