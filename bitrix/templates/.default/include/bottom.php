<footer>
<div class="footer-top hidden-xs">
    <div class="container">
        <div class="menu-centr">
            <div class="menu">
                <?$APPLICATION->IncludeComponent(
                    "bitrix:menu",
                    "bottom",
                    array(
                        "ROOT_MENU_TYPE" => "bottom",
                        "MENU_CACHE_TYPE" => "Y",
                        "MENU_CACHE_TIME" => "36000000",
                        "MENU_CACHE_USE_GROUPS" => "Y",
                        "MENU_CACHE_GET_VARS" => array(
                        ),
                        "MAX_LEVEL" => "1",
                        "CHILD_MENU_TYPE" => "",
                        "USE_EXT" => "N",
                        "DELAY" => "N",
                        "ALLOW_MULTI_SELECT" => "N"
                    ),
                    false
                );?>
            </div>
        </div>
    </div>
</div>
<div class="footer-middle hidden-xs">
    <div class="container border">
        <div class="row">
            <div class="col-sm-6 hidden-xs">
                <div class="footer-text">
                    <?$APPLICATION->IncludeComponent("bitrix:main.include", ".default", array(
                            "AREA_FILE_SHOW" => "file",
                            "PATH" => SITE_DEFAULT_TEMPLATE_PATH."include/slogan.php",
                            "EDIT_TEMPLATE" => ""
                        ),false
                    );?>
                </div>
               <!-- <a href="/dizayneram-intererov/"><p class="link">Дизайнерам интерьеров</p></a>-->
                <?$APPLICATION->IncludeComponent("bitrix:main.include", ".default", array(
                        "AREA_FILE_SHOW" => "file",
                        "PATH" => SITE_DEFAULT_TEMPLATE_PATH."include/social.php",
                        "EDIT_TEMPLATE" => ""
                    ),false
                );?>
            </div>
            
            
                <div class="col-sm-3 custom-footer-text hidden-xs">
                 <?$APPLICATION->IncludeComponent(
    "bitrix:main.include",
    "",
    Array(
        "AREA_FILE_SHOW" => "file",
        "AREA_FILE_SUFFIX" => "inc",
        "EDIT_TEMPLATE" => "",
        "PATH" => SITE_DIR."/include/footer-custom-text.php"
    )
);?>
                </div> 
            
            
            
            <div class="col-sm-3 hidden-xs">
				<a href="/contacts/"><p>Адреса магазинов</p></a>
                <p class="footer-text">Салон-магазин</p>
                <div class="footer-text-phone">
                    <?$APPLICATION->IncludeComponent("bitrix:main.include", ".default", array(
                            "AREA_FILE_SHOW" => "file",
                            "PATH" => SITE_DEFAULT_TEMPLATE_PATH."include/phone1.php",
                            "EDIT_TEMPLATE" => ""
                        ),false
                    );?>
                </div>
                <p class="footer-text">Отдел по работе с дизайнерами</p>
                <div class="footer-text-phone">
                    <?$APPLICATION->IncludeComponent("bitrix:main.include", ".default", array(
                            "AREA_FILE_SHOW" => "file",
                            "PATH" => SITE_DEFAULT_TEMPLATE_PATH."include/phone2.php",
                            "EDIT_TEMPLATE" => ""
                        ),false
                    );?>
                </div>
            </div>
            <?/*if(!$USER->isAdmin()):?>
            <div class="col-sm-3 text-right hidden-xs">
                <button class="btn footer-button" data-toggle="modal" data-target="#Modal-help">Помощь специалиста</button>
                <div class="modal fade" id="Modal-help" tabindex="-1" role="dialog" aria-labelledby="ModalLabel" aria-hidden="true">
                    <div class="modal-dialog">
                        <div class="modal-content">
                            <div class="modal-header">
                                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                                <h4 class="modal-title" id="myModalLabel">Помощь специалиста</h4>
                            </div>
                            <div class="modal-body">
                                <div class="form-inline header-filter" role="form">
                                    <?$APPLICATION->IncludeComponent("bitrix:form", "specialist", Array(
                                            "START_PAGE" => "new",	// Начальная страница
                                            "SHOW_LIST_PAGE" => "N",	// Показывать страницу со списком результатов
                                            "SHOW_EDIT_PAGE" => "N",	// Показывать страницу редактирования результата
                                            "SHOW_VIEW_PAGE" => "N",	// Показывать страницу просмотра результата
                                            "SUCCESS_URL" => "",	// Страница с сообщением об успешной отправке
                                            "WEB_FORM_ID" => "4",	// ID веб-формы
                                            "RESULT_ID" => $_REQUEST[RESULT_ID],	// ID результата
                                            "SHOW_ANSWER_VALUE" => "N",	// Показать значение параметра ANSWER_VALUE
                                            "SHOW_ADDITIONAL" => "N",	// Показать дополнительные поля веб-формы
                                            "SHOW_STATUS" => "N",	// Показать текущий статус результата
                                            "EDIT_ADDITIONAL" => "N",	// Выводить на редактирование дополнительные поля
                                            "EDIT_STATUS" => "N",	// Выводить форму смены статуса
                                            "NOT_SHOW_FILTER" => array(	// Коды полей, которые нельзя показывать в фильтре
                                                0 => "",
                                                1 => "",
                                            ),
                                            "NOT_SHOW_TABLE" => array(	// Коды полей, которые нельзя показывать в таблице
                                                0 => "",
                                                1 => "",
                                            ),
                                            "IGNORE_CUSTOM_TEMPLATE" => "N",	// Игнорировать свой шаблон
                                            "USE_EXTENDED_ERRORS" => "N",	// Использовать расширенный вывод сообщений об ошибках
                                            "SEF_MODE" => "N",	// Включить поддержку ЧПУ
                                            "AJAX_MODE" => "Y",	// Включить режим AJAX
                                            "AJAX_OPTION_JUMP" => "N",	// Включить прокрутку к началу компонента
                                            "AJAX_OPTION_STYLE" => "N",	// Включить подгрузку стилей
                                            "AJAX_OPTION_HISTORY" => "N",	// Включить эмуляцию навигации браузера
                                            "CACHE_TYPE" => "A",	// Тип кеширования
                                            "CACHE_TIME" => "36000000000",	// Время кеширования (сек.)
                                            "CHAIN_ITEM_TEXT" => "",	// Название дополнительного пункта в навигационной цепочке
                                            "CHAIN_ITEM_LINK" => "",	// Ссылка на дополнительном пункте в навигационной цепочке
                                            "AJAX_OPTION_ADDITIONAL" => "",	// Дополнительный идентификатор
                                            "SEF_FOLDER" => "/about/",	// Каталог ЧПУ (относительно корня сайта)
                                            "VARIABLE_ALIASES" => array(
                                                "action" => "action",
                                            )
                                        ),
                                        false
                                    );?>
                                </div>


                            </div>
                        </div>
                    </div>
                </div>

                <button class="btn footer-button" data-toggle="modal" data-target="#footer2">ЗАКАЗАТЬ ЗВОНОК</button>
                <div class="modal fade" id="footer2" tabindex="-1" role="dialog" aria-labelledby="ModalLabel" aria-hidden="true">
                    <div class="modal-dialog">
                        <div class="modal-content">
                            <div class="modal-header">
                                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                                <h4 class="modal-title" id="myModalLabel">Заказать звонок</h4>
                            </div>
                            <div class="modal-body">
                                <div class="form-inline header-filter" role="form">
                                    <?$APPLICATION->IncludeComponent(
	"bitrix:form", 
	"callback", 
	array(
		"START_PAGE" => "new",
		"SHOW_LIST_PAGE" => "N",
		"SHOW_EDIT_PAGE" => "N",
		"SHOW_VIEW_PAGE" => "N",
		"SUCCESS_URL" => "",
		"WEB_FORM_ID" => "3",
		"RESULT_ID" => $_REQUEST[RESULT_ID],
		"SHOW_ANSWER_VALUE" => "N",
		"SHOW_ADDITIONAL" => "N",
		"SHOW_STATUS" => "N",
		"EDIT_ADDITIONAL" => "N",
		"EDIT_STATUS" => "N",
		"NOT_SHOW_FILTER" => array(
			0 => "",
			1 => "",
		),
		"NOT_SHOW_TABLE" => array(
			0 => "",
			1 => "",
		),
		"IGNORE_CUSTOM_TEMPLATE" => "N",
		"USE_EXTENDED_ERRORS" => "N",
		"SEF_MODE" => "N",
		"AJAX_MODE" => "Y",
		"AJAX_OPTION_JUMP" => "N",
		"AJAX_OPTION_STYLE" => "N",
		"AJAX_OPTION_HISTORY" => "N",
		"CACHE_TYPE" => "A",
		"CACHE_TIME" => "36000000000",
		"CHAIN_ITEM_TEXT" => "",
		"CHAIN_ITEM_LINK" => "",
		"AJAX_OPTION_ADDITIONAL" => "",
		"SEF_FOLDER" => "/about/",
		"COMPONENT_TEMPLATE" => "callback",
		"VARIABLE_ALIASES" => array(
			"action" => "action",
		)
	),
	false
);?>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="buttons">
                    <a href="http://lk.mango-office.ru/widget/MTAwMDE1MzI=?lang=ru-ru" class="btn footer-button mangotele_btn" onclick="window.open(this.href, '_blank', 'width=238,height=350,resizable=no,toolbar=no,menubar=no,location=no,status=no'); return false;">ЗВОНОК С САЙТА</a>
                    
                </div>
            </div>
            <?endif;*/?>
        </div>
    </div>
</div>
<div class="footer-buttom hidden-xs">
    <div class="container">
        <div class="pole1">
            <div>
                <?$APPLICATION->IncludeComponent("bitrix:main.include", ".default", array(
                        "AREA_FILE_SHOW" => "file",
                        "PATH" => SITE_DEFAULT_TEMPLATE_PATH."include/copyright.php",
                        "EDIT_TEMPLATE" => ""
                    ),false
                );?>
            </div>
        </div>
            </div>
</div>
<div class="footer-xs text-center hidden-sm hidden-md hidden-lg">
    <div class="container">
        <p class="footer-text">Салон-магазин</p>
        <div class="footer-text-phone">
            <?$APPLICATION->IncludeComponent("bitrix:main.include", ".default", array(
                    "AREA_FILE_SHOW" => "file",
                    "PATH" => SITE_DEFAULT_TEMPLATE_PATH."include/phone1.php",
                    "EDIT_TEMPLATE" => ""
                ),false
            );?>
        </div>
        <p class="footer-text">Отдел по работе с дизайнерами</p>
        <div class="footer-text-phone">
            <?$APPLICATION->IncludeComponent("bitrix:main.include", ".default", array(
                    "AREA_FILE_SHOW" => "file",
                    "PATH" => SITE_DEFAULT_TEMPLATE_PATH."include/phone1.php",
                    "EDIT_TEMPLATE" => ""
                ),false
            );?>
        </div>
        <hr class="footer-line">
        <div class="footer-text">
            <?$APPLICATION->IncludeComponent("bitrix:main.include", ".default", array(
                    "AREA_FILE_SHOW" => "file",
                    "PATH" => SITE_DEFAULT_TEMPLATE_PATH."include/slogan.php",
                    "EDIT_TEMPLATE" => ""
                ),false
            );?>
        </div>
        <a href="/contacts/" rel="nofollow"><p class="link">Карта проезда</p></a>
        <hr class="footer-line">
        <div class="pole1">
            <div>
                <?$APPLICATION->IncludeComponent("bitrix:main.include", ".default", array(
                        "AREA_FILE_SHOW" => "file",
                        "PATH" => SITE_DEFAULT_TEMPLATE_PATH."include/copyright.php",
                        "EDIT_TEMPLATE" => ""
                    ),false
                );?>
            </div>
        </div>
    </div>
</div>
</footer>

<div class="modal fade" id="Modal" tabindex="-1" role="dialog" aria-labelledby="ModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                <h4 class="modal-title" id="myModalLabel">Запись на консультацию</h4>
            </div>
            <div class="modal-body">
                <div class="form-inline header-filter" role="form">
                    <?$APPLICATION->IncludeComponent("bitrix:form", "consultation", Array(
                        "START_PAGE" => "new",	// Начальная страница
                        "SHOW_LIST_PAGE" => "N",	// Показывать страницу со списком результатов
                        "SHOW_EDIT_PAGE" => "N",	// Показывать страницу редактирования результата
                        "SHOW_VIEW_PAGE" => "N",	// Показывать страницу просмотра результата
                        "SUCCESS_URL" => "",	// Страница с сообщением об успешной отправке
                        "WEB_FORM_ID" => "2",	// ID веб-формы
                        "RESULT_ID" => $_REQUEST[RESULT_ID],	// ID результата
                        "SHOW_ANSWER_VALUE" => "N",	// Показать значение параметра ANSWER_VALUE
                        "SHOW_ADDITIONAL" => "N",	// Показать дополнительные поля веб-формы
                        "SHOW_STATUS" => "N",	// Показать текущий статус результата
                        "EDIT_ADDITIONAL" => "N",	// Выводить на редактирование дополнительные поля
                        "EDIT_STATUS" => "N",	// Выводить форму смены статуса
                        "NOT_SHOW_FILTER" => array(	// Коды полей, которые нельзя показывать в фильтре
                            0 => "",
                            1 => "",
                        ),
                        "NOT_SHOW_TABLE" => array(	// Коды полей, которые нельзя показывать в таблице
                            0 => "",
                            1 => "",
                        ),
                        "IGNORE_CUSTOM_TEMPLATE" => "N",	// Игнорировать свой шаблон
                        "USE_EXTENDED_ERRORS" => "N",	// Использовать расширенный вывод сообщений об ошибках
                        "SEF_MODE" => "N",	// Включить поддержку ЧПУ
                        "AJAX_MODE" => "Y",	// Включить режим AJAX
                        "AJAX_OPTION_JUMP" => "N",	// Включить прокрутку к началу компонента
                        "AJAX_OPTION_STYLE" => "N",	// Включить подгрузку стилей
                        "AJAX_OPTION_HISTORY" => "N",	// Включить эмуляцию навигации браузера
                        "CACHE_TYPE" => "A",	// Тип кеширования
                        "CACHE_TIME" => "360000000",	// Время кеширования (сек.)
                        "CHAIN_ITEM_TEXT" => "",	// Название дополнительного пункта в навигационной цепочке
                        "CHAIN_ITEM_LINK" => "",	// Ссылка на дополнительном пункте в навигационной цепочке
                        "AJAX_OPTION_ADDITIONAL" => "",	// Дополнительный идентификатор
                        "SEF_FOLDER" => "/about/",	// Каталог ЧПУ (относительно корня сайта)
                        "VARIABLE_ALIASES" => array(
                            "action" => "action",
                        )
                    ),
                        false
                    );?>
                </div>
            </div>
        </div>
    </div>
</div>

<?if( $curPage == '/howto/faq/' ){?>
    <div class="modal fade" id="Modal-faq" tabindex="-1" role="dialog" aria-labelledby="ModalLabel" aria-hidden="true">


        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                    <h4 class="modal-title" id="myModalLabel">Задать вопрос</h4>
                </div>
                <div class="modal-body">
                    <div class="form-inline header-filter" role="form">
                        <?$APPLICATION->IncludeComponent("bitrix:iblock.element.add.form", "faq", Array(
                                "IBLOCK_TYPE" => "data",	// Тип инфоблока
                                "AJAX_MODE" => "Y",
                                "IBLOCK_ID" => "7",	// Инфоблок
                                "STATUS_NEW" => "NEW",	// Деактивировать элемент
                                "LIST_URL" => "",	// Страница со списком своих элементов
                                "USE_CAPTCHA" => "N",	// Использовать CAPTCHA
                                "USER_MESSAGE_EDIT" => "",	// Сообщение об успешном сохранении
                                "USER_MESSAGE_ADD" => "Ваш отзыв успешно добавлен. После модерации он будет доступен.",	// Сообщение об успешном добавлении
                                "DEFAULT_INPUT_SIZE" => "30",	// Размер полей ввода
                                "RESIZE_IMAGES" => "N",	// Использовать настройки инфоблока для обработки изображений
                                "PROPERTY_CODES" => array(	// Свойства, выводимые на редактирование
                                    0 => "NAME",
                                    1 => "PREVIEW_TEXT",
                                ),
                                "PROPERTY_CODES_REQUIRED" => array(	// Свойства, обязательные для заполнения
                                    0 => "NAME",
                                    1 => "PREVIEW_TEXT",
                                ),
                                "GROUPS" => array(	// Группы пользователей, имеющие право на добавление/редактирование
                                    0 => "2",
                                ),
                                "STATUS" => "ANY",	// Редактирование возможно
                                "ELEMENT_ASSOC" => "CREATED_BY",	// Привязка к пользователю
                                "MAX_USER_ENTRIES" => "100000",	// Ограничить кол-во элементов для одного пользователя
                                "MAX_LEVELS" => "100000",	// Ограничить кол-во рубрик, в которые можно добавлять элемент
                                "LEVEL_LAST" => "Y",	// Разрешить добавление только на последний уровень рубрикатора
                                "MAX_FILE_SIZE" => "0",	// Максимальный размер загружаемых файлов, байт (0 - не ограничивать)
                                "PREVIEW_TEXT_USE_HTML_EDITOR" => "N",	// Использовать визуальный редактор для редактирования текста анонса
                                "DETAIL_TEXT_USE_HTML_EDITOR" => "N",	// Использовать визуальный редактор для редактирования подробного текста
                                "SEF_MODE" => "N",	// Включить поддержку ЧПУ
                                "CUSTOM_TITLE_NAME" => "ФИО",	// * наименование *
                                "CUSTOM_TITLE_TAGS" => "",	// * теги *
                                "CUSTOM_TITLE_DATE_ACTIVE_FROM" => "",	// * дата начала *
                                "CUSTOM_TITLE_DATE_ACTIVE_TO" => "",	// * дата завершения *
                                "CUSTOM_TITLE_IBLOCK_SECTION" => "",	// * раздел инфоблока *
                                "CUSTOM_TITLE_PREVIEW_TEXT" => "Текст отзыва",	// * текст анонса *
                                "CUSTOM_TITLE_PREVIEW_PICTURE" => "",	// * картинка анонса *
                                "CUSTOM_TITLE_DETAIL_TEXT" => "",	// * подробный текст *
                                "CUSTOM_TITLE_DETAIL_PICTURE" => "",	// * подробная картинка *
                            ),
                            false
                        );?>
                    </div>
                </div>
            </div>
        </div>
    </div>
<?}
elseif( $curPage == '/howto/varianty-raskladki/' ){?>

    <div class="modal fade" id="Modal-var" tabindex="-1" role="dialog" aria-labelledby="ModalLabel" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                    <h4 class="modal-title" id="myModalLabel">Заказать раскладку</h4>
                </div>
                <div class="modal-body">

                    <div class="form-inline header-filter">
                        <?$APPLICATION->IncludeComponent("bitrix:form", "raskladka", Array(
                                "START_PAGE" => "new",	// Начальная страница
                                "SHOW_LIST_PAGE" => "N",	// Показывать страницу со списком результатов
                                "SHOW_EDIT_PAGE" => "N",	// Показывать страницу редактирования результата
                                "SHOW_VIEW_PAGE" => "N",	// Показывать страницу просмотра результата
                                "SUCCESS_URL" => "",	// Страница с сообщением об успешной отправке
                                "WEB_FORM_ID" => "5",	// ID веб-формы
                                "RESULT_ID" => $_REQUEST[RESULT_ID],	// ID результата
                                "SHOW_ANSWER_VALUE" => "N",	// Показать значение параметра ANSWER_VALUE
                                "SHOW_ADDITIONAL" => "N",	// Показать дополнительные поля веб-формы
                                "SHOW_STATUS" => "N",	// Показать текущий статус результата
                                "EDIT_ADDITIONAL" => "N",	// Выводить на редактирование дополнительные поля
                                "EDIT_STATUS" => "N",	// Выводить форму смены статуса
                                "NOT_SHOW_FILTER" => array(	// Коды полей, которые нельзя показывать в фильтре
                                    0 => "",
                                    1 => "",
                                ),
                                "NOT_SHOW_TABLE" => array(	// Коды полей, которые нельзя показывать в таблице
                                    0 => "",
                                    1 => "",
                                ),
                                "IGNORE_CUSTOM_TEMPLATE" => "N",	// Игнорировать свой шаблон
                                "USE_EXTENDED_ERRORS" => "N",	// Использовать расширенный вывод сообщений об ошибках
                                "SEF_MODE" => "N",	// Включить поддержку ЧПУ
                                "AJAX_MODE" => "Y",	// Включить режим AJAX
                                "AJAX_OPTION_JUMP" => "N",	// Включить прокрутку к началу компонента
                                "AJAX_OPTION_STYLE" => "N",	// Включить подгрузку стилей
                                "AJAX_OPTION_HISTORY" => "N",	// Включить эмуляцию навигации браузера
                                "CACHE_TYPE" => "A",	// Тип кеширования
                                "CACHE_TIME" => "3600000000",	// Время кеширования (сек.)
                                "CHAIN_ITEM_TEXT" => "",	// Название дополнительного пункта в навигационной цепочке
                                "CHAIN_ITEM_LINK" => "",	// Ссылка на дополнительном пункте в навигационной цепочке
                                "AJAX_OPTION_ADDITIONAL" => "",	// Дополнительный идентификатор
                                "SEF_FOLDER" => "/about/",	// Каталог ЧПУ (относительно корня сайта)
                                "VARIABLE_ALIASES" => array(
                                    "action" => "action",
                                )
                            ),
                            false
                        );?>
                    </div>
                </div>
            </div>
        </div>
    </div>
<?}
elseif( $curPage == '/howto/gotovye-proekty/' ){?>

    <div class="modal fade" id="Modal-prj" tabindex="-1" role="dialog" aria-labelledby="ModalLabel" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                    <h4 class="modal-title" id="myModalLabel">Заказать проект</h4>
                </div>
                <div class="modal-body">
                    <div class="form-inline header-filter">
                        <?$APPLICATION->IncludeComponent("bitrix:form", "project", Array(
                                "START_PAGE" => "new",	// Начальная страница
                                "SHOW_LIST_PAGE" => "N",	// Показывать страницу со списком результатов
                                "SHOW_EDIT_PAGE" => "N",	// Показывать страницу редактирования результата
                                "SHOW_VIEW_PAGE" => "N",	// Показывать страницу просмотра результата
                                "SUCCESS_URL" => "",	// Страница с сообщением об успешной отправке
                                "WEB_FORM_ID" => "6",	// ID веб-формы
                                "RESULT_ID" => $_REQUEST[RESULT_ID],	// ID результата
                                "SHOW_ANSWER_VALUE" => "N",	// Показать значение параметра ANSWER_VALUE
                                "SHOW_ADDITIONAL" => "N",	// Показать дополнительные поля веб-формы
                                "SHOW_STATUS" => "N",	// Показать текущий статус результата
                                "EDIT_ADDITIONAL" => "N",	// Выводить на редактирование дополнительные поля
                                "EDIT_STATUS" => "N",	// Выводить форму смены статуса
                                "NOT_SHOW_FILTER" => array(	// Коды полей, которые нельзя показывать в фильтре
                                    0 => "",
                                    1 => "",
                                ),
                                "NOT_SHOW_TABLE" => array(	// Коды полей, которые нельзя показывать в таблице
                                    0 => "",
                                    1 => "",
                                ),
                                "IGNORE_CUSTOM_TEMPLATE" => "N",	// Игнорировать свой шаблон
                                "USE_EXTENDED_ERRORS" => "N",	// Использовать расширенный вывод сообщений об ошибках
                                "SEF_MODE" => "N",	// Включить поддержку ЧПУ
                                "AJAX_MODE" => "Y",	// Включить режим AJAX
                                "AJAX_OPTION_JUMP" => "N",	// Включить прокрутку к началу компонента
                                "AJAX_OPTION_STYLE" => "N",	// Включить подгрузку стилей
                                "AJAX_OPTION_HISTORY" => "N",	// Включить эмуляцию навигации браузера
                                "CACHE_TYPE" => "A",	// Тип кеширования
                                "CACHE_TIME" => "360000000",	// Время кеширования (сек.)
                                "CHAIN_ITEM_TEXT" => "",	// Название дополнительного пункта в навигационной цепочке
                                "CHAIN_ITEM_LINK" => "",	// Ссылка на дополнительном пункте в навигационной цепочке
                                "AJAX_OPTION_ADDITIONAL" => "",	// Дополнительный идентификатор
                                "SEF_FOLDER" => "/about/",	// Каталог ЧПУ (относительно корня сайта)
                                "VARIABLE_ALIASES" => array(
                                    "action" => "action",
                                )
                            ),
                            false
                        );?>
                    </div>
                </div>
            </div>
        </div>
    </div>
<?}
?>

<?
# детальная страница коллекции
global $idSectionDetail;
global $collectionName;
if( $idSectionDetail ){?>
    <div class="modal fade" id="Modal_collection" tabindex="-1" role="dialog" aria-labelledby="ModalLabel" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                    <h4 class="modal-title" id="myModalLabel">Задать вопрос по коллекции</h4>
                </div>
                <div class="modal-body">
                    <?$APPLICATION->IncludeComponent(
                        "bitrix:form",
                        "callback",
                        array(
                            "START_PAGE" => "new",
                            "SHOW_LIST_PAGE" => "N",
                            "SHOW_EDIT_PAGE" => "N",
                            "SHOW_VIEW_PAGE" => "N",
                            "SUCCESS_URL" => "",
                            "WEB_FORM_ID" => "8",
                            "RESULT_ID" => $_REQUEST[RESULT_ID],
                            "SHOW_ANSWER_VALUE" => "N",
                            "SHOW_ADDITIONAL" => "N",
                            "SHOW_STATUS" => "N",
                            "EDIT_ADDITIONAL" => "N",
                            "EDIT_STATUS" => "N",
                            "NOT_SHOW_FILTER" => array(
                                0 => "",
                                1 => "",
                            ),
                            "NOT_SHOW_TABLE" => array(
                                0 => "",
                                1 => "",
                            ),
                            "IGNORE_CUSTOM_TEMPLATE" => "N",
                            "USE_EXTENDED_ERRORS" => "N",
                            "SEF_MODE" => "N",
                            "AJAX_MODE" => "Y",
                            "AJAX_OPTION_JUMP" => "N",
                            "AJAX_OPTION_STYLE" => "N",
                            "AJAX_OPTION_HISTORY" => "N",
                            "CACHE_TYPE" => "A",
                            "CACHE_TIME" => "360000000",
                            "CHAIN_ITEM_TEXT" => "",
                            "CHAIN_ITEM_LINK" => "",
                            "AJAX_OPTION_ADDITIONAL" => "",
                            "SEF_FOLDER" => "/about/",
                            "COMPONENT_TEMPLATE" => "callback",
                            "VARIABLE_ALIASES" => array(
                                "action" => "action",
                            ),
                            "UF_ELEMENT_NAME" => $collectionName
                        ),
                        false
                    );?>
                </div>
            </div>
        </div>
    </div>
<?}?>

<?
# детальная страница плитки
global $ElementID;
global $plitkaName;
if( $ElementID ){?>
    <div class="modal fade" id="Modal_item" tabindex="-1" role="dialog" aria-labelledby="ModalLabel" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                    <h4 class="modal-title" id="myModalLabel">Задать вопрос по плитке</h4>
                </div>
                <div class="modal-body">
                    <?$APPLICATION->IncludeComponent(
                        "bitrix:form",
                        "callback",
                        array(
                            "START_PAGE" => "new",
                            "SHOW_LIST_PAGE" => "N",
                            "SHOW_EDIT_PAGE" => "N",
                            "SHOW_VIEW_PAGE" => "N",
                            "SUCCESS_URL" => "",
                            "WEB_FORM_ID" => "7",
                            "RESULT_ID" => $_REQUEST[RESULT_ID],
                            "SHOW_ANSWER_VALUE" => "N",
                            "SHOW_ADDITIONAL" => "N",
                            "SHOW_STATUS" => "N",
                            "EDIT_ADDITIONAL" => "N",
                            "EDIT_STATUS" => "N",
                            "NOT_SHOW_FILTER" => array(
                                0 => "",
                                1 => "",
                            ),
                            "NOT_SHOW_TABLE" => array(
                                0 => "",
                                1 => "",
                            ),
                            "IGNORE_CUSTOM_TEMPLATE" => "N",
                            "USE_EXTENDED_ERRORS" => "N",
                            "SEF_MODE" => "N",
                            "AJAX_MODE" => "Y",
                            "AJAX_OPTION_JUMP" => "N",
                            "AJAX_OPTION_STYLE" => "N",
                            "AJAX_OPTION_HISTORY" => "N",
                            "CACHE_TYPE" => "A",
                            "CACHE_TIME" => "3600000000",
                            "CHAIN_ITEM_TEXT" => "",
                            "CHAIN_ITEM_LINK" => "",
                            "AJAX_OPTION_ADDITIONAL" => "",
                            "SEF_FOLDER" => "/about/",
                            "COMPONENT_TEMPLATE" => "callback",
                            "VARIABLE_ALIASES" => array(
                                "action" => "action",
                            ),
                            "UF_ELEMENT_NAME" => $plitkaName
                        ),
                        false
                    );?>
                </div>
            </div>
        </div>
    </div>
<?}?>

<div class="scrollup-button">
    Наверх
</div>

<?
if(!$USER->IsAdmin())
{
    $APPLICATION->IncludeComponent("bitrix:main.include", ".default", array(
        "AREA_FILE_SHOW" => "file",
        "PATH" => SITE_DEFAULT_TEMPLATE_PATH."include/counters.php",
        "EDIT_TEMPLATE" => ""
    ),false
    );
}
?>

</body>
</html>