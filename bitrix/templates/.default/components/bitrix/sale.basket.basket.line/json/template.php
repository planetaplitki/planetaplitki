<?if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();

CModule::IncludeModule('sale');
CModule::IncludeModule('catalog');

$place = $_REQUEST['place'];

# обновляем параметры корзины
$list = CSaleBasket::GetList(
	array(),
	array(
		'FUSER_ID' => CSaleBasket::GetBasketUserID(),
		'LID' => SITE_ID,
		'ORDER_ID' => 'NULL',
		'CAN_BUY' => 'Y',
		'DELAY' => 'N'
	)
);

$priceCount = 0;
$count = 0;
$price = 0;

//задаем константу минимального заказа
$CONS_MIN_PRICE_ORDER = COption::GetOptionString("grain.customsettings","CONS_MIN_PRICE_ORDER");

while($item = $list->Fetch()){
    $count += $item['QUANTITY'];
	$price += $item['QUANTITY'] * $item['PRICE'];
}

# обновляем корзину в шапке
ob_start();
$APPLICATION->IncludeComponent("bitrix:sale.basket.basket.small", "top", Array(
	"COMPONENT_TEMPLATE" => ".default",
	"PATH_TO_BASKET" => "/personal/cart/",	// Страница корзины
	"PATH_TO_ORDER" => "/personal/order/make/",	// Страница оформления заказа
	"SHOW_DELAY" => "Y",	// Показывать отложенные товары
	"SHOW_NOTAVAIL" => "N",	// Показывать товары, недоступные для покупки
	"SHOW_SUBSCRIBE" => "N",	// Показывать товары, на которые подписан покупатель
),
	false
);
$topBasket = trim(ob_get_contents());
ob_end_clean();

# обновляем основную корзину корзину
if( $place == 'basket' ){
	ob_start();
	$APPLICATION->IncludeComponent(
		"bitrix:sale.basket.basket",
		"basket",
		array(
			"COUNT_DISCOUNT_4_ALL_QUANTITY" => "N",
			"COLUMNS_LIST" => array(
				0 => "NAME",
				1 => "DISCOUNT",
				2 => "PROPS",
				3 => "DELETE",
				4 => "DELAY",
				5 => "PRICE",
				6 => "QUANTITY",
				7 => "SUM",
			),
			"AJAX_MODE" => "N",
			"AJAX_OPTION_JUMP" => "N",
			"AJAX_OPTION_STYLE" => "Y",
			"AJAX_OPTION_HISTORY" => "N",
			"PATH_TO_ORDER" => "/personal/order/make/",
			"HIDE_COUPON" => "N",
			"QUANTITY_FLOAT" => "N",
			"PRICE_VAT_SHOW_VALUE" => "Y",
			"SET_TITLE" => "Y",
			"AJAX_OPTION_ADDITIONAL" => "",
			"OFFERS_PROPS" => array(
				0 => "SIZES_SHOES",
				1 => "SIZES_CLOTHES",
				2 => "COLOR_REF",
			),
			"COMPONENT_TEMPLATE" => "basket",
			"USE_PREPAYMENT" => "N",
			"ACTION_VARIABLE" => "action",
			"PLACE" => "basket"
		),
		false
	);
	$basket = trim(ob_get_contents());
	ob_end_clean();
}

$output = array(
	'count' => $arResult['NUM_PRODUCTS'],
	'countAll' => $count,
	'price' => $priceCount,
	'priceFormatted' => SaleFormatCurrency( $priceCount, 'RUB', true),
	'pricePrint' => FormatCurrency($priceCount, 'RUB'),
	'plural' => plural($count, 'товар', 'товара', 'товаров'),
	'items' => $items,
	'discount' => $discount,
	'discountSum' => $pricediscount + $pricediscounAll,
	'percent' => $percent,
	'weight' => $priceWeightAll,
	'weightFormatted' => SaleFormatCurrency( $priceWeightAll, 'RUB', true ),
	'topBasketHtml' => $topBasket,
	'baseBasketHtml' => $basket,
    'price' => $price,
    'CONS_MIN_PRICE_ORDER' => $CONS_MIN_PRICE_ORDER,
);

echo json_encode($output);
?>