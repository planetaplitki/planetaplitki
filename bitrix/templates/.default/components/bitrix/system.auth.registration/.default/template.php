<?
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
?>


<?$APPLICATION->IncludeComponent("bitrix:main.register", "base", array(
	"SHOW_FIELDS" => array(
		0 => "NAME",
        1 => "PERSONAL_MOBILE"
	),
	"REQUIRED_FIELDS" => array(
		0 => "PERSONAL_MOBILE",
	),
	"AUTH" => "Y",
	"USE_BACKURL" => "Y",
	"SUCCESS_PAGE" => $APPLICATION->GetCurPageParam("",array("backurl")),
	"SET_TITLE" => "N",
	"USER_PROPERTY" => array(
	),
	"USER_PROPERTY_NAME" => ""
	),
	false
);?>