<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)	die();?>

<?if($USER->IsAuthorized()):?>

<p><?echo GetMessage("MAIN_REGISTER_AUTH")?></p>

<?else:?>
<?
if (count($arResult["ERRORS"]) > 0):
	foreach ($arResult["ERRORS"] as $key => $error)
		if (intval($key) == 0 && $key !== 0) 
			$arResult["ERRORS"][$key] = str_replace("#FIELD_NAME#", "&quot;".GetMessage("REGISTER_FIELD_".$key)."&quot;", $error);

	ShowError(implode("<br />", $arResult["ERRORS"]));

elseif($arResult["USE_EMAIL_CONFIRMATION"] === "Y"):
?>
<p><?echo GetMessage("REGISTER_EMAIL_WILL_BE_SENT")?></p>
<?endif?>


<div class="regblock">

    <form class="regblock__content__form" method="post" action="<?=POST_FORM_ACTION_URI?>" name="regform" enctype="multipart/form-data">

        <?if($arResult["BACKURL"] <> ''):?>
            <input type="hidden" name="backurl" value="<?=$arResult["BACKURL"]?>" />
        <?endif;?>


        <div class="form-group">
            <label for="">ФИО<sup>*</sup></label>
            <input type="text" name="REGISTER[NAME]" class="form-control regblock__form_control" value="<?=$_REQUEST['REGISTER']['NAME']?>" />
        </div>

        <div class="form-group">
            <label>E-mail<sup>*</sup></label>
            <input type="text" name="REGISTER[EMAIL]" class="form-control regblock__form_control" value="<?=$_REQUEST['REGISTER']['EMAIL']?>" />
        </div>

        <?/*?>
        <div class="form-group">
            <label>Логин</label>
            <input type="text" name="REGISTER[LOGIN]" class="form-control regblock__form_control" value="<?=$_REQUEST['REGISTER']['LOGIN']?>" />
        </div>
        <?*/?>

        <input type="hidden" name="REGISTER[LOGIN]" value="<?=rand(1,10000000)?>" />

        <?#USER_LOGIN?>

        <div class="form-group">
            <label>Мобильный телефон<sup>*</sup></label>
            <label class="my_before_after_personal_mobile"></label><input type="text" name="REGISTER[PERSONAL_MOBILE]" class="form-control regblock__form_control" value="<?=$_REQUEST['REGISTER']['PERSONAL_MOBILE']?>" />
        </div>

        <div class="form-group">
            <label>Пароль<sup>*</sup></label>
            <input type="password" name="REGISTER[PASSWORD]" class="form-control regblock__form_control" value="<?=$_REQUEST['REGISTER']['PASSWORD']?>" autocomplete="off" />
        </div>

        <div class="form-group">
            <label>Подтверждение пароля<sup>*</sup></label>
            <input type="password" name="REGISTER[CONFIRM_PASSWORD]" class="form-control regblock__form_control" value="<?=$_REQUEST['REGISTER']['CONFIRM_PASSWORD']?>" autocomplete="off" />
        </div>

        <?if($arResult["USER_PROPERTIES"]["SHOW"] == "Y"):?>
            <!--<tr><td colspan="2"><?/*=strLen(trim($arParams["USER_PROPERTY_NAME"])) > 0 ? $arParams["USER_PROPERTY_NAME"] : GetMessage("USER_TYPE_EDIT_TAB")*/?></td></tr>-->
            <?foreach ($arResult["USER_PROPERTIES"]["DATA"] as $FIELD_NAME => $arUserField):?>
                <div class="form-group">
                    <label><?=$arUserField["EDIT_FORM_LABEL"]?>:<?if ($arUserField["MANDATORY"]=="Y"):?><sup class="required">*</sup><?endif;?></label>
                    <?$APPLICATION->IncludeComponent(
                        "bitrix:system.field.edit",
                        $arUserField["USER_TYPE"]["USER_TYPE_ID"],
                        array("bVarsFromForm" => $arResult["bVarsFromForm"], "arUserField" => $arUserField, "form_name" => "regform"), null, array("HIDE_ICONS"=>"Y"));
                    ?>
                </div>
            <?endforeach;?>
        <?endif;?>
        
        <div class="form-group">
            <label><b><?=GetMessage("REGISTER_CAPTCHA_TITLE")?></b></label>
            <input type="hidden" name="captcha_sid" value="<?=$arResult["CAPTCHA_CODE"]?>" />
            <img src="/bitrix/tools/captcha.php?captcha_sid=<?=$arResult["CAPTCHA_CODE"]?>" width="180" height="40" alt="CAPTCHA" />
        </div>

        <div class="form-group">
            <div class="col-sm-12 label-text2 confirm-block">
                                <input type="checkbox" required="" checked="checked" id="select_data3" name="CONFIRM_POLITICS" value="y" <?=$confirmed == 'y' ? "checked" : "" ?> />
                                <label for="select_data3"><span></span>Настоящим подтверждаю, что я ознакомлен и согласен с условиями политики конфиденциальности * <a href="" class="confirm-trigger" data-toggle="modal" data-target="#Modal3">Узнать больше</a></label>
                            </div>
            <input type="text" class="form-control regblock__form_control" name="captcha_word" maxlength="50" value="" />
        </div>

        <input type="submit" class="btn regblock__form_btn" name="register_submit_button" value="<?=GetMessage("AUTH_REGISTER")?>" />

        <?/*?><input type="hidden" name="REGISTER[LOGIN]" value="<?=rand(1,10000000)?>" /><?*/?>
        <input type="hidden" name="REGISTER[TYPE]" value="WHOLESALE" />
        <br/><br/>
        <p><?echo $arResult["GROUP_POLICY"]["PASSWORD_REQUIREMENTS"];?></p>
        <p><span class="starrequired">*</span><?=GetMessage("AUTH_REQ")?></p>

    </form>
</div>


<?endif?>