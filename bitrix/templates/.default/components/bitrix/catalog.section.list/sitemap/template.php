<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
$TOP_DEPTH = $arResult["SECTION"]["DEPTH_LEVEL"];
$CURRENT_DEPTH = $TOP_DEPTH;
GLOBAL $sm, $OFFSET;

foreach($arResult["SECTIONS"] as $arSection) {
//	$sm->SetUrl(array('lastmod' => strftime('%Y-%m-%d', time()-$OFFSET), 'changefreq' => 'monthly', 'priority' => '0.8', 'loc' => sprintf('http://%s%s', $_SERVER['HTTP_HOST'], $arSection["SECTION_PAGE_URL"])));

	if ($CURRENT_DEPTH < $arSection["DEPTH_LEVEL"]) { echo "\n",str_repeat("\t", $arSection["DEPTH_LEVEL"]-$TOP_DEPTH),"<ul>"; } elseif($CURRENT_DEPTH == $arSection["DEPTH_LEVEL"]) { echo "</li>"; }
	else {
		while($CURRENT_DEPTH > $arSection["DEPTH_LEVEL"]) {
			echo "</li>";
			echo "\n", str_repeat("\t", $CURRENT_DEPTH-$TOP_DEPTH),"</ul>","\n",str_repeat("\t", $CURRENT_DEPTH-$TOP_DEPTH-1);
			$CURRENT_DEPTH--;
		}
		echo "\n",str_repeat("\t", $CURRENT_DEPTH-$TOP_DEPTH),"</li>";
	}

	echo "\n",str_repeat("\t", $arSection["DEPTH_LEVEL"]-$TOP_DEPTH);
	?><li><a href="<?= $arSection["SECTION_PAGE_URL"]?>"><?=$arSection["NAME"]?></a>
	<?
	$CURRENT_DEPTH = $arSection["DEPTH_LEVEL"];
};

while($CURRENT_DEPTH > $TOP_DEPTH)
{
	echo "</li>";
	echo "\n",str_repeat("\t", $CURRENT_DEPTH-$TOP_DEPTH),"</ul>","\n",str_repeat("\t", $CURRENT_DEPTH-$TOP_DEPTH-1);
	$CURRENT_DEPTH--;
}
?>