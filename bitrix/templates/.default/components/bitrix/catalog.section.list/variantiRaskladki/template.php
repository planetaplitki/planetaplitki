<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);

$arViewModeList = $arResult['VIEW_MODE_LIST'];

$arViewStyles = array(
    'LIST' => array(
        'CONT' => 'bx_sitemap',
        'TITLE' => 'bx_sitemap_title',
        'LIST' => 'bx_sitemap_ul',
    ),
    'LINE' => array(
        'CONT' => 'bx_catalog_line',
        'TITLE' => 'bx_catalog_line_category_title',
        'LIST' => 'bx_catalog_line_ul',
        'EMPTY_IMG' => $this->GetFolder().'/images/line-empty.png'
    ),
    'TEXT' => array(
        'CONT' => 'bx_catalog_text',
        'TITLE' => 'bx_catalog_text_category_title',
        'LIST' => 'bx_catalog_text_ul'
    ),
    'TILE' => array(
        'CONT' => 'bx_catalog_tile',
        'TITLE' => 'bx_catalog_tile_category_title',
        'LIST' => 'bx_catalog_tile_ul',
        'EMPTY_IMG' => $this->GetFolder().'/images/tile-empty.png'
    )
);
$arCurView = $arViewStyles[$arParams['VIEW_MODE']];

$strSectionEdit = CIBlock::GetArrayByID($arParams["IBLOCK_ID"], "SECTION_EDIT");
$strSectionDelete = CIBlock::GetArrayByID($arParams["IBLOCK_ID"], "SECTION_DELETE");
$arSectionDeleteParams = array("CONFIRM" => GetMessage('CT_BCSL_ELEMENT_DELETE_CONFIRM'));

?>

<?if ('Y' == $arParams['SHOW_PARENT_NAME'] && 0 < $arResult['SECTION']['ID']){
    $this->AddEditAction($arResult['SECTION']['ID'], $arResult['SECTION']['EDIT_LINK'], $strSectionEdit);
    $this->AddDeleteAction($arResult['SECTION']['ID'], $arResult['SECTION']['DELETE_LINK'], $strSectionDelete, $arSectionDeleteParams);
}?>

<?
$count = count($arResult['SECTIONS']);
?>

<div class="variant_raskladki_page">

    <?if(0 < $arResult["SECTIONS_COUNT"]){?>

        <div class="element-row clearfix">

            <?$i = 1;?>

            <?foreach ($arResult['SECTIONS'] as &$arSection){

                $this->AddEditAction($arSection['ID'], $arSection['EDIT_LINK'], $strSectionEdit);
                $this->AddDeleteAction($arSection['ID'], $arSection['DELETE_LINK'], $strSectionDelete, $arSectionDeleteParams);

                if (false === $arSection['PICTURE'])
                    $arSection['PICTURE'] = array(
                        'SRC' => $arCurView['EMPTY_IMG'],
                        'ALT' => (
                            '' != $arSection["IPROPERTY_VALUES"]["SECTION_PICTURE_FILE_ALT"]
                                ? $arSection["IPROPERTY_VALUES"]["SECTION_PICTURE_FILE_ALT"]
                                : $arSection["NAME"]
                            ),
                        'TITLE' => (
                            '' != $arSection["IPROPERTY_VALUES"]["SECTION_PICTURE_FILE_TITLE"]
                                ? $arSection["IPROPERTY_VALUES"]["SECTION_PICTURE_FILE_TITLE"]
                                : $arSection["NAME"]
                            )
                    );
                ?>
                <div class="col-xs-4 usligi-pict" id="<? echo $this->GetEditAreaId($arSection['ID']); ?>">

                    <?$img = i($arSection['PICTURE']["ID"], 230, 230, BX_RESIZE_IMAGE_EXACT);?>
                    <a href="<? echo $arSection['SECTION_PAGE_URL'];?>" title="<? echo $arSection['PICTURE']['TITLE'];?>">
                        <img
                            src="<?=$img?>"
                            title="<? echo $arSection['PICTURE']['TITLE']; ?>"
                            alt="<? echo $arSection['PICTURE']['ALT']; ?>"
                            />
                    </a>
                    <p class="text-pict">
                        <a href="<? echo $arSection['SECTION_PAGE_URL'];?>" title="<? echo $arSection['PICTURE']['TITLE'];?>">
                            <?=$arSection['NAME']; ?>
                        </a>
                    </p> 
                   <p class="text-fio"><?=$arResult["RASKLADKA"][$arSection["ID"]]["UF_RASKLADKA"]?></p>
                    <?
                    $collectionName = $arSection['UF_COLLECTION_NAME'];
                    $collectionLink = $arSection['UF_COLLECTION_LINK'] ? $arSection['UF_COLLECTION_LINK'] : "javascript:void(0);";
                    ?>

                    <?if( $collectionName ){?>
                        <p class="text-coll">Коллекция:</p>
                        <p class="text-coll-link"><a href="<?=$collectionLink?>"><?=$collectionName?></a></p>
                    <?}?>
                </div>

                <?if( $i%3 == 0 && $i != $count ){?>
                    </div><div class="element-row clearfix">
                <?}?>


                <?$i++;?>
            <?}?>
        </div>
    <?}?>
</div>

<?
#pRU($arResult, 'all');
?>