<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>

<?
global $APPLICATION;

/*if(preg_match('#^/(catalog|solutions|news|actions|personal)/[a-z0-9_-]+/[a-z0-9_-]+/(.*)?$#ui', $APPLICATION->GetCurPage())){
	$arResult[] = array('TITLE' => htmlspecialcharsBack($APPLICATION->sDocTitle) );
}*/

/*$mainArr = array('TITLE' => 'Главная', 'LINK' => '/');
array_unshift($arResult, $mainArr);*/
global $uClass;

//delayed function must return a string
if(empty($arResult))
	return "";

$itemSize = count($arResult);
$itemSize_ = $itemSize - 1;

$strReturn = '<ol class="breadcrumb '.$uClass.'">';

for($index = 0; $index < $itemSize; $index++){

	$title = htmlspecialcharsex($arResult[$index]["TITLE"]);
	if($itemSize_ == $index){
		$strReturn .= '<li itemscope itemtype="http://data-vocabulary.org/Breadcrumb" class="breadcrumbs__item active">
        <a itemprop="url" class="breadcrumbs__link" href="'.$APPLICATION->GetCurPage().'" title="'.$title.'"><span itemprop="title">'.$title.'</span></a></li>';
	}
	elseif($arResult[$index]["LINK"] <> ""){
		$strReturn .= '<li itemscope itemtype="http://data-vocabulary.org/Breadcrumb" class="breadcrumbs__item">
        <a itemprop="url" class="breadcrumbs__link" href="'.$arResult[$index]["LINK"].'" title="'.$title.'"><span itemprop="title">'.$title.'</span></a></li>';
	}
	else{
		$strReturn .= '<li class="breadcrumbs__item active">'.$title.'</li>';
	}
}

$strReturn .= '</ol>';
return $strReturn;
?>
