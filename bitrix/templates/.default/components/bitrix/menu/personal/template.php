<?if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>

<?if( eRU($arResult['MODIFIED']) ){

//	pRU(parse_url($_SERVER['REQUEST_URI']), 'all');


	foreach( $arResult['MODIFIED'] as $type => $items ){?>

		<div class="lc_left_box">
			<a class="lc_left_box_p"><?=$type?><span></span></a>

			<?
			$selected = '';
			foreach($items as $oneItem){
				if( $oneItem['SELECTED'] ){
					$selected = 'y';
					break;
				}
			}?>

			<ul <?=$selected ? 'style="display:block"' : "";?>>
				<?foreach($items as $oneItem){
					if($arParams["MAX_LEVEL"] == 1 && $oneItem["DEPTH_LEVEL"] > 1)
						continue;

					if( $oneItem["SELECTED"] ){?>
						<li class="selected"><a href="<?=$oneItem["LINK"]?>"><?=$oneItem["TEXT"]?></a></li>
					<?}
					else{?>
						<li><a href="<?=$oneItem["LINK"]?>"><?=$oneItem["TEXT"]?></a></li>
					<?}
				}?>
			</ul>
		</div>
	<?}
}?>

<div><a href="?logout=yes" class="btn btn-hollow">Выйти</a></div>