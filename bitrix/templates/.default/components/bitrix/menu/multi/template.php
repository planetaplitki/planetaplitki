<?if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>

<?/*?>
<ul class="one">
    <input class="toggle-box" id="top-id-1" type="checkbox" >
    <label for="top-id-1">Розничный каталог</label>
    <div class="two">
        <li><a href="#">Новые коллекции</a></li>
        <li><a href="#">Эксклюзив</a></li>
        <li><a href="#">Акции</a></li>
        <li><a href="#">Распродажа</a></li>
        <li><a href="#">Сантехника</a></li>
        <li><a href="#">ТВ Проекты</a></li>
        <li><a href="#">Каталог PDF</a></li>
    </div>
    <input class="toggle-box" id="top-id-2" type="checkbox" >
    <label for="top-id-2">Оптовый каталог</label>
    <div class="two">
        <li><a href="#">Новые коллекции</a></li>
        <li><a href="#">Эксклюзив</a></li>
        <li><a href="#">Акции</a></li>
        <li><a href="#">Распродажа</a></li>
        <li><a href="#">Сантехника</a></li>
        <li><a href="#">ТВ Проекты</a></li>
        <li><a href="#">Каталог PDF</a></li>
    </div>
</ul>
<?*/?>

<?if (!empty($arResult)):?>

    <ul class="one">
        <? $previousLevel = 0;
        foreach($arResult as $id => $arItem):?>

            <?if ($previousLevel && $arItem["DEPTH_LEVEL"] < $previousLevel):?>
                <?=str_repeat("</ul></li>", ($previousLevel - $arItem["DEPTH_LEVEL"]));?>
            <?endif?>

            <?if ($arItem["IS_PARENT"]):?>

                <?if ($arItem["DEPTH_LEVEL"] == 1):?>
                    <li>
                        <input class="toggle-box" id="top-id-<?=$id?>" type="checkbox" >
                        <label for="top-id-<?=$id?>"><a href="<?=$arItem["LINK"]?>" class="<?if ($arItem["SELECTED"]):?>root-item-selected<?else:?>root-item<?endif?>"><?=$arItem["TEXT"]?></a></label>
                        <ul class="root-item two">
                <?else:?>
                    <li><a href="<?=$arItem["LINK"]?>" class="parent<?if ($arItem["SELECTED"]):?> item-selected<?endif?>"><?=$arItem["TEXT"]?></a>
                        <ul class="two">
                <?endif?>

            <?else:?>

                <?if ($arItem["PERMISSION"] > "D"):?>

                    <?if ($arItem["DEPTH_LEVEL"] == 1):?>
                        <li><a href="<?=$arItem["LINK"]?>" class="<?if ($arItem["SELECTED"]):?>root-item-selected<?else:?>root-item<?endif?>"><?=$arItem["TEXT"]?></a></li>
                    <?else:?>
                        <li><a href="<?=$arItem["LINK"]?>" <?if ($arItem["SELECTED"]):?> class="item-selected"<?endif?>><?=$arItem["TEXT"]?></a></li>
                    <?endif?>

                <?else:?>

                    <?if ($arItem["DEPTH_LEVEL"] == 1):?>
                        <li><a href="" class="<?if ($arItem["SELECTED"]):?>root-item-selected<?else:?>root-item<?endif?>" title="<?=GetMessage("MENU_ITEM_ACCESS_DENIED")?>"><?=$arItem["TEXT"]?></a></li>
                    <?else:?>
                        <li><a href="" class="denied" title="<?=GetMessage("MENU_ITEM_ACCESS_DENIED")?>"><?=$arItem["TEXT"]?></a></li>
                    <?endif?>

                <?endif?>
            <?endif?>
            <?$previousLevel = $arItem["DEPTH_LEVEL"];?>
        <?endforeach?>
        <?if ($previousLevel > 1)://close last item tags?>
            <?=str_repeat("</ul></li>", ($previousLevel-1) );?>
        <?endif?>
    </ul>
<?endif?>