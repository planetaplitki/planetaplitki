<?if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>

<?if($arResult["READY"]=="Y"){?>

    <?
    $itemBasketCount = 0;
    $itemBasketCountItems = 0;
    $itemDelayCount = 0;
    $itemDelayCountItems = 0;

    $sum = $arResult['DELIVERY_PRICE'];

    foreach ($arResult["ITEMS"] as $v){
        if ($v["DELAY"]=="N" && $v["CAN_BUY"]=="Y"){

            $itemBasketCount += $v['QUANTITY'];
            $itemBasketCountItems += 1;

            $sum += $v['PRICE'] * $v['QUANTITY'];
            $sumBase += $v['PRICE_BASE'] * $v['QUANTITY'];
            $discount += $v['DISCOUNT_PRICE'] * $v['QUANTITY'];
        }
        if ($v["DELAY"]=="Y" && $v["CAN_BUY"]=="Y"){
            $itemDelayCount += $v['QUANTITY'];
            $itemDelayCountItems += 1;
        }
    }?>

    <?if( $itemBasketCount ){?>

        <div class="pole-num"><p><?=$itemBasketCount?></p></div>
        <div class="pole-num-text"><p>Корзина:</p></div>

    <?}?>
<?}
else{?>
    <div class="pole-num"><p>0</p></div>
    <div class="pole-num-text"><p>Корзина:</p></div>
<?}?>

<?$this->SetViewTarget("basket_mobile_menu");?>
<a href="<?=$arParams['PATH_TO_BASKET']?>">
    <span class="glyphicon-icon5"></span>Корзина
    <span class="point pull-right"><?=$itemBasketCount;?></span>
</a>
<?$this->EndViewTarget("basket_mobile_menu");?>
<?$this->SetViewTarget("basket_mobile_icon");?>
<?=$itemBasketCount;?>
<?$this->EndViewTarget("basket_mobile_icon");?>
