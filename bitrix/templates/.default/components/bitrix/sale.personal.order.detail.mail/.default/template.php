<?if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();

//echo '<pre>'; print_r($arResult); echo '</pre>';
?>  
<p class="bx_order_list">
	<?if(strlen($arResult["ERROR_MESSAGE"])):?>
		<?=ShowError($arResult["ERROR_MESSAGE"]);?>
	<?else:?>	
		<?if($arParams["SHOW_ORDER_BASE"]=='Y' || $arParams["SHOW_ORDER_USER"]=='Y' || $arParams["SHOW_ORDER_PARAMS"]=='Y' || $arParams["SHOW_ORDER_BUYER"]=='Y' || $arParams["SHOW_ORDER_DELIVERY"]=='Y' || $arParams["SHOW_ORDER_PAYMENT"]=='Y'):?>
		<table class="bx_order_list_table">
			<thead>
				<tr>
					<td style="padding: 3px 10px" colspan="2">
						<?=GetMessage('SPOD_ORDER')?> <?=GetMessage('SPOD_NUM_SIGN')?><?=$arResult["ACCOUNT_NUMBER"]?>
						<?if(strlen($arResult["DATE_INSERT_FORMATED"])):?>
							<?=GetMessage("SPOD_FROM")?> <?=$arResult["DATE_INSERT_FORMATED"]?>
						<?endif?>
					</td>
				</tr>
			</thead>
			<tbody>
			<?if($arParams["SHOW_ORDER_BASE"]=='Y'):?>
				<tr>
					<td style="padding: 3px 10px">
						<?=GetMessage('SPOD_ORDER_STATUS')?>:
					</td>
					<td style="padding: 3px 10px">
						<?=$arResult["STATUS"]["NAME"]?>
						<?if(strlen($arResult["DATE_STATUS_FORMATED"])):?>
							(<?=GetMessage("SPOD_FROM")?> <?=$arResult["DATE_STATUS_FORMATED"]?>)
						<?endif?>
					</td>
				</tr>
				<tr>
					<td style="padding: 3px 10px">
						<?=GetMessage('SPOD_ORDER_PRICE')?>:
					</td>
					<td style="padding: 3px 10px">
						<?=$arResult["PRICE_FORMATED"]?>
						<?if(floatval($arResult["SUM_PAID"])):?>
							(<?=GetMessage('SPOD_ALREADY_PAID')?>:&nbsp;<?=$arResult["SUM_PAID_FORMATED"]?>)
						<?endif?>
					</td>
				</tr>

				<?if($arResult["CANCELED"] == "Y" || $arResult["CAN_CANCEL"] == "Y"):?>
					<tr>
						<td style="padding: 3px 10px"><?=GetMessage('SPOD_ORDER_CANCELED')?>:</td>
						<td style="padding: 3px 10px">
							<?if($arResult["CANCELED"] == "Y"):?>
								<?=GetMessage('SPOD_YES')?>
								<?if(strlen($arResult["DATE_CANCELED_FORMATED"])):?>
									(<?=GetMessage('SPOD_FROM')?> <?=$arResult["DATE_CANCELED_FORMATED"]?>)
								<?endif?>
							<?elseif($arResult["CAN_CANCEL"] == "Y"):?>
								<?=GetMessage('SPOD_NO')?>&nbsp;&nbsp;&nbsp;[<a href="<?=$arResult["URL_TO_CANCEL"]?>"><?=GetMessage("SPOD_ORDER_CANCEL")?></a>]
							<?endif?>
						</td>
					</tr>
				<?endif?>
				<tr><td style="padding: 3px 10px"><br></td><td style="padding: 3px 10px"></td></tr>
			<?endif?>
				
			<?if($arParams["SHOW_ORDER_USER"]=='Y'):?>
				<?if(intval($arResult["USER_ID"])):?>

					<tr>
						<td style="padding: 3px 10px" colspan="2"><?=GetMessage('SPOD_ACCOUNT_DATA')?></td>
					</tr>
					<?if(strlen($arResult["USER_NAME"])):?>
						<tr>
							<td style="padding: 3px 10px"><?=GetMessage('SPOD_ACCOUNT')?>:</td>
							<td style="padding: 3px 10px"><?=$arResult["USER_NAME"]?></td>
						</tr>
					<?endif?>
					<tr>
						<td style="padding: 3px 10px"><?=GetMessage('SPOD_LOGIN')?>:</td>
						<td style="padding: 3px 10px"><?=$arResult["USER"]["LOGIN"]?></td>
					</tr>
					<tr>
						<td style="padding: 3px 10px"><?=GetMessage('SPOD_EMAIL')?>:</td>
						<td style="padding: 3px 10px"><a href="mailto:<?=$arResult["USER"]["EMAIL"]?>"><?=$arResult["USER"]["EMAIL"]?></a></td>
					</tr>

					<tr><td style="padding: 3px 10px"><br></td><td style="padding: 3px 10px"></td></tr>

				<?endif?>
			<?endif?>

			<?if($arParams["SHOW_ORDER_PARAMS"]=='Y'):?>
				<tr>
					<td style="padding: 3px 10px" colspan="2"><?=GetMessage('SPOD_ORDER_PROPERTIES')?></td>
				</tr>
				<tr>
					<td style="padding: 3px 10px"><?=GetMessage('SPOD_ORDER_PERS_TYPE')?>:</td>
					<td style="padding: 3px 10px"><?=$arResult["PERSON_TYPE"]["NAME"]?></td>
				</tr>
			<?endif?>
			
			<?if($arParams["SHOW_ORDER_BUYER"]=='Y'):?>
				<?foreach($arResult["ORDER_PROPS"] as $prop):?>

					<?if($prop["SHOW_GROUP_NAME"] == "Y"):?>

						<tr><td style="padding: 3px 10px"><br></td><td style="padding: 3px 10px"></td></tr>
						<tr>
							<td style="padding: 3px 10px" colspan="2"><?=$prop["GROUP_NAME"]?></td>
						</tr>

					<?endif?>

					<tr>
						<td style="padding: 3px 10px"><?=$prop['NAME']?>:</td>
						<td style="padding: 3px 10px">

							<?if($prop["TYPE"] == "CHECKBOX"):?>
								<?=GetMessage('SPOD_'.($prop["VALUE"] == "Y" ? 'YES' : 'NO'))?>
							<?else:?>
								<?=$prop["VALUE"]?>
							<?endif?>

						</td>
					</tr>

				<?endforeach?>

				
				<?if(!empty($arResult["USER_DESCRIPTION"])):?>

					<tr>
						<td style="padding: 3px 10px"><?=GetMessage('SPOD_ORDER_USER_COMMENT')?>:</td>
						<td style="padding: 3px 10px"><?=$arResult["USER_DESCRIPTION"]?></td>
					</tr>

				<?endif?>

				<tr><td style="padding: 3px 10px"><br></td><td style="padding: 3px 10px"></td></tr>
			<?endif?>

			<?if($arParams["SHOW_ORDER_PAYMENT"]=='Y'):?>
				<tr>
					<td style="padding: 3px 10px" colspan="2"><?=GetMessage("SPOD_ORDER_PAYMENT")?></td>
				</tr>
				<tr>
					<td style="padding: 3px 10px"><?=GetMessage('SPOD_PAY_SYSTEM')?>:</td>
					<td style="padding: 3px 10px">
						<?if(intval($arResult["PAY_SYSTEM_ID"])):?>
							<?=$arResult["PAY_SYSTEM"]["NAME"]?>
						<?else:?>
							<?=GetMessage("SPOD_NONE")?>
						<?endif?>
					</td>
				</tr>
				<tr>
					<td style="padding: 3px 10px"><?=GetMessage('SPOD_ORDER_PAYED')?>:</td>
					<td style="padding: 3px 10px">
						<?if($arResult["PAYED"] == "Y"):?>
							<?=GetMessage('SPOD_YES')?>
							<?if(strlen($arResult["DATE_PAYED_FORMATED"])):?>
								(<?=GetMessage('SPOD_FROM')?> <?=$arResult["DATE_PAYED_FORMATED"]?>)
							<?endif?>
						<?else:?>
							<?=GetMessage('SPOD_NO')?>
							<?if($arResult["CAN_REPAY"]=="Y" && $arResult["PAY_SYSTEM"]["PSA_NEW_WINDOW"] == "Y"):?>
								&nbsp;&nbsp;&nbsp;[<a href="<?=$arResult["PAY_SYSTEM"]["PSA_ACTION_FILE"]?>" target="_blank"><?=GetMessage("SPOD_REPEAT_PAY")?></a>]
							<?endif?>
						<?endif?>
					</td>
				</tr>

				<tr>
					<td style="padding: 3px 10px"><?=GetMessage("SPOD_ORDER_DELIVERY")?>:</td>
					<td style="padding: 3px 10px">
						<?if(strpos($arResult["DELIVERY_ID"], ":") !== false || intval($arResult["DELIVERY_ID"])):?>
							<?=$arResult["DELIVERY"]["NAME"]?>

							<?if(intval($arResult['STORE_ID']) && !empty($arResult["DELIVERY"]["STORE_LIST"][$arResult['STORE_ID']])):?>

								<?$store = $arResult["DELIVERY"]["STORE_LIST"][$arResult['STORE_ID']];?>
								<div class="bx_ol_store">
									<div class="bx_old_s_row_title">
										<?=GetMessage('SPOD_TAKE_FROM_STORE')?>: <b><?=$store['TITLE']?></b>

										<?if(!empty($store['DESCRIPTION'])):?>
											<div class="bx_ild_s_desc">
												<?=$store['DESCRIPTION']?>
											</div>
										<?endif?>

									</div>
									
									<?if(!empty($store['ADDRESS'])):?>
										<div class="bx_old_s_row">
											<b><?=GetMessage('SPOD_STORE_ADDRESS')?></b>: <?=$store['ADDRESS']?>
										</div>
									<?endif?>

									<?if(!empty($store['SCHEDULE'])):?>
										<div class="bx_old_s_row">
											<b><?=GetMessage('SPOD_STORE_WORKTIME')?></b>: <?=$store['SCHEDULE']?>
										</div>
									<?endif?>

									<?if(!empty($store['PHONE'])):?>
										<div class="bx_old_s_row">
											<b><?=GetMessage('SPOD_STORE_PHONE')?></b>: <?=$store['PHONE']?>
										</div>
									<?endif?>

									<?if(!empty($store['EMAIL'])):?>
										<div class="bx_old_s_row">
											<b><?=GetMessage('SPOD_STORE_EMAIL')?></b>: <a href="mailto:<?=$store['EMAIL']?>"><?=$store['EMAIL']?></a>
										</div>
									<?endif?>
								</div>

							<?endif?>

						<?else:?>
							<?=GetMessage("SPOD_NONE")?>
						<?endif?>
					</td>
				</tr>

				<?if($arResult["TRACKING_NUMBER"]):?>

					<tr>
						<td style="padding: 3px 10px"><?=GetMessage('SPOD_ORDER_TRACKING_NUMBER')?>:</td>
						<td style="padding: 3px 10px"><?=$arResult["TRACKING_NUMBER"]?></td>
					</tr>

					<tr><td style="padding: 3px 10px"><br></td><td style="padding: 3px 10px"></td></tr>

				<?endif?>
			<?endif?>
			</tbody>
		</table>
			
			<?if($arParams["SHOW_ORDER_BASKET"]=='Y'):?>
				<h3><?=GetMessage('SPOD_ORDER_BASKET')?></h3>
			<?endif?>
		<?endif?>

		
		
		<?if($arParams["SHOW_ORDER_BASKET"]=='Y'):?>
		<table class="bx_order_list_table_order">
			<thead>
				<tr>			
					<?
					foreach ($arParams["CUSTOM_SELECT_PROPS"] as $headerId):						
						if($headerId == 'PICTURE' && in_array('NAME', $arParams["CUSTOM_SELECT_PROPS"]))
							continue;
							
						$colspan = '';
						if($headerId == 'NAME')
							$colspan = 'colspan="2"';
						
						$headerName = GetMessage('SPOD_'.$headerId);
						if(strlen($headerName)<=0)
						{
							foreach(array_values($arResult['PROPERTY_DESCRIPTION']) as $prop_head_desc):
								if(array_key_exists($headerId, $prop_head_desc))
									$headerName = $prop_head_desc[$headerId]['NAME'];
							endforeach;
						}
						?><td style="padding: 3px 10px" <?=$colspan?>><?=$headerName?></td><?
					endforeach;
					?>
				</tr>
			</thead>
			<tbody>
				<?//echo "<pre>".print_r($arParams['CUSTOM_SELECT_PROPS'], true).print_R($arResult["BASKET"], true)."</pre>"?>
				<?
				foreach($arResult["BASKET"] as $prod):
					?><tr>
                        <td style="padding: 3px 10px"><img src="<?=$prod['PICTURE']['SRC']?>" width="<?=$prod['PICTURE']['WIDTH']?>" height="<?=$prod['PICTURE']['HEIGHT']?>" alt="<?=$prod['NAME']?>" /></td>
                    <?
					
					$hasLink = !empty($prod["DETAIL_PAGE_URL"]);
					$actuallyHasProps = is_array($prod["PROPS"]) && !empty($prod["PROPS"]);
					
					foreach ($arParams["CUSTOM_SELECT_PROPS"] as $headerId):
						
						?><td style="padding: 3px 10px" class="custom"><?
						
						if($headerId == "NAME"):
							
							if($hasLink):
								?><a href="<?=$prod["DETAIL_PAGE_URL"]?>" target="_blank"><?
							endif;
							?><?=$prod["NAME"]?><?
							if($hasLink):
								?></a><?
							endif;
							
						elseif($headerId == "PICTURE"):
							
							if($hasLink):
								?><a href="<?=$prod["DETAIL_PAGE_URL"]?>" target="_blank"><?
							endif;
							if($prod['PICTURE']['SRC']):
								?><img src="<?=$prod['PICTURE']['SRC']?>" width="<?=$prod['PICTURE']['WIDTH']?>" height="<?=$prod['PICTURE']['HEIGHT']?>" alt="<?=$prod['NAME']?>" /><?
							endif;
							if($hasLink):
								?></a><?
							endif;
							
						elseif($headerId == "PROPS" && $arResult['HAS_PROPS'] && $actuallyHasProps):
							
							?>
							<table cellspacing="0" class="bx_ol_sku_prop">
								<?foreach($prod["PROPS"] as $prop):?>
									<tr>
										<td style="padding: 3px 10px"><nobr><?=$prop["NAME"]?>:</nobr></td>
										<td style="padding: 3px 10px" style="padding-left: 10px !important"><b><?=$prop["VALUE"]?></b></td>
									</tr>
								<?endforeach?>
							</table>
							<?

						elseif($headerId == "QUANTITY"):
						
							?>
							<?=$prod["QUANTITY"]?>
							<?if(strlen($prod['MEASURE_TEXT'])):?>
								<?=$prod['MEASURE_TEXT']?>
							<?else:?>
								<?=GetMessage('SPOD_DEFAULT_MEASURE')?>
							<?endif?>
							<?
							
						else:
						
							?><?=$prod[(strpos($headerId, 'PROPERTY_')===0 ? $headerId."_VALUE" : $headerId)]?><?
						
						endif;
						
						?></td><?
						
					endforeach;
					
					?></tr><?
					
				endforeach;
				?>
			</tbody>
		</table>
		<br>
		<?endif?>

		<?if($arParams["SHOW_ORDER_SUM"]=='Y'):?>
		<table class="bx_ordercart_order_sum">
			<tbody>

				<? ///// WEIGHT ?>
				<?if(floatval($arResult["ORDER_WEIGHT"])):?>
					<tr>
						<td style="padding: 3px 10px" class="custom_t1"><?=GetMessage('SPOD_TOTAL_WEIGHT')?>:</td>
						<td style="padding: 3px 10px" class="custom_t2"><?=$arResult['ORDER_WEIGHT_FORMATED']?></td>
					</tr>
				<?endif?>

				<? ///// PRICE SUM ?>
                <?if($arResult['FULL_PRICE']):?>
				<tr>
					<td style="padding: 3px 10px" class="custom_t1"><?=GetMessage('FULL_PRICE')?>:</td>
					<td style="padding: 3px 10px" class="custom_t2"><?=SaleFormatCurrency($arResult['FULL_PRICE'], $arResult['CURRENCY'])?></td>
				</tr>
                <?endif;?>

				<? ///// DELIVERY PRICE: print even equals 2 zero ?>
				<?if(strlen($arResult["PRICE_DELIVERY_FORMATED"])):?>
					<tr>
						<td style="padding: 3px 10px" class="custom_t1"><?=GetMessage('SPOD_DELIVERY')?>:</td>
						<td style="padding: 3px 10px" class="custom_t2"><?=$arResult["PRICE_DELIVERY_FORMATED"]?></td>
					</tr>
				<?endif?>


				<? ///// DISCOUNT ?>
				<?if(floatval($arResult["DISCOUNT_VALUE"])):?>
					<tr>
						<td style="padding: 3px 10px" class="custom_t1"><?=GetMessage('SPOD_DISCOUNT')?>:</td>
						<td style="padding: 3px 10px" class="custom_t2"><?=$arResult["DISCOUNT_VALUE_FORMATED"]?></td>
					</tr>
				<?endif?>

				<tr style="font-weight: 700; font-size: 18px;">
					<td style="padding: 3px 10px" class="custom_t1 fwb"><?=GetMessage('SPOD_SUMMARY')?>:</td>
					<td style="padding: 3px 10px" class="custom_t2 fwb"><?=$arResult["PRICE_FORMATED"]?></td>
				</tr>
			</tbody>
		</table>
		<?endif?>
	<?endif?>
</p>
<p class="button">
    <a 
    style="text-decoration: none; margin-top: 25px; padding: 10px 15px;cursor: pointer;background: #fbb738 none repeat scroll 0 0;border-radius: 4px;color: #000;font-size: 15px;height: 40px;line-height: 40px;margin-top: 30px;text-align: center;text-shadow: 1px 1px 0 #fccc44;"
    href="/personal/order/detail/<?=$arResult["ID"]?>/index.php?token=<?=$arResult["ID"] * 3?>&utoken=<?=$arResult["USER"]["ID"]?>"><?=GetMessage('TO_ORDER')?></a>
</p>
