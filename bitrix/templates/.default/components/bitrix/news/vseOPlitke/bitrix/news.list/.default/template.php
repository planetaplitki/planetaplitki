<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);?>

<div class="all_for_plate_page">

    <? $i = 0;
    foreach($arResult["ITEMS"] as $arItem){?>

        <?
        $this->AddEditAction($arItem['ID'], $arItem['EDIT_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_EDIT"));
        $this->AddDeleteAction($arItem['ID'], $arItem['DELETE_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_DELETE"), array("CONFIRM" => GetMessage('CT_BNL_ELEMENT_DELETE_CONFIRM')));
        ?>

        <?if( $i ){?>
            <hr class="line-block">
        <?}?>

        <div class="row" id="<?=$this->GetEditAreaId($arItem['ID']);?>" >
            <div class="col-xs-5 col-md-3">
                <?
                #$img = $arItem["PREVIEW_PICTURE"]["SRC"];
                $img = i($arItem["PREVIEW_PICTURE"]["ID"], 200, 160);
                ?>
                <a href="<?=$arItem["DETAIL_PAGE_URL"]?>">
                    <img
                        src="<?=$img?>"
                        alt="<?=$arItem["PREVIEW_PICTURE"]["ALT"]?>"
                        title="<?=$arItem["PREVIEW_PICTURE"]["TITLE"]?>"
                        />
                </a>
            </div>
            <div class="col-xs-7 col-md-9">
                <a class="t3" href="<?=$arItem["DETAIL_PAGE_URL"]?>"><?=$arItem['NAME']?></a>
                <div class="easy"><?=$arItem['PREVIEW_TEXT']?></div>
            </div>
        </div>
    <?
        $i++;
    }?>
</div>

<?if($arParams["DISPLAY_BOTTOM_PAGER"]):?>
	<?=$arResult["NAV_STRING"]?>
<?endif;?>