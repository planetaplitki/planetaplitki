<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>

<?
$similarCollections = $arResult['PROPERTIES']['COLLECTIONS']['VALUE'];
$arResult['SIMILAR_COLLECTIONS'] = $similarCollections;

if (is_object($this->__component)){
    $this->__component->SetResultCacheKeys(array('SIMILAR_COLLECTIONS'));
}
?>