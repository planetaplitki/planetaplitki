<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);
?>

<?if( !count($arResult['ITEMS']) ){
    return;
}?>

<div class="all_shares_page">
    <div class="row">
        <?$i = 1;
        foreach($arResult["ITEMS"] as $arItem){?>

            <?
            $this->AddEditAction($arItem['ID'], $arItem['EDIT_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_EDIT"));
            $this->AddDeleteAction($arItem['ID'], $arItem['DELETE_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_DELETE"), array("CONFIRM" => GetMessage('CT_BNL_ELEMENT_DELETE_CONFIRM')));
            ?>

            <div class="col-xs-6 col-sm-3 usligi-pict" id="<?=$this->GetEditAreaId($arItem['ID']);?>">

                <?
                #$img = $arItem["PREVIEW_PICTURE"]["SRC"];
                $img = i($arItem["PREVIEW_PICTURE"]["ID"], 263, 203, BX_RESIZE_IMAGE_EXACT);
                ?>
                <a href="<?=$arItem["DETAIL_PAGE_URL"]?>">
                    <img
                        src="<?=$img?>"
                        alt="<?=$arItem["PREVIEW_PICTURE"]["ALT"]?>"
                        title="<?=$arItem["PREVIEW_PICTURE"]["TITLE"]?>"
                        />
                </a>
                <p class="text-pict"><a href="<?=$arItem['DETAIL_PAGE_URL']?>"><?=$arItem['NAME']?></a></p>
            </div>

            <?if( $i%4 == 0 ){?>
                </div><div>
            <?}

            $i++;
        }?>
    </div>
</div>