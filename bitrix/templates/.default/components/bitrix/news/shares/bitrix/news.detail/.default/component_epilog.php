<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>

<?if( CModule::IncludeModule('iblock') ){?>

    <?
    $similarCollections = $arResult['SIMILAR_COLLECTIONS'];
    if( eRU($similarCollections) ){?>

        <?
        $GLOBALS['arrSectionsFilter'] = array();
        $GLOBALS['arrSectionsFilter']['ID'] = $similarCollections;
        ?>

        <div class="shares_page">
            <?$APPLICATION->IncludeComponent(
                "up:iblock.section.list",
                "collections.list",
                array(
                    "ID" => "0",
                    "SHOW_PARENT_NAME" => "Y",
                    "IBLOCK_TYPE" => "1_catalog",
                    "IBLOCK_ID" => "34",
                    "SECTION_ID" => "",
                    "SECTION_CODE" => "",
                    "SECTION_URL" => "",
                    "COUNT_ELEMENTS" => "Y",
                    "TOP_DEPTH" => "3",
                    "SECTION_FIELDS" => array(
                        0 => "ID",
                        1 => "NAME",
                        2 => "PICTURE",
                        3 => "SECTION_PAGE_URL",
                        4 => "DEPTH_LEVEL",
                        5 => "IBLOCK_SECTION_ID",
                    ),
                    "SECTION_USER_FIELDS" => array(
                        0 => "UF_MIN_PRICE",
                        1 => "UF_NEW_COLLECTION",
                        2 => "UF_EXCLUSIVE",
                        3 => "UF_SALE",
                        4 => "UF_PRICE_RETAIL",
                        5 => "UF_PRICE_WHOLESALE",
                    ),
                    "ADD_SECTIONS_CHAIN" => "Y",
                    "CACHE_TYPE" => "A",
                    "CACHE_TIME" => "36000000",
                    "CACHE_NOTES" => "",
                    "CACHE_GROUPS" => "Y",
                    "COMPONENT_TEMPLATE" => "collections.list",
                    "FILTER_NAME" => "arrSectionsFilter",
                    "SORT_FIELD" => $sortField,
                    "SORT_ORDER" => $sortOrder,
                    "SORT_FIELD2" => "id",
                    "SORT_ORDER2" => "desc",
                    "URL_404" => "/404.php",
                    "ELEMENT_CONTROLS" => "N",
                    "AJAX_MODE" => "N",
                    "AJAX_OPTION_JUMP" => "N",
                    "AJAX_OPTION_STYLE" => "N",
                    "AJAX_OPTION_HISTORY" => "N",
                    "AJAX_OPTION_ADDITIONAL" => "",
                    "CACHE_FILTER" => "Y",
                    "PAGER_TEMPLATE" => "modern",
                    "DISPLAY_TOP_PAGER" => "N",
                    "DISPLAY_BOTTOM_PAGER" => "Y",
                    "PAGER_TITLE" => "",
                    "PAGER_SHOW_ALWAYS" => "N",
                    "PAGER_DESC_NUMBERING" => "N",
                    "PAGER_DESC_NUMBERING_CACHE_TIME" => "36000",
                    "PAGE_ELEMENT_COUNT" => 100,
                    "PAGER_SHOW_ALL" => "Y",
                    "IS_ACTION" =>"Y",
                    "UF_USER_TYPE" => USER_TYPE
                ),
                false
            );?>
        </div>

    <?}?>

    <?
    $idCurrent = $arResult['ID'];

    $arFilter = Array("IBLOCK_ID" => 5, "ACTIVE"=>"Y");
    $res = CIBlockElement::GetList(Array("ID" => "ASC"), $arFilter, false, false, array("ID", "CODE"));
    $count = intval($res->SelectedRowsCount());

    if( $count > 2 ){
        $i = 1;
        while( $ob = $res->GetNextElement() ){

            $arFields = $ob->GetFields();
            $id = $arFields['ID'];

            $arrCode[] = $arFields['CODE'];
            $arrId[] = $id;
        }

        $last = count($arrId) - 1;
        $current = array_search($idCurrent, $arrId);

        $prev = $current ? $current - 1 : array_search(end($arrId), $arrId);
        $next = $current != $last ? $current + 1 : 0;

        $prev = $arrCode[$prev];
        $next = $arrCode[$next];
        ?>

        <div class="shares_page">
            <div class="row obj50">
                <div class="col-xs-12">
                    <div class="box-slide">
                        <div class="left"><a href="/all-shares/<?=$prev;?>/">« <span>ПРЕДЫДУЩАЯ АКЦИЯ</span></a></div>
                        <div class="right"><a href="/all-shares/<?=$next;?>/"><span>СЛЕДУЮЩАЯ АКЦИЯ</span> »</a></div>
                    </div>
                </div>
            </div>
        </div>
    <?}
}?>