<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);
?>

<div class="news_page">
    <?foreach($arResult["ITEMS"] as $arItem){?>

        <?
        $this->AddEditAction($arItem['ID'], $arItem['EDIT_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_EDIT"));
        $this->AddDeleteAction($arItem['ID'], $arItem['DELETE_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_DELETE"), array("CONFIRM" => GetMessage('CT_BNL_ELEMENT_DELETE_CONFIRM')));
        ?>

        <div class="row" id="<?=$this->GetEditAreaId($arItem['ID']);?>" >
            <?if( $arItem["PREVIEW_PICTURE"]["ID"] ){?>
                <div class="col-xs-5 col-md-4">
                    <?
                    $img = i($arItem["PREVIEW_PICTURE"]["ID"], 200, 160);
                    ?>
                    <a href="<?=$arItem["DETAIL_PAGE_URL"]?>">
                        <img
                            src="<?=$img?>"
                            alt="<?=$arItem["PREVIEW_PICTURE"]["ALT"]?>"
                            title="<?=$arItem["PREVIEW_PICTURE"]["TITLE"]?>"
                            />
                    </a>
                </div>
                <?$contentClass = "col-md-8";?>
            <?}
            else{
                $contentClass = "col-md-12";
            }?>

            <div class="col-xs-7 <?=$contentClass;?>">
                <p class="t3"><a href="<?=$arItem["DETAIL_PAGE_URL"]?>"><?=$arItem['NAME']?></a></p>
                <div class="easy"><span><?=$arItem['DATE_ACTIVE_FROM']?></span><br><?=$arItem['PREVIEW_TEXT']?></div>
            </div>
        </div>
        <hr class="line-block">
    <?}?>

    <?if($arParams["DISPLAY_BOTTOM_PAGER"]):?>
        <?=$arResult["NAV_STRING"]?>
    <?endif;?>
</div>