<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>

<?
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);
?>
 
<?if (!empty($arResult['ITEMS'])){?>

    <div class="row obj50">
        <?
        $strElementEdit = CIBlock::GetArrayByID($arParams["IBLOCK_ID"], "ELEMENT_EDIT");
        $strElementDelete = CIBlock::GetArrayByID($arParams["IBLOCK_ID"], "ELEMENT_DELETE");
        $arElementDeleteParams = array("CONFIRM" => GetMessage('CT_BCS_TPL_ELEMENT_DELETE_CONFIRM'));
        ?>

        <?foreach ($arResult['ITEMS'] as $key => $arItem){

            $this->AddEditAction($arItem['ID'], $arItem['EDIT_LINK'], $strElementEdit);
            $this->AddDeleteAction($arItem['ID'], $arItem['DELETE_LINK'], $strElementDelete, $arElementDeleteParams);
            $strMainID = $this->GetEditAreaId($arItem['ID']);

            $arItemIDs = array(
                'ID' => $strMainID,
                'PICT' => $strMainID.'_pict',
                'SECOND_PICT' => $strMainID.'_secondpict',
                'STICKER_ID' => $strMainID.'_sticker',
                'SECOND_STICKER_ID' => $strMainID.'_secondsticker',
                'QUANTITY' => $strMainID.'_quantity',
                'QUANTITY_DOWN' => $strMainID.'_quant_down',
                'QUANTITY_UP' => $strMainID.'_quant_up',
                'QUANTITY_MEASURE' => $strMainID.'_quant_measure',
                'BUY_LINK' => $strMainID.'_buy_link',
                'SUBSCRIBE_LINK' => $strMainID.'_subscribe',

                'PRICE' => $strMainID.'_price',
                'DSC_PERC' => $strMainID.'_dsc_perc',
                'SECOND_DSC_PERC' => $strMainID.'_second_dsc_perc',

                'PROP_DIV' => $strMainID.'_sku_tree',
                'PROP' => $strMainID.'_prop_',
                'DISPLAY_PROP_DIV' => $strMainID.'_sku_prop',
                'BASKET_PROP_DIV' => $strMainID.'_basket_prop',
            );

            $strObName = 'ob'.preg_replace("/[^a-zA-Z0-9_]/", "x", $strMainID);

            $productTitle = (
            isset($arItem['IPROPERTY_VALUES']['ELEMENT_PAGE_TITLE'])&& $arItem['IPROPERTY_VALUES']['ELEMENT_PAGE_TITLE'] != ''
                ? $arItem['IPROPERTY_VALUES']['ELEMENT_PAGE_TITLE']
                : $arItem['NAME']
            );
            $imgTitle = (
            isset($arItem['IPROPERTY_VALUES']['ELEMENT_PREVIEW_PICTURE_FILE_TITLE']) && $arItem['IPROPERTY_VALUES']['ELEMENT_PREVIEW_PICTURE_FILE_TITLE'] != ''
                ? $arItem['IPROPERTY_VALUES']['ELEMENT_PREVIEW_PICTURE_FILE_TITLE']
                : $arItem['NAME']
            );?>

            <div class="col-xs-6 col-sm-4 col-md-15 usligi-pict" id="<? echo $strMainID; ?>">
                <a
                    href="<? echo $arItem['DETAIL_PAGE_URL'];?>"
                    title="<? echo $arItem['PREVIEW_PICTURE']['TITLE'];?>"
                    >

                    <? $img = 'http://placehold.it/230x230';
                    if( $arItem['PREVIEW_PICTURE']['ID'] ){
                        $img = i($arItem['PREVIEW_PICTURE']['ID'], 170, 170);
                    }?>

                    <img
                        src="<?=$img?>"
                        title="<? echo $arItem['PREVIEW_PICTURE']['TITLE']; ?>"
                        alt="<? echo $arItem['PREVIEW_PICTURE']['ALT']; ?>"
                        />
                </a>
                <div class="box_under">
                    <a href="<?=$arItem['DETAIL_PAGE_URL'];?>" class="link"><?=$arItem['NAME']?></a>
                    <p class="easy">VIVES CERAMICA (Испания)</p>
                    <p class="easy">44.00x44.00</p>
                    <p class="metr">от <span>1 110 Р</span> м2</p>
                </div>
            </div>
        <?}?>
    </div>
<?}?>