<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>

<?
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);
?>
 
<?
CModule::IncludeModule('sale');

$sections = getSections();
$countries = $sections['COUNTRIES'];
$brands = $sections['BRANDS'];
$collections = $sections['COLLECTIONS'];
$retailM2PriceId = 10;
$type = $arParams['UF_TYPE'];
$userType = $arParams['UF_USER_TYPE'];
# минимальная разница в ценах для отображения скидок 1руб
$minPriceDiff = 1;
$i = 1;
$count = count($arResult["SECTIONS"]);
$quantity_store = 0;
$quantity_store_2 = 0;
$quantity_store_6 = 0;
//echo '<pre>'; print_r($arResult["SECTIONS"]); echo '</pre>';
?>

<?foreach($arResult["SECTIONS"] as $name => $coll):?>
<h2><?=$name;?></h2>
<div class="row obj25">
<?$arResult['ITEMS'] = $coll;?>
<?if( $userType == 'wholesale' ){?>

    <?foreach ($arResult['ITEMS'] as $key => $arItem){

        $productTitle = (
        isset($arItem['IPROPERTY_VALUES']['ELEMENT_PAGE_TITLE'])&& $arItem['IPROPERTY_VALUES']['ELEMENT_PAGE_TITLE'] != ''
            ? $arItem['IPROPERTY_VALUES']['ELEMENT_PAGE_TITLE']
            : $arItem['NAME']
        );
        $imgTitle = (
        isset($arItem['IPROPERTY_VALUES']['ELEMENT_PREVIEW_PICTURE_FILE_TITLE']) && $arItem['IPROPERTY_VALUES']['ELEMENT_PREVIEW_PICTURE_FILE_TITLE'] != ''
            ? $arItem['IPROPERTY_VALUES']['ELEMENT_PREVIEW_PICTURE_FILE_TITLE']
            : $arItem['NAME']
        );?>

        <?
        $idSection = $arItem['IBLOCK_SECTION_ID'];

        $visota = $arItem['PROPERTIES']['VYSOTA']['VALUE'];
        $shirina = $arItem['PROPERTIES']['SHIRINA']['VALUE'];

        $price = $arItem['MIN_PRICE'];
        $priceDiscount = $price['DISCOUNT_VALUE'];
        $priceBase = $price['VALUE'];

        $article = $arItem['PROPERTIES']['CML2_ARTICLE']['VALUE'];
        $quantity = $arItem['CATALOG_QUANTITY'];
        
        $quantity_store = 0;
        $quantity_store_2 = $arItem['CATALOG_QUANTITY_STORE'];
        $quantity_store_6 = $arItem['CATALOG_QUANTITY_STORE_6'];
        if($quantity_store_2){
            $quantity_store = $quantity_store + $quantity_store_2;    
        }
        if($quantity_store_6){
            $quantity_store = $quantity_store + $quantity_store_6;    
        }
        
        $img = "/upload/images/net-foto-tovar.jpg";
        if( $arItem['PREVIEW_PICTURE']['ID'] ){
            $img = i($arItem['PREVIEW_PICTURE']['ID'], 100, 100);
        }?>
        <div class="box-catalog">
            <div class="row" id="<? echo $strMainID;?>">
                <div class="col-sm-3 col-lg-1 box-catalog-half">
                    <div class="box-pict">
                        <a href="<?=$arItem['DETAIL_PAGE_URL']?>">
                            <img
                                src="<?=$img?>"
                                title="<? echo $arItem['PREVIEW_PICTURE']['TITLE']; ?>"
                                alt="<? echo $arItem['PREVIEW_PICTURE']['ALT']; ?>"
                                />
                        </a>
                    </div>
                </div>
                <div class="col-sm-9 col-lg-11 box-catalog-half">
                    <div class="row">
                        <div class="col-sm-4 col-lg-3">
                            <div class="box-article">
                                <a href="<?=$arItem['DETAIL_PAGE_URL']?>" class="name"><?=$arItem['NAME']?></a>
                                <?if( $article ){?>
                                    <p class="article">Артикул: <?=$article?></p>
                                <?}?>
                            </div>
                        </div>
                        <div class="col-sm-4 col-lg-2">
                            <div class="box-easy">
                                <?if( $quantity_store ){?>
                                    <p class="have ok"><span class="icon-ok"></span> Есть в наличии (<?=$quantity_store?> шт.)</p>
                                <?}
                                else{?>
                                    <p class="have fail"><span class="icon-fail"></span> Нет в наличии</p>
                                <?}?>
                            </div>
                        </div>
                        <div class="col-sm-4 col-lg-2">
                            <div class="box-easy">
                                <?if( $priceBase > $priceDiscount && (($priceBase - $priceDiscount) >= $minPriceDiff)){?>
                                    <p class="price priceDiscount">Цена <b><?=SaleFormatCurrency($priceDiscount, 'RUB')?></b> шт</p>
                                    <p class="price priceBase"><b><?=SaleFormatCurrency($priceBase, 'RUB')?></b> шт</p>
                                <?}
                                else{?>
                                    <p class="price priceSingle">Цена <b><?=SaleFormatCurrency($priceDiscount, 'RUB')?></b> шт</p>
                                <?}?>
                            </div>
                        </div>
                        <div class="col-sm-4 col-lg-2">
                            <div class="box-easy">
                                <div class="minus">-</div>
                                <div class="kolvo quantity">
                                    <input class="kolvo" type="text" value="1" name="QUANTITY_<?=$arResult['ID']?>" />
                                </div>
                                <div class="plus">+</div>
                                <p class="text">шт</p>
                            </div>
                        </div>
                        <div class="col-sm-4 col-lg-2">
                            <?if( $visota && $shirina ){?>
                                <div class="box-easy">
                                    <p class="razmer">Размер: <?=$visota?>*<?=$shirina?></p>
                                </div>
                            <?}?>
                        </div>
                        <div class="col-sm-4 col-lg-1 box-easy-marg">
                            <div class="box-easy">
                                <a href="javascript:void(0);" data-operation="buy" data-itemid="<?=$arItem['ID']?>" data-place="list" data-parentid="<?=$arItem['IBLOCK_SECTION_ID']?>" >
                                    <div class="cart"></div>
                                </a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    <?}?>
<?}
else{?>
    <?if( $type == 'otheritems.mobile' ){?>

<!--      --><?//pRU($arParams, 'all')?>

        <?foreach ($arResult['ITEMS'] as $key => $arItem){

            $productTitle = (
            isset($arItem['IPROPERTY_VALUES']['ELEMENT_PAGE_TITLE'])&& $arItem['IPROPERTY_VALUES']['ELEMENT_PAGE_TITLE'] != ''
                ? $arItem['IPROPERTY_VALUES']['ELEMENT_PAGE_TITLE']
                : $arItem['NAME']
            );
            $imgTitle = (
            isset($arItem['IPROPERTY_VALUES']['ELEMENT_PREVIEW_PICTURE_FILE_TITLE']) && $arItem['IPROPERTY_VALUES']['ELEMENT_PREVIEW_PICTURE_FILE_TITLE'] != ''
                ? $arItem['IPROPERTY_VALUES']['ELEMENT_PREVIEW_PICTURE_FILE_TITLE']
                : $arItem['NAME']
            );?>

            <?
            $idSection = $arItem['IBLOCK_SECTION_ID'];
            $collection = $collections[$idSection];
            $brand = $brands[$collection['IBLOCK_SECTION_ID']];
            $country = $countries[$brand['IBLOCK_SECTION_ID']];

            $visota = $arItem['PROPERTIES']['VYSOTA']['VALUE'];
            $shirina = $arItem['PROPERTIES']['SHIRINA']['VALUE'];

            $price = $arItem['MIN_PRICE'];
            $priceDiscount = $price['DISCOUNT_VALUE'];
            $priceBase = $price['VALUE'];

//            $m2PriceId = 10;
//            if( $userType == 'wholesale' ){
//                $m2PriceId = 12;
//            }
//
//            $db_res = CPrice::GetList(array(), array("PRODUCT_ID" => $arResult['ID'], "CATALOG_GROUP_ID" => $m2PriceId));
//            if ($ar_res = $db_res->Fetch()){
//                $retailM2 = $ar_res['PRICE'];
//            }

//            $retailM2 = (10000 / floatval( floatval(str_replace(',','.',$visota))*floatval(str_replace(',','.',$shirina)))) * $priceDiscount;
//            $retailM2Base = (10000 / floatval( floatval(str_replace(',','.',$visota))*floatval(str_replace(',','.',$shirina)))) * $priceBase;
            $hidePriceM2 = $arItem['PROPERTIES']['TSENA_V_SHT']['VALUE'];

//            pRU($arItem, 'all');
            foreach( $arItem['PRICES'] as $price ){
                if( $price['PRICE_ID'] == 10 ){
                    $retailM2 = $price['DISCOUNT_VALUE'];
                    $retailM2Base = $price['VALUE'];
                }
                elseif( $price['PRICE_ID'] == 12 ){
                    $retailM2 = $price['DISCOUNT_VALUE'];
                    $retailM2Base = $price['VALUE'];
                }
            }
            ?>

            <div class="col-xs-4 usligi-pict2">
                <?  
                $img = "/upload/images/net-foto-tovar.jpg";
                if( $arItem['PREVIEW_PICTURE']['ID'] ){
                    $img = i($arItem['PREVIEW_PICTURE']['ID'], 200, 200);
                }?>

                <? $marker = getMarker($arItem);?>
                <?if($marker['HTML']){?>
                    <?=$marker['HTML']['LIST']?>
                <?}?>

                <a href="<?=$arItem['DETAIL_PAGE_URL']?>">
                    <img
                        src="<?=$img?>"
                        title="<? echo $arItem['PREVIEW_PICTURE']['TITLE']; ?>"
                        alt="<? echo $arItem['PREVIEW_PICTURE']['ALT']; ?>"
                        />
                </a>
                <div class="box_under">
                    <a href="<?=$arItem['DETAIL_PAGE_URL']?>" class="link"><?=$arItem['NAME']?></a>
                    <p class="easy"><?=$brand['NAME']?> (<?=$country['NAME']?>)</p>
                    <?if( $visota && $shirina ){?>
                        <p class="easy"><?=$visota;?>x<?=$shirina;?></p>
                    <?}?>

                    <?if( $hidePriceM2 ){?>
                        <?if( $priceDiscount < $priceBase && (($priceBase - $priceDiscount) >= $minPriceDiff) ){?>
                            <p class="metr priceDiscount"><span><?=SaleFormatCurrency($priceDiscount, 'RUB', true)?> р</span>/шт</p>
                            <p class="metr priceBase"><span><?=SaleFormatCurrency($priceBase, 'RUB', true)?> р</span> шт</p>
                        <?}
                        else{?>
                            <p class="metr priceSingle">Цена <span><?=SaleFormatCurrency($priceDiscount, 'RUB', true)?> р</span>/шт</p>
                        <?}?>
                    <?}
                    else{?>
                        <?if( $retailM2 < $retailM2Base ){?>
                            <p class="metr priceDiscount"><span><?=SaleFormatCurrency($retailM2, 'RUB', true)?> р</span>/м2</p>
                            <p class="metr priceBase"><span><?=SaleFormatCurrency($retailM2Base, 'RUB', true)?> р</span>/м2</p>
                        <?}
                        else{?>
                            <p class="metr priceSingle">Цена <span><?=SaleFormatCurrency($retailM2, 'RUB', true)?> р</span>/м2</p>
                        <?}?>
                    <?}?>
                    <!--<a href="<?=$arItem['DETAIL_PAGE_URL']?>"><div class="cart"></div></a>-->
                </div>
            </div>
        <?}?>

    <?}
    else{?>

        <?if($arResult['DESCRIPTION']){?>

            <?$this->SetViewTarget("catalog_section_text",100);?>
            <div class="description"><?=$arResult["DESCRIPTION"]?></div>
            <?$this->EndViewTarget("catalog_section_text");?>

        <?}?>

        <?if (!empty($arResult['ITEMS'])){?>

            <?
            $strElementEdit = CIBlock::GetArrayByID($arParams["IBLOCK_ID"], "ELEMENT_EDIT");
            $strElementDelete = CIBlock::GetArrayByID($arParams["IBLOCK_ID"], "ELEMENT_DELETE");
            $arElementDeleteParams = array("CONFIRM" => GetMessage('CT_BCS_TPL_ELEMENT_DELETE_CONFIRM'));
            ?>

            <?foreach ($arResult['ITEMS'] as $key => $arItem){
                //__($arItem);

                $this->AddEditAction($arItem['ID'], $arItem['EDIT_LINK'], $strElementEdit);
                $this->AddDeleteAction($arItem['ID'], $arItem['DELETE_LINK'], $strElementDelete, $arElementDeleteParams);
                $strMainID = $this->GetEditAreaId($arItem['ID']);

                $arItemIDs = array(
                    'ID' => $strMainID,
                    'PICT' => $strMainID.'_pict',
                    'SECOND_PICT' => $strMainID.'_secondpict',
                    'STICKER_ID' => $strMainID.'_sticker',
                    'SECOND_STICKER_ID' => $strMainID.'_secondsticker',
                    'QUANTITY' => $strMainID.'_quantity',
                    'QUANTITY_DOWN' => $strMainID.'_quant_down',
                    'QUANTITY_UP' => $strMainID.'_quant_up',
                    'QUANTITY_MEASURE' => $strMainID.'_quant_measure',
                    'BUY_LINK' => $strMainID.'_buy_link',
                    'SUBSCRIBE_LINK' => $strMainID.'_subscribe',

                    'PRICE' => $strMainID.'_price',
                    'DSC_PERC' => $strMainID.'_dsc_perc',
                    'SECOND_DSC_PERC' => $strMainID.'_second_dsc_perc',

                    'PROP_DIV' => $strMainID.'_sku_tree',
                    'PROP' => $strMainID.'_prop_',
                    'DISPLAY_PROP_DIV' => $strMainID.'_sku_prop',
                    'BASKET_PROP_DIV' => $strMainID.'_basket_prop',
                );

                $strObName = 'ob'.preg_replace("/[^a-zA-Z0-9_]/", "x", $strMainID);

                $productTitle = (
                isset($arItem['IPROPERTY_VALUES']['ELEMENT_PAGE_TITLE'])&& $arItem['IPROPERTY_VALUES']['ELEMENT_PAGE_TITLE'] != ''
                    ? $arItem['IPROPERTY_VALUES']['ELEMENT_PAGE_TITLE']
                    : $arItem['NAME']
                );
                $imgTitle = (
                isset($arItem['IPROPERTY_VALUES']['ELEMENT_PREVIEW_PICTURE_FILE_TITLE']) && $arItem['IPROPERTY_VALUES']['ELEMENT_PREVIEW_PICTURE_FILE_TITLE'] != ''
                    ? $arItem['IPROPERTY_VALUES']['ELEMENT_PREVIEW_PICTURE_FILE_TITLE']
                    : $arItem['NAME']
                );?>

                <?
                $idSection = $arItem['IBLOCK_SECTION_ID'];
                $collection = $collections[$idSection];
                $brand = $brands[$collection['IBLOCK_SECTION_ID']];
                $country = $countries[$brand['IBLOCK_SECTION_ID']];
                $imgFlag = i($country['UF_FLAG'], 16, 11, BX_RESIZE_IMAGE_EXACT);
                $price = $arItem['MIN_PRICE'];
                $priceDiscount = $price['DISCOUNT_VALUE'];
                $priceBase = $price['VALUE'];


                $visota = $arItem['PROPERTIES']['VYSOTA']['VALUE'];
                $shirina = $arItem['PROPERTIES']['SHIRINA']['VALUE'];

//            $db_res = CPrice::GetList(array(), array("PRODUCT_ID" => $arItem['ID'], "CATALOG_GROUP_ID" => $retailM2PriceId));
//            $retailM2 = "";
//            if ($ar_res = $db_res->Fetch()){
//                $retailM2 = $ar_res['PRICE'];
//            }
//                pRU($arItem, 'all');

//                $retailM2 = (10000 / floatval( floatval(str_replace(',','.',$visota))*floatval(str_replace(',','.',$shirina)))) * $priceDiscount;
//                $retailM2Base = (10000 / floatval( floatval(str_replace(',','.',$visota))*floatval(str_replace(',','.',$shirina)))) * $priceBase;


                //__($arParams);   
                $retailM2 = 0;         
                $retailM2Base = 0;         
                foreach( $arItem['PRICES'] as $price ){
                    if( $price['PRICE_ID'] == 10 ){
                        $retailM2 = $price['DISCOUNT_VALUE'];
                        $retailM2Base = $price['VALUE'];
                    }
                    elseif( $price['PRICE_ID'] == 12 ){
                        $retailM2 = $price['DISCOUNT_VALUE'];
                        $retailM2Base = $price['VALUE'];
                    }
                }
                                     
                $hidePriceM2 = $arItem['PROPERTIES']['TSENA_V_SHT']['VALUE'];
                ?>

                <?/*?><div class="col-xs-6 col-sm-4 col-md-15 usligi-pict"><?*/?>
                <div class="col-xs-6 col-sm-3 col-md-3 usligi-pict" id="<? echo $strMainID;?>" itemscope itemtype="http://schema.org/Product">
                    <div class="collection_item_container">
                    <?    
                    $img = "/upload/images/net-foto-tovar.jpg";
                    if( $arItem['PREVIEW_PICTURE']['ID'] ){
                        $img = i($arItem['PREVIEW_PICTURE']['ID'], 200, 200);
                    }?>

                    <? $marker = getMarker($arItem);?>
                    <?if($marker['HTML']){?>
                        <?=$marker['HTML']['LIST']?>
                    <?}?>

                    <div class="catalog-image-container">
                        <div>
                            <a href="<?=$arItem['DETAIL_PAGE_URL']?>" class="image-link">
                                <img
                                    itemprop="image"
                                    src="<?=$img?>"
                                    title="<? echo $arItem['PREVIEW_PICTURE']['TITLE']; ?>"
                                    alt="<? echo $arItem['PREVIEW_PICTURE']['ALT']; ?>"
                                    />
                            </a>
                        </div>
                    </div>
                    <div class="box_under">
                        <a href="<?=$arItem['DETAIL_PAGE_URL']?>" class="link"><span itemprop="name"><?=$arItem['NAME']?></span></a>
                        <p class="easy country-container"><?=$collection['NAME']?> (<?=$country['NAME']?>)
                            <span class="flag"><img src="<?=$imgFlag;?>" alt="" /></span>
                        </p>
                        <?if( $visota && $shirina ){?>
                            <p class="easy"><?=$visota;?>x<?=$shirina;?></p>
                        <?}?>

                        <?if( $hidePriceM2 ){?>
                            <?if( $priceDiscount < $priceBase && (($priceBase - $priceDiscount) >= $minPriceDiff) ){?>
                                <p itemprop="offers" itemscope itemtype="http://schema.org/Offer" class="metr priceDiscount">
                                <span itemprop="price" content="<?=$priceDiscount?>"><?=SaleFormatCurrency($priceDiscount, 'RUB', true)?> <span itemprop="priceCurrency" content="RUR">р</span></span>/шт
                                </p>
                                <p class="metr priceBase"><span><?=SaleFormatCurrency($priceBase, 'RUB', true)?> Р</span>/шт</p>
                            <?}
                            else{?>
                                <p itemprop="offers" itemscope itemtype="http://schema.org/Offer" class="metr priceSingle">
                                Цена <span itemprop="price" content="<?=$priceDiscount?>"><?=SaleFormatCurrency($priceDiscount, 'RUB', true)?> <span itemprop="priceCurrency" content="RUR">р</span></span>/шт
                                </p>
                            <?}?>
                        <?}
                        elseif($retailM2){?>
                            <?if( $retailM2 < $retailM2Base ){?>
                                <p itemprop="offers" itemscope itemtype="http://schema.org/Offer" class="metr priceDiscount">
                                <span itemprop="price" content="<?=$retailM2?>"><?=SaleFormatCurrency($retailM2, 'RUB', true)?> <span itemprop="priceCurrency" content="RUR">р</span></span>/м2
                                </p>
                                <p class="metr priceBase"><span><?=SaleFormatCurrency($retailM2Base, 'RUB', true)?> р</span>/м2</p>
                            <?}
                            else{?>
                                <p itemprop="offers" itemscope itemtype="http://schema.org/Offer" class="metr priceSingle">
                                Цена <span itemprop="price" content="<?=$retailM2?>"><?=SaleFormatCurrency($retailM2, 'RUB', true)?> <span itemprop="priceCurrency" content="RUR">р</span></span>/м2
                                </p>
                            <?}?>
                        <?} else {?>
                                <p itemprop="offers" itemscope itemtype="http://schema.org/Offer" class="metr priceSingle">
                                Цена <span itemprop="price" content="<?=$priceBase?>"><?=SaleFormatCurrency($priceBase, 'RUB', true)?> <span itemprop="priceCurrency" content="RUR">р</span></span>/м2
                                </p>
                        <?}?>

                        <!--<a href="<?=$arItem['DETAIL_PAGE_URL']?>"><div class="cart"></div></a>-->
                    </div>

                    
                        <div class="bottom_item_container">
                        
                            <?# отдаем в js параметры плитки для калькулятора
                            
                            $vUpakovkeShtuk = $arItem['PROPERTIES']['V_UPAKOVKE_SHTUK']['VALUE'];
                            $vUpakovkePrice = $vUpakovkeShtuk * $retailOne;
                            $relations = array();
                            $relations['V_UPAKOVKE_SHTUK'] = $vUpakovkeShtuk;
                            $relations['V_UPAKOVKE_PRICE'] = $vUpakovkePrice;
                            $relations['RETAIL_M2_PRICE'] = $retailM2;
                            $relations['RETAIL_ONE'] = $priceBase;
                            $relations['VISOTA'] = str_replace(',', '.', $visota) / 100;
                            $relations['SHIRINA'] = str_replace(',', '.', $shirina) / 100;?>    
                            <form class="j_hidden_params" data-item="<?=$arItem['ID'];?>">
                                <?foreach($relations as $key=>$value):?>
                                    <input type="hidden" name="<?=$key;?>" value="<?=$value;?>"> 
                                <?endforeach;?>
                            </form>                        
                        
                        <div class="box-position quantity" data-parametr="shtuki">
                            <div class="m_block1">
                                <div class="minus">-</div>
                                <input class="kolvo" type="text" value="1" name="QUANTITY_<?= $arItem['ID'] ?>"
                                       data-price="<?= $priceDiscount ?>" data-place="detail"/>
                                <div class="plus">+</div>
                            </div>
                            <div class="m_block2">
                                <p class="easy"><span>штук</span></p>

                            </div>

                        </div>
                    
                        <? if (!$hidePriceM2) { ?>
                        <br>
                            <div class="box-position" data-parametr="metri" itemprop="offers" itemscope itemtype="http://schema.org/Offer">
                                <div class="m_block1">
                                    <input class="kolvo kvm" type="text" value="<?= round($visota * $shirina / 10000, 3) ?>"
                                           name="QUANTITY_<?= $arItem['ID'] ?>" data-place="detail"/>
                                </div>
                                <div class="m_block2">
                                    <p class="easy"><span>кв. м.</span></p>
                                   
                                </div>
                            </div>
                        <? } ?>
                            <a href="javascript:void(0);" onclick="yaCounter14449306.reachGoal('cart'); return true;" class="product-basket-link a_to_basket" data-operation="buy"
                               data-itemid="<?= $arItem['ID'] ?>" data-place="detail"
                               data-parentid="<?= $arItem['IBLOCK_SECTION_ID'] ?>">В корзину</a>
                    </div>  
                    
                    </div>                    
                </div>

            <?}?>

            <div class="row">
                <div class="col-xs-12">
                    <?if($arParams["DISPLAY_BOTTOM_PAGER"]):?>
                        <?=$arResult["NAV_STRING"]?>
                    <?endif;?>
                </div>
            </div>

            <?/*?>
            <hr class="line-buttom">
            <div class="t2"><?=$arResult['NAME']?></div>
            <div class="easy">
                <?=$arResult['DESCRIPTION']?>
            </div>
        <?*/?>
        <?}?>
    <?}?>
<?}?>
</div>
<?if( $i != $count ){?>
    <hr class="line">
<?}?>

<?$i++;?>
<?endforeach;?>