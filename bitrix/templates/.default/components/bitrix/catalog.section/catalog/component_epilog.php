<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $templateData */
/** @var @global CMain $APPLICATION */
use Bitrix\Main\Loader;
global $APPLICATION;?>

<?
IncludeTemplateLangFile(__FILE__);
$APPLICATION->AddHeadString('<script>window.langObj = {COMPARE: "'.GetMessage("ELEMENT_COMPARE").'", IN_COMPARE: "'.GetMessage("ELEMENT_IN_COMPARE").'"}</script>');
?>