<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>
<?
$bDefaultColumns = $arResult["GRID"]["DEFAULT_COLUMNS"];
$colspan = ($bDefaultColumns) ? count($arResult["GRID"]["HEADERS"]) : count($arResult["GRID"]["HEADERS"]) - 1;
$bPropsColumn = false;
$bUseDiscount = false;
$bPriceType = false;
$bShowNameWithPicture = ($bDefaultColumns) ? true : false; // flat to show name and picture column in one column
?>


<div class="line-white"></div>

<div class="row obj">
    <div class="col-sm-12">
        <div class="title2">Состав заказа</div>
    </div>
</div>

<div class="row header-top-table hidden-xs hidden-sm">
    <div class="col-xs-6 col-sm-3 col-md-2">
        <div class="col-sm-2">Фото</div>
    </div>
    <div class="col-xs-6 col-sm-9 col-md-10">
        <div class="row">
            <div class="col-sm-6">Наименование</div>
            <?/*?><div class="col-sm-3">Размер</div><?*/?>
            <div class="col-sm-2">Количество</div>
            <div class="col-sm-4">Стоимость</div>
            <div class="col-sm-1"></div>
        </div>
    </div>
</div>

<?foreach ($arResult["BASKET_ITEMS"] as $key => $arItem){?>

    <div class="line-top"></div>

    <div class="row header-top-table2 tab2">
        <div class="col-xs-6 col-sm-3 col-md-2 image-pict">
            <?if (strlen($arItem["DETAIL_PAGE_URL"]) > 0):?><a href="<?=$arItem["DETAIL_PAGE_URL"] ?>"><?endif;?>
                <?$img = $arItem['PICTURE']?>
                <img src="<?=$img?>" alt="<?=$arItem['NAME'];?>" />
            <?if (strlen($arItem["DETAIL_PAGE_URL"]) > 0):?></a><?endif;?>
        </div>
        <div class="col-xs-6 col-sm-9 col-md-10">
            <div class="row">
                <div class="col-xs-6 col-sm-4 col-md-6 box">
                    <div class="article">
                        <p>
                            <?if (strlen($arItem["DETAIL_PAGE_URL"]) > 0):?><a href="<?=$arItem["DETAIL_PAGE_URL"] ?>"><?endif;?>
                            <?=$arItem['NAME']?>
                            <?if (strlen($arItem["DETAIL_PAGE_URL"]) > 0):?></a><?endif;?>
                            <?if( $arItem['ARTICUL'] ){?>
								<br>Артикул - <?=$arItem['ARTICUL']?>
                            <?}?>
                            <?if( $arItem['SIZE'] ){?>
                                <br>Размер: <?=$arItem['SIZE'];?>
                            <?}?>
                        </p>
                    </div>
                </div>

                <?/*?>
                <div class="col-xs-6 col-sm-5 col-md-3 box">
                    <div class="article">
                        <p>-</p>
                    </div>
                </div>
                <?*/?>

                <div class="col-xs-6 col-sm-4 col-md-2 box">
                    <div class="price">
                        <p><?=$arItem['QUANTITY']?> шт</p>
                    </div>
                </div>
                <div class="col-xs-6 col-sm-4 col-md-4 box">
                    <div class="price">
                        <p><?=SaleFormatCurrency( ($arItem["PRICE"] + $arItem["DISCOUNT_PRICE"]) * $arItem['QUANTITY'] , $arItem["CURRENCY"]);?></p>
                    </div>
                </div>
                <?/*?>
                    <div class="col-xs-6 col-sm-1 box">
                        <div>
                            <a href="#"><img src="/bitrix/templates/.default/img/icon/icon_close.png"></a>
                        </div>
                    </div>
                <?*/?>
            </div>
        </div>
    </div>
<?}?>

<div class="row obj">
    <div class="col-sm-7">
        <textarea name="ORDER_DESCRIPTION" id="ORDER_DESCRIPTION" class="filters textarea-fleld" placeholder="Дополнительная информация" ><?=$arResult["USER_VALS"]["ORDER_DESCRIPTION"]?></textarea>
    </div>
    <div class="col-sm-5 box-inner3">
        <div class="itogo2"><?=$arResult["ORDER_TOTAL_PRICE_FORMATED"]?></div>
        <div class="itogo2-text">Итоговая стоимость без учета доставки</div>
        <a href="javascript:void();" onclick="submitForm('Y'); yaCounter14449306.reachGoal('order'); return false;" id="ORDER_CONFIRM_BUTTON" class="btn link-buttom checkout">ОФОРМИТЬ ЗАКАЗ</a></div>
    </div>
</div>