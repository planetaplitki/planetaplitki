<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>
<?
if (!function_exists("showFilePropertyField"))
{
	function showFilePropertyField($name, $property_fields, $values, $max_file_size_show=50000)
	{
		$res = "";

		if (!is_array($values) || empty($values))
			$values = array(
				"n0" => 0,
			);

		if ($property_fields["MULTIPLE"] == "N")
		{
			$res = "<label for=\"\"><input type=\"file\" size=\"".$max_file_size_show."\" value=\"".$property_fields["VALUE"]."\" name=\"".$name."[0]\" id=\"".$name."[0]\"></label>";
		}
		else
		{
			$res = '
			<script type="text/javascript">
				function addControl(item)
				{
					var current_name = item.id.split("[")[0],
						current_id = item.id.split("[")[1].replace("[", "").replace("]", ""),
						next_id = parseInt(current_id) + 1;

					var newInput = document.createElement("input");
					newInput.type = "file";
					newInput.name = current_name + "[" + next_id + "]";
					newInput.id = current_name + "[" + next_id + "]";
					newInput.onchange = function() { addControl(this); };

					var br = document.createElement("br");
					var br2 = document.createElement("br");

					BX(item.id).parentNode.appendChild(br);
					BX(item.id).parentNode.appendChild(br2);
					BX(item.id).parentNode.appendChild(newInput);
				}
			</script>
			';

			$res .= "<label for=\"\"><input type=\"file\" size=\"".$max_file_size_show."\" value=\"".$property_fields["VALUE"]."\" name=\"".$name."[0]\" id=\"".$name."[0]\"></label>";
			$res .= "<br/><br/>";
			$res .= "<label for=\"\"><input type=\"file\" size=\"".$max_file_size_show."\" value=\"".$property_fields["VALUE"]."\" name=\"".$name."[1]\" id=\"".$name."[1]\" onChange=\"javascript:addControl(this);\"></label>";
		
		}

		return $res;
	}
}

if (!function_exists("PrintPropsForm"))
{
	function PrintPropsForm($arSource = array(), $locationTemplate = ".default", $userVals)
	{


//		pRU($userVals, 'all');

		if (!empty($arSource)){?>
				<div>
					<?
                    $i = 0;
					foreach ($arSource as $arProperties)
					{
						$params = "";

						if( $userVals['PERSON_TYPE_ID'] == 2 ){
							if(
								!in_array($arProperties['ID'], array(16,19))
							){
								$params = "readonly";
							}
						}

                        # пропускаем параметры дома
                        if(
                            #$arProperties['ID'] == 6 ||
                            $arProperties['ID'] == 22 ||
                            $arProperties['ID'] == 23 ||
                            $arProperties['ID'] == 24 ||
                            $arProperties['ID'] == 25 ||
                            $arProperties['ID'] == 31 ||
							//согласен с правилами покупки (физики)
                            $arProperties['ID'] == 32 ||
							//согласен с правилами покупки (юрики)
                            $arProperties['ID'] == 33 ||
                            $arProperties['ID'] == 35 ||
                            $arProperties['ID'] == 37
                        ){
                            continue;
                        }

                        if ($arProperties["TYPE"] == "CHECKBOX"){?>
							<input type="hidden" name="<?=$arProperties["FIELD_NAME"]?>" value="">

							<div class="bx_block r1x3 pt8">
								<?=$arProperties["NAME"]?>
								<?if ($arProperties["REQUIED_FORMATED"]=="Y"):?>
									<span class="bx_sof_req">*</span>
								<?endif;?>
							</div>

							<div class="bx_block r1x3 pt8">
								<input type="checkbox" name="<?=$arProperties["FIELD_NAME"]?>" id="<?=$arProperties["FIELD_NAME"]?>" value="Y"<?if ($arProperties["CHECKED"]=="Y") echo " checked";?>>

								<?if (strlen(trim($arProperties["DESCRIPTION"])) > 0):?>
                                    <div class="bx_description">
                                        <?=$arProperties["DESCRIPTION"]?>
                                    </div>
								<?endif;?>
							</div>

							<div style="clear: both;"></div>
					    <?}
						elseif ($arProperties["TYPE"] == "TEXT"){?>

                            <?if( $arProperties['ID'] == 21 ){?>
                                <div class="row">
                                    <div class="col-sm-2 label-text2">Дом<?/*<span>*</span>*/?></div>
                                    <div class="col-sm-1"><input type="text" class="filters" id="ORDER_PROP_21" name="ORDER_PROP_21" value="<?=htmlspecialchars($_REQUEST['ORDER_PROP_21']) ? htmlspecialchars($_REQUEST['ORDER_PROP_21']) : $arSource[21]['VALUE']?>" placeholder="Дом"></div>
                                    <div class="col-sm-2"><input type="text" class="filters" id="ORDER_PROP_22" name="ORDER_PROP_22" value="<?=htmlspecialchars($_REQUEST['ORDER_PROP_22']) ? htmlspecialchars($_REQUEST['ORDER_PROP_22']) : $arSource[22]['VALUE']?>" placeholder="Корп./Стр."></div>
                                    <div class="col-sm-2"><input type="text" class="filters" id="ORDER_PROP_23" name="ORDER_PROP_23" value="<?=htmlspecialchars($_REQUEST['ORDER_PROP_23']) ? htmlspecialchars($_REQUEST['ORDER_PROP_23']) : $arSource[23]['VALUE']?>" placeholder="Подъезд"></div>
                                </div>
                                <div class="row">
                                    <div class="col-sm-2 label-text2"></div>
                                    <div class="col-sm-2"><input type="text" class="filters" id="ORDER_PROP_24" name="ORDER_PROP_24" value="<?=htmlspecialchars($_REQUEST['ORDER_PROP_24']) ? htmlspecialchars($_REQUEST['ORDER_PROP_24']) : $arSource[24]['VALUE']?>" placeholder="Домофон"></div>
                                    <div class="col-sm-3"><input type="text" class="filters" id="ORDER_PROP_25" name="ORDER_PROP_25" value="<?=htmlspecialchars($_REQUEST['ORDER_PROP_25']) ? htmlspecialchars($_REQUEST['ORDER_PROP_25']) : $arSource[25]['VALUE']?>" placeholder="Квартира/офис*"></div>
                                </div>
                            <?}
                            else{?>

                                <div class="row<?=!$i ? ' obj' : '';?>" >

                                    <div class="col-sm-2 label-text2"><?=$arProperties["NAME"]?>
                                        <?if ($arProperties["REQUIED_FORMATED"]=="Y"):?>
                                            <span class="bx_sof_req">*</span>
                                        <?endif;?>
                                    </div>

                                    <?if( $arProperties['ID'] == 20 ){?>
                                    
                                        <div class="col-sm-2">
                                            <input type="text" class="filters" value="<?=$arProperties["VALUE"]?>" name="<?=$arProperties["FIELD_NAME"]?>" id="<?=$arProperties["FIELD_NAME"]?>">
                                        </div>
                                        <div class="col-sm-3 label-text2">руб.</div>
                                    <?}
                                    else{?>
                                        <div class="col-sm-5">
                                        <?if($arProperties["FIELD_NAME"] == "ORDER_PROP_3"):?>
                                            <label class="my_before_after_number"></label><input type="text" class="filters" <?=$params?> value="<?=$arProperties["VALUE"]?>" name="<?=$arProperties["FIELD_NAME"]?>" id="<?=$arProperties["FIELD_NAME"]?>">
                                        <?else:?>
                                            <input type="text" class="filters" <?=$params?> value="<?=$arProperties["VALUE"]?>" name="<?=$arProperties["FIELD_NAME"]?>" id="<?=$arProperties["FIELD_NAME"]?>">
                                        <?endif;?>
                                        </div>
                                    <?}?>
                                </div>
                            <?}?>
                        <?}
						elseif ($arProperties["TYPE"] == "SELECT"){
							?>
							<br/>
							<div class="bx_block r1x3 pt8">
								<?=$arProperties["NAME"]?>
								<?if ($arProperties["REQUIED_FORMATED"]=="Y"):?>
									<span class="bx_sof_req">*</span>
								<?endif;?>
							</div>

							<div class="bx_block r3x1">
								<select name="<?=$arProperties["FIELD_NAME"]?>" id="<?=$arProperties["FIELD_NAME"]?>" size="<?=$arProperties["SIZE1"]?>">
									<?foreach($arProperties["VARIANTS"] as $arVariants):?>
										<option value="<?=$arVariants["VALUE"]?>"<?if ($arVariants["SELECTED"] == "Y") echo " selected";?>><?=$arVariants["NAME"]?></option>
									<?endforeach;?>
								</select>

								<?
								if (strlen(trim($arProperties["DESCRIPTION"])) > 0):
								?>
								<div class="bx_description">
									<?=$arProperties["DESCRIPTION"]?>
								</div>
								<?
								endif;
								?>
							</div>
							<div style="clear: both;"></div>
							<?
						}
						elseif ($arProperties["TYPE"] == "MULTISELECT")
						{
							?>
							<br/>
							<div class="bx_block r1x3 pt8">
								<?=$arProperties["NAME"]?>
								<?if ($arProperties["REQUIED_FORMATED"]=="Y"):?>
									<span class="bx_sof_req">*</span>
								<?endif;?>
							</div>

							<div class="bx_block r3x1">
								<select multiple name="<?=$arProperties["FIELD_NAME"]?>" id="<?=$arProperties["FIELD_NAME"]?>" size="<?=$arProperties["SIZE1"]?>">
									<?
									foreach($arProperties["VARIANTS"] as $arVariants):
									?>
										<option value="<?=$arVariants["VALUE"]?>"<?if ($arVariants["SELECTED"] == "Y") echo " selected";?>><?=$arVariants["NAME"]?></option>
									<?
									endforeach;
									?>
								</select>

								<?
								if (strlen(trim($arProperties["DESCRIPTION"])) > 0):
								?>
								<div class="bx_description">
									<?=$arProperties["DESCRIPTION"]?>
								</div>
								<?
								endif;
								?>
							</div>
						<?}
						elseif ($arProperties["TYPE"] == "TEXTAREA"){?>
							<div class="row">
								<div class="col-sm-2 label-text2">
									<?=$arProperties["NAME"]?>
									<?if ($arProperties["REQUIED_FORMATED"]=="Y"):?>
										<span class="bx_sof_req">*</span>
									<?endif;?>
								</div>
								<div class="col-sm-5">
									<textarea <?=$params?> name="<?=$arProperties["FIELD_NAME"]?>" id="<?=$arProperties["FIELD_NAME"]?>"><?=$arProperties["VALUE"]?></textarea>
								</div>
							</div>
						<?}
						elseif ($arProperties["TYPE"] == "LOCATION")
						{
							$value = 0;
							if (is_array($arProperties["VARIANTS"]) && count($arProperties["VARIANTS"]) > 0)
							{
								foreach ($arProperties["VARIANTS"] as $arVariant)
								{
									if ($arVariant["SELECTED"] == "Y")
									{
										$value = $arVariant["ID"];
										break;
									}
								}
							}?>

                            <div class="row">
                                <div class="col-sm-2 label-text2"><?=$arProperties["NAME"]?></div>
                                <div class="col-sm-5">

                                    <?
                                    $GLOBALS["APPLICATION"]->IncludeComponent(
                                        "bitrix:sale.ajax.locations",
                                        $locationTemplate,
                                        array(
                                            "AJAX_CALL" => "N",
                                            "COUNTRY_INPUT_NAME" => "COUNTRY",
                                            "REGION_INPUT_NAME" => "REGION",
                                            "CITY_INPUT_NAME" => $arProperties["FIELD_NAME"],
                                            "CITY_OUT_LOCATION" => "Y",
                                            "LOCATION_VALUE" => $value,
                                            "ORDER_PROPS_ID" => $arProperties["ID"],
                                            "ONCITYCHANGE" => ($arProperties["IS_LOCATION"] == "Y" || $arProperties["IS_LOCATION4TAX"] == "Y") ? "submitForm()" : "",
                                            "SIZE1" => $arProperties["SIZE1"],
                                        ),
                                        null,
                                        array('HIDE_ICONS' => 'Y')
                                    );
                                    ?>
                                </div>
                            </div>

                            <?/*?>
							<div class="bx_block r1x3 pt8">

								<?if ($arProperties["REQUIED_FORMATED"]=="Y"):?>
									<span class="bx_sof_req">*</span>
								<?endif;?>
							</div>
							<div class="bx_block r3x1">
								<?
								if (strlen(trim($arProperties["DESCRIPTION"])) > 0):
								?>
								<div class="bx_description">
									<?=$arProperties["DESCRIPTION"]?>
								</div>
								<?
								endif;
								?>
							</div>
							<div style="clear: both;"></div>
							<?*/
						}
						elseif ($arProperties["TYPE"] == "RADIO")
						{
							?>
							<div class="bx_block r1x3 pt8">
								<?=$arProperties["NAME"]?>
								<?if ($arProperties["REQUIED_FORMATED"]=="Y"):?>
									<span class="bx_sof_req">*</span>
								<?endif;?>
							</div>

							<div class="bx_block r3x1">
								<?
								if (is_array($arProperties["VARIANTS"]))
								{
									foreach($arProperties["VARIANTS"] as $arVariants):
									?>
										<input
											type="radio"
											name="<?=$arProperties["FIELD_NAME"]?>"
											id="<?=$arProperties["FIELD_NAME"]?>_<?=$arVariants["VALUE"]?>"
											value="<?=$arVariants["VALUE"]?>" <?if($arVariants["CHECKED"] == "Y") echo " checked";?> />

										<label for="<?=$arProperties["FIELD_NAME"]?>_<?=$arVariants["VALUE"]?>"><?=$arVariants["NAME"]?></label></br>
									<?
									endforeach;
								}
								?>

								<?
								if (strlen(trim($arProperties["DESCRIPTION"])) > 0):
								?>
								<div class="bx_description">
									<?=$arProperties["DESCRIPTION"]?>
								</div>
								<?
								endif;
								?>
							</div>
							<div style="clear: both;"></div>
						<?}
						elseif ($arProperties["TYPE"] == "FILE"){?>
							<br/>
							<div class="bx_block r1x3 pt8">
								<?=$arProperties["NAME"]?>
								<?if ($arProperties["REQUIED_FORMATED"]=="Y"):?>
									<span class="bx_sof_req">*</span>
								<?endif;?>
							</div>

							<div class="bx_block r3x1">
								<?=showFilePropertyField("ORDER_PROP_".$arProperties["ID"], $arProperties, $arProperties["VALUE"], $arProperties["SIZE1"])?>

								<?
								if (strlen(trim($arProperties["DESCRIPTION"])) > 0):
								?>
								<div class="bx_description">
									<?=$arProperties["DESCRIPTION"]?>
								</div>
								<?
								endif;
								?>
							</div>
					    <?}

                        $i++;
					}?>
				</div>
			<?
		}
	}
}
?>