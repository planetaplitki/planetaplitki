<?if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>

<?
//pRU($arResult, 'all');
?>

<?if(
    CModule::IncludeModule('sale') &&
    CModule::IncludeModule('catalog') &&
    CModule::IncludeModule('iblock')
){
    # общая корзина
    foreach( $arResult["BASKET_ITEMS"] as $k => &$item ){
        $res = CIBlockElement::GetByID($item['PRODUCT_ID']);
        if($ar_res = $res->GetNext()){
            if( $ar_res['IBLOCK_ID'] == IBLOCK_ID_CATALOG ){
                $res_ = CIBlockElement::GetList(Array(), array("IBLOCK_ID"=> IBLOCK_ID_CATALOG, "ID" => $ar_res['ID'],"ACTIVE"=>"Y"), false, false,
                    array(
                        "ID",
                        "NAME",
                        "PREVIEW_PICTURE",
                        "DETAIL_PAGE_URL",
                        "PROPERTY_NAZVANIE_DLYA_SAYTA",
                        "PROPERTY_CML2_ARTICLE",

                        "PROPERTY_VYSOTA",
                        "PROPERTY_SHIRINA",
                    )
                );
                while($ob = $res_->GetNextElement()){
                    $arFields = $ob->GetFields();
                    $name = $arFields['PROPERTY_NAZVANIE_DLYA_SAYTA_VALUE'];
                    $item['NAME'] = $name ? $name : $arFields['NAME'];
                    $item['ARTICUL'] = $arFields['PROPERTY_CML2_ARTICLE_VALUE'];
                    $item['PICTURE'] = $arFields['PREVIEW_PICTURE'] ? i($arFields['PREVIEW_PICTURE'], 170, 170) : 'http://dummyimage.com/170x170/c7c7c7/000000&text=%D0%9D%D0%95%D0%A2+%D0%A4%D0%9E%D0%A2%D0%9E';
                    $item['SIZE'] = $arFields['PROPERTY_SHIRINA_VALUE'].' x '.$arFields['PROPERTY_VYSOTA_VALUE'];
                }
            }
        }
    }
}

?>