<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>

<?if (!empty($arResult["ORDER"])){?>

    <div class="tab-pane fade active in" id="zakaz">
        <div class="box-inner">
            <div class="row">
                <div class="col-sm-12 done-title">Ваш заказ <span>№<a href="/personal/order/detail/<?=$arResult["ORDER"]["ACCOUNT_NUMBER"]?>/"><?=$arResult["ORDER"]["ACCOUNT_NUMBER"]?></a></span> успешно создан</div>
            </div>
            <div class="row">
                <div class="col-sm-4">
                    <p class="done-easy">
                        Дата оформления: <?=$arResult["ORDER"]["DATE_INSERT"]?><br><br>

                        <?if (!empty($arResult["PAY_SYSTEM"])){?>

                            Способ оплаты: <?= $arResult["PAY_SYSTEM"]["NAME"] ?><br><br>

                            <?/*if (strlen($arResult["PAY_SYSTEM"]["ACTION_FILE"]) > 0){?>
                                <tr>
                                    <td>
                                        <?if ($arResult["PAY_SYSTEM"]["NEW_WINDOW"] == "Y"){?>
                                            <script language="JavaScript">
                                                window.open('<?=$arParams["PATH_TO_PAYMENT"]?>?ORDER_ID=<?=urlencode(urlencode($arResult["ORDER"]["ACCOUNT_NUMBER"]))?>');
                                            </script>
                                            <?= GetMessage("SOA_TEMPL_PAY_LINK", Array("#LINK#" => $arParams["PATH_TO_PAYMENT"]."?ORDER_ID=".urlencode(urlencode($arResult["ORDER"]["ACCOUNT_NUMBER"]))))?>
                                            <?if (CSalePdf::isPdfAvailable() && CSalePaySystemsHelper::isPSActionAffordPdf($arResult['PAY_SYSTEM']['ACTION_FILE'])){
                                                ?><br />
                                                <?= GetMessage("SOA_TEMPL_PAY_PDF", Array("#LINK#" => $arParams["PATH_TO_PAYMENT"]."?ORDER_ID=".urlencode(urlencode($arResult["ORDER"]["ACCOUNT_NUMBER"]))."&pdf=1&DOWNLOAD=Y")) ?>
                                            <?}
                                        }
                                        else {
                                            if (strlen($arResult["PAY_SYSTEM"]["PATH_TO_ACTION"])>0){
                                                include($arResult["PAY_SYSTEM"]["PATH_TO_ACTION"]);
                                            }
                                        }?>
                                    </td>
                                </tr>
                            <?}*/?>
                        <?}?>

                        Сумма заказа: <?=SaleFormatCurrency( $arResult['ORDER']['PRICE'] , $arResult["ORDER"]["CURRENCY"]);?><br><br>
                        Статус: <?=GetMessage("SOA_TEMPL_ORDER_SUC")?>
                    </p>
                    <div class="box-inner4">
                        <a href="/personal/order/print/?PRINT=Y&ORDER_ID=<?=$arResult['ORDER']['ACCOUNT_NUMBER']?>" class="print btn link-buttom"><span class="icon-print"></span>РАСПЕЧАТАТЬ</a>
                    </div>
                </div>
                <div class="col-sm-5 ">
                    <p class="done-easy">
                        Вы автоматически авторизировались на сайте и теперь сможете следить за выполнением заказа в личном кабинете сайта. Обратите внимание, что для входа в этот раздел вам необходимо будет ввести логин и пароль пользователя сайта.
                        <br><br>
                        <span>Внимание!</span><br>
                        Пароль сгенерировался автоматически и отправлен вам по электрой почте. Измените его при первом входе в Ваш личный кабинет.
                    </p>
                </div>
                <div class="col-sm-3">
                    <p class="done-easy">
                        Наш менеджер свяжется с Вами в ближайшее время.<br>
                        Спасибо за заказ!
                    </p>
                </div>
            </div>
        </div>
    </div>


	<?/*if (!empty($arResult["PAY_SYSTEM"])){?>
		<br /><br />

		<table class="sale_order_full_table">
			<tr>
				<td class="ps_logo">
					<div class="pay_name"><?=GetMessage("SOA_TEMPL_PAY")?></div>
					<?=CFile::ShowImage($arResult["PAY_SYSTEM"]["LOGOTIP"], 100, 100, "border=0", "", false);?>
					<div class="paysystem_name"><?= $arResult["PAY_SYSTEM"]["NAME"] ?></div><br>
				</td>
			</tr>
			<?
			if (strlen($arResult["PAY_SYSTEM"]["ACTION_FILE"]) > 0)
			{
				?>
				<tr>
					<td>
						<?
						if ($arResult["PAY_SYSTEM"]["NEW_WINDOW"] == "Y")
						{
							?>
							<script language="JavaScript">
								window.open('<?=$arParams["PATH_TO_PAYMENT"]?>?ORDER_ID=<?=urlencode(urlencode($arResult["ORDER"]["ACCOUNT_NUMBER"]))?>');
							</script>
							<?= GetMessage("SOA_TEMPL_PAY_LINK", Array("#LINK#" => $arParams["PATH_TO_PAYMENT"]."?ORDER_ID=".urlencode(urlencode($arResult["ORDER"]["ACCOUNT_NUMBER"]))))?>
							<?
							if (CSalePdf::isPdfAvailable() && CSalePaySystemsHelper::isPSActionAffordPdf($arResult['PAY_SYSTEM']['ACTION_FILE']))
							{
								?><br />
								<?= GetMessage("SOA_TEMPL_PAY_PDF", Array("#LINK#" => $arParams["PATH_TO_PAYMENT"]."?ORDER_ID=".urlencode(urlencode($arResult["ORDER"]["ACCOUNT_NUMBER"]))."&pdf=1&DOWNLOAD=Y")) ?>
								<?
							}
						}
						else
						{
							if (strlen($arResult["PAY_SYSTEM"]["PATH_TO_ACTION"])>0)
							{
								include($arResult["PAY_SYSTEM"]["PATH_TO_ACTION"]);
							}
						}
						?>
					</td>
				</tr>
				<?
			}
			?>
		</table>
		<?
	}*/
}
else{?>
	<b><?=GetMessage("SOA_TEMPL_ERROR_ORDER")?></b><br /><br />
	<table class="sale_order_full_table">
		<tr>
			<td>
				<?=GetMessage("SOA_TEMPL_ERROR_ORDER_LOST", Array("#ORDER_ID#" => $arResult["ACCOUNT_NUMBER"]))?>
				<?=GetMessage("SOA_TEMPL_ERROR_ORDER_LOST1")?>
			</td>
		</tr>
	</table>
<?}?>

<?
#pRU($arResult);
?>
