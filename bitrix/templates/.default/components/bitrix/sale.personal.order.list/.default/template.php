<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>
<?/*if($USER->isAdmin()):?><?echo "<pre>"; print_r($arResult); echo "</pre>";?> <?endif;*/?>
<?if(!empty($arResult['ERRORS']['FATAL'])):?>
	<?foreach($arResult['ERRORS']['FATAL'] as $error):?>
		<?=ShowError($error)?>
	<?endforeach?>
<?else:?>

	<?if(!empty($arResult['ERRORS']['NONFATAL'])):?>
		<?foreach($arResult['ERRORS']['NONFATAL'] as $error):?>
			<?=ShowError($error)?>
		<?endforeach?>
	<?endif?>

    <?/*?>
        <div class="bx_my_order_switch">
            <?$nothing = !isset($_REQUEST["filter_history"]) && !isset($_REQUEST["show_all"]);?>
            <?if($nothing || isset($_REQUEST["filter_history"])):?>
                <a class="bx_mo_link" href="<?=$arResult["CURRENT_PAGE"]?>?show_all=Y"><?=GetMessage('SPOL_ORDERS_ALL')?></a>
            <?endif?>
            <?if($_REQUEST["filter_history"] == 'Y' || $_REQUEST["show_all"] == 'Y'):?>
                <a class="bx_mo_link" href="<?=$arResult["CURRENT_PAGE"]?>?filter_history=N"><?=GetMessage('SPOL_CUR_ORDERS')?></a>
            <?endif?>
            <?if($nothing || $_REQUEST["filter_history"] == 'N' || $_REQUEST["show_all"] == 'Y'):?>
                <a class="bx_mo_link" href="<?=$arResult["CURRENT_PAGE"]?>?filter_history=Y"><?=GetMessage('SPOL_ORDERS_HISTORY')?></a>
            <?endif?>
        </div>
    <?*/?>

	<?if(!empty($arResult['ORDERS'])):?>

        <?if ($_REQUEST["filter_history"] == "Y"){?>
            <a href="<?=$arResult["CURRENT_PAGE"]?>?filter_history=N"><?echo GetMessage("STPOL_CUR_ORDERS")?></a>
        <?}
        else{?>
            <a href="<?=$arResult["CURRENT_PAGE"]?>?filter_history=Y&filter_status=F"><?echo GetMessage("STPOL_ORDERS_HISTORY")?></a><?
        }?>

        <? $bNoOrder = true;
        foreach($arResult["ORDER_BY_STATUS"] as $key => $val){
            //if($USER->isAdmin()):echo"<pre>";print_r($val);echo"</pre>";endif;
            $bShowStatus = true;
            foreach($val as $vval){

                /*$bNoOrder = false;
                if($bShowStatus){?>
                    <h2><?echo GetMessage("STPOL_STATUS")?> "<?=$arResult["INFO"]["STATUS"][$key]["NAME"] ?>"</h2>
                    <small><?=$arResult["INFO"]["STATUS"][$key]["DESCRIPTION"] ?></small>
                <?}
                $bShowStatus = false;
                */?>
                


                <div class="order_status">
                    <div class="order_status__box">
                        <div class="order_status__box__left">
                            <p class="box__left_title">Заказ №
                                <?=GetMessage("STPOL_ORDER_NO")?>
                                <a title="<?echo GetMessage("STPOL_DETAIL_ALT")?>" href="<?=$vval["ORDER"]["URL_TO_DETAIL"] ?>"><?=$vval["ORDER"]["ACCOUNT_NUMBER"]?></a>
                                <?=GetMessage("STPOL_FROM")?>
                                <?=$vval["ORDER"]["DATE_INSERT"]; ?>
                            </p>

                            <p class="box__left_easy">
                                Сумма: <?=GetMessage("STPOL_SUM")?> <strong>
                                    <?=$vval["ORDER"]["FORMATED_PRICE"]?>
                                    <?if($vval["ORDER"]["PAYED"]=="Y")
                                        echo GetMessage("STPOL_PAYED_Y");
                                    else
                                        echo GetMessage("STPOL_PAYED_N");
                                    ?>
                                </strong><br>

                                <?if(IntVal($vval["ORDER"]["PAY_SYSTEM_ID"])>0){?>
                                    Способ оплаты: <?=GetMessage("P_PAY_SYS")?> <strong><?=$arResult["INFO"]["PAY_SYSTEM"][$vval["ORDER"]["PAY_SYSTEM_ID"]]["NAME"]?></strong><br/>
                                <?}?>

                                <?if(IntVal($vval["ORDER"]["DELIVERY_ID"])>0){?>
                                    Способ доставки: <?=GetMessage("P_DELIVERY")?> <strong><?=$arResult["INFO"]["DELIVERY"][$vval["ORDER"]["DELIVERY_ID"]]["NAME"];?></strong></br>
                                <?}
                                elseif (strpos($vval["ORDER"]["DELIVERY_ID"], ":") !== false){?>
                                    <?$arId = explode(":", $vval["ORDER"]["DELIVERY_ID"]);?>
                                    <?=GetMessage("P_DELIVERY")?> <strong><?=$arResult["INFO"]["DELIVERY_HANDLERS"][$arId[0]]["NAME"]." (".$arResult["INFO"]["DELIVERY_HANDLERS"][$arId[0]]["PROFILES"][$arId[1]]["TITLE"].")";?></strong><br/>
                                <?}?>

                                <?if ($vval["ORDER"]["CANCELED"] == "Y"){?>
                                    <strong><?=GetMessage("SPOL_CANCELLED");?></strong>
                                <?}?>
                            </p>
                            <p class="box__left_title2">Состав заказа:</p>
                            <p class="box__left_easy">
                                <?foreach ($vval["BASKET_ITEMS"] as $vvval){
                                    $measure = (isset($vvval["MEASURE_TEXT"])) ? $vvval["MEASURE_TEXT"] :GetMessage("STPOL_SHT");?>
                                    <?if (strlen($vvval["DETAIL_PAGE_URL"]) > 0)
                                        echo "<a href=\"".$vvval["DETAIL_PAGE_URL"]."\">";
                                    echo $vvval["NAME"];
                                    if (strlen($vvval["DETAIL_PAGE_URL"]) > 0)
                                        echo "</a>";
                                    ?>
                                    (<?= $vvval["QUANTITY"] ?> <?=$measure?>)
                                    <?if( $vvval['SIZE'] ){?>
                                        <div>Размер: <?=$vvval['SIZE'];?></div><br/>
                                    <?}?>
                                <?}?>
                            </p>
                        </div>
                        <div class="order_status__box__right">
                            <?if ($vval["ORDER"]["CANCELED"] == "Y"){?>
                                <div class="box__right__statusname cancelled">
                                    Отменён
                                </div>
                            <?}
                            else{?>
                                <div class="box__right__statusname">
                                    <?=$arResult["INFO"]["STATUS"][$vval["ORDER"]["STATUS_ID"]]["NAME"]?>
                                </div>
                            <?}?>

                            <div class="box__right__links">
                                <a class="box__right__link" title="<?= GetMessage("STPOL_REORDER") ?>" href="<?=$vval["ORDER"]["URL_TO_COPY"]?>"><span class="repeat"></span>Повторить заказ</a><br/>
                                <?if ($vval["ORDER"]["CAN_CANCEL"] == "Y"):?>
                                    <a class="box__right__link" title="<?= GetMessage("STPOL_CANCEL") ?>" href="<?=$vval["ORDER"]["URL_TO_CANCEL"]?>"><span class="cancel"></span>Отменить заказ</a>
                                <?endif;?>
                            </div>
                            <a href="<?=$vval["ORDER"]["URL_TO_DETAIL"]?>" class="box__right__more">Подробнее<span></span></a>  
                            <?/*if($USER->isAdmin()):?>
                                  <?echo "<pre>"; print_r($arResult["ORDER_PROPS"]); echo "</pre>";?>
                            
                            <?$Sum = CSalePaySystemAction::GetParamValue("SHOULD_PAY");
$ShopID = CSalePaySystemAction::GetParamValue("SHOP_ID");
$scid = CSalePaySystemAction::GetParamValue("SCID");
$customerNumber = CSalePaySystemAction::GetParamValue("ORDER_ID");
$orderDate = CSalePaySystemAction::GetParamValue("ORDER_DATE");
$orderNumber = CSalePaySystemAction::GetParamValue("ORDER_ID");
$paymentType = CSalePaySystemAction::GetParamValue("PAYMENT_VALUE");
$fio = "";
if( CModule::IncludeModule('sale') ){
    if( $arOrder = CSaleOrder::GetByID($orderNumber) ){
        $Sum = $arOrder['PRICE'] - $arOrder['PRICE_DELIVERY'];
    }
}

$Sum = number_format($Sum, 2, ',', '');

?>
<?foreach($arResult["ORDER_PROPS"] as $prop):?>
<?php if ($prop['ORDER_PROPS_ID'] == 1) {
    $fio = $prop['VALUE'];
}
?>
<?endforeach?>
                            
                                 <form name="ShopForm" action="https://money.yandex.ru/eshop.xml" method="post">
                                <font class="tablebodytext">
<input name="ShopID" value="<?=$ShopID?>" >
<input name="scid" value="<?=$scid?>" >
<input name="customerNumber" value="<?=$customerNumber?>" >
<input name="orderNumber" value="<?=$orderNumber?>:<?=$fio?>" >
<input name="Sum" value="<?=$Sum?>" >
<input name="paymentType" value="<?=$paymentType?>" >
<input name="cms_name" value="1C-Bitrix" >
<input name="fio" value="<?=$fio?>" >

<!-- <br /> -->
<!-- Детали заказа:<br /> -->
<!-- <input name="OrderDetails" value="заказ №<?=$orderNumber?> (<?=$orderDate?>)" > -->
<br />
<input name="BuyButton" value="Оплатить" type="submit">

</font><p><font class="tablebodytext">
</form>
                            
                            
                            <?endif;*/?>
                        </div>
                        
                        <?
                if($vval["ORDER"]["PAYED"] != "Y" && $vval["ORDER"]["CANCELED"] != "Y"):?>
                <? //finding path_to_action
                $dbPaySysAction = CSalePaySystemAction::GetList(
                    array(),
                    array(
                            "PAY_SYSTEM_ID" => $vval["ORDER"]["PAY_SYSTEM_ID"],
                            "PERSON_TYPE_ID" => $vval["ORDER"]["PERSON_TYPE_ID"]
                        ),
                    false,
                    false,
                    array("NAME", "ACTION_FILE", "NEW_WINDOW", "PARAMS", "ENCODING")
                );

                if ($arPaySysAction = $dbPaySysAction->Fetch())
                {
                     if (strlen($arPaySysAction["ACTION_FILE"]))
                    {
                        $pathToAction = $_SERVER["DOCUMENT_ROOT"].$arPaySysAction["ACTION_FILE"];
                        $pathToAction = str_replace("\\", "/", $pathToAction);
                        while (substr($pathToAction, strlen($pathToAction) - 1, 1) == "/")
                            $pathToAction = substr($pathToAction, 0, strlen($pathToAction) - 1);
                        if (file_exists($pathToAction))
                        {
                            if (is_dir($pathToAction) && file_exists($pathToAction."/payment.php"))
                                $pathToAction .= "/payment.php";
                        }
                        if (strlen($arPaySysAction["ENCODING"]))
                        {
                            define("BX_SALE_ENCODING", $arPaySysAction["ENCODING"]);
                            AddEventHandler("main", "OnEndBufferContent", array($this, "changeBodyEncoding"));
                        }
                    }
                } 
                ?>
                <div class="custom-buy-button">
                <?include(SITE_DIR.$pathToAction);
                 ?></div> <?
                endif;?>
                    </div>
                </div>
            <?}?>
        <?}?>

		<?/*foreach($arResult["ORDER_BY_STATUS"] as $key => $group):?>
			<?foreach($group as $k => $order):?>
				<?if(!$k):?>
					<div class="bx_my_order_status_desc">

						<h2><?=GetMessage("SPOL_STATUS")?> "<?=$arResult["INFO"]["STATUS"][$key]["NAME"] ?>"</h2>
						<div class="bx_mos_desc"><?=$arResult["INFO"]["STATUS"][$key]["DESCRIPTION"] ?></div>

					</div>

				<?endif?>

				<div class="bx_my_order">

					<table class="bx_my_order_table">
						<thead>
							<tr>
								<td>
									<?=GetMessage('SPOL_ORDER')?> <?=GetMessage('SPOL_NUM_SIGN')?><?=$order["ORDER"]["ACCOUNT_NUMBER"]?>
									<?if(strlen($order["ORDER"]["DATE_INSERT_FORMATED"])):?>
										<?=GetMessage('SPOL_FROM')?> <?=$order["ORDER"]["DATE_INSERT_FORMATED"];?>
									<?endif?>
								</td>
								<td style="text-align: right;">
									<a href="<?=$order["ORDER"]["URL_TO_DETAIL"]?>"><?=GetMessage('SPOL_ORDER_DETAIL')?></a>
								</td>
							</tr>
						</thead>
						<tbody>
							<tr>
								<td>
									<strong><?=GetMessage('SPOL_PAY_SUM')?>:</strong> <?=$order["ORDER"]["FORMATED_PRICE"]?> <br />

									<strong><?=GetMessage('SPOL_PAYED')?>:</strong> <?=GetMessage('SPOL_'.($order["ORDER"]["PAYED"] == "Y" ? 'YES' : 'NO'))?> <br />

									<? // PAY SYSTEM ?>
									<?if(intval($order["ORDER"]["PAY_SYSTEM_ID"])):?>
										<strong><?=GetMessage('SPOL_PAYSYSTEM')?>:</strong> <?=$arResult["INFO"]["PAY_SYSTEM"][$order["ORDER"]["PAY_SYSTEM_ID"]]["NAME"]?> <br />
									<?endif?>

									<? // DELIVERY SYSTEM ?>
									<?if($order['HAS_DELIVERY']):?>

										<strong><?=GetMessage('SPOL_DELIVERY')?>:</strong>

										<?if(intval($order["ORDER"]["DELIVERY_ID"])):?>

											<?=$arResult["INFO"]["DELIVERY"][$order["ORDER"]["DELIVERY_ID"]]["NAME"]?> <br />

										<?elseif(strpos($order["ORDER"]["DELIVERY_ID"], ":") !== false):?>

											<?$arId = explode(":", $order["ORDER"]["DELIVERY_ID"])?>
											<?=$arResult["INFO"]["DELIVERY_HANDLERS"][$arId[0]]["NAME"]?> (<?=$arResult["INFO"]["DELIVERY_HANDLERS"][$arId[0]]["PROFILES"][$arId[1]]["TITLE"]?>) <br />

										<?endif?>

									<?endif?>

									<strong><?=GetMessage('SPOL_BASKET')?>:</strong>
									<ul class="bx_item_list">

										<?foreach ($order["BASKET_ITEMS"] as $item):?>

											<li>
												<?if(strlen($item["DETAIL_PAGE_URL"])):?>
													<a href="<?=$item["DETAIL_PAGE_URL"]?>" target="_blank">
												<?endif?>
													<?=$item['NAME']?>
												<?if(strlen($item["DETAIL_PAGE_URL"])):?>
													</a>
												<?endif?>
												<nobr>&nbsp;&mdash; <?=$item['QUANTITY']?> <?=(isset($item["MEASURE_NAME"]) ? $item["MEASURE_NAME"] : GetMessage('SPOL_SHT'))?></nobr>
											</li>

										<?endforeach?>

									</ul>

								</td>
								<td>
									<?=$order["ORDER"]["DATE_STATUS_FORMATED"];?>


									<?if($order["ORDER"]["CANCELED"] != "Y"):?>
										<a href="<?=$order["ORDER"]["URL_TO_CANCEL"]?>" style="min-width:140px"class="bx_big bx_bt_button_type_2 bx_cart bx_order_action"><?=GetMessage('SPOL_CANCEL_ORDER')?></a>
									<?endif?>

									<a href="<?=$order["ORDER"]["URL_TO_COPY"]?>" style="min-width:140px"class="bx_big bx_bt_button_type_2 bx_cart bx_order_action"><?=GetMessage('SPOL_REPEAT_ORDER')?></a>
								</td>
							</tr>
						</tbody>
					</table>
				</div>
			<?endforeach?>
		<?endforeach*/?>

	<?else:?>
		<?=GetMessage('SPOL_NO_ORDERS')?>
	<?endif?>

    <?if(strlen($arResult['NAV_STRING'])):?>
        <?=$arResult['NAV_STRING']?>
    <?endif?>

<?endif?>

