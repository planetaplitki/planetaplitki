<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>

<?if ($arResult["isFormErrors"] == "Y"):?>
  <script>
	    Recaptchafree.reset();
    </script>
<?=$arResult["FORM_ERRORS_TEXT"];?><?endif;?>

<?//reload page after adding form
if($_REQUEST['formresult'] == 'addok' && $arResult['arForm']['ID'] == $_REQUEST['WEB_FORM_ID']){?>
    <script>
        setTimeout(function() {
                location.reload();
            }, 3000
        );
    </script>
<?}?>

<?=$arResult["FORM_NOTE"]?>

<?if ($arResult["isFormNote"] != "Y"){?>

<?=$arResult["FORM_HEADER"]?>

    <?foreach ($arResult["QUESTIONS"] as $FIELD_SID => $arQuestion){?>

        <?if (is_array($arResult["FORM_ERRORS"]) && array_key_exists($FIELD_SID, $arResult['FORM_ERRORS'])):?>
            <span class="error-fld" title="<?=$arResult["FORM_ERRORS"][$FIELD_SID]?>"></span>
        <?endif;?>

        <? # вопрос
        $question = $arResult['arQuestions'][$FIELD_SID];
        $fieldType = $arQuestion['STRUCTURE']['0']['FIELD_TYPE'];
        $id = $arQuestion['STRUCTURE']['0']['ID'];

        if(
            ( $fieldType == 'text' || $fieldType == 'email' ) &&
            $question['ID'] != 13 &&
            $question['ID'] != 14 &&
            $question['ID'] != 17
        ){?>

            <div class="row">
                <div class="col-sm-3 modal-text text-right"><?=$arQuestion["CAPTION"]?> <span><?if ($arQuestion["REQUIRED"] == "Y"):?><?=$arResult["REQUIRED_SIGN"];?><?endif;?></span></div>
                <div class="col-sm-7"><input type="text" class="filters" name="form_<?=$fieldType?>_<?=$id?>" id="ModalInput<?=$id?>" value="<?=htmlspecialchars($_REQUEST['form_text_'.$id]);?>"></div>
            </div>
        <?}
        elseif( $question['ID'] == 17 ){?>
            <div class="row">
                <div class="col-sm-3 modal-text text-right"><?=$arQuestion["CAPTION"]?> <span><?if ($arQuestion["REQUIRED"] == "Y"):?><?=$arResult["REQUIRED_SIGN"];?><?endif;?></span></div>
                <div class="col-sm-7"><textarea name="form_textarea_<?=$id?>" class="filters textarea-fleld"><?=htmlspecialchars($_REQUEST['form_textarea_'.$id]);?></textarea></div>
            </div>
        <?}
        elseif(
            $question['ID'] == 13 ||
            $question['ID'] == 14
        ){?>

            <?
            #pRU($arQuestion, 'all');
            ?>

            <div class="row">
                <div class="col-sm-3 modal-text text-right"><?=$arQuestion["CAPTION"]?> <span><?if ($arQuestion["REQUIRED"] == "Y"):?><?=$arResult["REQUIRED_SIGN"];?><?endif;?></span></div>
                <div class="col-sm-7 text-left">
                    <select class="filters select" id="ModalInput<?=$question['ID']?>" name="form_dropdown_<?=$FIELD_SID?>">
                        <? $select = $arResult['arDropDown'][$FIELD_SID];
                        foreach( $select['reference'] as $k => $variant ){?>
                            <option <?=$_REQUEST['form_dropdown_'.$FIELD_SID] == $select['reference_id'][$k] ? 'selected' : ''; ?> value="<?=$select['reference_id'][$k]?>"><?=$variant?></option>
                        <?}?>
                    </select>
                </div>
            </div>
        <?}?>
    <?}?>
    
        <div class="row">
        <div class="col-sm-7 col-sm-offset-3">
         
         <?
$capCode = $GLOBALS["APPLICATION"]->CaptchaGetCode();
?>
<input type="hidden" name="captcha_sid" value="<?= htmlspecialchars($capCode)?>">
<img align="left" src="/bitrix/tools/captcha.php?captcha_sid=<?= htmlspecialchars($capCode)?>" width="" height="40" alt="CAPTCHA" style="display: none;">
<input type="hidden" name="captcha_word" size="20" maxlength="20" value="">

<div class="g-recaptcha" data-sitekey="6LfWlREUAAAAANAT36Nqn9Qc8l6a5-kiLlpXDnyq"></div>
        </div>
    </div>

    <div class="row">
        <div class="col-sm-7 col-sm-offset-3">
            <input type="submit" name="web_form_submit" class="btn modal-buttom-form" value="Отправить">
        </div>
    </div>
    
    <?
	    function CaptchaCheckCode($captcha_word, $captcha_sid)
{
    include_once($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/classes/general/captcha.php");

    $cpt = new CCaptcha();
    return $cpt->CheckCode($captcha_word, $captcha_sid);
}

/*
extract($_POST, EXTR_SKIP);
if (!CaptchaCheckCode($captcha_word, $captcha_sid) && ($ID == 0)) {
ShowMessage('Введен неправильный код');
}
*/
	    
    ?>

    <?=$arResult["FORM_FOOTER"]?>
<?}?>

<?
#pRU($arResult, 'all');
?>
