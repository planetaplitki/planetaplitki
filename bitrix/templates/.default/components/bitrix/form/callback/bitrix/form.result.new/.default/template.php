<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>

<?if ($arResult["isFormErrors"] == "Y"):?> 
    <script>
        Recaptchafree.reset();
    </script>
<?=$arResult["FORM_ERRORS_TEXT"];?><?endif;?>

<?//reload page after adding form   
if($_REQUEST['formresult'] == 'addok' && $arResult['arForm']['ID'] == $_REQUEST['WEB_FORM_ID']){?>
    <script>
        Recaptchafree.reset();

        setTimeout(function() {
                location.reload();
            }, 3000
        );
        
    </script>
<?}?>

<?=$arResult["FORM_NOTE"]?>

<?if ($arResult["isFormNote"] != "Y"){?>

<?=$arResult["FORM_HEADER"]?>
<?//if($USER->isAdmin()): echo "<pre>"; print_r($arResult["QUESTIONS"]); echo "</pre>"; endif;?>
    <?
    # заказать звонок
    ob_start();
    foreach ($arResult["QUESTIONS"] as $FIELD_SID => $arQuestion){
        $question = $arResult['arQuestions'][$FIELD_SID];
        $idSite = $arQuestion['STRUCTURE'][0]['ID'];
        $idAdminka = $arQuestion['STRUCTURE'][0]['FIELD_ID'];
        $type = $arQuestion['STRUCTURE'][0]['FIELD_TYPE'];

        if( $idAdminka == 11 ){?>
            <div class="col-sm-2 text-left">
                <select class="filters select" id="ModalInput<?=$idSite?>" name="form_dropdown_<?=$FIELD_SID?>">
                    <? $select = $arResult['arDropDown'][$FIELD_SID];
                    foreach( $select['reference'] as $k => $variant ){?>
                        <option <?=$_REQUEST['form_dropdown_'.$FIELD_SID] == $select['reference_id'][$k] ? 'selected' : ''; ?> value="<?=$select['reference_id'][$k]?>"><?=$variant?></option>
                    <?}?>
                </select>
            </div>
        <?}
        elseif( $idAdminka == 12 ){?>
            <div class="col-sm-5">
                <div class="input-group date">
                    <input type="text" name="form_text_<?=$idSite?>" value="<?=htmlspecialchars($_REQUEST['form_text_'.$idSite]);?>" class="filters clear"><span class="input-group-addon"><i class="icon-cal"></i></span>
                </div>
            </div>
        <?}?>
    <?}
    $content = ob_get_contents();
    ob_end_clean();
    ?>
    
    <?foreach ($arResult["QUESTIONS"] as $FIELD_SID => $arQuestion){?>
        <?
        $question = $arResult['arQuestions'][$FIELD_SID];
        $idSite = $arQuestion['STRUCTURE'][0]['ID'];
        $idAdminka = $arQuestion['STRUCTURE'][0]['FIELD_ID'];
        $type = $arQuestion['STRUCTURE'][0]['FIELD_TYPE'];

        if( $idAdminka == 31 || $idAdminka == 37 ){?>
                <?/*if($USER->IsAdmin()):?>
                
                    <div class="row">
                        <div class="col-sm-3 modal-text text-right"><?=$arQuestion["CAPTION"]?> <span><?if ($arQuestion["REQUIRED"] == "Y"):?><?=$arResult["REQUIRED_SIGN"];?><?endif;?></span></div>
                    <div class="col-sm-7">
                        <?if($idSite == 5):?>
                            <label class="my"></label><input type="text" readonly class="filters" name="form_text_<?=$idSite?>" id="ModalInput<?=$idSite?>" value="<?=$arParams['UF_ELEMENT_NAME']?>">
                        <?else:?>
                            <input type="text" readonly class="filters" name="form_text_<?=$idSite?>" id="ModalInput<?=$idSite?>" value="<?=$arParams['UF_ELEMENT_NAME']?>">
                        <?endif;?>
                    </div>
                    </div>
                <?else:*/?>
                    <div class="row">
                        <div class="col-sm-3 modal-text text-right"><?=$arQuestion["CAPTION"]?> <span><?if ($arQuestion["REQUIRED"] == "Y"):?><?=$arResult["REQUIRED_SIGN"];?><?endif;?></span></div>
                    <div class="col-sm-7">
                        <input type="text" readonly class="filters" name="form_text_<?=$idSite?>" id="ModalInput<?=$idSite?>" value="<?=$arParams['UF_ELEMENT_NAME']?>">
                    </div>
                    </div>
                <?//endif;?>
        <?}
        elseif( $type == 'textarea' ){?>
            <div class="row">
                <div class="col-sm-3 modal-text text-right"><?=$arQuestion["CAPTION"]?> <span><?if ($arQuestion["REQUIRED"] == "Y"):?><?=$arResult["REQUIRED_SIGN"];?><?endif;?></span></div>
                <div class="col-sm-7"><textarea name="form_<?=$type?>_<?=$idSite?>" class="filters textarea-fleld"><?=htmlspecialchars($_REQUEST['form_'.$type.'_'.$idSite]);?></textarea></div>
            </div>
        <?}
        elseif(
            ($type == 'text' || $type == 'email') &&
            $idAdminka != 11 &&
            $idAdminka != 12
        ){?>
           <div class="row">
                <div class="col-sm-3 modal-text text-right"><?=$arQuestion["CAPTION"]?> <span><?if ($arQuestion["REQUIRED"] == "Y"):?><?=$arResult["REQUIRED_SIGN"];?><?endif;?></span></div>
                <div class="col-sm-7">
                <?if($idSite == 12 || $idSite == 71 || $idSite == 70):?>
                    <label for="ModalInput<?=$idSite?>" class="my_before_after_number"></label><input type="<?=$type == 'email' ? 'text' : $type;?>" class="filters" name="form_<?=$type;?>_<?=$idSite?>" id="ModalInput<?=$idSite?>" value="<?=htmlspecialchars($_REQUEST['form_'.$type.'_'.$idSite]);?>">
                <?else:?>
                    <input type="<?=$type == 'email' ? 'text' : $type;?>" class="filters" name="form_<?=$type;?>_<?=$idSite?>" id="ModalInput<?=$idSite?>" value="<?=htmlspecialchars($_REQUEST['form_'.$type.'_'.$idSite]);?>">
                <?endif;?>
                </div>
                </div> 
        <?}
        elseif($idAdminka == 11){?>
            <div class="row">
                <div class="col-sm-3 modal-text text-right">Удобное время <span>*</span></div>
                <?=$content;?>
            </div>
        <?}?>
    <?}?>
    <input type="hidden" name="hooorai" id="hooorai" />
    
            <div class="row">
        <div class="col-sm-7 col-sm-offset-3">
         
         <?
$capCode = $GLOBALS["APPLICATION"]->CaptchaGetCode();
?>
<input type="hidden" name="captcha_sid" value="<?= htmlspecialchars($capCode)?>">
<img align="left" src="/bitrix/tools/captcha.php?captcha_sid=<?= htmlspecialchars($capCode)?>" width="" height="40" alt="CAPTCHA" style="display: none;">
<input type="hidden" name="captcha_word" size="20" maxlength="20" value="">

<div class="g-recaptcha" data-sitekey="6LfWlREUAAAAANAT36Nqn9Qc8l6a5-kiLlpXDnyq"></div>
        </div>
    </div>
    
    <? 
    //Get web form id for 'Yandex Metrics reachGoal'
    $a_web_form_id;
    if($arParams["WEB_FORM_ID"] == 3) {
        $a_web_form_id = "callback";    
    } elseif($arParams["WEB_FORM_ID"] == 7) {
        $a_web_form_id = "question-tile";    
    } elseif($arParams["WEB_FORM_ID"] == 8) {
        $a_web_form_id = "question-collection";    
    }
    ?>
     
    <div class="row">
        <div class="col-sm-7 col-sm-offset-3"> 
       <? if(!in_array('9',$USER-> GetUserGroupArray())):?>
                            <noindex>
                                <div class="col-sm-12 label-text2 confirm-block">
                                    <input type="checkbox" id="<?=$idAdminka?>" required="" checked="checked" name="CONFIRM_POLITICS" value="y" <?=$confirmed == 'y' ? "checked" : "" ?> />
                                    <label for="<?=$idAdminka?>"><span></span>Настоящим подтверждаю, что я ознакомлен и согласен с условиями политики конфиденциальности<i>* </i> </label><span class="confirm-trigger" data-toggle="modal" data-target="#Modal3">Узнать больше</span>
                                </div>
                            </noindex>
                        <?endif; ?>
           
            <input type="submit" onclick="yaCounter14449306.reachGoal('<?=$a_web_form_id?>'); return true;" name="web_form_submit" class="btn modal-buttom-form" value="Отправить">
        </div>
    </div>

    <?=$arResult["FORM_FOOTER"]?>
<?}?>
       