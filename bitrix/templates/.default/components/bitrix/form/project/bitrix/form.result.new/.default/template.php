<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>

<?if ($arResult["isFormErrors"] == "Y"):?><?=$arResult["FORM_ERRORS_TEXT"];?><?endif;?>

<?//reload page after adding form
if($_REQUEST['formresult'] == 'addok' && $arResult['arForm']['ID'] == $_REQUEST['WEB_FORM_ID']){?>
    <script>
        setTimeout(function() {
                location.reload();
            }, 3000
        );
    </script>
<?}?>

<?=$arResult["FORM_NOTE"]?>

<?if ($arResult["isFormNote"] != "Y"){?>

    <?=$arResult["FORM_HEADER"]?>

    <?foreach ($arResult["QUESTIONS"] as $FIELD_SID => $arQuestion){?>

        <?if (is_array($arResult["FORM_ERRORS"]) && array_key_exists($FIELD_SID, $arResult['FORM_ERRORS'])):?>
            <span class="error-fld" title="<?=$arResult["FORM_ERRORS"][$FIELD_SID]?>"></span>
        <?endif;?>

        <? # вопрос
        $question = $arResult['arQuestions'][$FIELD_SID];
        $id = $arQuestion['STRUCTURE'][0]['ID'];

        if(
            $question['FIELD_TYPE'] == 'text' &&
            $arQuestion['STRUCTURE'][0]['FIELD_TYPE'] != 'email' &&
            $arQuestion['STRUCTURE'][0]['FIELD_TYPE'] != 'textarea'
        ){?>
            <div class="row">
                <div class="col-sm-3 modal-text text-right"><?=$arQuestion["CAPTION"]?> <span><?if ($arQuestion["REQUIRED"] == "Y"):?><?=$arResult["REQUIRED_SIGN"];?><?endif;?></span></div>
                <div class="col-sm-7"><input type="text" class="filters" name="form_text_<?=$id;?>" id="ModalInput<?=$id;?>" value="<?=htmlspecialchars($_REQUEST['form_text_'.$id]);?>"></div>
            </div>
        <?}
        elseif( $arQuestion['STRUCTURE'][0]['FIELD_TYPE'] == 'email' ){?>
            <div class="row">
                <div class="col-sm-3 modal-text text-right"><?=$arQuestion["CAPTION"]?> <span><?if ($arQuestion["REQUIRED"] == "Y"):?><?=$arResult["REQUIRED_SIGN"];?><?endif;?></span></div>
                <div class="col-sm-7"><input type="text" class="filters" name="form_email_<?=$id;?>" id="ModalInput<?=$id;?>" value="<?=htmlspecialchars($_REQUEST['form_email_'.$id]);?>"></div>
            </div>
        <?}
        elseif( $arQuestion['STRUCTURE'][0]['FIELD_TYPE'] == 'textarea' ){?>
            <div class="row">
                <div class="col-sm-3 modal-text text-right"><?=$arQuestion["CAPTION"]?> <span><?if ($arQuestion["REQUIRED"] == "Y"):?><?=$arResult["REQUIRED_SIGN"];?><?endif;?></span></div>
                <div class="col-sm-7"><textarea name="form_textarea_<?=$id;?>" class="filters textarea-fleld"><?=htmlspecialchars($_REQUEST['form_textarea_'.$id]);?></textarea></div>
            </div>
        <?}?>
    <?}?>

    <div class="row">
        <div class="col-sm-7 col-sm-offset-3">
            <input type="submit" name="web_form_submit" class="btn modal-buttom-form" value="Отправить заказ">
        </div>
    </div>

    <?=$arResult["FORM_FOOTER"]?>
<?}?>