<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>

<?if ($arResult["isFormErrors"] == "Y"):?>
    <script>
        Recaptchafree.reset();
    </script>
    <?=$arResult["FORM_ERRORS_TEXT"];?>
<?endif;?>

<?//reload page after adding form
if($_REQUEST['formresult'] == 'addok' && $arResult['arForm']['ID'] == $_REQUEST['WEB_FORM_ID']){?>
    <script>
        Recaptchafree.reset();
    
        setTimeout(function() {
                location.reload();
            }, 3000
        );
    </script>
<?}?>

<?=$arResult["FORM_NOTE"]?>

<?if ($arResult["isFormNote"] != "Y"){?>

<?=$arResult["FORM_HEADER"]?>

    <?foreach ($arResult["QUESTIONS"] as $FIELD_SID => $arQuestion){?>

        <?if (is_array($arResult["FORM_ERRORS"]) && array_key_exists($FIELD_SID, $arResult['FORM_ERRORS'])):?>
            <span class="error-fld" title="<?=$arResult["FORM_ERRORS"][$FIELD_SID]?>"></span>
        <?endif;?>

        <? # вопрос
        $arResult['arQuestions']['SIMPLE_QUESTION_640']['FIELD_TYPE'] = 'text';
        
        $question = $arResult['arQuestions'][$FIELD_SID];
        
        if(
            $question['FIELD_TYPE'] == 'text' &&
            $question['ID'] != 6 &&
            $question['ID'] != 7 &&
            $question['ID'] != 8
        ){?>
                <div class="row">
                <div class="col-sm-3 modal-text text-right"><?=$arQuestion["CAPTION"]?> <span><?if ($arQuestion["REQUIRED"] == "Y"):?><?=$arResult["REQUIRED_SIGN"];?><?endif;?></span></div>
                <div class="col-sm-7">
                <?if($question['ID'] == 5):?>
                <label for="ModalInput<?=$question['ID']?>" class="my_before_after_number"></label><input type="text" class="filters" name="form_text_<?=$question['ID']?>" id="ModalInput<?=$question['ID']?>" value="<?=htmlspecialchars($_REQUEST['form_text_'.$question['ID']]);?>"></div>
                <?else:?>
                <input type="text" class="filters" name="form_text_<?=$question['ID']?>" id="ModalInput<?=$question['ID']?>" value="<?=htmlspecialchars($_REQUEST['form_text_'.$question['ID']]);?>"></div>
                <?endif;?>
                </div>
        <?}
        elseif($question['ID'] == 6){?>
            <div class="row">
                <div class="col-sm-3 modal-text text-right">Удобное время <span>*</span></div>
                <div class="col-sm-2">
                    <select class="filters select" id="ModalInput<?=$question['ID']?>" name="form_dropdown_<?=$FIELD_SID?>">
                        <? $select = $arResult['arDropDown'][$FIELD_SID];
                        foreach( $select['reference'] as $k => $variant ){?>
                            <option <?=$_REQUEST['form_dropdown_'.$FIELD_SID] == $select['reference_id'][$k] ? 'selected' : ''; ?> value="<?=$select['reference_id'][$k]?>"><?=$variant?></option>
                        <?}?>
                    </select>
                </div>
                <div class="col-sm-5">
                    <div class="input-group date">
                        <input type="text" name="form_text_7" value="<?=htmlspecialchars($_REQUEST['form_text_7']);?>" class="filters clear"><span class="input-group-addon"><i class="icon-cal"></i></span>
                    </div>
                </div>
            </div>
        <?}
        elseif( $question['ID'] == 8 ){?>

            <div class="row">
                <div class="col-sm-3 modal-text text-right"><?=$arQuestion["CAPTION"]?> <span><?if ($arQuestion["REQUIRED"] == "Y"):?><?=$arResult["REQUIRED_SIGN"];?><?endif;?></span></div>
                <div class="col-sm-7">
                    <select class="filters select" id="ModalInput<?=$question['ID']?>" name="form_dropdown_<?=$FIELD_SID?>">
                        <? $select = $arResult['arDropDown'][$FIELD_SID];
                        foreach( $select['reference'] as $k => $variant ){?>
                            <option <?=$_REQUEST['form_dropdown_'.$FIELD_SID] == $select['reference_id'][$k] ? 'selected' : ''; ?> value="<?=$select['reference_id'][$k]?>"><?=$variant?></option>
                        <?}?>
                    </select>
                </div>
            </div>
        <?}?>
    <?}?>
    
    <div class="row">
        <div class="col-sm-7 col-sm-offset-3"> 
            <?
                $capCode = $GLOBALS["APPLICATION"]->CaptchaGetCode();
            ?>
                <input type="hidden" name="captcha_sid" value="<?= htmlspecialchars($capCode)?>">
                <img align="left" src="/bitrix/tools/captcha.php?captcha_sid=<?= htmlspecialchars($capCode)?>" width="" height="40" alt="CAPTCHA" style="display: none;">
                <input type="hidden" name="captcha_word" size="20" maxlength="20" value="">

                <div class="g-recaptcha" data-sitekey="6LfWlREUAAAAANAT36Nqn9Qc8l6a5-kiLlpXDnyq"></div>
            </div>
    </div>

    <div class="row">
        <div class="col-sm-7 col-sm-offset-3">
        <? if(!in_array('9',$USER-> GetUserGroupArray())):?>
                            <div class="col-sm-12 label-text2 confirm-block">
                                <input type="checkbox" required="" checked="checked" id="select_data4" name="CONFIRM_POLITICS" value="y" />
                                <label for="select_data4"><span></span>Настоящим подтверждаю, что я ознакомлен и согласен с условиями политики конфиденциальности<i>* </i> 
                                </label><span href="" class="confirm-trigger" data-toggle="modal" data-target="#Modal3">Узнать больше</span>
                            </div>
                           <?endif;?>
            <input type="submit" onclick="yaCounter14449306.reachGoal('consultation'); return true;" name="web_form_submit" class="btn modal-buttom-form" value="Записаться">
        </div>
    </div>

    <?=$arResult["FORM_FOOTER"]?>
<?}?>