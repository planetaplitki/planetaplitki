<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);
?>
<?if($USER->isAdmin()):
            //echo "<pre>"; print_r($arResult); echo "</pre>";
            endif;?>
            <?if ($USER->isAdmin()):?>
            <div class="box-position2">
                <div class="title">Варианты 3d раскладки коллекции</div>
                <div class="row obj25">
                
                <?foreach ($arResult["3D_OPTIONS"] as $item_custom):?>
                    <div class="col-xs-6 col-sm-3 col-md-3 usligi-pict">
                        <a href="<?=$item_custom['DETAIL_PAGE_URL']?>" style="max-width: 100%;max-height: 198px;" class="collections-catalog-placeholder">
                            <img src="<?=$item_custom["PICTURE"];?>"  style="max-width: 100%;max-height: 198px;" >
                        
                        </a>
                        
                        <div class="box_under">
                <a href="<?=$item['DETAIL_PAGE_URL']?>" class="link"> <?='Вариант раскладки'.$item_custom["NAME"];?></a>
                <p class="easy">
                    <?=$item_custom["PREVIEW_TEXT"];?>
                </p>
                
                        </div>
                    
                    </div>
                <?endforeach;?>
                
                </div>
            </div>
            
            <?endif;?>
