<?if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();

if ($USER->isAdmin()):
$arSelect = Array("ID", "IBLOCK_ID", "NAME", "PREVIEW_PICTURE", "PREVIEW_TEXT", "DETAIL_PAGE_URL");
$arFilter = Array("IBLOCK_ID"=>11, "PROPERTY_COLLECTION" => $arResult["SECTION"]["ID"], "ACTIVE_DATE"=>"Y", "ACTIVE"=>"Y");
$res = CIBlockElement::GetList(Array(), $arFilter, false, Array("nPageSize"=>50), $arSelect);
while($ob = $res->GetNextElement())
{
    $arResult["3D_OPTIONS"][] = $ob->GetFields();
    
}
foreach($arResult["3D_OPTIONS"] as &$item_custom) {
    $item_custom["PICTURE"] = CFile::GetPath($item_custom["PREVIEW_PICTURE"]);
}

//echo "<pre>"; print_r($arResult["3D_OPTIONS"]); echo "</pre>";
endif;?>