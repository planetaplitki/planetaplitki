<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);
?>

<?
$count = count($arResult['ITEMS']);
if( !$count ){
    return;
}?>


<div class="container hidden-xs">
    <div class="row">
        <div class="col-sm-12">
            <? $i = 1;
            foreach($arResult["ITEMS"] as $arItem){?>

            <?
            $this->AddEditAction($arItem['ID'], $arItem['EDIT_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_EDIT"));
            $this->AddDeleteAction($arItem['ID'], $arItem['DELETE_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_DELETE"), array("CONFIRM" => GetMessage('CT_BNL_ELEMENT_DELETE_CONFIRM')));
            ?>

            <?
            $link = $arItem['PROPERTIES']['LINK']['VALUE'];
            $class = $arItem['PROPERTIES']['CLASS']['VALUE'];
            ?>

            <?if($i != 1){?>
                <div class="pad"></div>
            <?}?>

            <a href="<?= $link ? $link : 'javascript:void(0);'; ?>" id="<?=$this->GetEditAreaId($arItem['ID']);?>">
                <div class="<?=$class?>">
                    <img
                        src="<?=$arItem["PREVIEW_PICTURE"]["SRC"]?>"
                        alt="<?=$arItem["PREVIEW_PICTURE"]["ALT"]?>"
                        title="<?=$arItem["PREVIEW_PICTURE"]["TITLE"]?>"
                        />
                    <span><?=$arItem['NAME']?></span>
                </div>
            </a>

            <?if( $i%4 == 0 ){?>
        </div>
    </div>
    <div class="row obj">
        <div class="col-sm-12">
            <?$i = 0;?>
            <?}?>
            <?
            $i++;
            $items[$arItem['PROPERTIES']['SORT']['VALUE']] = $arItem;
            }?>
        </div>
    </div>
</div>

<?ksort($items); ?>

<div class="container hidden-sm hidden-md hidden-lg">

    <div class="row">
        <div class="col-xs-12">
            <? $i = 1;
            foreach($items as $arItem){?>

            <?
            $link = $arItem['PROPERTIES']['LINK']['VALUE'];
            $class = $arItem['PROPERTIES']['CLASS']['VALUE'];
            ?>

            <?if($i != 1){?>
                <div class="pad"></div>
            <?}?>

            <a href="<?= $link ? $link : 'javascript:void(0);'; ?>">
                <div class="<?=$class?>">
                    <img
                        src="<?=$arItem["PREVIEW_PICTURE"]["SRC"]?>"
                        alt="<?=$arItem["PREVIEW_PICTURE"]["ALT"]?>"
                        title="<?=$arItem["PREVIEW_PICTURE"]["TITLE"]?>"
                        />
                    <span><?=$arItem['NAME']?></span>
                </div>
            </a>

            <?if( $i%3 == 0 ){?>
        </div>
    </div>
    <div class="row obj">
        <div class="col-sm-12">
            <?$i = 0;?>
            <?}?>
            <?
            $i++;
            }?>
        </div>
    </div>
</div>
