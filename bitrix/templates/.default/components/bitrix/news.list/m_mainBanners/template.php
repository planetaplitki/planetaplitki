<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);
?>

<? if (!count($arResult['ITEMS'])) {
    return;
} ?>

<div class="col-md-8">
    <div class="carousel-inner">
        <? $i = 0;
        foreach ($arResult["ITEMS"] as $arItem) {
            ?>

            <?
            $this->AddEditAction($arItem['ID'], $arItem['EDIT_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_EDIT"));
            $this->AddDeleteAction($arItem['ID'], $arItem['DELETE_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_DELETE"), array("CONFIRM" => GetMessage('CT_BNL_ELEMENT_DELETE_CONFIRM')));
            ?>

            <div class="item box-carousel<?= !$i ? ' active' : ''; ?>" id="<?= $this->GetEditAreaId($arItem['ID']); ?>">
                <? if (is_array($arItem["DETAIL_PICTURE"])) {
                    $img = i($arItem["DETAIL_PICTURE"]["ID"], 750, 360, BX_RESIZE_IMAGE_EXACT);
                } else {
                    $img = 'http://placehold.it/620x356';
                } ?>

                <a href="<?= $arItem['DETAIL_PAGE_URL'] ?>"><img
                        src="<?= $img ?>"
                        alt="<?= $arItem["PREVIEW_PICTURE"]["ALT"] ?>"
                        title="<?= $arItem["PREVIEW_PICTURE"]["TITLE"] ?>"
                    /></a>

                <? /*?>
                <div class="text-carousel">
                    <p class="title"><?=$arItem['NAME']?></p>
                    <p class="text"><?=$arItem['PREVIEW_TEXT']?></p>
                    <a href="<?=$arItem["DETAIL_PAGE_URL"]?>" class="btn buttom-banner3">ПОДРОБНЕЕ</a>
                </div>
                <?*/
                ?>
            </div>
            <?
            $i++;
        } ?>
    </div>
    <!-- End Carousel Inner -->
</div>
<div class="col-md-4">
    <ul class="nav nav-pills nav-justified hidden-sm hidden-xs">

        <? $i = 0;
        foreach ($arResult["ITEMS"] as $arItem) {
            ?>

            <?
            $this->AddEditAction($arItem['ID'], $arItem['EDIT_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_EDIT"));
            $this->AddDeleteAction($arItem['ID'], $arItem['DELETE_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_DELETE"), array("CONFIRM" => GetMessage('CT_BNL_ELEMENT_DELETE_CONFIRM')));
            ?>

            <li data-target="#banner1" data-slide-to="<?= $i; ?>" class="<?= !$i ? 'active' : ''; ?>">
                <div class="pill-inner">
                    <div class="text-banner"><?= $arItem['NAME'] ?></div>
                    <a class="btn buttom-banner" href="<?= $arItem["DETAIL_PAGE_URL"] ?>">ПОДРОБНЕЕ</a>
                </div>
            </li>
            <?
            $i++;
        } ?>
    </ul>
    <div class="box-centr">
        <a href="/all-shares/" class="btn buttom-banner2">ВСЕ АКЦИИ »</a>
        <? /*?><button type="button" class="btn buttom-banner2">ВСЕ АКЦИИ »</button><?*/ ?>
    </div>

</div>