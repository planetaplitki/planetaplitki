<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);
?>

<?if( !count($arResult['ITEMS']) ) return; ?>

<div class="container hidden-xs">
    <div class="row">
        <div class="col-sm-12">
            <div class="carousel-line">
                <div class="title">Дизайнерские решения</div>
            </div>
        </div>
    </div>
    <div class="row obj">
        <div class="col-sm-12">
            <?
            $count = count($arResult['ITEMS']);
            $i = 1;
            foreach($arResult["ITEMS"] as $arItem){?>

                <?
                $this->AddEditAction($arItem['ID'], $arItem['EDIT_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_EDIT"));
                $this->AddDeleteAction($arItem['ID'], $arItem['DELETE_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_DELETE"), array("CONFIRM" => GetMessage('CT_BNL_ELEMENT_DELETE_CONFIRM')));
                ?>

                <?
                $img = i($arItem['PREVIEW_PICTURE']['ID'], 365, 195, BX_RESIZE_IMAGE_EXACT);
                $link = $arItem['PROPERTIES']['LINK']['VALUE'];
                $link = $link ? $link : "javascript:void(0);";
                ?>

                <a href="<?=$link;?>" id="<?=$this->GetEditAreaId($arItem['ID']);?>">
                    <div class="box2">
                        <div class="photo">
                            <img src="<?=$img;?>">
                        </div>
                        <span><?=$arItem["NAME"]?></span>
                    </div>
                </a>

                <?if( $i != $count ){?>
                    <div class="pad"></div>
                <?}?>

                <?
                $i++;
                ?>

            <?}?>
        </div>
    </div>
</div>