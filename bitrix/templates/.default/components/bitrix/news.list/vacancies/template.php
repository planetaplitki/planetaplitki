<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);
?>

<div class="vakansii_page">

    <?
    $i = 0;
    foreach($arResult["ITEMS"] as $arItem){?>

        <?
        $this->AddEditAction($arItem['ID'], $arItem['EDIT_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_EDIT"));
        $this->AddDeleteAction($arItem['ID'], $arItem['DELETE_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_DELETE"), array("CONFIRM" => GetMessage('CT_BNL_ELEMENT_DELETE_CONFIRM')));
        ?>

        <div id="<?=$this->GetEditAreaId($arItem['ID']);?>">
            <p class="t1<?=$i ? ' obj50' : '';?>"><?=$arItem['NAME']?></p>
            <div class="easy">

                <b>Требования:</b><br>
                <?=$arItem['PROPERTIES']['TREBOVANIYA']['~VALUE']['TEXT']?>

                <b>Обязанности:</b><br>
                <?=$arItem['PROPERTIES']['OBYAZANNOSTI']['~VALUE']['TEXT']?>

                <b>Условия:</b><br>
                <?=$arItem['PROPERTIES']['USLOVIYA']['~VALUE']['TEXT']?>
            </div>
        </div>

    <?
        $i++;
    }?>
</div>

<?#pRU($arResult)?>

<?if($arParams["DISPLAY_BOTTOM_PAGER"]):?>
	<?=$arResult["NAV_STRING"]?>
<?endif;?>