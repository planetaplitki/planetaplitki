<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_before.php");

foreach($arResult["ITEMS"] as $key => $arItem){
    $img = CFile::ResizeImageGet($arItem['PREVIEW_PICTURE'], array('width'=>165, 'height'=>220), BX_RESIZE_IMAGE_PROPORTIONAl, true);
    $arResult["ITEMS"][$key]["PREVIEW_PICTURE"]["SRC"] = $img["src"];
}