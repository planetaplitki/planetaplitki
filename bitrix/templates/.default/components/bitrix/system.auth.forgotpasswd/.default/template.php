<?if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>
<?
ShowMessage($arParams["~AUTH_RESULT"]);
?>
<p><?=GetMessage("AUTH_FORGOT_PASSWORD_1")?></p>
<div class="serviceForm">
	<form name="bform" method="post" target="_top" action="<?=$arResult["AUTH_URL"]?>">
		<?if (strlen($arResult["BACKURL"]) > 0){?>
			<input type="hidden" name="backurl" value="<?=$arResult["BACKURL"]?>" />
		<?}?>
		<input type="hidden" name="AUTH_FORM" value="Y">
		<input type="hidden" name="TYPE" value="SEND_PWD">

        <div class="fld fld_req">
            <label for="registration_1_3">E-Mail</label>
            <input type="text" name="USER_EMAIL" id="registration_1_3">
        </div>

        <input type="submit" name="send_account_info" class="mybtnA" value="<?=GetMessage("AUTH_SEND")?>" />

		<?/*=GetMessage("AUTH_LOGIN")?><br>
		<input type="text" name="USER_LOGIN" maxlength="50" class="input_text_style" value="<?=$arResult["LAST_LOGIN"]?>" /><br><br><?*/?>

	</form>
	<script type="text/javascript">
	document.bform.USER_LOGIN.focus();
	</script>
</div>
<?/*?>
<a href="<?=$arResult["AUTH_AUTH_URL"]?>" onclick='var modalH = $("#login").height(); $("#login").css({"display":"block","margin-top":"-"+(parseInt(modalH)/2)+"px" }); return false;'><?=GetMessage("AUTH_AUTH")?></a>
<?*/?>