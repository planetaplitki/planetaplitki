<?if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>

<div class="serviceForm">
	
	<?ShowMessage($arParams["~AUTH_RESULT"]);?>
	<form method="post" action="<?=$arResult["AUTH_FORM"]?>" name="bform">
		<?if (strlen($arResult["BACKURL"]) > 0): ?>
		<input type="hidden" name="backurl" value="<?=$arResult["BACKURL"]?>" />
		<? endif ?>
		<input type="hidden" name="AUTH_FORM" value="Y">
		<input type="hidden" name="TYPE" value="CHANGE_PWD">


        <div class="fld fld_req">
            <label for="registration_1_1"><?=GetMessage("AUTH_LOGIN")?><sup>*</sup></label>
            <input type="text" name="USER_LOGIN" id="registration_1_1" maxlength="50" class="input_text_style" value="<?=$arResult["LAST_LOGIN"]?>" />
        </div>

        <div class="fld fld_req">
            <label for="registration_1_2"><?=GetMessage("AUTH_CHECKWORD")?><sup>*</sup></label>
            <input type="text" name="USER_CHECKWORD" id="registration_1_2" maxlength="50" class="input_text_style" value="<?=$arResult["USER_CHECKWORD"]?>" />
        </div>

        <div class="fld fld_req">
            <label for="registration_1_3">Пароль<sup>*</sup></label>
            <input type="password" name="USER_PASSWORD" id="registration_1_3" maxlength="50" class="input_text_style" value="<?=$arResult["USER_PASSWORD"]?>" />
        </div>

        <div class="fld fld_req">
            <label for="registration_1_3"><?=GetMessage("AUTH_NEW_PASSWORD_CONFIRM")?><sup>*</sup></label>
            <input type="password" name="USER_CONFIRM_PASSWORD" maxlength="50" id="registration_1_4" class="input_text_style" value="<?=$arResult["USER_CONFIRM_PASSWORD"]?>"  />
        </div>

        <input type="submit" class="mybtnA" name="change_pwd" value="<?=GetMessage("AUTH_CHANGE")?>" />

	</form>

	<script type="text/javascript">
	document.bform.USER_LOGIN.focus();
	</script>
</div>
<?/*?>
<br><a href="<?=$arResult["AUTH_AUTH_URL"]?>" onclick='var modalH = $("#login").height(); $("#login").css({"display":"block","margin-top":"-"+(parseInt(modalH)/2)+"px" }); return false;'><?=GetMessage("AUTH_AUTH")?></a>
<?*/?>