<?
/**
 * @global CMain $APPLICATION
 * @param array $arParams
 * @param array $arResult
 */
if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true)
	die();
?>

<?ShowError($arResult["strProfileError"]);?>
<?if ($arResult['DATA_SAVED'] == 'Y')
	ShowNote(GetMessage('PROFILE_DATA_SAVED'));
?>

<?
//pRU($arResult, 'all');
if( $arResult["arUser"]["NAME"] ){
	$name = $arResult["arUser"]["NAME"];
}
elseif( $arResult["arUser"]["LAST_NAME"] ){
	$name = $arResult["arUser"]["LAST_NAME"];
}

?>

<form method="post" class="regblock__tabs__content__form" name="form1" action="<?=$arResult["FORM_TARGET"]?>" enctype="multipart/form-data">
	<?=$arResult["BX_SESSION_CHECK"]?>
	<input type="hidden" name="lang" value="<?=LANG?>" />
	<input type="hidden" name="ID" value=<?=$arResult["ID"]?> />

	<div class="form-group">
		<label>ФИО<span>*</span></label>
		<input type="text" class="form-control regblock__form_control" name="NAME" value="<?=$name;?>" />
	</div>

	<div class="form-group">
		<label for="">E-mail <span>*</span></label>
		<input type="text" name="EMAIL" class="form-control regblock__form_control" value="<? echo $arResult["arUser"]["EMAIL"]?>" />
	</div>

	<div class="form-group">
		<label for="">Телефон <span>*</span></label>
		<label for="PERSONAL_MOBILE" class="my_before_after_personal_mobile"></label><input id="PERSONAL_MOBILE" type="text" name="PERSONAL_MOBILE" class="form-control regblock__form_control" value="<?=$arResult["arUser"]["PERSONAL_MOBILE"]?>" />
	</div>

	<div class="form-group">
		<label for="">Пароль <span>*</span></label>
		<input type="password" name="NEW_PASSWORD" class="form-control regblock__form_control" value="" autocomplete="off" />
	</div>

	<div class="form-group">
		<label for="">Повторите пароль <span>*</span></label>
		<input type="password" name="NEW_PASSWORD_CONFIRM" class="form-control regblock__form_control" value="" autocomplete="off" />
	</div>

	<input type="hidden" name="LOGIN" value="<?=rand(1, 100000);?>" />
	<br/>
	<button type="submit" class="btn regblock__form_btn">Сохранить
		<input type="hidden" name="save" value="<?=(($arResult["ID"]>0) ? GetMessage("MAIN_SAVE") : GetMessage("MAIN_ADD"))?>">
	</button>
</form>