<?
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
?>

<??>
<div class="myform">

    <?
    ShowMessage($arParams["~AUTH_RESULT"]);
    ShowMessage($arResult['ERROR_MESSAGE']);
    ?>

    <form name="form_auth" method="post" target="_top" action="<?=SITE_DIR?>auth/<?//=$arResult["AUTH_URL"]?>">

        <div class="modalbody clearfix">

            <input type="hidden" name="AUTH_FORM" value="Y" />
            <input type="hidden" name="TYPE" value="AUTH" />

            <?if (strlen($arResult["BACKURL"]) > 0):?>
                <input type="hidden" name="backurl" value="<?=$arResult["BACKURL"]?>" />
            <?endif?>
            <?foreach ($arResult["POST"] as $key => $value):?>
                <input type="hidden" name="<?=$key?>" value="<?=$value?>" />
            <?endforeach?>

            <div class="fld fld_req">
                <label for="order_1_1">Логин</label>
                <input id="order_1_1" type="text" name="USER_LOGIN" value="<?=$arResult["LAST_LOGIN"]?>" />
                <a class="forgot-link" href="/login/?forgot_password=yes">Забыли пароль</a>
            </div>

            <div class="fld fld_req">
                <label for="order_1_2">Пароль</label>
                <input id="order_1_2" type="password" name="USER_PASSWORD" />
                <a class="forgot-link" href="/login/?register=yes&backurl=%2Flogin%2F">Регистрация</a>
            </div>

            <?if($arResult["SECURE_AUTH"]):?>
                <span class="bx-auth-secure" id="bx_auth_secure" title="<?echo GetMessage("AUTH_SECURE_NOTE")?>" style="display:none">
						<div class="bx-auth-secure-icon"></div>
				</span>
                <noscript>
					<span class="bx-auth-secure" title="<?echo GetMessage("AUTH_NONSECURE_NOTE")?>">
						<div class="bx-auth-secure-icon bx-auth-secure-unlock"></div>
					</span>
                </noscript>
                <script type="text/javascript">
                    document.getElementById('bx_auth_secure').style.display = 'inline-block';
                </script>
            <?endif?>

            <input class="orange-btn" type="submit" name="Login" value="Войти"/>

        </div>
    </form>

    <div class="socials-login b-socialMenu clearfix">
        <h5>Войти с помощью социальных сетей</h5>

        <?if($arResult["AUTH_SERVICES"]):?>
            <?$APPLICATION->IncludeComponent("bitrix:socserv.auth.form", "",
                array(
                    "AUTH_SERVICES"=>$arResult["AUTH_SERVICES"],
                    "CURRENT_SERVICE"=>$arResult["CURRENT_SERVICE"],
                    "AUTH_URL"=>$arResult["AUTH_URL"],
                    "POST"=>$arResult["POST"],
                ),
                $component,
                array("HIDE_ICONS"=>"Y")
            );?>
        <?endif?>
    </div>

</div>

<??>


<?/*?>
<div class="modal hide fade" tabindex="-1" role="dialog" aria-hidden="true" id="loginModal">
	<?
	ShowMessage($arParams["~AUTH_RESULT"]);
	ShowMessage($arResult['ERROR_MESSAGE']);
	?>


	<div class="modal-header clearfix">
	    <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
	    
	    <h3 class="float-left">Авторизация</h3>

	    <?if($arResult["AUTH_SERVICES"]):?>
			<?$APPLICATION->IncludeComponent("bitrix:socserv.auth.form", "",
				array(
					"AUTH_SERVICES"=>$arResult["AUTH_SERVICES"],
					"CURRENT_SERVICE"=>$arResult["CURRENT_SERVICE"],
					"AUTH_URL"=>$arResult["AUTH_URL"],
					"POST"=>$arResult["POST"],
				),
				$component,
				array("HIDE_ICONS"=>"Y")
			);?>
		<?endif?>
	</div>
	
	<form name="form_auth" method="post" target="_top" action="<?=SITE_DIR?>auth/<?//=$arResult["AUTH_URL"]?>">
		
		<div class="modalbody clearfix">

			<input type="hidden" name="AUTH_FORM" value="Y" />
			<input type="hidden" name="TYPE" value="AUTH" />
			<?if (strlen($arResult["BACKURL"]) > 0):?>
			<input type="hidden" name="backurl" value="<?=$arResult["BACKURL"]?>" />
			<?endif?>
			<?foreach ($arResult["POST"] as $key => $value):?>
			<input type="hidden" name="<?=$key?>" value="<?=$value?>" />
			<?endforeach?>

	        <input class="float-left" type="text" name="USER_LOGIN" value="<?=$arResult["LAST_LOGIN"]?>" placeholder="Логин" />		
			<input class="float-right" type="password" name="USER_PASSWORD" placeholder="Пароль" />

	        <a class="forgot" href="/login/?forgot_password=yes">Забыли пароль?</a>

	        <?if($arResult["SECURE_AUTH"]):?>
				<span class="bx-auth-secure" id="bx_auth_secure" title="<?echo GetMessage("AUTH_SECURE_NOTE")?>" style="display:none">
						<div class="bx-auth-secure-icon"></div>
				</span>
				<noscript>
					<span class="bx-auth-secure" title="<?echo GetMessage("AUTH_NONSECURE_NOTE")?>">
						<div class="bx-auth-secure-icon bx-auth-secure-unlock"></div>
					</span>
				</noscript>
				<script type="text/javascript">
					document.getElementById('bx_auth_secure').style.display = 'inline-block';
				</script>
			<?endif?>

	    </div>

	    <div class="modal-footer clearfix">
	        <label class="float-left"><input type="checkbox" id="USER_REMEMBER" name="USER_REMEMBER" value="Y" checked/>Запомнить меня</label>
	        <a href="/login/?register=yes&backurl=%2Flogin%2F" class="to-reg blue-btn">Регистрация</a>	        
	        <input type="submit" name="Login" class="green-btn" value="Войти"  />
	    </div>

	</form>

	<script type="text/javascript">
	<?if (strlen($arResult["LAST_LOGIN"])>0):?>
	try{document.form_auth.USER_PASSWORD.focus();}catch(e){}
	<?else:?>
	try{document.form_auth.USER_LOGIN.focus();}catch(e){}
	<?endif?>
	</script>
</div>
<?*/?>