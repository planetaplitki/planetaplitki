<?
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
?>
<?
ShowMessage($arParams["~AUTH_RESULT"]);
ShowMessage($arResult['ERROR_MESSAGE']);
?>
<?if($arResult["AUTH_SERVICES"]):?>
<p class="tal"><strong><?echo GetMessage("AUTH_TITLE")?></strong></p>
<?endif?>
<?if($arResult["AUTH_SERVICES"]):?>
<?$APPLICATION->IncludeComponent("bitrix:socserv.auth.form", "flat",
	array(
		"AUTH_SERVICES"=>$arResult["AUTH_SERVICES"],
		"CURRENT_SERVICE"=>$arResult["CURRENT_SERVICE"],
		"AUTH_URL"=>$arResult["AUTH_URL"],
		"POST"=>$arResult["POST"],
		"SUFFIX" => "main",
	),
	$component,
	array("HIDE_ICONS"=>"Y")
);?>
<?endif?>
<br/>
<form name="form_auth" method="post" target="_top" action="<?=$arResult["AUTH_URL"]?>" class="data-form">
	<div class="serviceForm">
		<input type="hidden" name="AUTH_FORM" value="Y" />
		<input type="hidden" name="TYPE" value="AUTH" />
		<?if (strlen($arResult["BACKURL"]) > 0):?>
		<input type="hidden" name="backurl" value="<?=$arResult["BACKURL"]?>" />
		<?endif?>
		<?foreach ($arResult["POST"] as $key => $value):?>
		<input type="hidden" name="<?=$key?>" value="<?=$value?>" />
		<?endforeach?>

        <div class="fld fld_req">
            <label for="registration_1_1"><?=GetMessage("AUTH_LOGIN")?></label>
            <input class="input_text_style" type="text" id="registration_1_1" name="USER_LOGIN" value="<?=$arResult["LAST_LOGIN"]?>" />
        </div>

        <div class="fld fld_req">
            <label for="registration_1_2"><?=GetMessage("AUTH_PASSWORD")?></label>
            <input class="input_text_style" type="password" id="registration_1_2" name="USER_PASSWORD" />
        </div>

		<?if($arResult["SECURE_AUTH"]):?>
			<span class="bx-auth-secure" id="bx_auth_secure" title="<?echo GetMessage("AUTH_SECURE_NOTE")?>" style="display:none">
					<div class="bx-auth-secure-icon"></div>
			</span>
			<noscript>
				<span class="bx-auth-secure" title="<?echo GetMessage("AUTH_NONSECURE_NOTE")?>">
					<div class="bx-auth-secure-icon bx-auth-secure-unlock"></div>
				</span>
			</noscript>
			<script type="text/javascript">
				document.getElementById('bx_auth_secure').style.display = 'inline-block';
			</script>
		<?endif?>

		<?if($arResult["CAPTCHA_CODE"]):?>
			<input type="hidden" name="captcha_sid" value="<?echo $arResult["CAPTCHA_CODE"]?>" />
			<img src="/bitrix/tools/captcha.php?captcha_sid=<?echo $arResult["CAPTCHA_CODE"]?>" width="180" height="40" alt="CAPTCHA" />
			<?echo GetMessage("AUTH_CAPTCHA_PROMT")?>:
			<input class="bx-auth-input" type="text" name="captcha_word" maxlength="50" value="" size="15" />
		<?endif;?>
		
		<span style="display:block;height:7px;"></span>
		
		<?if ($arResult["STORE_PASSWORD"] == "Y"):?>
			<span class="rememberme"><input type="checkbox" id="USER_REMEMBER" name="USER_REMEMBER" value="Y" checked/><label for="USER_REMEMBER"><span></span><?=GetMessage("AUTH_REMEMBER_ME")?></label></span>
		<?endif?>

		<div class="submit-container">
			<input type="submit" name="Login" class="mybtnA" value="<?=GetMessage("AUTH_AUTHORIZE")?>" />
			<?if ($arParams["NOT_SHOW_LINKS"] != "Y"):?>
					<noindex>
						<span class="forgotpassword" style="padding-left:75px;">
							<a href="<?=$arResult["AUTH_FORGOT_PASSWORD_URL"]?>" rel="nofollow"><?=GetMessage("AUTH_FORGOT_PASSWORD_2")?></a>
						</span>
					</noindex>
			<?endif?>
			<?if($arParams["NOT_SHOW_LINKS"] != "Y" && $arResult["NEW_USER_REGISTRATION"] == "Y" && $arParams["AUTHORIZE_REGISTRATION"] != "Y"):?>
				<noindex>
					<span class="forgotpassword" style="padding-left:75px;">
						<a href="<?=$arResult["AUTH_REGISTER_URL"]?>" rel="nofollow"><?=GetMessage("AUTH_REGISTER")?></a>
						<?=GetMessage("AUTH_FIRST_ONE")?>
					</span>
				</noindex>
			<?endif?>
		</div>
	</div>
</form>

<script type="text/javascript">
<?if (strlen($arResult["LAST_LOGIN"])>0):?>
try{document.form_auth.USER_PASSWORD.focus();}catch(e){}
<?else:?>
try{document.form_auth.USER_LOGIN.focus();}catch(e){}
<?endif?>
</script>