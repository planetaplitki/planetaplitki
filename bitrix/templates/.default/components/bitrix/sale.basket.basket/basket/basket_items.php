<?if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();

use Bitrix\Sale\DiscountCouponsManager;

echo ShowError($arResult["ERROR_MESSAGE"]);

$bDelayColumn  = false;
$bDeleteColumn = false;
$bWeightColumn = false;
$bPropsColumn  = false;
$bPriceType    = false;
?>

<?if ($normalCount > 0){?>

    <div class="tab-content active">
        <div class="tab-pane fade in active" id="cart">
            <div class="box-inner">
                <div class="row header-top-table hidden-xs hidden-sm">
                    <div class="col-xs-6 col-sm-3 col-md-2">
                        <div class="col-sm-2">Фото</div>
                    </div>
                    <div class="col-xs-6 col-sm-9 col-md-10">
                        <div class="row">
                            <div class="col-sm-3">Наименование</div>
                            <div class="col-sm-4">Количество</div>
                            <div class="col-sm-2">Цена</div>
                            <div class="col-sm-2">Стоимость</div>
                            <div class="col-sm-1"></div>
                        </div>
                    </div>
                </div>
                <div class="line-top hidden-xs hidden-sm"></div>

                <?foreach ( $arResult["ITEMS"]["AnDelCanBuy"] as $k => $arItem ){?>

                    <div class="row header-top-table2 oneProduct" data-basketitemid="<?=$arItem['ID']?>"  data-itemid="<?=$arItem['PRODUCT_ID']?>" >
                        <div class="col-xs-6 col-sm-3 col-md-2 image-pict">
                            <?if (strlen($arItem["PREVIEW_PICTURE_SRC"]) > 0):
                                $url = $arItem["PREVIEW_PICTURE_SRC"];
                            elseif (strlen($arItem["DETAIL_PICTURE_SRC"]) > 0):
                                $url = $arItem["DETAIL_PICTURE_SRC"];
                            else:
                                $url = $templateFolder."/images/no_photo.png";
                            endif;?>

                            <?if (strlen($arItem["DETAIL_PAGE_URL"]) > 0):?><a href="<?=$arItem["DETAIL_PAGE_URL"] ?>"><?endif;?>
                                <img src="<?=$url?>" alt="<?=$arItem['NAME']?>" />
                            <?if (strlen($arItem["DETAIL_PAGE_URL"]) > 0):?></a><?endif;?>
                        </div>
                        <div class="col-xs-6 col-sm-9 col-md-10 cart-items-inner">
                            <div class="row">
                                <div class="col-xs-6 col-sm-4 col-md-3 box">
                                    <div class="article">
                                        <p>
											<a href="<?=$arItem["DETAIL_PAGE_URL"];?>"><?=$arItem['NAME']?></a>
											<?if( $arItem['ARTICUL'] ){?>
												<br>Артикул <?=$arItem['ARTICUL'];?>
											<?}?>
                                            <?if( $arItem['SIZE'] ){?>
                                                <br>Размер: <?=$arItem['SIZE'];?>
                                            <?}?>
										</p>
                                    </div>
                                </div>
                                <div class="col-xs-6 col-sm-5 col-md-4 box">
                                    <div class="quantity">
                                        <div class="minus">-</div>
                                        <div class="kolvo">
                                            <input
                                                type="text"
                                                id="QUANTITY_INPUT_<?=$arItem["ID"]?>"
                                                name="QUANTITY_INPUT_<?=$arItem["ID"]?>"
                                                value="<?=$arItem["QUANTITY"]?>"
                                                />
                                        </div>
                                        <div class="plus">+</div>
                                        <div class="unit">шт</div>
                                    </div>
                                </div>
                                <div class="col-xs-6 col-sm-4 col-md-2 box hidden-xs priceWrap">
                                    <div class="price">
                                        <p><?=SaleFormatCurrency($arItem['PRICE'], "RUB");?></p>
                                    </div>
                                </div>
                                <div class="col-xs-6 col-sm-4 col-md-2 box costWrap">
                                    <div class="price">
                                        <p>
                                           <?$cost = $arItem['PRICE'] * $arItem['QUANTITY']?>
                                            <?=SaleFormatCurrency($cost, "RUB");?>
                                        </p>
                                    </div>
                                </div>
                                <div class="col-xs-6 col-sm-1 box">
                                    <div>
                                        <a class="delete" href="javascript:void(0)" ><img src="/bitrix/templates/.default/img/icon/icon_close.png"></a>
                                        <?/*?><a class="delete" href="<?=$APPLICATION->GetCurPage();?>?action=delete&id=<?=$arItem['ID']?>" ><img src="/bitrix/templates/.default/img/icon/icon_close.png"></a><?*/?>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="line-top2"></div>
                <?}?>

                <div class="box-inner2">


                    <div class="box-inner-left bx_ordercart_order_pay">
                        <?if(COption::GetOptionString("grain.customsettings", 'show_coupon_field') === 'Y'):?>
                            <div class="bx_ordercart_order_pay_left" id="coupons_block">

                            <?if ($arParams["HIDE_COUPON"] != "Y")
                            {
                                ?>
                                <div class="bx_ordercart_coupon">
                                <span class="title"><?=GetMessage("STB_COUPON_PROMT")?></span>
                                <input type="text" id="coupon" name="COUPON" value="" class="form-control">
                                <a class="bx_bt_button bx_big link-buttom" href="javascript:void(0)" onclick="enterCoupon();" title="<?=GetMessage('SALE_COUPON_APPLY_TITLE'); ?>"><?/*=GetMessage('SALE_COUPON_APPLY'); */?>Применить</a>
                                </div><?
                                if (!empty($arResult['COUPON_LIST']))
                                {
                                    foreach ($arResult['COUPON_LIST'] as $oneCoupon)
                                    {
                                        $couponClass = 'disabled';
                                        switch ($oneCoupon['STATUS'])
                                        {
                                            case DiscountCouponsManager::STATUS_NOT_FOUND:
                                            case DiscountCouponsManager::STATUS_FREEZE:
                                                $couponClass = 'bad';
                                                break;
                                            case DiscountCouponsManager::STATUS_APPLYED:
                                                $couponClass = 'good';
                                                break;
                                        }
                                        ?>
                            <div class="bx_ordercart_coupon">
                                <?/*<input disabled readonly type="text" name="OLD_COUPON[]" value="<?=htmlspecialcharsbx($oneCoupon['COUPON']);?>" class="<? echo $couponClass; ?>">*/?>

                                <div class="cart-coupon-code <? echo $couponClass; ?>"><?=htmlspecialcharsbx($oneCoupon['COUPON']);?></div>

                                <div class="bx_ordercart_coupon_notes"><?
                                            if (isset($oneCoupon['CHECK_CODE_TEXT']))
                                            {
                                                echo (is_array($oneCoupon['CHECK_CODE_TEXT']) ? implode('<br>', $oneCoupon['CHECK_CODE_TEXT']) : $oneCoupon['CHECK_CODE_TEXT']);
                                            }
                                            ?></div>
                                <span class="cart-coupon-remove <? echo $couponClass; ?>" data-coupon="<? echo htmlspecialcharsbx($oneCoupon['COUPON']); ?>"></span>

                            </div><?
                                    }
                                    unset($couponClass, $oneCoupon);
                                }
                            }
                            else
                            {
                                ?>&nbsp;<?
                            }?>
                        </div>
                        <?endif;?>
                    </div>


                    <div class="buttom-media">
                        <!--<button type="button" class="btn link-buttom">Перейти к оформлению</button>-->
                        <a class="btn link-buttom" href="/personal/order/make/">Перейти к оформлению</a>
                    </div>
                    
                    <?if($arResult["DISCOUNT_PRICE_ALL"] != 0):?>
                        <div class="itogo dis_itogo">ИТОГО <span class="through"><?=str_replace(" ", "&nbsp;", $arResult["PRICE_WITHOUT_DISCOUNT"])?></span> <span class="red_sum"><?=str_replace(" ", "&nbsp;", $arResult["allSum_FORMATED"])?></span></div>
                    <?else:?>
                        <div class="itogo">ИТОГО <?=str_replace(" ", "&nbsp;", $arResult["allSum_FORMATED"])?></div>        
                    <?endif;?>
                    
                </div>
            </div>
        </div>
    </div>

    <input type="hidden" id="column_headers" value="<?=CUtil::JSEscape(implode($arHeaders, ","))?>" />
    <input type="hidden" id="offers_props" value="<?=CUtil::JSEscape(implode($arParams["OFFERS_PROPS"], ","))?>" />
    <input type="hidden" id="action_var" value="<?=CUtil::JSEscape($arParams["ACTION_VARIABLE"])?>" />
    <input type="hidden" id="quantity_float" value="<?=$arParams["QUANTITY_FLOAT"]?>" />
    <input type="hidden" id="count_discount_4_all_quantity" value="<?=($arParams["COUNT_DISCOUNT_4_ALL_QUANTITY"] == "Y") ? "Y" : "N"?>" />
    <input type="hidden" id="price_vat_show_value" value="<?=($arParams["PRICE_VAT_SHOW_VALUE"] == "Y") ? "Y" : "N"?>" />
    <input type="hidden" id="hide_coupon" value="<?=($arParams["HIDE_COUPON"] == "Y") ? "Y" : "N"?>" />
    <input type="hidden" id="use_prepayment" value="<?=($arParams["USE_PREPAYMENT"] == "Y") ? "Y" : "N"?>" />


<?}
else{?>
    <div class="tab-content"></div>
<?}?>