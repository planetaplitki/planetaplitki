<?if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>

<?if(
    CModule::IncludeModule('sale') &&
    CModule::IncludeModule('catalog') &&
    CModule::IncludeModule('iblock')
){
    # общая корзина
    foreach( $arResult["ITEMS"]["AnDelCanBuy"] as $k => &$item ){
        $res = CIBlockElement::GetByID($item['PRODUCT_ID']);
        if($ar_res = $res->GetNext()){
            if( $ar_res['IBLOCK_ID'] == IBLOCK_ID_CATALOG ){
                $res_ = CIBlockElement::GetList(Array(), array("IBLOCK_ID"=> IBLOCK_ID_CATALOG, "ID" => $ar_res['ID'],"ACTIVE"=>"Y"), false, false,
                    array(
                        "ID",
                        "NAME",
                        "DETAIL_PAGE_URL",
                        "PROPERTY_NAZVANIE_DLYA_SAYTA",
                        "PROPERTY_CML2_ARTICLE",
                        "PROPERTY_VYSOTA",
                        "PROPERTY_SHIRINA",
                    )
                );
                while($ob = $res_->GetNextElement()){
                    $arFields = $ob->GetFields();
                    $name = $arFields['PROPERTY_NAZVANIE_DLYA_SAYTA_VALUE'];
                    $item['NAME'] = $name ? $name : $arFields['NAME'];
                    $item['ARTICUL'] = $arFields['PROPERTY_CML2_ARTICLE_VALUE'];
                    $item['SIZE'] = $arFields['PROPERTY_SHIRINA_VALUE'].' x '.$arFields['PROPERTY_VYSOTA_VALUE'];
                }
            }
        }

        $allSum += $item['QUANTITY'] * $item['FULL_PRICE'];
    }

    # отложенные
    foreach( $arResult["ITEMS"]["DelDelCanBuy"] as $k => &$item ){
        $res = CIBlockElement::GetByID($item['PRODUCT_ID']);
        if($ar_res = $res->GetNext()){
            if( $ar_res['IBLOCK_ID'] == IBLOCK_ID_CATALOG ){
                $res_ = CIBlockElement::GetList(Array(), array("IBLOCK_ID"=> IBLOCK_ID_CATALOG, "ID" => $ar_res['ID'],"ACTIVE"=>"Y"), false, false,
                    array(
                        "ID",
                        "NAME",
                        "DETAIL_PAGE_URL",
                        "PROPERTY_NAZVANIE_DLYA_SAYTA",
                        "PROPERTY_CML2_ARTICLE",
                        "PROPERTY_VYSOTA",
                        "PROPERTY_SHIRINA",
                    )
                );
                while($ob = $res_->GetNextElement()){
                    $arFields = $ob->GetFields();
                    $name = $arFields['PROPERTY_NAZVANIE_DLYA_SAYTA_VALUE'];
                    $item['NAME'] = $name ? $name : $arFields['NAME'];
                    $item['ARTICUL'] = $arFields['PROPERTY_CML2_ARTICLE_VALUE'];
                    $item['SIZE'] = $arFields['PROPERTY_SHIRINA_VALUE'].' x '.$arFields['PROPERTY_VYSOTA_VALUE'];
                }
            }
        }
    }

    $arResult["allSum_"] = $allSum;
}

?>