<?
use Bitrix\Main\Loader;
use Bitrix\Main\ModuleManager;   

//pRU($arResult,'all');
$arParams['USE_FILTER'] = "Y";
$arParams["FILTER_NAME"] = $arParams["FILTER_NAME"]?: "arrSmartFilter";
$isFilter = ($arParams['USE_FILTER'] == 'Y');
if ($isFilter)
{          
    $arFilter = array(
        "IBLOCK_ID" => $arParams["IBLOCK_ID"],
        "ACTIVE" => "Y",
        "GLOBAL_ACTIVE" => "Y",
    );
    if (0 < intval($arResult["VARIABLES"]["SECTION_ID"]))
        $arFilter["ID"] = $arResult["VARIABLES"]["SECTION_ID"];
    elseif ('' != $arResult["VARIABLES"]["SECTION_CODE"])
        $arFilter["=CODE"] = $arResult["VARIABLES"]["SECTION_CODE"];

    $obCache = new CPHPCache();
    if ($obCache->InitCache(36000, serialize($arFilter), "/iblock/catalog"))
    {
        $arCurSection = $obCache->GetVars();
    }
    elseif ($obCache->StartDataCache())
    {
        $arCurSection = array();
        if (Loader::includeModule("iblock"))
        {
            $dbRes = CIBlockSection::GetList(array(), $arFilter, false, array("ID"));

            if(defined("BX_COMP_MANAGED_CACHE"))
            {
                global $CACHE_MANAGER;
                $CACHE_MANAGER->StartTagCache("/iblock/catalog");

                if( !$arFilter['ID'] ){
                    $arCurSection = array();
                }
                else{
                    if ($arCurSection = $dbRes->Fetch()){
//                        pRU($arFilter,'all');
//                        pRU($arCurSection,'all');
                        $CACHE_MANAGER->RegisterTag("iblock_id_".$arParams["IBLOCK_ID"]);
                    }
                }

                $CACHE_MANAGER->EndTagCache();
            }
            else
            {
                if(!$arCurSection = $dbRes->Fetch())
                    $arCurSection = array();
            }
        }
        $obCache->EndDataCache($arCurSection);
    }
    if (!isset($arCurSection))
        $arCurSection = array();
       
}

if( CURPAGE == SITE_DIR.'catalog'.DIRECTORY_SEPARATOR ){
    $arCurSection['ID'] = '';
}
?>

<?
//pRU($arFilter,'all');
//pRU($arCurSection,'all');
?>

<?

/**
 * Возвращает фильтр для разделов/коллекций
 * @param $idSection
 * @param $ufType
 * @return mixed
 */
function getSectionFilter($idSection, $arSmartFilter, $ufType){

//    pRU($arSmartFilter,'all');
//    echo serialize($arSmartFilter).'<br/>';
//    echo $idSection.'<br/>';
//    echo USER_TYPE.'<br/>';
//    echo $ufType.'<br/>';

    $status = htmlspecialchars($_REQUEST['STATUSES']);

    $hash = md5(serialize($arSmartFilter).$idSection.$ufType.$status.USER_TYPE);

    # кешируем результат
    $phpCache = new CPHPCache;
    $cacheTime = 3600*30*24; # время кеширования - 1 месяц
    $cacheID = 'catalog_filter_'.$hash; # формируем уникальный id кеша
    $cachePath = '/cached_sections'; # папка с кешем

    if($phpCache->InitCache($cacheTime, $cacheID, $cachePath)) {
        $filter = $phpCache->GetVars();
    }
    else {
        # помечаем кеш тегом, связанным с инфоблоком
        $tagCache = $GLOBALS['CACHE_MANAGER'];
        $tagCache->StartTagCache($cachePath);
        $tagCache->RegisterTag('iblock_id_'.IBLOCK_ID_CATALOG);
        $tagCache->EndTagCache();

        if( CModule::IncludeModule('iblock') ){
            if( eRU($arSmartFilter) || $idSection ){

//                $sections_ = getSectionsByUserType();

                unset($arSmartFilter['FACET_OPTIONS']);

                if( eRU($arSmartFilter['ID']) ){
                    $filter['ID'] = $arSmartFilter['ID'];
                }
                else{
                    $arFilter = Array("IBLOCK_ID"=>IBLOCK_ID_CATALOG, "ACTIVE"=>"Y", "INCLUDE_SUBSECTIONS" => "Y");
                    $arFilter = !$idSection ? $arFilter : array_merge($arFilter, array("SECTION_ID" => $idSection));
                    $arFilter = array_merge($arFilter, $arSmartFilter);
                        
//                    pRU($arFilter,'all');

                    $res = CIBlockElement::GetList(Array(), $arFilter, array("IBLOCK_SECTION_ID"), false, array("ID", "IBLOCK_SECTION_ID"));
                    while($ob = $res->GetNextElement()){
                        $arFields = $ob->GetFields();

//                        pRU($arFields,'all');
//                    if(
//                        eRU($sections_['COUNTRIES'][$arFields['IBLOCK_SECTION_ID']]) ||
//                        eRU($sections_['BRANDS'][$arFields['IBLOCK_SECTION_ID']]) ||
//                        eRU($sections_['COLLECTIONS'][$arFields['IBLOCK_SECTION_ID']])
//                    ){
//                        $sections[] = $arFields['IBLOCK_SECTION_ID'];
//                    }
                        $sections[] = $arFields['IBLOCK_SECTION_ID'];
                    }           

                    if( eRU($sections) ){
                        $filter['ID'] = $sections;
                    }
                    else{
                        $filter['LEFT_MARGIN'] = 0;
                        $filter['RIGHT_MARGIN'] = 0;
                    }
                }
            }

            if( $ufType ){
                switch($ufType){
                    case 'sale' : $filter['UF_SALE'] = 1;
                        break;
                    case 'new' : $filter['UF_NEW_COLLECTION'] = 1;
                        break;
                    case 'popular' : $filter['UF_POP_COLLECTIONS'] = 1;
                        break;                        
                    case 'exclusive' : $filter['UF_EXCLUSIVE'] = 1;
                        break;
                }
            }

            if( $status ){
                $filter[$status] = 1;
            }

            # фильтр по типу пользователя: для розницы не используем фильтр
            if( USER_TYPE == "wholesale" ){
                $filter['UF_USER_GROUP'] = 7;
            }

            $filter['DEPTH_LEVEL'] = 3;
        }

        $phpCache->StartDataCache();
        $phpCache->EndDataCache($filter);
    }

    return $filter;
}

/**
 * Список цветов плитки для отображения в фильтре
 * @return array
 */
function getColors (){

    $colors = array();

    # кешируем результат
    $phpCache = new CPHPCache;
    $cacheTime = 3600*30*24; # время кеширования - 1 месяц
    $cacheID = 'colors'; # формируем уникальный id кеша
    $cachePath = '/colors'; # папка с кешем

    if($phpCache->InitCache($cacheTime, $cacheID, $cachePath)) {
        $colors = $phpCache->GetVars();
    }
    else {
        # помечаем кеш тегом, связанным с инфоблоком
        $tagCache = $GLOBALS['CACHE_MANAGER'];
        $tagCache->StartTagCache($cachePath);
        $tagCache->RegisterTag('iblock_id_'.IBLOCK_ID_COLORS);
        $tagCache->EndTagCache();

        if( CModule::IncludeModule('iblock') ){
            $arSelect = Array("ID", "NAME", "DETAIL_PICTURE", "XML_ID");
            $arFilter = Array("IBLOCK_ID" => IBLOCK_ID_COLORS, "ACTIVE"=>"Y",);
            $res = CIBlockElement::GetList(Array(), $arFilter, false, false, $arSelect);
            while( $ob = $res->GetNextElement() ){
                $arFields = $ob->GetFields();
                $arFields['ICON'] = i($arFields['DETAIL_PICTURE'], 50, 50);
                $colors[$arFields['XML_ID']] = $arFields['ICON'];
            }
        }

        $phpCache->StartDataCache();
        $phpCache->EndDataCache($colors);
    }

    return $colors;
}

/**
 * Возвращает сортировку
 * @param array $variants
 * @param string $name
 * @param string $text
 * @return string
 */
function getSortHtml($variants = array(), $name = "", $text = ""){

    $str = "";
    $options = "";
    $selectedVariant = htmlspecialchars($_REQUEST['SORT']['PRICES']);

    if( eRU($variants) ){
        foreach($variants as $variant){
            $options .= '<option value="'.$variant['VALUE'].'" '.($variant['VALUE'] == $selectedVariant ? "selected" : "").'>'.$variant['NAME'].'</option>';
        }

        $str = '
                <div class="col-xs-3 col-sm-4 col-md-4 header-filter-col">
                    <div class="label-text2">'.$text.'</div>
                    <select class="filters2" id="'.$name.'" name="SORT['.$name.']">'.$options.'</select>
                </div>
            ';
    }

    return $str;
}

/**
 * Возвращает фильтр по статусу (флагу)
 * @param array $variants
 * @param string $name
 * @param string $text
 * @return string
 */
function getStatusHtml($variants = array(), $name = "", $text = ""){

    $str = "";
    $options = "";
    $rStatus = $_REQUEST['STATUSES'];

    if( eRU($variants) ){
        $options = '<option value="">не выбран</option>';
        foreach($variants as $variant){
            $options .= '<option '.($rStatus == $variant['VALUE'] ? "selected" : "").' value="'.$variant['VALUE'].'">'.$variant['NAME'].'</option>';
        }

        $str = '
                <div class="col-xs-3 col-sm-3 header-filter-col">
                    <div class="label-text2">'.$text.'</div>
                    <select class="filters2" id="'.$name.'" name="'.$name.'">'.$options.'</select>
                </div>
            ';
    }

    return $str;
}

/**
 * Сортировка по производителю и коллекции по возрастанию по алфавиту
 * @param $a
 * @param $b
 * @return int
 */
function sortFields ($a, $b){
    if ( $a['VALUE'] == $b['VALUE'] ){
        return 0;
    }
    return ($a['VALUE'] < $b['VALUE']) ? -1 : 1;
}

?>

<div class="filtr">
    <div class="container pad">
        <div class="row">
            <div class="col-xs-12">
                <?$APPLICATION->IncludeComponent("bitrix:breadcrumb", "breadcrumbs",
                    array(
                        "START_FROM" => "0",
                        "PATH" => "",
                        "SITE_ID" => "-",
                    ),
                    false
                );?>
                <h1 class="hidden-xs"><?=$APPLICATION->ShowTitle(false);?></h1>
                <?/*<p class="title hidden-xs"><?=$APPLICATION->ShowTitle(false);?></p>*/?>
                <p class="title2 hidden-sm hidden-md hidden-lg"><?=$APPLICATION->ShowTitle(false);?></p>
<noindex>
                <?$APPLICATION->ShowViewContent("catalog_mobile_filter");?>
            </div>
        </div>

        <div class="row obj">
            <div class="col-sm-12">
                <?
                $view = htmlspecialchars($_REQUEST['view']);
                ?>

                <?/*$APPLICATION->IncludeComponent(
                    "up:iblock.section.list.filter",
                    "collections.list",
                    array(
                        "ID" => "0",
                        "SHOW_PARENT_NAME" => "Y",
                        "IBLOCK_TYPE" => $arParams["IBLOCK_TYPE"],
                        "IBLOCK_ID" => $arParams["IBLOCK_ID"],
                        "SECTION_ID" => $arResult['VARIABLES']['SECTION_ID'],
                        "SECTION_CODE" => "",
                        "CACHE_TYPE" => "A",
                        "CACHE_TIME" => "36000000",
                        "CACHE_NOTES" => "",
                        "CACHE_GROUPS" => "Y",
                        "FILTER_NAME" => "arrSectionsFilter",
                        "DEPTH_LEVEL" => 3,
                        "UF_TYPE" => $ufType,
                        "UF_SEO_PAGE_PROPS" => $ufSeoPageProps,
                        "UF_USER_TYPE" => USER_TYPE
                    ),
                    false
                );*/?>

                <?if( USER_TYPE == 'retail' ){
                    $arParams["PRICE_CODE"] = array(
//                        'Розничные руб. шт для сайта',
                        'Розничные руб. м2 для сайта',
                    );
                }
                elseif( USER_TYPE == 'wholesale' ){
                    $arParams["PRICE_CODE"] = array(
//                        'Оптовые руб. шт для сайта',
                        'Оптовые руб. м2 для сайта',
                    );
                }?>

                <?if( eRU($GLOBALS['arrSectionsFilter']) ){
                    if( eRU($GLOBALS[$arParams["FILTER_NAME"]]) ){
                        $GLOBALS[$arParams["FILTER_NAME"]] = array_merge($GLOBALS['arrSectionsFilter'], $GLOBALS[$arParams["FILTER_NAME"]]);
                    }
                    else{
                        $GLOBALS[$arParams["FILTER_NAME"]] = $GLOBALS['arrSectionsFilter'];
                    }
                }?>

                <?
                    $filter_tmplt = 'simple';
                ?> 
                
                <?$APPLICATION->IncludeComponent(
	"bitrix:catalog.smart.filter", 
	"simple", 
	array(
		"SHOW_ALL_WO_SECTION" => "Y",
		"IBLOCK_TYPE" => $arParams["IBLOCK_TYPE"],
		"IBLOCK_ID" => $arParams["IBLOCK_ID"],
		"SECTION_ID" => $arCurSection["ID"],
		"FILTER_NAME" => $arParams["FILTER_NAME"],
		"PRICE_CODE" => array(
		),
		"CACHE_TYPE" => "A",
		"CACHE_TIME" => $arParams["CACHE_TIME"],
		"CACHE_GROUPS" => "N",
		"SAVE_IN_SESSION" => "N",
		"FILTER_VIEW_MODE" => $arParams["FILTER_VIEW_MODE"],
		"XML_EXPORT" => "N",
		"SECTION_TITLE" => "NAME",
		"SECTION_DESCRIPTION" => "DESCRIPTION",
		"HIDE_NOT_AVAILABLE" => "N",
		"TEMPLATE_THEME" => $arParams["TEMPLATE_THEME"],
		"CONVERT_CURRENCY" => "N",
		"CURRENCY_ID" => $arParams["CURRENCY_ID"],
		"SEF_MODE" => "Y",
		"SEF_RULE" => $arResult["FOLDER"].$arResult["URL_TEMPLATES"]["smart_filter"],
		"SMART_FILTER_PATH" => $arResult["VARIABLES"]["SMART_FILTER_PATH"],
		"PAGER_PARAMS_NAME" => $arParams["PAGER_PARAMS_NAME"],
		"COMPONENT_TEMPLATE" => "simple",
		"SECTION_CODE" => "",
		"DISPLAY_ELEMENT_COUNT" => "Y",
		"INSTANT_RELOAD" => "N",
		"SECTION_CODE_PATH" => ""
	),
	$component
);?>
            </div>
        </div>
    </div>
</div>
</noindex>

<?                
$filter = getSectionFilter($arCurSection['ID'], $GLOBALS[$arParams['FILTER_NAME']], $ufType);       
$GLOBALS['arrSectionsFilter'] = $filter;     
?>

<?
//pRU($filter, 'all');
//pRU($sections, 'all');
//pRU($sections_,'all');
//pRU($GLOBALS['arrSectionsFilter']);
?>
<div class="container obj">
    <?
    $sortPrice = htmlspecialchars($_REQUEST['SORT']['PRICES']);
    $sortOrder = $sortPrice ? $sortPrice : 'asc';
//    $sortField = $sortPrice ? $sortPrice : ((USER_TYPE == 'retail') ? "UF_PRICE_RETAIL" : "UF_PRICE_WHOLESALE");
    $sortField = USER_TYPE == 'retail' ? "UF_PRICE_RETAIL" : "UF_PRICE_WHOLESALE";
    ?>

    <?
    $defaultElementCount = 80;
    $elCount = intval($_REQUEST['PAGE_ELEMENT_COUNT']);
    $elCount = $elCount ? $elCount : $defaultElementCount;
    ?>
    <div class="collections-page-list">
        <div class="row">
            <?$APPLICATION->IncludeComponent(
                "up:iblock.section.list", 
                "collections.list", 
                array(

                    "PRICE_CODE" => $arParams["PRICE_CODE"],
            //        "ID" => "0",
                    "SHOW_PARENT_NAME" => "Y",
                    "IBLOCK_TYPE" => "catalog",
                    "IBLOCK_ID" => $arParams["IBLOCK_ID"],
                    "SECTION_ID" => $arResult["VARIABLES"]["SECTION_ID"],
                    "SECTION_CODE" => "",
                    "SECTION_URL" => "",
                    "COUNT_ELEMENTS" => "Y",
                    "TOP_DEPTH" => "3",
                    "SECTION_FIELDS" => array(
                        0 => "ID",
                        1 => "NAME",
                        2 => "DESCRIPTION",
                        3 => "PICTURE",
                        4 => "SECTION_PAGE_URL",
                        5 => "DEPTH_LEVEL",
                        6 => "IBLOCK_SECTION_ID",
                        7 => "",
                    ),
                    "SECTION_USER_FIELDS" => array(
                        0 => "",
                        1 => "UF_NEW_COLLECTION",
                        2 => "UF_EXCLUSIVE",
                        3 => "UF_SALE",
                        4 => "UF_PRICE_RETAIL",
                        5 => "UF_PRICE_WHOLESALE",
                        6 => "UF_MORE_PHOTO",
                        7 => "UF_POP_COLLECTIONS",
                    ),
                    "ADD_SECTIONS_CHAIN" => "Y",
                    "CACHE_TYPE" => "A",
                    "CACHE_TIME" => "36000000",
                    "CACHE_NOTES" => "",
                    "CACHE_GROUPS" => "Y",
                    "COMPONENT_TEMPLATE" => "collections.list",
                    "FILTER_NAME" => "arrSectionsFilter",
            //        "SORT_FIELD" => "id",
            //        "SORT_ORDER" => $sortOrder,
                    "SORT_FIELD" => $sortField,
                    "SORT_ORDER" => $sortOrder,
                    "SORT_FIELD2" => "id",
                    "SORT_ORDER2" => "desc",
                    "URL_404" => "/404.php",
                    "ELEMENT_CONTROLS" => "N",
                    "AJAX_MODE" => "N",
                    "AJAX_OPTION_JUMP" => "N",
                    "AJAX_OPTION_STYLE" => "N",
                    "AJAX_OPTION_HISTORY" => "N",
                    "AJAX_OPTION_ADDITIONAL" => "",
                    "CACHE_FILTER" => "Y",
                    "PAGER_TEMPLATE" => "modern",
                    "DISPLAY_TOP_PAGER" => "N",
                    "DISPLAY_BOTTOM_PAGER" => "Y",
                    "PAGER_TITLE" => "",
                    "PAGER_SHOW_ALWAYS" => "N",
                    "PAGER_DESC_NUMBERING" => "N",
                    "PAGER_DESC_NUMBERING_CACHE_TIME" => "36000",
                    "PAGE_ELEMENT_COUNT" => $elCount,
                    "PAGER_SHOW_ALL" => "Y",
                    "UF_VIEW" => $view,
                    "UF_USER_TYPE" => USER_TYPE
                ),
                false
            );?>
        </div>
    </div>
</div>


<?
# для вывода seo оптимизации
$intSectionID = $APPLICATION->IncludeComponent(
    "bitrix:catalog.section",
    "empty",
    array(
        "IBLOCK_TYPE" => $arParams["IBLOCK_TYPE"],
        "IBLOCK_ID" => $arParams["IBLOCK_ID"],
        "ELEMENT_SORT_FIELD" => $arParams["ELEMENT_SORT_FIELD"],
        "ELEMENT_SORT_ORDER" => $arParams["ELEMENT_SORT_ORDER"],
        "ELEMENT_SORT_FIELD2" => $arParams["ELEMENT_SORT_FIELD2"],
        "ELEMENT_SORT_ORDER2" => $arParams["ELEMENT_SORT_ORDER2"],
        "PROPERTY_CODE" => $arParams["LIST_PROPERTY_CODE"],
        "META_KEYWORDS" => $arParams["LIST_META_KEYWORDS"],
        "META_DESCRIPTION" => $arParams["LIST_META_DESCRIPTION"],
        "BROWSER_TITLE" => $arParams["LIST_BROWSER_TITLE"],
        "SET_LAST_MODIFIED" => $arParams["SET_LAST_MODIFIED"],
        "INCLUDE_SUBSECTIONS" => $arParams["INCLUDE_SUBSECTIONS"],
        "BASKET_URL" => $arParams["BASKET_URL"],
        "ACTION_VARIABLE" => $arParams["ACTION_VARIABLE"],
        "PRODUCT_ID_VARIABLE" => $arParams["PRODUCT_ID_VARIABLE"],
        "SECTION_ID_VARIABLE" => $arParams["SECTION_ID_VARIABLE"],
        "PRODUCT_QUANTITY_VARIABLE" => $arParams["PRODUCT_QUANTITY_VARIABLE"],
        "PRODUCT_PROPS_VARIABLE" => $arParams["PRODUCT_PROPS_VARIABLE"],
        "FILTER_NAME" => $arParams["FILTER_NAME"],
        "CACHE_TYPE" => $arParams["CACHE_TYPE"],
        "CACHE_TIME" => $arParams["CACHE_TIME"],
        "CACHE_FILTER" => "Y",
        "CACHE_GROUPS" => $arParams["CACHE_GROUPS"],
        "SET_TITLE" => $arParams["SET_TITLE"],
        "MESSAGE_404" => $arParams["MESSAGE_404"],
        "SET_STATUS_404" => $arParams["SET_STATUS_404"],
        "SHOW_404" => $arParams["SHOW_404"],
        "FILE_404" => $arParams["FILE_404"],
        "DISPLAY_COMPARE" => $arParams["USE_COMPARE"],
        "PAGE_ELEMENT_COUNT" => $arParams["PAGE_ELEMENT_COUNT"],
        "LINE_ELEMENT_COUNT" => $arParams["LINE_ELEMENT_COUNT"],
        "PRICE_CODE" => $arParams["PRICE_CODE"],
        "USE_PRICE_COUNT" => $arParams["USE_PRICE_COUNT"],
        "SHOW_PRICE_COUNT" => $arParams["SHOW_PRICE_COUNT"],

        "PRICE_VAT_INCLUDE" => $arParams["PRICE_VAT_INCLUDE"],
        "USE_PRODUCT_QUANTITY" => $arParams['USE_PRODUCT_QUANTITY'],
        "ADD_PROPERTIES_TO_BASKET" => (isset($arParams["ADD_PROPERTIES_TO_BASKET"]) ? $arParams["ADD_PROPERTIES_TO_BASKET"] : ''),
        "PARTIAL_PRODUCT_PROPERTIES" => (isset($arParams["PARTIAL_PRODUCT_PROPERTIES"]) ? $arParams["PARTIAL_PRODUCT_PROPERTIES"] : ''),
        "PRODUCT_PROPERTIES" => $arParams["PRODUCT_PROPERTIES"],

        "DISPLAY_TOP_PAGER" => $arParams["DISPLAY_TOP_PAGER"],
        "DISPLAY_BOTTOM_PAGER" => $arParams["DISPLAY_BOTTOM_PAGER"],
        "PAGER_TITLE" => $arParams["PAGER_TITLE"],
        "PAGER_SHOW_ALWAYS" => $arParams["PAGER_SHOW_ALWAYS"],
        "PAGER_TEMPLATE" => $arParams["PAGER_TEMPLATE"],
        "PAGER_DESC_NUMBERING" => $arParams["PAGER_DESC_NUMBERING"],
        "PAGER_DESC_NUMBERING_CACHE_TIME" => $arParams["PAGER_DESC_NUMBERING_CACHE_TIME"],
        "PAGER_SHOW_ALL" => $arParams["PAGER_SHOW_ALL"],
        "PAGER_BASE_LINK_ENABLE" => $arParams["PAGER_BASE_LINK_ENABLE"],
        "PAGER_BASE_LINK" => $arParams["PAGER_BASE_LINK"],
        "PAGER_PARAMS_NAME" => $arParams["PAGER_PARAMS_NAME"],

        "OFFERS_CART_PROPERTIES" => $arParams["OFFERS_CART_PROPERTIES"],
        "OFFERS_FIELD_CODE" => $arParams["LIST_OFFERS_FIELD_CODE"],
        "OFFERS_PROPERTY_CODE" => $arParams["LIST_OFFERS_PROPERTY_CODE"],
        "OFFERS_SORT_FIELD" => $arParams["OFFERS_SORT_FIELD"],
        "OFFERS_SORT_ORDER" => $arParams["OFFERS_SORT_ORDER"],
        "OFFERS_SORT_FIELD2" => $arParams["OFFERS_SORT_FIELD2"],
        "OFFERS_SORT_ORDER2" => $arParams["OFFERS_SORT_ORDER2"],
        "OFFERS_LIMIT" => $arParams["LIST_OFFERS_LIMIT"],

        "SECTION_ID" => $arResult["VARIABLES"]["SECTION_ID"],
        "SECTION_CODE" => $arResult["VARIABLES"]["SECTION_CODE"],
        "SECTION_URL" => $arResult["FOLDER"].$arResult["URL_TEMPLATES"]["section"],
        "DETAIL_URL" => $arResult["FOLDER"].$arResult["URL_TEMPLATES"]["element"],
        "USE_MAIN_ELEMENT_SECTION" => $arParams["USE_MAIN_ELEMENT_SECTION"],
        'CONVERT_CURRENCY' => $arParams['CONVERT_CURRENCY'],
        'CURRENCY_ID' => $arParams['CURRENCY_ID'],
        'HIDE_NOT_AVAILABLE' => $arParams["HIDE_NOT_AVAILABLE"],

        'LABEL_PROP' => $arParams['LABEL_PROP'],
        'ADD_PICT_PROP' => $arParams['ADD_PICT_PROP'],
        'PRODUCT_DISPLAY_MODE' => $arParams['PRODUCT_DISPLAY_MODE'],

        'OFFER_ADD_PICT_PROP' => $arParams['OFFER_ADD_PICT_PROP'],
        'OFFER_TREE_PROPS' => $arParams['OFFER_TREE_PROPS'],
        'PRODUCT_SUBSCRIPTION' => $arParams['PRODUCT_SUBSCRIPTION'],
        'SHOW_DISCOUNT_PERCENT' => $arParams['SHOW_DISCOUNT_PERCENT'],
        'SHOW_OLD_PRICE' => $arParams['SHOW_OLD_PRICE'],
        'MESS_BTN_BUY' => $arParams['MESS_BTN_BUY'],
        'MESS_BTN_ADD_TO_BASKET' => $arParams['MESS_BTN_ADD_TO_BASKET'],
        'MESS_BTN_SUBSCRIBE' => $arParams['MESS_BTN_SUBSCRIBE'],
        'MESS_BTN_DETAIL' => $arParams['MESS_BTN_DETAIL'],
        'MESS_NOT_AVAILABLE' => $arParams['MESS_NOT_AVAILABLE'],

        'TEMPLATE_THEME' => (isset($arParams['TEMPLATE_THEME']) ? $arParams['TEMPLATE_THEME'] : ''),
        "ADD_SECTIONS_CHAIN" => "N",
        'ADD_TO_BASKET_ACTION' => $basketAction,
        'SHOW_CLOSE_POPUP' => isset($arParams['COMMON_SHOW_CLOSE_POPUP']) ? $arParams['COMMON_SHOW_CLOSE_POPUP'] : '',
        'COMPARE_PATH' => $arResult['FOLDER'].$arResult['URL_TEMPLATES']['compare'],
        'BACKGROUND_IMAGE' => (isset($arParams['SECTION_BACKGROUND_IMAGE']) ? $arParams['SECTION_BACKGROUND_IMAGE'] : '')
    ),
    $component
);
?>

