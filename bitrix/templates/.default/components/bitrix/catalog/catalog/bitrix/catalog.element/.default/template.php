<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */

$quantity_store = 0;
$quantity_store_2 = 0;
$quantity_store_6 = 0;
 
function arr($arr, $stop=false)
{
	global $USER;
	if ($USER->IsAdmin()) {
		echo "<pre>"; print_r($arr); echo "</pre>"; 
		if ($stop==true) die();
	}
}

$this->setFrameMode(true);
$templateLibrary = array('popup');
$currencyList = '';
if (!empty($arResult['CURRENCIES'])) {
    $templateLibrary[] = 'currency';
    $currencyList = CUtil::PhpToJSObject($arResult['CURRENCIES'], false, true, true);
}
$templateData = array(
    'TEMPLATE_THEME' => $this->GetFolder() . '/themes/' . $arParams['TEMPLATE_THEME'] . '/style.css',
    'TEMPLATE_CLASS' => 'bx_' . $arParams['TEMPLATE_THEME'],
    'TEMPLATE_LIBRARY' => $templateLibrary,
    'CURRENCIES' => $currencyList
);
unset($currencyList, $templateLibrary);

$strMainID = $this->GetEditAreaId($arResult['ID']);
$arItemIDs = array(
    'ID' => $strMainID,
    'PICT' => $strMainID . '_pict',
    'DISCOUNT_PICT_ID' => $strMainID . '_dsc_pict',
    'STICKER_ID' => $strMainID . '_sticker',
    'BIG_SLIDER_ID' => $strMainID . '_big_slider',
    'BIG_IMG_CONT_ID' => $strMainID . '_bigimg_cont',
    'SLIDER_CONT_ID' => $strMainID . '_slider_cont',
    'SLIDER_LIST' => $strMainID . '_slider_list',
    'SLIDER_LEFT' => $strMainID . '_slider_left',
    'SLIDER_RIGHT' => $strMainID . '_slider_right',
    'OLD_PRICE' => $strMainID . '_old_price',
    'PRICE' => $strMainID . '_price',
    'DISCOUNT_PRICE' => $strMainID . '_price_discount',
    'SLIDER_CONT_OF_ID' => $strMainID . '_slider_cont_',
    'SLIDER_LIST_OF_ID' => $strMainID . '_slider_list_',
    'SLIDER_LEFT_OF_ID' => $strMainID . '_slider_left_',
    'SLIDER_RIGHT_OF_ID' => $strMainID . '_slider_right_',
    'QUANTITY' => $strMainID . '_quantity',
    'QUANTITY_DOWN' => $strMainID . '_quant_down',
    'QUANTITY_UP' => $strMainID . '_quant_up',
    'QUANTITY_MEASURE' => $strMainID . '_quant_measure',
    'QUANTITY_LIMIT' => $strMainID . '_quant_limit',
    'BASIS_PRICE' => $strMainID . '_basis_price',
    'BUY_LINK' => $strMainID . '_buy_link',
    'ADD_BASKET_LINK' => $strMainID . '_add_basket_link',
    'BASKET_ACTIONS' => $strMainID . '_basket_actions',
    'NOT_AVAILABLE_MESS' => $strMainID . '_not_avail',
    'COMPARE_LINK' => $strMainID . '_compare_link',
    'PROP' => $strMainID . '_prop_',
    'PROP_DIV' => $strMainID . '_skudiv',
    'DISPLAY_PROP_DIV' => $strMainID . '_sku_prop',
    'OFFER_GROUP' => $strMainID . '_set_group_',
    'BASKET_PROP_DIV' => $strMainID . '_basket_prop',
);
$strObName = 'ob' . preg_replace("/[^a-zA-Z0-9_]/", "x", $strMainID);
$templateData['JS_OBJ'] = $strObName;

$strTitle = (
isset($arResult["IPROPERTY_VALUES"]["ELEMENT_DETAIL_PICTURE_FILE_TITLE"]) && $arResult["IPROPERTY_VALUES"]["ELEMENT_DETAIL_PICTURE_FILE_TITLE"] != ''
    ? $arResult["IPROPERTY_VALUES"]["ELEMENT_DETAIL_PICTURE_FILE_TITLE"]
    : $arResult['NAME']
);
$strAlt = (
isset($arResult["IPROPERTY_VALUES"]["ELEMENT_DETAIL_PICTURE_FILE_ALT"]) && $arResult["IPROPERTY_VALUES"]["ELEMENT_DETAIL_PICTURE_FILE_ALT"] != ''
    ? $arResult["IPROPERTY_VALUES"]["ELEMENT_DETAIL_PICTURE_FILE_ALT"]
    : $arResult['NAME']
);
?>

<?
$poverhnost = $arResult['POVERHNOST'];
$visota = $arResult['DIMENSIONS']['VISOTA'];
$shirina = $arResult['DIMENSIONS']['SHIRINA'];

$elementSectionsInfo = $arResult['ELEMENT_SECTION_INFO'];
$idSection = $arResult['IBLOCK_SECTION_ID'];

$userType = USER_TYPE;

$m2PriceId = 10;
if ($userType == 'wholesale') {
    $m2PriceId = 12;
}

$db_res = CPrice::GetList(array(), array("PRODUCT_ID" => $arResult['ID'], "CATALOG_GROUP_ID" => $m2PriceId));
if ($ar_res = $db_res->Fetch()) {
    $retailM2 = $ar_res['PRICE'];
}

// получим цену за кв. м со скидкой и без
$rn = CCatalogGroup::GetByID($m2PriceId);
$retailM2 = $arResult['PRICES'][$rn['NAME']]['DISCOUNT_VALUE'];
$priceBaseM2 = $arResult['PRICES'][$rn['NAME']]['VALUE'];
$priceDiscountM2 = $arResult['PRICES'][$rn['NAME']]['DISCOUNT_VALUE'];

//pRU($arResult['MIN_PRICE'], 'all');

# минимальная разница в ценах для отображения скидок 1руб
$minPriceDiff = 1;
$retailOne = $arResult['MIN_PRICE']['DISCOUNT_VALUE'];
$priceBase = $arResult['MIN_PRICE']['VALUE'];
$priceDiscount = $arResult['MIN_PRICE']['DISCOUNT_VALUE'];

//$retailM2 = (10000 / floatval( floatval(str_replace(',','.',$visota))*floatval(str_replace(',','.',$shirina)))) * $retailOne;

$vUpakovkeShtuk = $arResult['PROPERTIES']['V_UPAKOVKE_SHTUK']['VALUE'];
$vUpakovkePrice = $vUpakovkeShtuk * $retailOne;

$picture = $arResult['PICTURE'];
$morePhoto = $arResult['MORE_PHOTO'];
$hidePriceM2 = $arResult['PROPERTIES']['TSENA_V_SHT']['VALUE'];

$quantity = $arResult['CATALOG_QUANTITY'];

$quantity_store = 0;
$quantity_store_2 = $arResult['CATALOG_QUANTITY_STORE'];
$quantity_store_6 = $arResult['CATALOG_QUANTITY_STORE_6'];
if($quantity_store_2){
    $quantity_store = $quantity_store + $quantity_store_2;    
}
if($quantity_store_6){
    $quantity_store = $quantity_store + $quantity_store_6;    
}

//$quantity_store = $arResult['CATALOG_QUANTITY_STORE'];
//$quantity_store_6 = $arResult['CATALOG_QUANTITY_STORE_6'];

?>
    <div class="hidden-xs"> 
        <div class="container">
            <div class="menu-link"><?= $elementSectionsInfo['DESKTOP'] ?></div>
        </div>
        <div class="container" itemscope itemtype="http://schema.org/Product">
                    <div class="m-sharing">
                        <script src="//yastatic.net/es5-shims/0.0.2/es5-shims.min.js"></script>
                        <script src="//yastatic.net/share2/share.js"></script>
                        <div class="ya-share2" data-services="collections,vkontakte,facebook,odnoklassniki,gplus,twitter,viber,                                whatsapp,skype,telegram" data-size="s"></div>
                    </div>
            <div class="row obj m-obj">
                <span itemprop="name" style="display: none;"><?=$arResult["NAME"]?></span>
                <div class="col-sm-6 gallery-container">
                    <? if ($picture || eRU($morePhoto)) { ?>
                        <a class="gallery-zoom-link">
                            <div class="zoom"></div>
                        </a>
                    <? } ?>

                    <div class="gallery" data-nav="thumbs" data-arrows="false"> 
                        <? if ($picture) {
                            $s = i($picture, 555, 396);
                            $b = CFile::GetPath($picture); ?>
                            <a href="<?= $b; ?>" class="gallery-preview-link" data-thumb="<?= $s; ?>"><img src="<?= $s; ?>" itemprop="image"
                                                                                                           class="gallery-preview-image"></a>
                            <?
                        } else {
                            ?>
                            <? $img = "/upload/images/net-foto-detail.jpg"; ?>
                            <a href="javascript:void(0);" class="gallery-preview-link"><img src="<?= $img ?>" itemprop="image"
                                                                                            class="gallery-preview-image"></a>
                        <? } ?>
                        <? if (eRU($morePhoto)) { ?>
                            <? if (eRU($morePhoto)) {  
                                foreach ($morePhoto as $photo) {
                                    $s = i($photo, 115, 115, BX_RESIZE_IMAGE_EXACT);
                                    $b = CFile::GetPath($photo); ?>
                                    <a href="<?= $b; ?>" class="gallery-preview-link" data-thumb="<?= $s; ?>"><img
                                            src="<?= $s; ?>" class="gallery-preview-image"></a>
                                    <?
                                }
                            } ?>
                        <? } ?>
                    </div>
                </div>
                <div class="col-sm-6">
                    <div class="box-right-top">
                        <div class="row obj_but">
                            <div class="col-sm-12">
                                <p class="easy">
                                    <? if ($visota && $shirina) { ?>
                                        Размер: <span><?= $visota; ?>x<?= $shirina; ?></span><br>
                                    <? } ?>
                                    <? if ($poverhnost) { ?>
                                        Поверхность: <span><?= $poverhnost; ?></span>
                                    <? } ?>




                                    <? if ($arResult['INSALE'] || $userType == 'wholesale'): ?>
                                    <? if ($quantity_store): ?>
                                        <br><br><span class="icon-ok"></span>&nbsp;Есть в наличии&nbsp;(<?= $quantity_store ?> шт.)
                                    <? else: ?>
                                <p class="have fail"><span class="icon-fail"></span> Нет в наличии</p>
                                <? endif ?>
                                <? else: ?>
                                    <div class="have ok">
                                        <table>
                                            <tr>
                                                <td><span class="icon-ok"></span>&nbsp;Узнать о наличии:</td>
                                                <td>
                                                    <? $APPLICATION->IncludeComponent("bitrix:main.include", ".default", array(
                                                        "AREA_FILE_SHOW" => "file",
                                                        "PATH" => SITE_DEFAULT_TEMPLATE_PATH . "include/phone_card.php",
                                                        "EDIT_TEMPLATE" => ""
                                                    ), false
                                                    ); ?>
                                                </td>
                                            </tr>
                                        </table>
                                    </div>
                                <? endif ?>
								
								<div class="add_board" style="position: absolute; right: 20px; top: 12px;">
									<?//=$arResult['PROPERTIES']['OUTPUT_BANNER']['VALUE'];?>
									<?
									GLOBAL $link_banner;
									//echo $link_banner;?>
									<?
									$arGroupAvalaible = array(9);
									$arGroups = CUser::GetUserGroup($USER->GetID());
									//arr($arGroups);
									$result_intersect = array_intersect($arGroupAvalaible, $arGroups);
									if(empty($result_intersect)) {									
									if($arResult['PROPERTIES']['OUTPUT_BANNER']['VALUE']) {
										$rsSection = CIBlockSection::GetList(array("SORT"=>"ASC"), array("IBLOCK_ID"=>$arResult["IBLOCK_ID"], "ID"=>$arResult["IBLOCK_SECTION_ID"]), false, array("UF_USER_GROUP"));
										while($arSection  = $rsSection->GetNext()) 
										{ 										  
										  $rsGender = CUserFieldEnum::GetList(array(), array(
											"ID" => $arSection["UF_USER_GROUP"],
										  ));
										  if($arGender = $rsGender->GetNext()) {
											//echo $arGender["VALUE"];
											if($arGender["VALUE"]=="Опт"){ //Опт Розница
												//echo "Нашли дешевле?";
												echo '<a href="'.$link_banner.'"><img src="/images/nashli-deshevle.png"></a>';
											}
										  }
										}
									}
									}
									?>
									<?//arr($arResult);?>
								</div>

                                <?/* if ($arResult['INSALE'] && $quantity): ?>
                                    <div class="have ok">

                                        <? if (!$arResult['INSALE']): ?>
                                            <table>
                                                <tr>
                                                    <td><span class="icon-ok"></span>&nbsp;Узнать о наличии:</td>
                                                    <td>
                                                        <? $APPLICATION->IncludeComponent("bitrix:main.include", ".default", array(
                                                            "AREA_FILE_SHOW" => "file",
                                                            "PATH" => SITE_DEFAULT_TEMPLATE_PATH . "include/phone1.php",
                                                            "EDIT_TEMPLATE" => ""
                                                        ), false
                                                        ); ?>
                                                    </td>
                                                </tr>
                                            </table>
                                        <? else: ?>
                                            <span class="icon-ok"></span>&nbsp;Есть в наличии&nbsp;
                                        <? endif ?>

                                        <? if ($arResult['INSALE']): ?>
                                            (<?= $quantity ?> шт.)
                                        <? endif ?>
                                    </div>
                                <? else: ?>
                                    <p class="have fail"><span class="icon-fail"></span> Нет в наличии</p>
                                <? endif */?>
                                </p>
                            </div>
                        </div>
                        <? /* todo не удалять?>
                            <div class="box-position" data-parametr="upakovki">
                                <div class="col1"><div class="kolvo"><?=round(1/$vUpakovkeShtuk, 3)?></div></div>
                                <div class="col2"><p class="easy"><span>Руб./упак.</span></p></div>
                                <div class="col2"><p class="easy2"><?=SaleFormatCurrency($vUpakovkePrice, 'RUB', true)?> Р</p></div>
                            </div>
                        <?*/ ?>
                        <div class="box-position quantity" data-parametr="shtuki">
                            <div class="col1">
                                <div class="minus">-</div>
                                <input class="kolvo" type="text" value="1" name="QUANTITY_<?= $arResult['ID'] ?>"
                                       data-price="<?= $retailOne ?>" data-place="detail"/>
                                <div class="plus">+</div>
                            </div>
                            <div class="col2"><p class="easy"><span>Руб./штук</span></p></div>
                            <div class="col2">
                                <? if ($priceBase > $priceDiscount && (($priceBase - $priceDiscount) >= $minPriceDiff)) { ?>
                                    <p class="easy2 priceDiscount"><?= SaleFormatCurrency($retailOne, 'RUB', true) ?> Р</p>
                                    <p class="easy2 priceBase"><?= SaleFormatCurrency($priceBase, 'RUB', true) ?> Р</p>
                                    <?
                                } else {
                                    ?>
                                    <p class="easy2 priceSingle"><?= SaleFormatCurrency($retailOne, 'RUB', true) ?> Р</p>
                                <? } ?>
                            </div>
                        </div>

                        <? if (!$hidePriceM2) { ?>
                            <div class="box-position" data-parametr="metri" itemprop="offers" itemscope itemtype="http://schema.org/Offer">
                                <div class="col1">
                                    <input class="kolvo" type="text" value="<?= round($visota * $shirina / 10000, 3) ?>"
                                           name="QUANTITY_<?= $arResult['ID'] ?>" data-place="detail"/>
                                </div>
                                <div class="col2"><p class="easy"><span>Руб./кв. м.</span></p></div>
                                <div class="col2">
                                    <? if ($priceBaseM2 > $priceDiscountM2 && (($priceBaseM2 - $priceDiscountM2) >= $minPriceDiff)) { ?>
                                        <p class="easy2 priceDiscount" itemprop="price" content="<?=$retailM2?>"><?= SaleFormatCurrency($retailM2, 'RUB', true) ?> <span itemprop="priceCurrency" content="RUR">Р</span></p>
                                        <p class="easy2 priceBase" itemprop="price" content="<?=$priceBaseM2?>"><?= SaleFormatCurrency($priceBaseM2, 'RUB', true) ?> <span itemprop="priceCurrency" content="RUR">Р</span></p>
                                        <?
                                    } else {
                                        ?>
                                        <p class="easy2 priceSingle"><?= SaleFormatCurrency($retailM2, 'RUB', true) ?> Р</p>
                                    <? } ?>
                                </div>
                                <?/*<p class="easy2" itemprop="price" content="<?=$retailM2?>"><?= SaleFormatCurrency($retailM2, 'RUB', true) ?> <span itemprop="priceCurrency" content="RUR">Р</span></p>*/?>
                            </div>
                        <? } ?>

                        <div class="row obj text-right finalSum">
                            <div class="col-sm-12">
                                <p class="price">Итого: <span><?= SaleFormatCurrency($retailOne, 'RUB', true) ?> Р</span></p>
                            </div>
                        </div>
                    </div>
                    <div class="box-right-bottom <?if(!$arResult['INSALE']):?>notsale<?endif;?>">
                        <!--если нет цены и пользователь оптовик, то не выводим конпки для заказа-->
                        <?if(empty($retailOne) && USER_TYPE == 'wholesale'):?>
                            <p> Этот товар не доступен для заказа</p>
                        <?else:?>
                            <button class="btn link-buttom <?if(!$arResult['INSALE']):?>notsale<?endif;?>" data-toggle="modal" data-target="#Modalfast">БЫСТРАЯ ПОКУПКА</button>
                            <a href="javascript:void(0);" class="product-basket-link a_product_delay" data-operation="delay"
                               data-itemid="<?= $arResult['ID'] ?>" data-place="detail"
                               data-parentid="<?= $arResult['IBLOCK_SECTION_ID'] ?>">Отложить</a>
                            <a href="javascript:void(0);" onclick="yaCounter14449306.reachGoal('cart'); return true;" class="product-basket-link a_to_basket" data-operation="buy"
                               data-itemid="<?= $arResult['ID'] ?>" data-place="detail"
                               data-parentid="<?= $arResult['IBLOCK_SECTION_ID'] ?>">В корзину</a>
                        <?endif;?>
                    </div>
                </div>
            </div>
            <div class="row obj collection-advantages">
                <? $APPLICATION->IncludeComponent("bitrix:main.include", ".default", array(
                    "AREA_FILE_SHOW" => "file",
                    "PATH" => SITE_DEFAULT_TEMPLATE_PATH . "include/plitka-advantage.php",
                    "EDIT_TEMPLATE" => ""
                ), false
                ); ?>
            </div>
            <div class="box-opis">
                <? if ($arResult['DETAIL_TEXT']) { ?>
                    <div class="easy-text"><?= $arResult['DETAIL_TEXT']; ?></div>
                <? } ?>
            </div>
        </div>
    </div>

<?

# отдаем в js параметры плитки для калькулятора
$relations = array();
$relations['V_UPAKOVKE_SHTUK'] = $vUpakovkeShtuk;
$relations['V_UPAKOVKE_PRICE'] = $vUpakovkePrice;
$relations['RETAIL_M2_PRICE'] = $retailM2;
$relations['RETAIL_ONE'] = $retailOne;
$relations['VISOTA'] = str_replace(',', '.', $visota) / 100;
$relations['SHIRINA'] = str_replace(',', '.', $shirina) / 100;
?>

<script>window.relations_tile = <?= json_encode($relations) ?></script><? ?>
