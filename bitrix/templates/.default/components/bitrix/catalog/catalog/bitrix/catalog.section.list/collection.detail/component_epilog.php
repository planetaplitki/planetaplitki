<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>
<?if($USER->isAdmin()):
    //echo "<pre>"; print_r($arResult); echo "</pre>";
    endif;?>
<?        
//echo __FILE__.' '.__LINE__.' '.microtime(true).'<br>';
global $collectionName;
$collectionName = $arResult['SECTION']['NAME'];

//pRU($arResult, 'all');
$APPLICATION->SetPageProperty('title', $arResult['SECTION']['NAME']);
$APPLICATION->SetTitle($arResult['SECTION']['NAME']);

$sectionInfo = $arResult['SECTION_INFO'];
$picture = $arResult['SECTION']['PICTURE'];
$morePhoto = $sectionInfo['UF_MORE_PHOTO'];

$idSection = $arResult['SECTION']['ID'];
$sectionsHeader = getElementSectionsHTML($idSection, 'section');

$description = $arResult['SECTION']['~DESCRIPTION'];
$sectionInfo = $arResult['SECTION_INFO'];
$similarCollecitonsStr = $sectionInfo['UF_SIMILAR_COLLECTIO'];    
?>

<?if( USER_TYPE == 'wholesale' ){?>

    <?          
    $APPLICATION->SetPageProperty('pageclass', 'catalog2_page');
    ?>

    <?$itemsByPoverhnost = getPoverhnostItems($idSection);?>

    <?if( eRU($itemsByPoverhnost) ){?>

        <?
        $count = count($itemsByPoverhnost);
        foreach( $itemsByPoverhnost as $type => $arrItemsId ){
            if( eRU($arrItemsId) ){

                $GLOBALS['arrFilterPoverhost'] = array();
                $GLOBALS['arrFilterPoverhost']['ID'] = $arrItemsId;
                ?>

                <div class="container obj50">
                    <div class="row">
                        <div class="col-sm-12">
                            <p class="t2"><?=$type;?></p>

                            <?$APPLICATION->IncludeComponent(
                                "bitrix:catalog.section",
                                "catalog",
                                array(
                                    "TEMPLATE_THEME" => "blue",
                                    "PRODUCT_DISPLAY_MODE" => "N",
                                    "ADD_PICT_PROP" => "-",
                                    "LABEL_PROP" => "-",
                                    "OFFER_ADD_PICT_PROP" => "FILE",
                                    "OFFER_TREE_PROPS" => "-",
                                    "PRODUCT_SUBSCRIPTION" => "N",
                                    "SHOW_DISCOUNT_PERCENT" => "N",
                                    "SHOW_OLD_PRICE" => "N",
                                    "SHOW_CLOSE_POPUP" => "N",
                                    "MESS_BTN_BUY" => "Купить",
                                    "MESS_BTN_ADD_TO_BASKET" => "В корзину",
                                    "MESS_BTN_SUBSCRIBE" => "Подписаться",
                                    "MESS_BTN_DETAIL" => "Подробнее",
                                    "MESS_NOT_AVAILABLE" => "Нет в наличии",
                                    "AJAX_MODE" => "N",
                                    "SEF_MODE" => "N",
                                    "IBLOCK_TYPE" => "1c_catalog",
                                    "IBLOCK_ID" => "34",
                                    "SECTION_ID" => "",
                                    "SECTION_CODE" => "",
                                    "SECTION_USER_FIELDS" => array(
                                        0 => "",
                                        1 => "",
                                    ),
                                    "ELEMENT_SORT_FIELD" => "sort",
                                    "ELEMENT_SORT_ORDER" => "asc",
                                    "ELEMENT_SORT_FIELD2" => "name",
                                    "ELEMENT_SORT_ORDER2" => "asc",
                                    "FILTER_NAME" => "arrFilterPoverhost",
                                    "INCLUDE_SUBSECTIONS" => "Y",
                                    "SHOW_ALL_WO_SECTION" => "Y",
                                    "SECTION_URL" => "#SITE_DIR#catalog/#SECTION_CODE_PATH#/",
                                    "DETAIL_URL" => "#SITE_DIR#catalog/#SECTION_CODE_PATH#/#ELEMENT_CODE#/",
                                    "BASKET_URL" => "/personal/basket.php",
                                    "ACTION_VARIABLE" => "action",
                                    "PRODUCT_ID_VARIABLE" => "id",
                                    "PRODUCT_QUANTITY_VARIABLE" => "quantity",
                                    "ADD_PROPERTIES_TO_BASKET" => "Y",
                                    "PRODUCT_PROPS_VARIABLE" => "prop",
                                    "PARTIAL_PRODUCT_PROPERTIES" => "N",
                                    "SECTION_ID_VARIABLE" => "SECTION_ID",
                                    "ADD_SECTIONS_CHAIN" => "N",
                                    "DISPLAY_COMPARE" => "N",
                                    "SET_TITLE" => "N",
                                    "SET_BROWSER_TITLE" => "N",
                                    "BROWSER_TITLE" => "-",
                                    "SET_META_KEYWORDS" => "N",
                                    "META_KEYWORDS" => "",
                                    "SET_META_DESCRIPTION" => "N",
                                    "META_DESCRIPTION" => "",
                                    "SET_LAST_MODIFIED" => "N",
                                    "USE_MAIN_ELEMENT_SECTION" => "N",
                                    "SET_STATUS_404" => "N",
                                    "PAGE_ELEMENT_COUNT" => "100",
                                    "LINE_ELEMENT_COUNT" => "3",
                                    "PROPERTY_CODE" => array(
                                        0 => "VYSOTA",
                                        1 => "SHIRINA",
                                        2 => "",
                                    ),
                                    "OFFERS_FIELD_CODE" => "",
                                    "OFFERS_PROPERTY_CODE" => "",
                                    "OFFERS_SORT_FIELD" => "sort",
                                    "OFFERS_SORT_ORDER" => "asc",
                                    "OFFERS_SORT_FIELD2" => "active_from",
                                    "OFFERS_SORT_ORDER2" => "desc",
                                    "OFFERS_LIMIT" => "5",
                                    "BACKGROUND_IMAGE" => "-",
                                    "PRICE_CODE" => $arParams['PRICE_CODE'],
                                    "USE_PRICE_COUNT" => "N",
                                    "SHOW_PRICE_COUNT" => "1",
                                    "PRICE_VAT_INCLUDE" => "Y",
                                    "PRODUCT_PROPERTIES" => array(
                                    ),
                                    "USE_PRODUCT_QUANTITY" => "Y",
                                    "CACHE_TYPE" => "A",
                                    "CACHE_TIME" => "36000000",
                                    "CACHE_FILTER" => "Y",
                                    "CACHE_GROUPS" => "Y",
                                    "DISPLAY_TOP_PAGER" => "N",
                                    "DISPLAY_BOTTOM_PAGER" => "N",
                                    "PAGER_TITLE" => "Товары",
                                    "PAGER_SHOW_ALWAYS" => "N",
                                    "PAGER_TEMPLATE" => "infinity",
                                    "PAGER_DESC_NUMBERING" => "N",
                                    "PAGER_DESC_NUMBERING_CACHE_TIME" => "36000",
                                    "PAGER_SHOW_ALL" => "N",
                                    "HIDE_NOT_AVAILABLE" => "N",
                                    "OFFERS_CART_PROPERTIES" => "",
                                    "AJAX_OPTION_JUMP" => "N",
                                    "AJAX_OPTION_STYLE" => "Y",
                                    "AJAX_OPTION_HISTORY" => "N",
                                    "CONVERT_CURRENCY" => "Y",
                                    "CURRENCY_ID" => "RUB",
                                    "ADD_TO_BASKET_ACTION" => "ADD",
                                    "PAGER_BASE_LINK_ENABLE" => "Y",
                                    "SHOW_404" => "N",
                                    "MESSAGE_404" => "",
                                    "PAGER_BASE_LINK" => "",
                                    "PAGER_PARAMS_NAME" => "arrPager",
                                    "COMPONENT_TEMPLATE" => "catalog",
                                    "MESS_BTN_COMPARE" => "Сравнить",
                                    "UF_TYPE" => "recommended",
                                    "AJAX_OPTION_ADDITIONAL" => "",
                                    "FILE_404" => "",
                                    "DISABLE_INIT_JS_IN_COMPONENT" => "N",
                                    "UF_USER_TYPE" => USER_TYPE
                                ),
                                $component
                            );?>

                        </div>
                    </div>
                </div>
                <?}
        }?>

        <?}?>

    <?}
elseif( USER_TYPE == 'retail' ){   

    $componentParams = array(
        "TEMPLATE_THEME" => "blue",
        "PRODUCT_DISPLAY_MODE" => "N",
        "ADD_PICT_PROP" => "-",
        "LABEL_PROP" => "-",
        "OFFER_ADD_PICT_PROP" => "FILE",
        "OFFER_TREE_PROPS" => "-",
        "PRODUCT_SUBSCRIPTION" => "N",
        "SHOW_DISCOUNT_PERCENT" => "N",
        "SHOW_OLD_PRICE" => "N",
        "SHOW_CLOSE_POPUP" => "N",
        "MESS_BTN_BUY" => "Купить",
        "MESS_BTN_ADD_TO_BASKET" => "В корзину",
        "MESS_BTN_SUBSCRIBE" => "Подписаться",
        "MESS_BTN_DETAIL" => "Подробнее",
        "MESS_NOT_AVAILABLE" => "Нет в наличии",
        "AJAX_MODE" => "N",
        "SEF_MODE" => "N",
        "IBLOCK_TYPE" => "1c_catalog",
        "IBLOCK_ID" => "34",
        "SECTION_ID" => "",
        "SECTION_CODE" => "",
        "SECTION_USER_FIELDS" => array(
            0 => "",
            1 => "",
        ),
        "ELEMENT_SORT_FIELD" => "id",
        "ELEMENT_SORT_ORDER" => "asc",
        "ELEMENT_SORT_FIELD2" => "name",
        "ELEMENT_SORT_ORDER2" => "asc",
        "FILTER_NAME" => "arrFilterPoverhost",
        "INCLUDE_SUBSECTIONS" => "Y",
        "SHOW_ALL_WO_SECTION" => "Y",
        "SECTION_URL" => "#SITE_DIR#catalog/#SECTION_CODE_PATH#/",
        "DETAIL_URL" => "#SITE_DIR#catalog/#SECTION_CODE_PATH#/#ELEMENT_CODE#/",
        "BASKET_URL" => "/personal/basket.php",
        "ACTION_VARIABLE" => "action",
        "PRODUCT_ID_VARIABLE" => "id",
        "PRODUCT_QUANTITY_VARIABLE" => "quantity",
        "ADD_PROPERTIES_TO_BASKET" => "Y",
        "PRODUCT_PROPS_VARIABLE" => "prop",
        "PARTIAL_PRODUCT_PROPERTIES" => "N",
        "SECTION_ID_VARIABLE" => "SECTION_ID",
        "ADD_SECTIONS_CHAIN" => "N",
        "DISPLAY_COMPARE" => "N",
        "SET_TITLE" => "N",
        "SET_BROWSER_TITLE" => "N",
        "BROWSER_TITLE" => "-",
        "SET_META_KEYWORDS" => "N",
        "META_KEYWORDS" => "",
        "SET_META_DESCRIPTION" => "N",
        "META_DESCRIPTION" => "",
        "SET_LAST_MODIFIED" => "N",
        "USE_MAIN_ELEMENT_SECTION" => "N",
        "SET_STATUS_404" => "N",
        "PAGE_ELEMENT_COUNT" => "1000",
        "LINE_ELEMENT_COUNT" => "3",
        "PROPERTY_CODE" => array(
            0 => "VYSOTA",
            1 => "SHIRINA",
            2 => "",
        ),
        "OFFERS_FIELD_CODE" => "",
        "OFFERS_PROPERTY_CODE" => "",
        "OFFERS_SORT_FIELD" => "sort",
        "OFFERS_SORT_ORDER" => "asc",
        "OFFERS_SORT_FIELD2" => "active_from",
        "OFFERS_SORT_ORDER2" => "desc",
        "OFFERS_LIMIT" => "5",
        "BACKGROUND_IMAGE" => "-",
        "PRICE_CODE" => $arParams['PRICE_CODE'],
        "USE_PRICE_COUNT" => "N",
        "SHOW_PRICE_COUNT" => "1",
        "PRICE_VAT_INCLUDE" => "Y",
        "PRODUCT_PROPERTIES" => array(
        ),
        "USE_PRODUCT_QUANTITY" => "Y",
        "CACHE_TYPE" => "A",
        "CACHE_TIME" => "36000000",
        "CACHE_FILTER" => "Y",
        "CACHE_GROUPS" => "Y",
        "DISPLAY_TOP_PAGER" => "N",
        "DISPLAY_BOTTOM_PAGER" => "N",
        "PAGER_TITLE" => "Товары",
        "PAGER_SHOW_ALWAYS" => "N",
        "PAGER_TEMPLATE" => "infinity",
        "PAGER_DESC_NUMBERING" => "N",
        "PAGER_DESC_NUMBERING_CACHE_TIME" => "36000",
        "PAGER_SHOW_ALL" => "N",
        "HIDE_NOT_AVAILABLE" => "N",
        "OFFERS_CART_PROPERTIES" => "",
        "AJAX_OPTION_JUMP" => "N",
        "AJAX_OPTION_STYLE" => "Y",
        "AJAX_OPTION_HISTORY" => "N",
        "CONVERT_CURRENCY" => "Y",
        "CURRENCY_ID" => "RUB",
        "ADD_TO_BASKET_ACTION" => "ADD",
        "PAGER_BASE_LINK_ENABLE" => "Y",
        "SHOW_404" => "N",
        "MESSAGE_404" => "",
        "PAGER_BASE_LINK" => "",
        "PAGER_PARAMS_NAME" => "arrPager",
        "COMPONENT_TEMPLATE" => "catalog",
        "MESS_BTN_COMPARE" => "Сравнить",
        "UF_TYPE" => "recommended",
        "AJAX_OPTION_ADDITIONAL" => "",
        "FILE_404" => "",
        "DISABLE_INIT_JS_IN_COMPONENT" => "N"
    );

    ?>
    <div class="hidden-xs">
        <div class="container">
            <div class="row obj50">
                <div class="col-xs-12">
                    <ul class="nav nav-tabs" role="tablist">
                        <li class="active"><a href="#itme" role="tab" data-toggle="tab">Элементы коллекции</a></li>
                        <?if (!empty($sectionInfo['~UF_FOR_PDF'])):?>
                            <li><a href="#pdf-catalog" role="tab" data-toggle="tab">PDF каталог</a></li>
                            <?endif;?>
                        <?/*if( $description ){?>
                        <li><a href="#description" role="tab" data-toggle="tab">Описание</a></li>
                        <?}*/?>
                    </ul>
                    <div class="m-sharing-col">
                        <script src="//yastatic.net/es5-shims/0.0.2/es5-shims.min.js"></script>
                        <script src="//yastatic.net/share2/share.js"></script>
                        <div class="ya-share2" data-services="collections,vkontakte,facebook,odnoklassniki,gplus,twitter,viber,                                whatsapp,skype,telegram" data-size="s"></div>
                    </div>
                    <div class="tab-content">
                        <div class="tab-pane fade in active" id="itme">
                            <div class="box-position1">
                                <?$itemsByPoverhnost = getPoverhnostItems($idSection);
                                //echo '<pre>'; print_r($itemsByPoverhnost); echo '</pre>';
                                ?>
                                <?$APPLICATION->IncludeComponent(
                                    "bitrix:catalog.section",
                                    "collection",                                                              
                                    array_merge($componentParams, array("SECTION_ID" => $idSection)),
                                    $component
                                );?>    
                            </div>
                            <?
                            /*if( 0 ){?>

                            <div class="box-position1">
                            <?
                            $i = 1;
                            $count = count($itemsByPoverhnost);       
                            foreach( $itemsByPoverhnost as $type => $arrItemsId ){
                            if( eRU($arrItemsId) ){

                            $GLOBALS['arrFilterPoverhost'] = array();
                            $GLOBALS['arrFilterPoverhost']['ID'] = $arrItemsId;
                            ?>

                            <h2><?=$type;?></h2>

                            <div class="row obj25">

                            <?$APPLICATION->IncludeComponent(
                            "bitrix:catalog.section",
                            "catalog",                                                              
                            $componentParams,
                            $component
                            );?>
                            </div>

                            <?if( $i != $count ){?>
                            <hr class="line">
                            <?}?>

                            <?$i++;?>
                            <?}
                            }?>
                            </div>
                            <?}*/?>
                        </div>
                        <?if (!empty($sectionInfo['~UF_FOR_PDF'])):?>
                            <div class="tab-pane" id="pdf-catalog">
                                <div class="box-position1">
                                    <?=$sectionInfo['~UF_FOR_PDF'];?>
                                </div>
                            </div>
                            <?endif;?>
                        <?/*if( $description ){?>

                        <div class="tab-pane fade " id="description">
                        <div class="box-opis">
                        <p class="title">Описание</p>
                        <div class="easy-text"><?=$description?></div>
                        </div>
                        </div>
                        <?}*/?>

                    </div>
                </div>
            </div>

            <?                                                
            $similarCollecitons = getSimilarCollections($similarCollecitonsStr);           
            ?>

            <?//3d-options block?> 

            <?if($arResult["3D_OPTIONS"]):?>
                <div class="box-position2 box-position-custom">
                    <div class="title"><?='Варианты 3d раскладки коллекции '.$arResult["SECTION"]["NAME"]?></div>
                    <div class="row obj25">

                        <?foreach ($arResult["3D_OPTIONS"] as $item_custom):?>
                            <div class="col-xs-6 col-sm-3 col-md-3 usligi-pict usligi-pict-custom">
                                <a href="<?=$item_custom['SECTION_PAGE_URL']?>" style="max-width: 100%;max-height: 198px;" class="">
                                    <img src="<?=$item_custom["PICTURE_SRC"];?>"  style="max-width: 100%;max-height: 198px;" >

                                </a>

                                <div class="box_under">
                                    <p class="text-pict">
                                        <a href="<? echo $item_custom['SECTION_PAGE_URL'];?>">
                                            <?=$item_custom['NAME']; ?>
                                        </a>
                                    </p>
                                    <p class="easy">
                                        <?=$item_custom["UF_RASKLADKA"];?>
                                    </p>



                                </div>

                            </div>
                            <?endforeach;?>

                    </div>
                </div>
                <?endif;?>
            <?if( eRU($similarCollecitons) ){?>
                <div class="box-position2 padding-modified">
                    <div class="title">Похожие коллекции</div>
                    <div class="row obj25">

                        <?
                        $GLOBALS['arrFilter'] = array();
                        $GLOBALS['arrFilter']['DEPTH_LEVEL'] = 3;
                        $GLOBALS['arrFilter']['XML_ID'] = $similarCollecitons;
                        ?>

                        <?$APPLICATION->IncludeComponent(
                            "up:iblock.section.list",
                            "collections.list",
                            array(
                                "ID" => "0",
                                "SHOW_PARENT_NAME" => "Y",
                                "IBLOCK_TYPE" => "1c_catalog",
                                "IBLOCK_ID" => "34",
                                "SECTION_ID" => "",
                                "SECTION_CODE" => "",
                                "SECTION_URL" => "",
                                "COUNT_ELEMENTS" => "Y",
                                "TOP_DEPTH" => "3",
                                "SECTION_FIELDS" => array(
                                    0 => "ID",
                                    1 => "NAME",
                                    2 => "PICTURE",
                                    3 => "SECTION_PAGE_URL",
                                    4 => "DEPTH_LEVEL",
                                    5 => "IBLOCK_SECTION_ID",
                                ),
                                "SECTION_USER_FIELDS" => array(
                                    0 => "UF_MIN_PRICE",
                                    1 => "",
                                ),
                                "ADD_SECTIONS_CHAIN" => "Y",
                                "CACHE_TYPE" => "A",
                                "CACHE_TIME" => "36000000",
                                "CACHE_NOTES" => "",
                                "CACHE_GROUPS" => "Y",
                                "COMPONENT_TEMPLATE" => "collections.list",
                                "FILTER_NAME" => "arrFilter",
                                "SORT_FIELD" => "sort",
                                "SORT_ORDER" => "asc",
                                "SORT_FIELD2" => "id",
                                "SORT_ORDER2" => "desc",
                                "URL_404" => "/404.php",
                                "ELEMENT_CONTROLS" => "N",
                                "AJAX_MODE" => "N",
                                "AJAX_OPTION_JUMP" => "N",
                                "AJAX_OPTION_STYLE" => "N",
                                "AJAX_OPTION_HISTORY" => "N",
                                "AJAX_OPTION_ADDITIONAL" => "",
                                "CACHE_FILTER" => "Y",
                                "PAGER_TEMPLATE" => "modern",
                                "DISPLAY_TOP_PAGER" => "N",
                                "DISPLAY_BOTTOM_PAGER" => "N",
                                "PAGER_TITLE" => "",
                                "PAGER_SHOW_ALWAYS" => "N",
                                "PAGER_DESC_NUMBERING" => "N",
                                "PAGER_DESC_NUMBERING_CACHE_TIME" => "36000",
                                "PAGE_ELEMENT_COUNT" => "4",
                                "UF_TYPE" => "similar"
                            ),
                            false
                        );?>
                    </div>
                </div>
                <?}?>

            <?if( $description ){?>
                <div>
                    <div class="box-opis">
                        <div class="easy-text"><?=$description?></div>
                    </div>
                </div>
                <?}?>
        </div>
    </div>

    <div class="hidden-sm hidden-md hidden-lg cont2">
        <div class="panel-group" id="accordion">
            <div class="panel">
                <a data-toggle="collapse" data-parent="#accordion" href="#collapse1">
                    <div class="title-heading">
                        <h4 class="title">Галерея</h4>
                        <div class="point"></div>
                    </div>
                </a>
                <div id="collapse1" class="panel-collapse collapse in">

                    <?if( $picture || eRU($morePhoto) ){?>
                        <div class="fotorama">
                            <?if( $picture ){
                                $s = i($picture, 115,115, BX_RESIZE_IMAGE_EXACT);
                                $b = CFile::GetPath($picture);
                                ?>
                                <a href="<?=$b;?>"><img src="<?=$s;?>"  width="115" height="115"></a>
                                <?
                            }?>
                            <?if( eRU($morePhoto) ){
                                foreach($morePhoto as $photo){
                                    $s = i($photo, 115,115, BX_RESIZE_IMAGE_EXACT);
                                    $b = CFile::GetPath($photo);
                                    ?>
                                    <a href="<?=$b;?>"><img src="<?=$s;?>"  width="115" height="115"></a>
                                    <?
                                }
                            }?>
                        </div>
                        <?}?>

                    <div class="menu-link"><?=$sectionsHeader['MOBILE']?></div>
                </div>
            </div>
            <?if( $description ){?>
                <div class="panel">
                    <a data-toggle="collapse" data-parent="#accordion" href="#collapse2">
                        <div class="title-heading">
                            <h4 class="title">Описание</h4>
                            <div class="point"></div>
                        </div>
                    </a>
                    <div id="collapse2" class="panel-collapse collapse">
                        <div class="box-opis">
                            <p class="title">Описание</p>
                            <div class="easy-text"><?=$description?></div>
                        </div>
                    </div>
                </div>
                <?}?>

            <?if( eRU($itemsByPoverhnost) ){?>
                <?$APPLICATION->IncludeComponent(
                    "bitrix:catalog.section",
                    "catalog-mobile",
                    array_merge($componentParams, array("UF_TYPE" => "otheritems.mobile", "SECTION_ID" => $idSection)),
                    $component
                );?>
                <?}?>

            <!-- похожие коллекции -->
            <?if( eRU($similarCollecitons) ){?>
                <?                     
                $GLOBALS['arrFilter'] = array();
                $GLOBALS['arrFilter']['DEPTH_LEVEL'] = 3;
                $GLOBALS['arrFilter']['XML_ID'] = $similarCollecitons;
                ?>

                <?$APPLICATION->IncludeComponent(
                    "up:iblock.section.list",
                    "collections.list",
                    array(
                        "SHOW_PARENT_NAME" => "Y",
                        "IBLOCK_TYPE" => "1c_catalog",
                        "IBLOCK_ID" => "34",
                        "SECTION_ID" => "",
                        "SECTION_CODE" => "",
                        "SECTION_URL" => "",
                        "COUNT_ELEMENTS" => "Y",
                        "TOP_DEPTH" => "3",
                        "SECTION_FIELDS" => array(
                            0 => "ID",
                            1 => "NAME",
                            2 => "PICTURE",
                            3 => "SECTION_PAGE_URL",
                            4 => "DEPTH_LEVEL",
                            5 => "IBLOCK_SECTION_ID",
                        ),
                        "SECTION_USER_FIELDS" => array(
                            0 => "UF_MIN_PRICE",
                            1 => "",
                        ),
                        "ADD_SECTIONS_CHAIN" => "Y",
                        "CACHE_TYPE" => "A",
                        "CACHE_TIME" => "36000000",
                        "CACHE_NOTES" => "",
                        "CACHE_GROUPS" => "Y",
                        "COMPONENT_TEMPLATE" => "collections.list",
                        "FILTER_NAME" => "arrFilter",
                        "SORT_FIELD" => "sort",
                        "SORT_ORDER" => "asc",
                        "SORT_FIELD2" => "id",
                        "SORT_ORDER2" => "desc",
                        "URL_404" => "/404.php",
                        "ELEMENT_CONTROLS" => "N",
                        "AJAX_MODE" => "N",
                        "AJAX_OPTION_JUMP" => "N",
                        "AJAX_OPTION_STYLE" => "N",
                        "AJAX_OPTION_HISTORY" => "N",
                        "AJAX_OPTION_ADDITIONAL" => "",
                        "CACHE_FILTER" => "Y",
                        "PAGER_TEMPLATE" => "modern",
                        "DISPLAY_TOP_PAGER" => "N",
                        "DISPLAY_BOTTOM_PAGER" => "N",
                        "PAGER_TITLE" => "",
                        "PAGER_SHOW_ALWAYS" => "N",
                        "PAGER_DESC_NUMBERING" => "N",
                        "PAGER_DESC_NUMBERING_CACHE_TIME" => "36000",
                        "PAGE_ELEMENT_COUNT" => "4",
                        "UF_TYPE" => "similar.mobile"
                    ),
                    false
                );?>
                <?}?>
            <!-- похожие коллекции -->
        </div>

        <div class="buttons">
            <a href="<?=SITE_DIR?>catalog/" class="btn link-buttom2">ПЕРЕЙТИ В КАТАЛОГ</a>
            <button type="button" class="btn link-up">НАВЕРХ<span class="icon-up"></span></button>
        </div>
    </div>
    <?}?>
<?   
# делаем запись о просмотре коллекции
$viewedObj = new CViewedCollectionsRU();
$viewedObj->setViewedCollections($idSection);   
?>
<style>
    h2 {
        text-align: center;
        font-family: "DINPro-Bold", sans-serif;
        font-size: 22px;
        color: #000;
}</style>