<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $templateData */
/** @var @global CMain $APPLICATION */
use Bitrix\Main\Loader;
global $APPLICATION;
global $plitkaName;

$plitkaName = $arResult['NAME'];
$idSection = $arResult['IBLOCK_SECTION_ID'];
$similarCollecitonsStr = $arResult['SIMILAR_COLLECTIONS'];

$picture = $arResult['PICTURE'];
$morePhoto = $arResult['MORE_PHOTO'];
$elementSectionsInfo = $arResult['ELEMENT_SECTION_INFO'];

$poverhnost = $arResult['POVERHNOST'];
$visota = $arResult['DIMENSIONS']['VISOTA'];
$shirina = $arResult['DIMENSIONS']['SHIRINA'];

$retailPrice = $arResult['MIN_PRICE']['DISCOUNT_VALUE'];

# минимальная разница в ценах для отображения скидок 1руб
$minPriceDiff = 1;
$retailOne = $arResult['MIN_PRICE']['DISCOUNT_VALUE'];
$priceBase = $arResult['MIN_PRICE']['VALUE'];
$priceDiscount = $arResult['MIN_PRICE']['DISCOUNT_VALUE'];
?>

<style>
h2 {
    text-align: center;
    font-family: "DINPro-Bold", sans-serif;
    font-size: 22px;
    color: #000;
}</style>
<div class="hidden-xs">
    <div class="container">
        <?
        $itemsByPoverhnost = getPoverhnostItems($idSection, $arResult['ID']);    
        ?>

        <?if( eRU($itemsByPoverhnost) ){?>

            
                <?
                $i = 1;
                $count = count($itemsByPoverhnost); 
                $arrItemsId = array();
                foreach( $itemsByPoverhnost as $type => $arrItemsIdSect ){
                    if( eRU($arrItemsIdSect) ){
                        $arrItemsId = array_merge($arrItemsId, $arrItemsIdSect);
                    }
                }
                $id_el = array_search($arResult['ID'], $arrItemsId);
                unset($arrItemsId[$id_el]);
                
                $GLOBALS['arrFilterPoverhost'] = array();
                $GLOBALS['arrFilterPoverhost']['ID'] = $arrItemsId;     
                
                ?>
                <?if(!empty($arrItemsId)):?>
                    <div class="box-position1">
                        <div class="row obj25">
                            <?$APPLICATION->IncludeComponent(
                                "bitrix:catalog.section",
                                "collection",
                                array(
                                    "TEMPLATE_THEME" => "blue",
                                    "PRODUCT_DISPLAY_MODE" => "N",
                                    "ADD_PICT_PROP" => "-",
                                    "LABEL_PROP" => "-",
                                    "OFFER_ADD_PICT_PROP" => "FILE",
                                    "OFFER_TREE_PROPS" => "-",
                                    "PRODUCT_SUBSCRIPTION" => "N",
                                    "SHOW_DISCOUNT_PERCENT" => "N",
                                    "SHOW_OLD_PRICE" => "N",
                                    "SHOW_CLOSE_POPUP" => "N",
                                    "MESS_BTN_BUY" => "Купить",
                                    "MESS_BTN_ADD_TO_BASKET" => "В корзину",
                                    "MESS_BTN_SUBSCRIBE" => "Подписаться",
                                    "MESS_BTN_DETAIL" => "Подробнее",
                                    "MESS_NOT_AVAILABLE" => "Нет в наличии",
                                    "AJAX_MODE" => "N",
                                    "SEF_MODE" => "N",
                                    "IBLOCK_TYPE" => "1c_catalog",
                                    "IBLOCK_ID" => "34",
                                    "SECTION_ID" => "",
                                    "SECTION_CODE" => "",
                                    "SECTION_USER_FIELDS" => array(
                                        0 => "",
                                        1 => "",
                                    ),
                                    "ELEMENT_SORT_FIELD" => "sort",
                                    "ELEMENT_SORT_ORDER" => "asc",
                                    "OFFERS_SORT_FIELD2" => "active_from",
                                    "ELEMENT_SORT_ORDER2" => "asc",
                                    "FILTER_NAME" => "arrFilterPoverhost",
                                    "INCLUDE_SUBSECTIONS" => "Y",
                                    "SHOW_ALL_WO_SECTION" => "Y",
                                    "SECTION_URL" => "#SITE_DIR#catalog/#SECTION_CODE_PATH#/",
                                    "DETAIL_URL" => "#SITE_DIR#catalog/#SECTION_CODE_PATH#/#ELEMENT_CODE#/",
                                    "BASKET_URL" => "/personal/basket.php",
                                    "ACTION_VARIABLE" => "action",
                                    "PRODUCT_ID_VARIABLE" => "id",
                                    "PRODUCT_QUANTITY_VARIABLE" => "quantity",
                                    "ADD_PROPERTIES_TO_BASKET" => "Y",
                                    "PRODUCT_PROPS_VARIABLE" => "prop",
                                    "PARTIAL_PRODUCT_PROPERTIES" => "N",
                                    "SECTION_ID_VARIABLE" => "SECTION_ID",
                                    "ADD_SECTIONS_CHAIN" => "N",
                                    "DISPLAY_COMPARE" => "N",
                                    "SET_TITLE" => "N",
                                    "SET_BROWSER_TITLE" => "N",
                                    "BROWSER_TITLE" => "-",
                                    "SET_META_KEYWORDS" => "N",
                                    "META_KEYWORDS" => "",
                                    "SET_META_DESCRIPTION" => "N",
                                    "META_DESCRIPTION" => "",
                                    "SET_LAST_MODIFIED" => "N",
                                    "USE_MAIN_ELEMENT_SECTION" => "N",
                                    "SET_STATUS_404" => "N",
                                    "PAGE_ELEMENT_COUNT" => "1000",
                                    "LINE_ELEMENT_COUNT" => "3",
                                    "PROPERTY_CODE" => array(
                                        0 => "VYSOTA",
                                        1 => "SHIRINA",
                                    ),
                                    "OFFERS_FIELD_CODE" => "",
                                    "OFFERS_PROPERTY_CODE" => "",
                                    "OFFERS_SORT_FIELD" => "sort",
                                    "OFFERS_SORT_ORDER" => "asc",
                                    "OFFERS_SORT_FIELD2" => "active_from",
                                    "OFFERS_SORT_ORDER2" => "desc",
                                    "OFFERS_LIMIT" => "5",
                                    "BACKGROUND_IMAGE" => "-",
                                    "PRICE_CODE" => $arParams['PRICE_CODE'],
                                    "USE_PRICE_COUNT" => "N",
                                    "SHOW_PRICE_COUNT" => "1",
                                    "PRICE_VAT_INCLUDE" => "Y",
                                    "PRODUCT_PROPERTIES" => array(
                                    ),
                                    "USE_PRODUCT_QUANTITY" => "Y",
                                    "CACHE_TYPE" => "A",
                                    "CACHE_TIME" => "36000000",
                                    "CACHE_FILTER" => "Y",
                                    "CACHE_GROUPS" => "Y",
                                    "DISPLAY_TOP_PAGER" => "N",
                                    "DISPLAY_BOTTOM_PAGER" => "N",
                                    "PAGER_TITLE" => "Товары",
                                    "PAGER_SHOW_ALWAYS" => "N",
                                    "PAGER_TEMPLATE" => "infinity",
                                    "PAGER_DESC_NUMBERING" => "N",
                                    "PAGER_DESC_NUMBERING_CACHE_TIME" => "36000",
                                    "PAGER_SHOW_ALL" => "N",
                                    "HIDE_NOT_AVAILABLE" => "N",
                                    "OFFERS_CART_PROPERTIES" => "",
                                    "AJAX_OPTION_JUMP" => "N",
                                    "AJAX_OPTION_STYLE" => "Y",
                                    "AJAX_OPTION_HISTORY" => "N",
                                    "CONVERT_CURRENCY" => "Y",
                                    "CURRENCY_ID" => "RUB",
                                    "ADD_TO_BASKET_ACTION" => "ADD",
                                    "PAGER_BASE_LINK_ENABLE" => "Y",
                                    "SHOW_404" => "N",
                                    "MESSAGE_404" => "",
                                    "PAGER_BASE_LINK" => "",
                                    "PAGER_PARAMS_NAME" => "arrPager",
                                    "COMPONENT_TEMPLATE" => "catalog",
                                    "MESS_BTN_COMPARE" => "Сравнить",
                                    //"UF_TYPE" => "",
                                    "AJAX_OPTION_ADDITIONAL" => "",
                                    "FILE_404" => "",
                                    "DISABLE_INIT_JS_IN_COMPONENT" => "N",
                                    "UF_USER_TYPE" => USER_TYPE
                                ),
                                false
                            );?>
                        </div>

                        <?if( $i != $count ){?>
                            <hr class="line">
                        <?}?>

                        <?$i++;?>
                    </div>
                <?endif;?>                       
        <?}?>

        <?
        $similarCollecitons = getSimilarCollections($similarCollecitonsStr);
        ?>
        
        <?if( eRU($similarCollecitons) ){?>
            <div class="box-position2">
                <div class="title">Похожие коллекции</div>
                <div class="row obj25">
                    <?
                    $GLOBALS['arrFilter'] = array();
                    $GLOBALS['arrFilter']['DEPTH_LEVEL'] = 3;
                    $GLOBALS['arrFilter']['XML_ID'] = $similarCollecitons;
                    ?>
                    <?$APPLICATION->IncludeComponent(
                        "up:iblock.section.list",
                        "collections.list",
                        array(
                            "ID" => "0",
                            "SHOW_PARENT_NAME" => "Y",
                            "IBLOCK_TYPE" => "1c_catalog",
                            "IBLOCK_ID" => "34",
                            "SECTION_ID" => "",
                            "SECTION_CODE" => "",
                            "SECTION_URL" => "",
                            "COUNT_ELEMENTS" => "Y",
                            "TOP_DEPTH" => "3",
                            "SECTION_FIELDS" => array(
                                0 => "ID",
                                1 => "NAME",
                                2 => "PICTURE",
                                3 => "SECTION_PAGE_URL",
                                4 => "DEPTH_LEVEL",
                                5 => "IBLOCK_SECTION_ID",
                            ),
                            "SECTION_USER_FIELDS" => array(
                                0 => "UF_MIN_PRICE",
                                1 => "UF_MORE_PHOTO",
                            ),
                            "ADD_SECTIONS_CHAIN" => "N",
                            "CACHE_TYPE" => "A",
                            "CACHE_TIME" => "36000000",
                            "CACHE_NOTES" => "",
                            "CACHE_GROUPS" => "Y",
                            "COMPONENT_TEMPLATE" => "collections.list",
                            "FILTER_NAME" => "arrFilter",
                            "SORT_FIELD" => "sort",
                            "SORT_ORDER" => "asc",
                            "SORT_FIELD2" => "id",
                            "SORT_ORDER2" => "desc",
                            "URL_404" => "/404.php",
                            "ELEMENT_CONTROLS" => "N",
                            "AJAX_MODE" => "N",
                            "AJAX_OPTION_JUMP" => "N",
                            "AJAX_OPTION_STYLE" => "N",
                            "AJAX_OPTION_HISTORY" => "N",
                            "AJAX_OPTION_ADDITIONAL" => "",
                            "CACHE_FILTER" => "Y",
                            "PAGER_TEMPLATE" => "modern",
                            "DISPLAY_TOP_PAGER" => "N",
                            "DISPLAY_BOTTOM_PAGER" => "N",
                            "PAGER_TITLE" => "N",
                            "PAGER_SHOW_ALWAYS" => "N",
                            "PAGER_DESC_NUMBERING" => "N",
                            "PAGER_DESC_NUMBERING_CACHE_TIME" => "36000",
                            "PAGE_ELEMENT_COUNT" => "4",
                            "UF_TYPE" => "similar"
                        ),
                        false
                    );?>
                </div>
            </div>
        <?}?>
    </div>
</div>



<div class="hidden-sm hidden-md hidden-lg cont2">
    <div class="panel-group" id="accordion">
        <div class="panel">
            <a data-toggle="collapse" data-parent="#accordion" href="#collapse1">
                <div class="title-heading">
                    <h4 class="title">Галерея</h4>
                    <div class="point"></div>
                </div>
            </a>
            <div id="collapse1" class="panel-collapse collapse in">
                <div class="fotorama">
                    <?if( $picture || eRU($morePhoto) ){?>
                        <div class="fotorama" data-nav="thumbs" data-arrows="true">
                            <?if( $picture ){
                                $s = i($picture, 128,128, BX_RESIZE_IMAGE_EXACT);
                                $b = CFile::GetPath($picture);
                                ?>
                                <a href="<?=$b;?>"><img src="<?=$s;?>"  width="128" height="128"></a>
                                <?
                            }?>
                            <?if( eRU($morePhoto) ){
                                foreach($morePhoto as $photo){
                                    $s = i($photo, 128,128, BX_RESIZE_IMAGE_EXACT);
                                    $b = CFile::GetPath($photo);
                                    ?>
                                    <a href="<?=$b;?>"><img src="<?=$s;?>"  width="128" height="128"></a>
                                <?}
                            }?>
                        </div>
                    <?}?>
                </div>
                <div class="menu-link">
                    <?=$elementSectionsInfo['MOBILE']?>
                </div>
                <div class="container">

                    <??>
                    <div class="mobileQuantity">
                        <div class="box-position quantity" data-parametr="shtuki">
                            <div class="col1">
                                <div class="minus">-</div>
                                <input class="kolvo" type="text" value="1" name="QUANTITY_<?= $arResult['ID'] ?>"
                                       data-price="<?= $retailOne ?>" data-place="detail"/>
                                <div class="plus">+</div>
                            </div>
                            <div class="col2"><p class="easy"><span>Руб./штук</span></p></div>
                            <div class="col2">
                                <? if ($priceBase > $priceDiscount && (($priceBase - $priceDiscount) >= $minPriceDiff)) { ?>
                                    <p class="easy2 priceDiscount"><?= SaleFormatCurrency($retailOne, 'RUB', true) ?> Р</p>
                                    <p class="easy2 priceBase"><?= SaleFormatCurrency($priceBase, 'RUB', true) ?> Р</p>
                                    <?
                                } else {
                                    ?>
                                    <p class="easy2 priceSingle"><?= SaleFormatCurrency($retailOne, 'RUB', true) ?> Р</p>
                                <? } ?>
                            </div>
                        </div>

                        <? if (!$hidePriceM2) { ?>
                            <div class="box-position" data-parametr="metri">
                                <div class="col1">
                                    <input class="kolvo" type="text" value="<?= round($visota * $shirina / 10000, 3) ?>"
                                           name="QUANTITY_<?= $arResult['ID'] ?>" data-place="detail"/>
                                </div>
                                <div class="col2"><p class="easy"><span>Руб./кв. м.</span></p></div>
                                <div class="col2"><p class="easy2"><?= SaleFormatCurrency($retailM2, 'RUB', true) ?> Р</p></div>
                            </div>
                        <? } ?>
                    </div>
                    <??>

                    <div class="row obj">
                        <div class="col-xs-6">
                            <p class="easy mobilePrice">
                                Цена: <span><?= SaleFormatCurrency($retailPrice, 'RUB', true) ?> руб.</span>
                            </p>
                            <p class="easy">
                                <?if( $visota && $shirina ){?>
                                    Размер: <span><?=$visota;?>x<?=$shirina;?></span><br>
                                <?}?>
                                <?if( $poverhnost ){?>
                                    Поверхность: <span><?=$poverhnost;?></span>
                                <?}?>
                            </p>
                        </div>
                        <div class="col-xs-6">
                            <button type="button" class="btn link-buttom" data-toggle="modal" data-target="#Modalfast">БЫСТРАЯ ПОКУПКА</button>
                            <a href="javascript:void(0);" class="buy" data-operation="buy" data-itemid="<?=$arResult['ID']?>" data-place="detail" data-parentid="<?=$arResult['IBLOCK_SECTION_ID']?>" >Положить в корзину</a>
                            <a href="javascript:void(0);" class="cart" data-operation="delay" data-itemid="<?=$arResult['ID']?>" data-place="detail" data-parentid="<?=$arResult['IBLOCK_SECTION_ID']?>" >Отложить</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <!-- другие плитки данной коллекции -->

        <?if( eRU($itemsByPoverhnost) && !empty($arrItemsId) ){?>




                                    <?$APPLICATION->IncludeComponent(
                                        "bitrix:catalog.section",
                                        "catalog-mobile",
                                        array(
                                            "TEMPLATE_THEME" => "blue",
                                            "PRODUCT_DISPLAY_MODE" => "N",
                                            "ADD_PICT_PROP" => "-",
                                            "LABEL_PROP" => "-",
                                            "OFFER_ADD_PICT_PROP" => "FILE",
                                            "OFFER_TREE_PROPS" => "-",
                                            "PRODUCT_SUBSCRIPTION" => "N",
                                            "SHOW_DISCOUNT_PERCENT" => "N",
                                            "SHOW_OLD_PRICE" => "N",
                                            "SHOW_CLOSE_POPUP" => "N",
                                            "MESS_BTN_BUY" => "Купить",
                                            "MESS_BTN_ADD_TO_BASKET" => "В корзину",
                                            "MESS_BTN_SUBSCRIBE" => "Подписаться",
                                            "MESS_BTN_DETAIL" => "Подробнее",
                                            "MESS_NOT_AVAILABLE" => "Нет в наличии",
                                            "AJAX_MODE" => "N",
                                            "SEF_MODE" => "N",
                                            "IBLOCK_TYPE" => "1c_catalog",
                                            "IBLOCK_ID" => "34",
                                            "SECTION_ID" => "",
                                            "SECTION_CODE" => "",
                                            "SECTION_USER_FIELDS" => array(
                                                0 => "",
                                                1 => "",
                                            ),
                                            "ELEMENT_SORT_FIELD" => "sort",
                                            "ELEMENT_SORT_ORDER" => "asc",
                                            "ELEMENT_SORT_FIELD2" => "name",
                                            "ELEMENT_SORT_ORDER2" => "asc",
                                            "FILTER_NAME" => "arrFilterPoverhost",
                                            "INCLUDE_SUBSECTIONS" => "Y",
                                            "SHOW_ALL_WO_SECTION" => "Y",
                                            "SECTION_URL" => "#SITE_DIR#catalog/#SECTION_CODE_PATH#/",
                                            "DETAIL_URL" => "#SITE_DIR#catalog/#SECTION_CODE_PATH#/#ELEMENT_CODE#/",
                                            "BASKET_URL" => "/personal/basket.php",
                                            "ACTION_VARIABLE" => "action",
                                            "PRODUCT_ID_VARIABLE" => "id",
                                            "PRODUCT_QUANTITY_VARIABLE" => "quantity",
                                            "ADD_PROPERTIES_TO_BASKET" => "Y",
                                            "PRODUCT_PROPS_VARIABLE" => "prop",
                                            "PARTIAL_PRODUCT_PROPERTIES" => "N",
                                            "SECTION_ID_VARIABLE" => "SECTION_ID",
                                            "ADD_SECTIONS_CHAIN" => "N",
                                            "DISPLAY_COMPARE" => "N",
                                            "SET_TITLE" => "N",
                                            "SET_BROWSER_TITLE" => "N",
                                            "BROWSER_TITLE" => "-",
                                            "SET_META_KEYWORDS" => "N",
                                            "META_KEYWORDS" => "",
                                            "SET_META_DESCRIPTION" => "N",
                                            "META_DESCRIPTION" => "",
                                            "SET_LAST_MODIFIED" => "N",
                                            "USE_MAIN_ELEMENT_SECTION" => "N",
                                            "SET_STATUS_404" => "N",
                                            "PAGE_ELEMENT_COUNT" => "20",
                                            "LINE_ELEMENT_COUNT" => "3",
                                            "PROPERTY_CODE" => array(
                                                0 => "VYSOTA",
                                                1 => "SHIRINA",
                                            ),
                                            "OFFERS_FIELD_CODE" => "",
                                            "OFFERS_PROPERTY_CODE" => "",
                                            "OFFERS_SORT_FIELD" => "sort",
                                            "OFFERS_SORT_ORDER" => "asc",
                                            "OFFERS_SORT_FIELD2" => "active_from",
                                            "OFFERS_SORT_ORDER2" => "desc",
                                            "OFFERS_LIMIT" => "5",
                                            "BACKGROUND_IMAGE" => "-",
                                            "PRICE_CODE" => $arParams['PRICE_CODE'],
                                            "USE_PRICE_COUNT" => "N",
                                            "SHOW_PRICE_COUNT" => "1",
                                            "PRICE_VAT_INCLUDE" => "Y",
                                            "PRODUCT_PROPERTIES" => array(
                                            ),
                                            "USE_PRODUCT_QUANTITY" => "Y",
                                            "CACHE_TYPE" => "A",
                                            "CACHE_TIME" => "36000000",
                                            "CACHE_FILTER" => "Y",
                                            "CACHE_GROUPS" => "Y",
                                            "DISPLAY_TOP_PAGER" => "N",
                                            "DISPLAY_BOTTOM_PAGER" => "N",
                                            "PAGER_TITLE" => "Товары",
                                            "PAGER_SHOW_ALWAYS" => "N",
                                            "PAGER_TEMPLATE" => "infinity",
                                            "PAGER_DESC_NUMBERING" => "N",
                                            "PAGER_DESC_NUMBERING_CACHE_TIME" => "36000",
                                            "PAGER_SHOW_ALL" => "N",
                                            "HIDE_NOT_AVAILABLE" => "N",
                                            "OFFERS_CART_PROPERTIES" => "",
                                            "AJAX_OPTION_JUMP" => "N",
                                            "AJAX_OPTION_STYLE" => "Y",
                                            "AJAX_OPTION_HISTORY" => "N",
                                            "CONVERT_CURRENCY" => "Y",
                                            "CURRENCY_ID" => "RUB",
                                            "ADD_TO_BASKET_ACTION" => "ADD",
                                            "PAGER_BASE_LINK_ENABLE" => "Y",
                                            "SHOW_404" => "N",
                                            "MESSAGE_404" => "",
                                            "PAGER_BASE_LINK" => "",
                                            "PAGER_PARAMS_NAME" => "arrPager",
                                            "COMPONENT_TEMPLATE" => "catalog",
                                            "MESS_BTN_COMPARE" => "Сравнить",
                                            "AJAX_OPTION_ADDITIONAL" => "",
                                            "FILE_404" => "",
                                            "DISABLE_INIT_JS_IN_COMPONENT" => "N",
                                            "UF_TYPE" => "otheritems.mobile",
                                            "UF_USER_TYPE" => USER_TYPE
                                        ),
                                        false
                                    );?>

                    <?$i++;?>

        <?}?>
        <!-- другие плитки данной коллекции -->

        <!-- похожие коллекции -->
        <?if( eRU($similarCollecitons) ){?>
            <?
            $GLOBALS['arrFilter'] = array();
            $GLOBALS['arrFilter']['DEPTH_LEVEL'] = 3;
            $GLOBALS['arrFilter']['XML_ID'] = $similarCollecitons;
            ?>

            <?$APPLICATION->IncludeComponent(
                "up:iblock.section.list",
                "collections.list",
                array(
                    "SHOW_PARENT_NAME" => "Y",
                    "IBLOCK_TYPE" => "1c_catalog",
                    "IBLOCK_ID" => "34",
                    "SECTION_ID" => "",
                    "SECTION_CODE" => "",
                    "SECTION_URL" => "",
                    "COUNT_ELEMENTS" => "Y",
                    "TOP_DEPTH" => "3",
                    "SECTION_FIELDS" => array(
                        0 => "ID",
                        1 => "NAME",
                        2 => "PICTURE",
                        3 => "SECTION_PAGE_URL",
                        4 => "DEPTH_LEVEL",
                        5 => "IBLOCK_SECTION_ID",
                    ),
                    "SECTION_USER_FIELDS" => array(
                        0 => "UF_MIN_PRICE",
                        1 => "",
                    ),
                    "ADD_SECTIONS_CHAIN" => "N",
                    "CACHE_TYPE" => "A",
                    "CACHE_TIME" => "36000000",
                    "CACHE_NOTES" => "",
                    "CACHE_GROUPS" => "Y",
                    "COMPONENT_TEMPLATE" => "collections.list",
                    "FILTER_NAME" => "arrFilter",
                    "SORT_FIELD" => "sort",
                    "SORT_ORDER" => "asc",
                    "SORT_FIELD2" => "id",
                    "SORT_ORDER2" => "desc",
                    "URL_404" => "/404.php",
                    "ELEMENT_CONTROLS" => "N",
                    "AJAX_MODE" => "N",
                    "AJAX_OPTION_JUMP" => "N",
                    "AJAX_OPTION_STYLE" => "N",
                    "AJAX_OPTION_HISTORY" => "N",
                    "AJAX_OPTION_ADDITIONAL" => "",
                    "CACHE_FILTER" => "Y",
                    "PAGER_TEMPLATE" => "modern",
                    "DISPLAY_TOP_PAGER" => "N",
                    "DISPLAY_BOTTOM_PAGER" => "N",
                    "PAGER_TITLE" => "N",
                    "PAGER_SHOW_ALWAYS" => "N",
                    "PAGER_DESC_NUMBERING" => "N",
                    "PAGER_DESC_NUMBERING_CACHE_TIME" => "36000",
                    "PAGE_ELEMENT_COUNT" => "4",
                    "UF_TYPE" => "similar.mobile"
                ),
                false
            );?>
        <?}?>
        <!-- похожие коллекции -->
    </div>
    <div class="buttons">
        <a href="<?=SITE_DIR?>catalog/" class="btn link-buttom2">ПЕРЕЙТИ В КАТАЛОГ</a>
        <button type="button" class="btn link-up">НАВЕРХ<span class="icon-up"></span></button>
    </div>
</div>

<div class="modal fade" id="Modalfast" tabindex="-1" role="dialog" aria-labelledby="ModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                <h4 class="modal-title" id="myModalLabel">Быстрая покупка</h4>
            </div>
            <div class="modal-body">
                <?if( array_key_exists('is_ajax', $_REQUEST) && $_REQUEST['is_ajax']=='y' ){
                    $APPLICATION->RestartBuffer();
                }?>

                <?$APPLICATION->IncludeComponent("up:click.buy", "catalog.item",
                    array(
                        'PICTURE' => $arResult['PICTURE'],
                        '1CLICK_ITEM_ID' => $arResult['ID'],
                        'PRICE' => $arResult['PRICE'],
                        'NAME' => $arResult['NAME'],
                    ),
                    false
                );?>

                <?if( array_key_exists('is_ajax', $_REQUEST) && $_REQUEST['is_ajax']=='y' ){
                    die();
                }?>
            </div>
        </div>
    </div>
</div>