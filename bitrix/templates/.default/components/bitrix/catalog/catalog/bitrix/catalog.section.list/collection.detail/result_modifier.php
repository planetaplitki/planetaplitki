<?
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();

$arViewModeList = array('LIST', 'LINE', 'TEXT', 'TILE');

$arDefaultParams = array(
    'VIEW_MODE' => 'LIST',
    'SHOW_PARENT_NAME' => 'Y',
    'HIDE_SECTION_NAME' => 'N'
);

$arParams = array_merge($arDefaultParams, $arParams);

if (!in_array($arParams['VIEW_MODE'], $arViewModeList))
    $arParams['VIEW_MODE'] = 'LIST';
if ('N' != $arParams['SHOW_PARENT_NAME'])
    $arParams['SHOW_PARENT_NAME'] = 'Y';
if ('Y' != $arParams['HIDE_SECTION_NAME'])
    $arParams['HIDE_SECTION_NAME'] = 'N';

$arResult['VIEW_MODE_LIST'] = $arViewModeList;

if (0 < $arResult['SECTIONS_COUNT'])
{
    if ('LIST' != $arParams['VIEW_MODE'])
    {
        $boolClear = false;
        $arNewSections = array();
        foreach ($arResult['SECTIONS'] as &$arOneSection)
        {
            if (1 < $arOneSection['RELATIVE_DEPTH_LEVEL'])
            {
                $boolClear = true;
                continue;
            }
            $arNewSections[] = $arOneSection;
        }
        unset($arOneSection);
        if ($boolClear)
        {
            $arResult['SECTIONS'] = $arNewSections;
            $arResult['SECTIONS_COUNT'] = count($arNewSections);
        }
        unset($arNewSections);
    }
}

if (0 < $arResult['SECTIONS_COUNT'])
{
    $boolPicture = false;
    $boolDescr = false;
    $arSelect = array('ID');
    $arMap = array();
    if ('LINE' == $arParams['VIEW_MODE'] || 'TILE' == $arParams['VIEW_MODE'])
    {
        reset($arResult['SECTIONS']);
        $arCurrent = current($arResult['SECTIONS']);
        if (!isset($arCurrent['PICTURE']))
        {
            $boolPicture = true;
            $arSelect[] = 'PICTURE';
        }
        if ('LINE' == $arParams['VIEW_MODE'] && !array_key_exists('DESCRIPTION', $arCurrent))
        {
            $boolDescr = true;
            $arSelect[] = 'DESCRIPTION';
            $arSelect[] = 'DESCRIPTION_TYPE';
        }
    }
    if ($boolPicture || $boolDescr)
    {
        foreach ($arResult['SECTIONS'] as $key => $arSection)
        {
            $arMap[$arSection['ID']] = $key;
        }
        $rsSections = CIBlockSection::GetList(array(), array('ID' => array_keys($arMap)), false, $arSelect);
        while ($arSection = $rsSections->GetNext())
        {
            if (!isset($arMap[$arSection['ID']]))
                continue;
            $key = $arMap[$arSection['ID']];
            if ($boolPicture)
            {                                                       
                $arSection['PICTURE'] = intval($arSection['PICTURE']);
                $arSection['PICTURE'] = (0 < $arSection['PICTURE'] ? CFile::GetFileArray($arSection['PICTURE']) : false);
                $arResult['SECTIONS'][$key]['PICTURE'] = $arSection['PICTURE'];
                $arResult['SECTIONS'][$key]['~PICTURE'] = $arSection['~PICTURE'];
            }
            if ($boolDescr)
            {
                $arResult['SECTIONS'][$key]['DESCRIPTION'] = $arSection['DESCRIPTION'];
                $arResult['SECTIONS'][$key]['~DESCRIPTION'] = $arSection['~DESCRIPTION'];
                $arResult['SECTIONS'][$key]['DESCRIPTION_TYPE'] = $arSection['DESCRIPTION_TYPE'];
                $arResult['SECTIONS'][$key]['~DESCRIPTION_TYPE'] = $arSection['~DESCRIPTION_TYPE'];
            }
        }
    }
}

$idSection = $arResult['SECTION']['ID'];
$sectionsArr = getSections();
$sectionInfo = $sectionsArr['COLLECTIONS'][$idSection];
$arResult['SECTION_INFO'] = $sectionInfo;

//pRU($sectionsArr, 'all');
//pRU($arResult, 'all');


/* $arSelect = Array("ID", "IBLOCK_ID", "NAME", "PREVIEW_PICTURE", "PREVIEW_TEXT", "DETAIL_PAGE_URL", "PROPERTY_COLLECTION.NAME");
$arFilter = Array("IBLOCK_ID"=>11, "PROPERTY_COLLECTION" => $arResult["SECTION"]["ID"], "ACTIVE_DATE"=>"Y", "ACTIVE"=>"Y");
$res = CIBlockElement::GetList(Array(), $arFilter, false, Array("nPageSize"=>50), $arSelect);
while($ob = $res->GetNextElement())
{
$arResult["3D_OPTIONS"][] = $ob->GetFields();

}
foreach($arResult["3D_OPTIONS"] as &$item_custom) {
$item_custom["PICTURE"] = CFile::GetPath($item_custom["PREVIEW_PICTURE"]);
$file = CFile::ResizeImageGet($item_custom['PREVIEW_PICTURE'], array('width'=>198, 'height'=>198), BX_RESIZE_IMAGE_EXACT, true);                
$item_custom["PICTURE"] = $file['src'];   
$res = CIBlockSection::GetByID($item_custom["IBLOCK_SECTION_ID"]);
if($ar_res = $res->GetNext())

$item_custom["SECTION"] = $ar_res['NAME'];     
$item_custom["SECTION_LINK"] = $ar_res['SECTION_PAGE_URL'];     
} */

//echo "<pre>"; print_r($arResult["3D_OPTIONS"]); echo "</pre>";
                     
$arFilter = Array('IBLOCK_ID'=>11, 'GLOBAL_ACTIVE'=>'Y', "UF_KOLLEKCIA" => $arResult["SECTION"]["ID"]);
$arSelect = array("ID", "IBLOCK_ID", "UF_COLLECTION_LINK", "UF_RASKLADKA", "PICTURE", "NAME", "SECTION_PAGE_URL");
$db_list = CIBlockSection::GetList(Array($by=>$order), $arFilter, true, $arSelect);
while($ar_result = $db_list->GetNext())
{
    $arResult["3D_OPTIONS"][] = $ar_result;
}  

foreach($arResult["3D_OPTIONS"] as &$item_custom) {
    $file = CFile::ResizeImageGet($item_custom['PICTURE'], array('width'=>198, 'height'=>198), BX_RESIZE_IMAGE_EXACT, true);                
    $item_custom["PICTURE_SRC"] = $file['src'];    
}
//echo "<pre>"; print_r($arResult["3D_OPTIONS"]); echo "</pre>";


if (is_object($this->__component)){
    $this->__component->SetResultCacheKeys(array('SECTION_INFO'));
    $this->__component->SetResultCacheKeys(array('3D_OPTIONS'));

}
?>