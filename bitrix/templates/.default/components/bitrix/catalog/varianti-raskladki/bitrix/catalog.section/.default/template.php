<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>

<?
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);
?>
<?if($USER->isAdmin()):
//echo "<pre>"; print_r($arResult); echo "</pre>";
endif;?>
<?
//$sections = getSections();
//$collections = $sections['COLLECTIONS'];

$idSection = $arResult['ITEMS'][0]['IBLOCK_SECTION_ID'];
if( CModule::IncludeModule('iblock') ){
    $arFilter = Array('IBLOCK_ID'=>$arParams['IBLOCK_ID'], "ID" => $idSection);
    $db_list = CIBlockSection::GetList(Array($by=>$order), $arFilter, true, array('UF_COLLECTION_NAME', 'UF_COLLECTION_LINK'));
    if( $ar_result = $db_list->GetNext() ){
        $collection = $ar_result;
        $collectionName = $ar_result['UF_COLLECTION_NAME'];
        $collectionLink = $ar_result['UF_COLLECTION_LINK'];
    }
}
$curPage = $APPLICATION->GetCurPage();
$description = $arResult['~DESCRIPTION'];
if( $description ){?>
    <div class="row catalog-top-text">
        <div class="col-sm-12">
            <div class="easy"><?=$description;?></div>
            <?if(stripos($curPage, '/gotovye-proekty/')):?>
            <div class="modal fade" id="Modal-prj" tabindex="-1" role="dialog" aria-labelledby="ModalLabel" aria-hidden="true">
                <div class="modal-dialog">
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                            <h4 class="modal-title" id="myModalLabel">Заказать проект</h4>
                        </div>
                        <div class="modal-body">
                            <div class="form-inline header-filter">
                                <?$APPLICATION->IncludeComponent("bitrix:form", "project", Array(
                                        "START_PAGE" => "new",    // Начальная страница
                                        "SHOW_LIST_PAGE" => "N",    // Показывать страницу со списком результатов
                                        "SHOW_EDIT_PAGE" => "N",    // Показывать страницу редактирования результата
                                        "SHOW_VIEW_PAGE" => "N",    // Показывать страницу просмотра результата
                                        "SUCCESS_URL" => "",    // Страница с сообщением об успешной отправке
                                        "WEB_FORM_ID" => "6",    // ID веб-формы
                                        "RESULT_ID" => $_REQUEST[RESULT_ID],    // ID результата
                                        "SHOW_ANSWER_VALUE" => "N",    // Показать значение параметра ANSWER_VALUE
                                        "SHOW_ADDITIONAL" => "N",    // Показать дополнительные поля веб-формы
                                        "SHOW_STATUS" => "N",    // Показать текущий статус результата
                                        "EDIT_ADDITIONAL" => "N",    // Выводить на редактирование дополнительные поля
                                        "EDIT_STATUS" => "N",    // Выводить форму смены статуса
                                        "NOT_SHOW_FILTER" => array(    // Коды полей, которые нельзя показывать в фильтре
                                            0 => "",
                                            1 => "",
                                        ),
                                        "NOT_SHOW_TABLE" => array(    // Коды полей, которые нельзя показывать в таблице
                                            0 => "",
                                            1 => "",
                                        ),
                                        "IGNORE_CUSTOM_TEMPLATE" => "N",    // Игнорировать свой шаблон
                                        "USE_EXTENDED_ERRORS" => "N",    // Использовать расширенный вывод сообщений об ошибках
                                        "SEF_MODE" => "N",    // Включить поддержку ЧПУ
                                        "AJAX_MODE" => "Y",    // Включить режим AJAX
                                        "AJAX_OPTION_JUMP" => "N",    // Включить прокрутку к началу компонента
                                        "AJAX_OPTION_STYLE" => "N",    // Включить подгрузку стилей
                                        "AJAX_OPTION_HISTORY" => "N",    // Включить эмуляцию навигации браузера
                                        "CACHE_TYPE" => "A",    // Тип кеширования
                                        "CACHE_TIME" => "360000000",    // Время кеширования (сек.)
                                        "CHAIN_ITEM_TEXT" => "",    // Название дополнительного пункта в навигационной цепочке
                                        "CHAIN_ITEM_LINK" => "",    // Ссылка на дополнительном пункте в навигационной цепочке
                                        "AJAX_OPTION_ADDITIONAL" => "",    // Дополнительный идентификатор
                                        "SEF_FOLDER" => "/about/",    // Каталог ЧПУ (относительно корня сайта)
                                        "VARIABLE_ALIASES" => array(
                                            "action" => "action",
                                        )
                                    ),
                                    false
                                );?>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="button-holder">
                <a rel="nofollow" data-toggle="modal" data-target="#Modal-prj" class="btn link-buttom">Заказать проект</a> 
            </div>
            <?else:?>
            <div class="modal fade" id="Modal-custom" tabindex="-1" role="dialog" aria-labelledby="ModalLabel" aria-hidden="true">
                <div class="modal-dialog">
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                            <div class="modal-title" id="myModalLabel" align="center">Заказать раскладку</div>
                        </div>
                        <div class="modal-body">
                            <div class="form-inline header-filter" role="form">
                                <?$APPLICATION->IncludeComponent("bitrix:form", "raskladka", Array(
                                "START_PAGE" => "new",    // Начальная страница
                                "SHOW_LIST_PAGE" => "N",    // Показывать страницу со списком результатов
                                "SHOW_EDIT_PAGE" => "N",    // Показывать страницу редактирования результата
                                "SHOW_VIEW_PAGE" => "N",    // Показывать страницу просмотра результата
                                "SUCCESS_URL" => "",    // Страница с сообщением об успешной отправке
                                "WEB_FORM_ID" => "5",    // ID веб-формы
                                "RESULT_ID" => $_REQUEST[RESULT_ID],    // ID результата
                                "SHOW_ANSWER_VALUE" => "N",    // Показать значение параметра ANSWER_VALUE
                                "SHOW_ADDITIONAL" => "N",    // Показать дополнительные поля веб-формы
                                "SHOW_STATUS" => "N",    // Показать текущий статус результата
                                "EDIT_ADDITIONAL" => "N",    // Выводить на редактирование дополнительные поля
                                "EDIT_STATUS" => "N",    // Выводить форму смены статуса
                                "NOT_SHOW_FILTER" => array(    // Коды полей, которые нельзя показывать в фильтре
                                    0 => "",
                                    1 => "",
                                ),
                                "NOT_SHOW_TABLE" => array(    // Коды полей, которые нельзя показывать в таблице
                                    0 => "",
                                    1 => "",
                                ),
                                "IGNORE_CUSTOM_TEMPLATE" => "N",    // Игнорировать свой шаблон
                                "USE_EXTENDED_ERRORS" => "N",    // Использовать расширенный вывод сообщений об ошибках
                                "SEF_MODE" => "N",    // Включить поддержку ЧПУ
                                "AJAX_MODE" => "Y",    // Включить режим AJAX
                                "AJAX_OPTION_JUMP" => "N",    // Включить прокрутку к началу компонента
                                "AJAX_OPTION_STYLE" => "N",    // Включить подгрузку стилей
                                "AJAX_OPTION_HISTORY" => "N",    // Включить эмуляцию навигации браузера
                                "CACHE_TYPE" => "A",    // Тип кеширования
                                "CACHE_TIME" => "3600000000",    // Время кеширования (сек.)
                                "CHAIN_ITEM_TEXT" => "",    // Название дополнительного пункта в навигационной цепочке
                                "CHAIN_ITEM_LINK" => "",    // Ссылка на дополнительном пункте в навигационной цепочке
                                "AJAX_OPTION_ADDITIONAL" => "",    // Дополнительный идентификатор
                                "SEF_FOLDER" => "/about/",    // Каталог ЧПУ (относительно корня сайта)
                                "VARIABLE_ALIASES" => array(
                                    "action" => "action",
                                )
                            ),
                            false
                        );?>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="button-holder">
                <a rel="nofollow" data-toggle="modal" data-target="#Modal-custom" class="btn link-buttom">Заказать раскладку</a> 
            </div>
            <?endif;?>

        </div>
    </div>
<?}?>

<?if (!empty($arResult['ITEMS'])){?>

    <div class="variant_raskladki_page">

        <?
        $strElementEdit = CIBlock::GetArrayByID($arParams["IBLOCK_ID"], "ELEMENT_EDIT");
        $strElementDelete = CIBlock::GetArrayByID($arParams["IBLOCK_ID"], "ELEMENT_DELETE");
        $arElementDeleteParams = array("CONFIRM" => GetMessage('CT_BCS_TPL_ELEMENT_DELETE_CONFIRM'));
        ?>

        <div class="row">

            <?foreach ($arResult['ITEMS'] as $key => $arItem){

                $this->AddEditAction($arItem['ID'], $arItem['EDIT_LINK'], $strElementEdit);
                $this->AddDeleteAction($arItem['ID'], $arItem['DELETE_LINK'], $strElementDelete, $arElementDeleteParams);
                $strMainID = $this->GetEditAreaId($arItem['ID']);

                $arItemIDs = array(
                    'ID' => $strMainID,
                    'PICT' => $strMainID.'_pict',
                    'SECOND_PICT' => $strMainID.'_secondpict',
                    'STICKER_ID' => $strMainID.'_sticker',
                    'SECOND_STICKER_ID' => $strMainID.'_secondsticker',
                    'QUANTITY' => $strMainID.'_quantity',
                    'QUANTITY_DOWN' => $strMainID.'_quant_down',
                    'QUANTITY_UP' => $strMainID.'_quant_up',
                    'QUANTITY_MEASURE' => $strMainID.'_quant_measure',
                    'BUY_LINK' => $strMainID.'_buy_link',
                    'SUBSCRIBE_LINK' => $strMainID.'_subscribe',

                    'PRICE' => $strMainID.'_price',
                    'DSC_PERC' => $strMainID.'_dsc_perc',
                    'SECOND_DSC_PERC' => $strMainID.'_second_dsc_perc',

                    'PROP_DIV' => $strMainID.'_sku_tree',
                    'PROP' => $strMainID.'_prop_',
                    'DISPLAY_PROP_DIV' => $strMainID.'_sku_prop',
                    'BASKET_PROP_DIV' => $strMainID.'_basket_prop',
                );

                $strObName = 'ob'.preg_replace("/[^a-zA-Z0-9_]/", "x", $strMainID);

                $productTitle = (
                isset($arItem['IPROPERTY_VALUES']['ELEMENT_PAGE_TITLE'])&& $arItem['IPROPERTY_VALUES']['ELEMENT_PAGE_TITLE'] != ''
                    ? $arItem['IPROPERTY_VALUES']['ELEMENT_PAGE_TITLE']
                    : $arItem['NAME']
                );
                $imgTitle = (
                isset($arItem['IPROPERTY_VALUES']['ELEMENT_PREVIEW_PICTURE_FILE_TITLE']) && $arItem['IPROPERTY_VALUES']['ELEMENT_PREVIEW_PICTURE_FILE_TITLE'] != ''
                    ? $arItem['IPROPERTY_VALUES']['ELEMENT_PREVIEW_PICTURE_FILE_TITLE']
                    : $arItem['NAME']
                );?>


                <div class="col-xs-4 usligi-pict" id="<? echo $strMainID; ?>">
                    <?$img = 'http://placehold.it/230x230';
                    if( $arItem['PREVIEW_PICTURE']['ID'] ){
                        #$img = CFile::GetPath($arItem['PREVIEW_PICTURE']['ID']);
                        $img = i($arItem['DETAIL_PICTURE']['ID'], 262, 264, BX_RESIZE_IMAGE_EXACT);
                        $imgBig = $arItem['DETAIL_PICTURE']['SRC'];
                    }?>
                    <a href="<?=$imgBig?>" rel="<?=$arItem['IBLOCK_SECTION_ID']?>" class="fancybox" >
                        <img
                            src="<?=$img?>"
                            title="<? echo $arItem['PREVIEW_PICTURE']['TITLE']; ?>"
                            alt="<? echo $arItem['PREVIEW_PICTURE']['ALT']; ?>"
                            />
                    </a>
                    <p class="text-pict"><p><?=$arResult['NAME']; ?></p>
                    <p class="text-fio"><?=$arItem['DETAIL_TEXT']?></p>
                    <?if( $collectionName ){?>
                        <p class="text-coll">Коллекция:</p>
                        <p class="text-coll-link">
                            <?if( $collectionLink ){?>
                                <a href="<?=$collectionLink;?>"><?=$collectionName?></a>
                            <?}
                            else{?>
                                <span><?=$collectionName?></span>
                            <?}?>
                        </p>
                    <?}?>
                </div>

            <?}?>
        </div>

    </div>
<?}?>