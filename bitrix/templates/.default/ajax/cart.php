<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();?>

<?
global $USER;
global $APPLICATION;

if( !isset($_REQUEST['id']) && !isset($_REQUEST['update']) ) die;
if( !isset($_REQUEST['quantity']) ){
    $quantity = 1;
}

$id = intval(htmlspecialchars($_REQUEST['id']));
$place = htmlspecialchars($_REQUEST['place']);
$type = htmlspecialchars($_REQUEST['typeaction']);

CModule::IncludeModule('sale');
CModule::IncludeModule('catalog');
CModule::IncludeModule('iblock');

if( $type == "delete" && $id ){
    CSaleBasket::Delete($id);
}
elseif( $place == 'basket' ){
    $arFields = array(
        "QUANTITY" => $quantity
    );

    CSaleBasket::Update($id, $arFields);
}
else{
    # добавляем товар в корзину
    $ID = Add2BasketByProductID(
        $id,
        $quantity,
        array(),
        array()
    );
}

$APPLICATION->IncludeComponent(
    "bitrix:sale.basket.basket.line",
    "json",
    Array(),
    false,
    array(
        'HIDE_ICONS' => 'Y'
    )
);