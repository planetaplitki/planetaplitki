<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();

//if( !isset($_REQUEST['id']) || !isset($_REQUEST['name']) || !isset($_REQUEST['link']) ) die;
if( !isset($_REQUEST['id']) ) die;

$PRODUCT_ID = intval($_REQUEST['id']);
$PRICE = intval($_REQUEST['price']);
$NAME = htmlspecialchars($_REQUEST['name']);
$LINK = strip_tags($_REQUEST['link']);

$quantity = 1;

if(
    CModule::IncludeModule("sale") &&
    CModule::IncludeModule('catalog')
){


    //добавляем товар в корзину
    $ID = Add2BasketByProductID(
        $PRODUCT_ID,
        $quantity,
        array("DELAY" => "Y"),
        array()
    );

    echo $ID;

}

?>