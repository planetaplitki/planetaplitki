/**
* проверяем на пустоту строковую переменную
* @param val
* @returns {*}
*/
window.checkEmpty = function(val){
    if(
        val != undefined &&
        val != null &&
        val != ''
    ){
        return val;
    }
    else{
        return false;
    }
};

// работа с корзиной
window.cart_ru = {
    // id родителя торгового предложения
    parentid : "",
    // id добавленного товара
    itemid : "",
    // тип операции над товаром (добавление, удаление)
    typeaction : "",
    // место, откуда вызван скрипт добавления /удаления товара
    place : "",
    // тип добавленного товара: обычный товар, торговое предложение
    itemtype : "",
    // цена товара
    itemprice : "",

    /**
    * добавляет товар в корзину
    */
    toCart : function(){

        var o = this;

        $.post(
            '/bitrix/templates/.default/ajax/connector.php',{
                'act' : 'cart',
                'id': o.itemid,
                'quantity': o.quantity,
                'place' : o.place,
                'typeaction' : o.typeaction
            },
            function(response, status, xhr) {

                var params = jQuery.parseJSON(response);
                if(!$.cookie('add_elementCookies')){
                    if(params.price < params.CONS_MIN_PRICE_ORDER){
                        $(".modal_min_price").addClass("modal_on").removeClass("modal_out");    
                    }
                    $.cookie('add_elementCookies', true, {
                        expires: 1, 
                        path: '/'
                    });
                }
                //обновляем основную корзину
                if( o.place == 'basket' ){
                    $('.basketWrap').html(params.baseBasketHtml);
                }

                //обновляем корзину в шапке
                $('.header-top-right').html(params.topBasketHtml);
                $('.point.pull-right, .chart .kolvo').text(params.countAll);

                //обнуляем тип операции
                o.typeaction = "";
            }
        );
    },
    /**
    * откладывает товар
    * @param obj
    */
    toDelay : function(obj){
        var o = this,
        id = $(obj).data('itemid'),
        name = $(obj).data('name'),
        link = $(obj).data('link'),
        quantity = 1;

        window.curObj = obj;

        $(obj).removeClass('active');

        $.post(
            '/bitrix/templates/.default/ajax/connector.php',
            {
                'act' : 'delay',
                'id': o.itemid,
                'name': name,
                'quantity': quantity,
                'link': link
            },
            function(response, status, xhr) {

                if( window.checkEmpty(response) ){
                    var params = jQuery.parseJSON(response),
                    obj = $('.delay[data-itemid="'+id+'"]');

                    if( $(obj).length > 0 && params['STATUS'] == 'Y' ){
                        $(obj).addClass('active');
                    }
                    else if( params['STATUS'] == 'N' ){

                    }
                }
                else{

                }
            }
        );
    },
    /**
    * обновляет корзину
    */
    updateCart: function(params){  

        var options = {'act':'cart'};
        $.extend(options, params);

        $.post(
            '/bitrix/templates/.default/ajax/connector.php',options,
            function(response, status, xhr) {

                var params = jQuery.parseJSON(response);

                //обновляем основную корзину
                if( options.place == 'basket' ){
                    $('.basketWrap').html(params.baseBasketHtml);
                }

                //обновляем корзину в шапке
                $('.header-top-right').html(params.topBasketHtml);
                $('.point.pull-right, .chart .kolvo').text(params.countAll);

                //обновляем страницу заказа
                if( $('.orderWrap').length ){
                    window.submitForm('Y');
                }
            }
        );
    },
    /**
    * Возвращает выбранное кол-во товара
    * @param obj
    * @returns {number}
    */
    getQuantity : function(obj){


        var parent = "",
        quantity = 1;

        if( this.place == "detail" ){
            //Элементы коллекции
            if ($(obj).parents('.bottom_item_container').length > 0){
                parent = $(obj).parents('.bottom_item_container').find('.box-position.quantity');
            } else {
                //Детальная карточка товара
                parent = $('.box-right-top .box-position.quantity');
            }

        }
        else if( this.place == "list" ){
            parent = $(obj).parents('.box-catalog');
        }
        else if( this.place == "basket" ){
            parent = $(obj).parents('.cart-items-inner');
        }                                 


        quantity = parseInt($(parent).find('input[name^="QUANTITY"]').val());

        quantity = quantity < 1 ? 1 : quantity;

        this.quantity = quantity;

        return quantity;
    },
    plus: function(obj){
        var input = obj.parent().find('input'),
        baseCount = parseInt(input.val()),
        place = input.data('place')
        count = 0;

        count = parseInt(baseCount) + 1;
        input.val(count);

        this.quantity = count;
        return false;
    },
    minus: function(obj){

        var input = obj.parent().find('input'),
        baseCount = parseInt(input.val()),
        count = parseInt(baseCount) - 1,
        place = input.data('place');

        count = count < 1 ? 1 : count;
        input.val(count);

        this.quantity = count;

        return false;
    },
    changeCount: function(obj){
        var count = $(obj).val(),
        place = $(obj).data('place');

        count = count < 1 ? 1 : count;

        this.quantity = count;
    },
    /**
    * Удалаяет товар из корзины
    * @param obj
    */
    deleteitem: function(obj){
        this.itemid = obj.parents('.oneProduct').data('basketitemid');
        this.typeaction = 'delete';
        this.toCart();
    },

    init: function(){
        var o = this;

        $('[data-operation="buy"]').on('click', function(){

            var obj = $(this);

            o.itemid = obj.data('itemid');
            o.parentid = obj.data('parentid');
            o.place = obj.data('place');
            o.itemtype = obj.data('itemtype');

            o.getQuantity(obj);
            o.toCart();
            if(window.matchMedia('(max-width: 767px)').matches && $('.test-block')) {
                $('.test-block').fadeIn();
                alignCenter($('.test-block'));
            }
        });

        $('[data-operation="delay"]').on('click', function(){

            var obj = $(this);

            o.itemid = obj.data('itemid');
            o.parentid = obj.data('parentid');
            o.place = obj.data('place');
            o.itemtype = obj.data('itemtype');

            o.toDelay(obj);
        });

        // в корзине
        if( $('.basketWrap').length ){

            o.place = 'basket';

            $('.basketWrap').on('click', '.minus', function(){
                var obj = $(this),
                parent = obj.parents('.oneProduct');

                o.itemid = parent.data('basketitemid');
                o.minus(obj);
                o.toCart();
            });

            $('.basketWrap').on('click', '.plus', function(){
                var obj = $(this);

                o.itemid = obj.parents('.oneProduct').data('basketitemid');    
                o.plus(obj);
                o.toCart();
            });

            $('.basketWrap').on('click', '.delete', function(){
                o.deleteitem($(this));
            });

            $('.basketWrap').on('change', 'input[name^="QUANTITY_"]', function(){
                var obj = $(this);

                o.itemid = obj.parents('.oneProduct').data('basketitemid');    
                o.getQuantity(obj);
                o.toCart();
            });

            // меняет периодичность
            $('.basketWrap').on('click', '.bCustomChechbox', function(){
                var obj = $(this);
                o.changeDeliveryPeriodicity(obj);
            });
            // меняет периодичность
            $('.basketWrap').on('change', 'select[name^="DELIVERY_PERIOD"]', function(){
                var obj = $(this);
                o.changeDeliveryPeriodicity(obj);
            });

            //меняет доставку
            $('.basketWrap').on('click', '.oneDelivery .radioContent', function(){
                var obj = $(this);
                o.changeDeliveryType(obj);
            });
        }
        else{

            $('.box-catalog').on('click', '.minus', function () {
                var obj = $(this);
                o.minus(obj);
            });

            $('.box-catalog').on('click', '.plus', function () {
                var obj = $(this);
                o.plus(obj);
            });

            $('.box-catalog .quantity input').on('change', function(){
                o.changeCount($(this));
            });
        }

        $('input[name^="QUANTITY"]').numeric({ negative : false });
    }

};

/**
* Расчет в карточке плитки
* @type {{relations: {}, quantity: null, shtuki: number, calculate: Function, changeShtuki: Function, changeMetri: Function, changeCount: Function, setProps: Function, plus: Function, minus: Function, formatPrice: Function, updateDetailCartSum: Function, init: Function}}
*/
window.calculate_tile = {

    // связи между параметрами
    relations: {},
    quantity: null,
    // кол-во штук
    shtuki: 0,

    /**
    * Все расчеты
    * @param obj
    */
    calculate: function(obj){
        var o = this,
        parent = $(obj).parents('.box-position'),
        parametr = $(parent).data('parametr');   

        // расчет идет в штуках
        if( parametr == 'shtuki' ){
            o.changeShtuki(obj);
        }
        // расчет идет в метрах
        else if( parametr == 'metri' ){     
            o.changeMetri(obj);
            $('.quantity input').trigger('change');
        }

                              
        // обновляем итоговую сумму в детальной карточке товара
        this.updateDetailCartSum(parseFloat(o.relations.RETAIL_ONE) * Math.ceil(o.shtuki));
    },

    /**
    * Меняем штуки
    */
    changeShtuki: function(obj){
        var o = this;
        var item_id;
        var params;
        if(obj.parents('.bottom_item_container').length > 0){
            item_id = obj.parents('.bottom_item_container').find('.a_to_basket').data('itemid');
            params = $('.j_hidden_params[data-item="'+item_id+'"]').serializeObject();
            o.relations  = params;
        }
        o.setProps({
            UPAKOVKI: o.quantity / o.relations.V_UPAKOVKE_SHTUK,
            SHTUKI: o.shtuki = o.quantity,
            METRI: parseFloat(o.relations.VISOTA) * parseFloat(o.relations.SHIRINA) * o.quantity
            },obj);
    },
    /**
    * Меняем метры
    */
    changeMetri: function(obj){
        var o = this;
        var item_id;
        var params;
        if(obj.parents('.bottom_item_container').length > 0){
            item_id = obj.parents('.bottom_item_container').find('.a_to_basket').data('itemid');
            params = $('.j_hidden_params[data-item="'+item_id+'"]').serializeObject();
            o.relations  = params;
        }
        o.setProps({
            UPAKOVKI: o.quantity / (o.relations.V_UPAKOVKE_SHTUK * ( parseFloat(o.relations.VISOTA) * parseFloat(o.relations.SHIRINA) )),
            SHTUKI: o.shtuki = o.quantity / ( parseFloat(o.relations.VISOTA) * parseFloat(o.relations.SHIRINA) ),
            METRI: o.quantity
            },obj)
    },
    changeCount: function(obj){
        var count = $(obj).val();
        count = count < 1 ? 1 : count;
        this.quantity = count;          
        this.calculate(obj);
    },        

    /**
    * Выставляет вычисленные значения
    * @param params
    */
    setProps: function(params, obj){

        params.UPAKOVKI = accounting.formatMoney(params.UPAKOVKI, "", 3, ".", ".");
        params.SHTUKI = Math.ceil(params.SHTUKI);
        params.METRI = parseFloat(accounting.formatMoney(params.METRI, "", 3, ".", "."));
        //Элементы коллекции
        if(obj.parents('.bottom_item_container').length > 0){
            obj.parents('.bottom_item_container').find('.box-position[data-parametr="upakovki"] .kolvo').text(params.UPAKOVKI);
            obj.parents('.bottom_item_container').find('.box-position[data-parametr="shtuki"] .kolvo').val(params.SHTUKI);
            obj.parents('.bottom_item_container').find('.box-position[data-parametr="metri"] .kolvo').val(params.METRI);
        } else {
            //Карточка товара
            obj.parents('.box-right-top').find('.box-position[data-parametr="upakovki"] .kolvo').text(params.UPAKOVKI);
            obj.parents('.box-right-top').find('.box-position[data-parametr="shtuki"] .kolvo').val(params.SHTUKI);
            obj.parents('.box-right-top').find('.box-position[data-parametr="metri"] .kolvo').val(params.METRI);
        }

    },
    plus: function(obj){

        var input = obj.parent().find('input'),
        baseCount = parseInt(input.val());

        count = parseInt(baseCount) + 1;
        input.val(count);

        this.quantity = count;
        this.calculate(obj);

        return false;
    },
    minus: function(obj){
        var input = obj.parent().find('input'),
        baseCount = parseInt(input.val()),
        count = parseInt(baseCount) - 1;

        count = count < 1 ? 1 : count;
        input.val(count);

        this.quantity = count;
        this.calculate(obj);

        return false;
    },
    /**
    * Возвращает отформатированную цену
    * @param price
    * @returns {*}
    */
    formatPrice: function(price){
        var priceFormatted = accounting.formatMoney(price, "", 1, " ", ".");
        return priceFormatted;
    },
    /**
    * Выставляет итоговую сумму
    * @param sum
    */
    updateDetailCartSum: function(sum){         
        $('.finalSum p').html('Итого: <span>'+this.formatPrice(sum)+' Р</span>');
        $('.mobilePrice').html('Цена: <span>'+this.formatPrice(sum)+' руб.</span>');
    },

    init: function(){
        var o = this;

        o.relations = window.relations_tile;
        //o.relations.V_UPAKOVKE_SHTUK = window.checkEmpty(o.relations.V_UPAKOVKE_SHTUK) ? o.relations.V_UPAKOVKE_SHTUK : 0;

        $('.box-position').on('click', '.minus', function () {
            var obj = $(this);

            o.minus(obj);
        });

        $('.box-position').on('click', '.plus', function () {
            var obj = $(this);
            o.plus(obj);
        });

        $('.box-right-top .box-position input').on('change', function(){       
            o.changeCount($(this));
        })
    }
};

function make_rows(element, rows_elements, el_class){
    var count = 0, row = 0, rows = '';
    for(i = 0; i < element.find('.usligi-pict').length / rows_elements; i++){ rows += '<div class="' + el_class + '"></div>'; }
    element.append(rows);
    element.find('.usligi-pict').each(function(){
        count++;
        $(this).appendTo(element.find('.' + el_class + ':eq(' + row + ')'));
        if(count == rows_elements){ count = 0, row++; }
    });
}

$(function(){     
    //Order mask
    $('#ORDER_PROP_3').mask('7(999)9999999');
    //Personal phone mask
    $('[name="PERSONAL_PHONE"]').mask('79999999999');
    $('[name="PERSONAL_MOBILE"]').mask('7(999)9999999');
    $('[name="REGISTER[PERSONAL_MOBILE]"]').mask('7(999)9999999');

    //Cart tabs
    $('.cart-tab-menu-item').click(function(){
        if(!$(this).hasClass('active')){
            $('.cart-tab-menu-item, .tab-content').removeClass('active');
            $(this).addClass('active');
            $('.tab-content').eq($(this).index()).addClass('active');
            if($(this).index() == 1){
                $('.nav-tabs').hide();
            } else{
                $('.nav-tabs').show();
            }
        }
    });

    //ScrollToUp button
    $('.cont2 .link-up').click(function(){
        $('html, body').stop().animate({scrollTop: 0}, 400);
    });

    //Catalog filter
    $('.extendedFilter .text-box a').click(function(){
        $('.extendedFilter .col-sm-4:not(:first-child)').stop().slideToggle();
    });

    // корзина
    window.cart_ru.init();
    // расчет в карточке плитки
    window.calculate_tile.init();

    $( ".slider-buttom" ).click(function() {
        $(this).closest('div').find( ".arrow_box" ).toggle();
    });

    $( ".lc_left_box a" ).click(function() {
        $(this).parent().find('ul').slideToggle();
    });

    $('.input-group.date').datepicker({
        language: "ru",
        autoclose: true
    });
    $(".input-group.date").datepicker().datepicker("setDate", new Date());

    $("#carousel_1").carousel();


    $('#banner1').carousel({
        interval: 4000
    });

    var clickEvent = false;
    $('#banner1').on('click', '.nav .pill-inner', function() {
        clickEvent = true;
        $('.nav li').removeClass('active');
        //$(".buttom-banner").hide();
        $(this).parent().addClass('active');
        //$(this).find(".buttom-banner").show();
    }).on('slid.bs.carousel', function(e) {
        if(!clickEvent) {
            var count = $('.nav').children().length -1;
            var current = $('.nav li.active');
            current.removeClass('active').next().addClass('active');
            var id = parseInt(current.data('slide-to'));
            if(count == id) {
                $('.nav li').first().addClass('active');
            }
        }
        clickEvent = false;
    });
    $('.pill-inner a').click(function(){
        window.location.href = $(this).attr('href');
    });

    //
    $('#carousel_1').carousel({
        interval: 10000
    });

    //
    $('#carousel_2').carousel({
        interval: 10000
    });

    //
    $( ".tooltip-info" ).hide();
    $( ".info" ).hover(
        function() {
            $( ".tooltip-info" ).show();
        }, function() {
            $( ".tooltip-info" ).hide();
        }
    );

    //
    $( window ).resize(function() {
        var mnfpict = $( ".pict_mnf" ).width();
        $('.pict_mnf').css('height', mnfpict);
    });

    var mnfpict = $( ".pict_mnf" ).width();
    $('.pict_mnf').css('height', mnfpict);

    //
    /*$(window).load(function(){
    $(".pict_mnf img").imgCenter();
    });
    $(".pict_mnf img").imgCenter();*/


    //

    $(window).scroll(function() {
        if ($(window).scrollTop() >= 180) {
            $('.scrollup-button').addClass('fixed');
        } else {
            $('.scrollup-button').removeClass('fixed');
        }
    });

    $('.scrollup-button').click(function(){
        $('html, body').stop().animate({scrollTop: 0}, 300);
    });

    $( window ).resize(function() {
        if($('body').width()>974) {
            var box3 = $( ".header-top-table2 .image-pict img" ).height();
            $('.header-top-table2 .box').css('height', box3);
        }
        else if ($('body').width()>750) {
            var box = $( ".header-top-table2 .image-pict img" ).height();
            $('.header-top-table2 .box').css('height', box/2);
        } else {
            var box2 = $( ".header-top-table2 .image-pict img" ).height();
            $('.header-top-table2 .box').css('height', box2/4);
        }
    });
    if($('body').width()>974) {
        var box3 = $( ".header-top-table2 .image-pict img" ).height();
        $('.header-top-table2 .box').css('height', box3);
    }
    else if ($('body').width()>750) {
        var box = $( ".header-top-table2 .image-pict img" ).height();
        $('.header-top-table2 .box').css('height', box/2);
    } else {
        var box2 = $( ".header-top-table2 .image-pict img" ).height();
        $('.header-top-table2 .box').css('height', box2/4);
    }

    //
    $( window ).resize(function() {
        if($('body').width()>974) {
            var box3 = $( ".tab2 .image-pict img" ).height();
            $('.tab2 .box').css('height', box3);
        }
        else if ($('body').width()>750) {
            var box = $( ".tab2 .image-pict img" ).height();
            $('.tab2 .box').css('height', box/2);
        } else {
            var box2 = $( ".tab2 .image-pict img" ).height();
            $('.tab2 .box').css('height', box2/4);
        }
    });
    if($('body').width()>974) {
        var box3 = $( ".tab2 .image-pict img" ).height();
        $('.tab2 .box').css('height', box3);
    }
    else if ($('body').width()>750) {
        var box = $( ".tab2 .image-pict img" ).height();
        $('.tab2 .box').css('height', box/2);
    } else {
        var box2 = $( ".tab2 .image-pict img" ).height();
        $('.tab2 .box').css('height', box2/4);
    }

    if($('.gallery').length) {
        var previewGallery = [], currentIndex = 0;
        $('.gallery-preview-link').each(function () {
            previewGallery.push({href: $(this).attr('href')});
        });

        $('.gallery-zoom-link').click(function(){
            $.fancybox.open(previewGallery, {
                index: currentIndex
            });
        });

        $('.gallery')
        .on('fotorama:load', function(e, fotorama){
            $('.gallery .fotorama__stage .fotorama__img').each(function(){
                $(this).css({marginTop: -$(this).height() / 2, marginLeft: -$(this).width() / 2});
            });
        })
        .on('fotorama:showend', function(e, fotorama){
            currentIndex = fotorama.activeIndex;
        })
        .fotorama({
            height: 396,
            //width: 555
            //width: 750,
            //height: 535
        });
    }
    if($('.gallery-container-collections-gallery').length) {
        var previewGallery = [], currentIndex = 0;
        $('.gallery-preview-link').each(function () {
            previewGallery.push({href: $(this).attr('href')});
        });

        $('.gallery-zoom-link').click(function(){
            $.fancybox.open(previewGallery, {
                index: currentIndex
            });
        });

        $('.gallery-container-collections-gallery')
        .on('fotorama:load', function(e, fotorama){
            $('.gallery-container-collections-gallery .fotorama__stage .fotorama__img').each(function(){
                //$(this).css({marginTop: -$(this).height() / 2, marginLeft: -$(this).width() / 2});
            });
        })
        .on('fotorama:showend', function(e, fotorama){
            currentIndex = fotorama.activeIndex;
        })
        .fotorama({
            //height: 396,
            //width: 555
            width: 750,
            height: 535
        });
    }

    if ($('.collections-catalog-gallery').length){
        $(' .usligi-pict').mouseover(function(){
            if($(this).find('.collections-catalog-gallery').length) {
                //$(this).find('.collections-catalog-placeholder').hide();
                $(this).find('.collections-catalog-gallery')
                .on('fotorama:load', function (e, fotorama) {
                    $(this).closest('.usligi-pict').find('.collections-catalog-placeholder').addClass('inactive');
                    $(this).find('.fotorama__stage__shaft').wrap('<a href="' + $(this).closest('.usligi-pict').find('.collections-catalog-placeholder').attr('href') + '"></a>');
                })
                .on('fotorama:showend', function (e, fotorama) {
                    var $this = $(this);
                    setTimeout(function () {
                        $this.find('.fotorama__stage').css('background', '#fff');
                        }, 1000);
                })
                .fotorama({
                    width: 198,
                    nav: false,
                    loop: true
                });
            }
        });
    }

    // печать заказа
    $('a.print').printPage();

    $('body').click(function(e){
        var target = $(e.target);
        if(!target.closest('.input-group.date').length){
            $('.input-group.date').datepicker('hide');
        }
    });

    $('.fancybox').fancybox({});

    //if($('.variant_raskladki_page').length){
    //    make_rows($('.variant_raskladki_page:has(.usligi-pict)'), 3, 'elements-row');
    //}


    //To-basket animation
    if($('.item_card_page').length) {
        $('.item_card_page .box-right-bottom a').click(function() {
            var $img = $(this).closest('.row').find('.gallery img:eq(0)');
            var $gallery = $('.gallery');
            var $basket = $('.header-top-right');
            var animDuration = 600;
            var animEasing = 'easeInCubic';
            $('body').append('<div class=basket-animate-element><div><img src=' + $img.attr('src') + '></div></div>');
            $('.basket-animate-element').last()
            .css({top: $gallery.offset().top, left: $gallery.offset().left + $gallery.width() - $('.basket-animate-element').width()})
            .animate({top: [$basket.offset().top, animEasing], left: [$basket.offset().left, animEasing]}, animDuration, function(){
                $(this).remove();
            })
            .find('img').animate({width: ['80px', animEasing], height: ['80px', animEasing]}, animDuration);
        });
    }
    $('.bottom_item_container a.a_to_basket').click(function() {
        var $img = $(this).parents('.collection_item_container').find('.image-link img:eq(0)');
        var $gallery = $(this).parents('.collection_item_container');
        var $basket = $('.header-top-right');
        var animDuration = 600;
        var animEasing = 'easeInCubic';
        $('body').append('<div class=basket-animate-element><div><img src=' + $img.attr('src') + '></div></div>');
        $('.basket-animate-element').last()
        .css({top: $gallery.offset().top, left: $gallery.offset().left + $gallery.width() - $('.basket-animate-element').width()})
        .animate({top: [$basket.offset().top, animEasing], left: [$basket.offset().left, animEasing]}, animDuration, function(){
            $(this).remove();
        })
        .find('img').animate({width: ['80px', animEasing], height: ['80px', animEasing]}, animDuration);
    });

    $('label.onoffswitch-label').on('click', function(){ 
        if($(this).prev().is(":checked"))
            setAttr('view','col');
        else
            setAttr('view','elem');
    });
});
$(document).on('click','.close-test-block', function() {
    $('.test-block').fadeOut();  
});
function alignCenter(elem) {
    elem.css({
        left: ($(window).width() - elem.width()) / 2 + 'px',
        top: ($(window).height() - elem.height()) / 2 + 'px'
    })
}
function setAttr(prmName,val){        
    var res = '';
    var d = location.href.split("#")[0].split("?");
    var base = d[0];
    var query = d[1];
    if(query) {
        var params = query.split("&");
        for(var i = 0; i < params.length; i++) {
            var keyval = params[i].split("=");
            if(keyval[0] != prmName) {
                res += params[i] + '&';
            }
        }
    }
    res += prmName + '=' + val;  
    window.location.href = base + '?' + res;
    return false;
}
$(document).ready(function(){        
    //setTimeout(function(){
    $('.fotorama').on('fotorama:load', function (e, fotorama, extra) {
        if($('.fotorama__stage__frame.fotorama__active').find('img').length < 2){
            if(!$('.fotorama__stage__frame.fotorama__active').hasClass('zoom')){
                $('.fotorama__stage__frame.fotorama__active').addClass('zoom');
            }

            $('.fotorama__stage__frame.fotorama__active').zoom(); 
        }
        $('.fotorama').on('fotorama:showend', function (e, fotorama, extra) {
            if($('.fotorama__stage__frame.fotorama__active').find('img').length < 2){
                if(!$('.fotorama__stage__frame.fotorama__active').hasClass('zoom')){
                    $('.fotorama__stage__frame.fotorama__active').addClass('zoom');
                }

                $('.fotorama__stage__frame.fotorama__active').zoom({
                    callback: function(){
                        if ($('.fotorama__stage__frame.fotorama__active:hover').length != 0) {
                            $('.fotorama__stage__frame.fotorama__active').trigger('mouseenter');
                        }

                        //$('.fotorama__stage__frame.fotorama__active').trigger('mouseout');
                    }
                });
            }

        });
        $('.fotorama').on('fotorama:load', function (e, fotorama, extra) {
            if($('.fotorama__stage__frame.fotorama__active').find('img').length < 2){
                if(!$('.fotorama__stage__frame.fotorama__active').hasClass('zoom')){
                    $('.fotorama__stage__frame.fotorama__active').addClass('zoom');
                }

                $('.fotorama__stage__frame.fotorama__active').zoom({
                    callback: function(){
                        if ($('.fotorama__stage__frame.fotorama__active:hover').length != 0) {
                            $('.fotorama__stage__frame.fotorama__active').trigger('mouseenter');
                        }

                        //$('.fotorama__stage__frame.fotorama__active').trigger('mouseout');
                    }
                });
            }

        });     
    });  
});
$.fn.serializeObject = function() {
    var o = {};
    var a = this.serializeArray();
    $.each(a, function() {
        if (o[this.name]) {
            if (!o[this.name].push) {
                o[this.name] = [o[this.name]];
            }
            o[this.name].push(this.value || '');
        } else {
            o[this.name] = this.value || '';
        }
    });
    return o;
};

$(document).ready(function(){ 
    $(".modal_min_price .close_modal").on("click",function(){
        $(".modal_min_price").addClass("modal_out").removeClass("modal_on");
        $.cookie('min_priceCookies', true, {
            expires: 1, 
            path: '/'
        });    
    });

    if (!$.cookie('min_priceCookies')) {
        $(".modal_min_price").addClass("modal_on").removeClass("modal_out");    
    };   

    $(document).on('click', '.usligi-pict__close', function(e) {
        var fav_id = $(this).parent().find('.usligi-pict__id').val();
        var item_del = this.parentNode;
        var collection = document.querySelector('.collections-page-list .row');
        
        collection.removeChild(item_del);
        
        favorites(fav_id);
    });
    
    $(document).on('click', '.js-show-more-col', function(e) {
        e.preventDefault();

        var href = $(this).attr('href');
        // preloader
        var preload = document.querySelector('.show-more-block__preloader');
        var showmore = document.querySelector('.js-show-more-col');
        preload.classList.add('show');
        showmore.textContent = "";
        
        $.ajax({
            url: href,
            type: "post",
            dataType: "html",
            data: {
                "AJAX_SHOW_MORE": true,
            },
            success: function(result) {
                var res = $(result).find('.collections-page-list').html();

                $('.js-pagination').remove();
                $('.collections-page-list').append(res);

                setWidthShowMore();
                isEndPagination();
                history.pushState({}, document.title, href);
                // preloader
                preload.classList.remove('show');
                showmore.textContent = "Показать еще";
            }
        })
    });

    $(document).on('click', '.js-show-more-el', function(e) {
        e.preventDefault();

        var href = $(this).attr('href');

        // preloader
        var preload = document.querySelector('.show-more-block__preloader');
        var showmore = document.querySelector('.js-show-more-el');
        preload.classList.add('show');
        showmore.textContent = "";
        
        $.ajax({
            url: href,
            type: "post",
            dataType: "html",
            data: {
                "AJAX_SHOW_MORE": true,
            },
            success: function(result) {
                var res = $(result).find('.elements-page-list').html();

                $('.js-pagination').remove();
                $('.elements-page-list').append(res);

                setWidthShowMore();
                isEndPagination();
                history.pushState({}, document.title, href);
                // preloader
                preload.classList.remove('show');
                showmore.textContent = "Показать еще";
            }
        })
    });
    setWidthShowMore();
    isEndPagination();
})

function setWidthShowMore() {
    var widthShowMore = $('.pagination-block').width();
    var marginL = parseInt($('.pagination-block').css('padding-left'));
    var marginR = parseInt($('.pagination-block').css('padding-right'));
    $('.show_more').css('width', widthShowMore + marginL + marginR + 'px');
}

function isEndPagination() {
    var next = $('.js-pagination').find('.modern-page-next');
    var showMore = $('.js-pagination').find('.show-more-block');
    if(next.length == 0) {
        showMore.prop('hidden', true);
    } else {
        showMore.prop('hidden', false);
    }
}
function favorites(id) {
    $.ajax({
        url: "/ajax/favorites.php",
        type: "post",
        data: {
            'FAV_ID': id,
        },
        success: function(res) {
            var result = JSON.parse(res);

            if(result['ELEMENT_IN_FAV'] == 1) {
                $('.m-sample__fav').addClass('fav-add');
                $('.m-sample__fav').text("В избранном");
            } else {
                $('.m-sample__fav').removeClass('fav-add');
                $('.m-sample__fav').text("В избранное");    
            }

            if(result['COUNT'] > 0) {
                $('.header-top__fav-count').addClass('count');
                $('.header-top__fav').addClass('count');
            } else {
                $('.header-top__fav-count').removeClass('count');
                $('.header-top__fav').removeClass('count');
            }

            $('.header-top__fav-count').text(result['COUNT']);
        }
    })
}  

window.onload = function() {
    setTimeout(confirmTextInner(), 3);
};

function confirmTextInner() {
    $.ajax({
        url: "/ajax/confirm_text_inner.php",
        type: "post",
        datatype: "html",
        success: function(result) {
            $('.modal-body.confirm-text-inner').append(result);
        }
    })
}                                                                                                                     



