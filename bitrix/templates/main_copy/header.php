<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
global $USER;                             
IncludeTemplateLangFile(__FILE__);
$curPage = $APPLICATION->GetCurPage();
# просмотренные коллекции
$viewedObj = new CViewedCollectionsRU();
$viewedCount = $viewedObj->getCount();       
?><!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, user-scalable=no">
    <?if (!empty($_REQUEST['view'])){$APPLICATION->SetPageProperty("robots", "noindex, nofollow");}?>
    <title><?$APPLICATION->ShowTitle()?></title>
    <!--<link rel="stylesheet" href="https://code.jquery.com/ui/1.11.2/themes/smoothness/jquery-ui.css">-->

    <link rel="shortcut icon" type="image/x-icon" href="<?=SITE_DIR?>favicon.ico" />
    <link rel="icon" href="<?=SITE_DIR?>favicon.ico" type="image/x-icon">
    <link rel="shortcut icon" href="<?=SITE_DIR?>favicon.ico" type="image/x-icon">

    <?
    $APPLICATION->SetAdditionalCSS(SITE_DEFAULT_TEMPLATE_PATH.'css/bootstrap.css');
    $APPLICATION->SetAdditionalCSS(SITE_DEFAULT_TEMPLATE_PATH.'css/style.css');
    $APPLICATION->SetAdditionalCSS(SITE_DEFAULT_TEMPLATE_PATH.'css/boot5.css');
    $APPLICATION->SetAdditionalCSS(SITE_DEFAULT_TEMPLATE_PATH.'css/fotorama.css');
    $APPLICATION->SetAdditionalCSS(SITE_DEFAULT_TEMPLATE_PATH.'css/datepicker3.css');
    $APPLICATION->SetAdditionalCSS(SITE_DEFAULT_TEMPLATE_PATH.'css/jquery.selectbox.css');
    $APPLICATION->SetAdditionalCSS(SITE_DEFAULT_TEMPLATE_PATH.'css/jquery-ui.css');
    $APPLICATION->SetAdditionalCSS(SITE_DEFAULT_TEMPLATE_PATH.'css/media.css');
    $APPLICATION->SetAdditionalCSS(SITE_DEFAULT_TEMPLATE_PATH.'css/jquery.fancybox.css');
    ?>
	
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>

    <?
    $APPLICATION->AddHeadScript(SITE_DEFAULT_TEMPLATE_PATH."js/bootstrap.min.js");
    $APPLICATION->AddHeadScript(SITE_DEFAULT_TEMPLATE_PATH."js/jquery.jcarousel.min.js");
    $APPLICATION->AddHeadScript(SITE_DEFAULT_TEMPLATE_PATH."js/carousel-swipe.js");
    $APPLICATION->AddHeadScript(SITE_DEFAULT_TEMPLATE_PATH."js/jquery.imgCenter.js");
    $APPLICATION->AddHeadScript(SITE_DEFAULT_TEMPLATE_PATH."js/fotorama.js");
    $APPLICATION->AddHeadScript(SITE_DEFAULT_TEMPLATE_PATH."js/bootstrap-datepicker.js");
    $APPLICATION->AddHeadScript(SITE_DEFAULT_TEMPLATE_PATH."js/bootstrap-datepicker.ru.js");
    $APPLICATION->AddHeadScript(SITE_DEFAULT_TEMPLATE_PATH."js/jquery.selectbox-0.2.js");
    $APPLICATION->AddHeadScript(SITE_DEFAULT_TEMPLATE_PATH."js/jquery-ui.js");
    $APPLICATION->AddHeadScript(SITE_DEFAULT_TEMPLATE_PATH."js/jquery.printPage.js");
    $APPLICATION->AddHeadScript(SITE_DEFAULT_TEMPLATE_PATH."js/jquery.fancybox.js");
    $APPLICATION->AddHeadScript(SITE_DEFAULT_TEMPLATE_PATH."js/jquery.numeric.js");
    $APPLICATION->AddHeadScript(SITE_DEFAULT_TEMPLATE_PATH."js/accounting.min.js");
    $APPLICATION->AddHeadScript(SITE_DEFAULT_TEMPLATE_PATH."js/jquery.validate.min.js");
    $APPLICATION->AddHeadScript(SITE_DEFAULT_TEMPLATE_PATH."js/jquery.mask.min.js");
    $APPLICATION->AddHeadScript(SITE_DEFAULT_TEMPLATE_PATH."js/jquery.json-2.3.min.js");
    $APPLICATION->AddHeadScript(SITE_DEFAULT_TEMPLATE_PATH."js/jquery.easing.1.3.min.js");
    $APPLICATION->AddHeadScript(SITE_DEFAULT_TEMPLATE_PATH."js/main.js");
    $APPLICATION->AddHeadScript(SITE_DEFAULT_TEMPLATE_PATH."js/masked_input.js");
    $APPLICATION->AddHeadScript(SITE_DEFAULT_TEMPLATE_PATH."js/events.js");
    ?>

    <?$APPLICATION->ShowHead();?>
	<link href="/bitrix/css/fix.css" type="text/css" rel="stylesheet" />
	
	

</head>
<body>


<div id="fb-root"></div>
<script>(function(d, s, id) {
  var js, fjs = d.getElementsByTagName(s)[0];
  if (d.getElementById(id)) return;
  js = d.createElement(s); js.id = id;
  js.src = "//connect.facebook.net/ru_RU/sdk.js#xfbml=1&version=v2.8&appId=1160515060667696";
  fjs.parentNode.insertBefore(js, fjs);
}(document, 'script', 'facebook-jssdk'));</script>

<!-- Yandex.Metrika counter -->
<script type="text/javascript">
    (function (d, w, c) {
        (w[c] = w[c] || []).push(function() {
            try {
                w.yaCounter14449306 = new Ya.Metrika({
                    id:14449306,
                    clickmap:true,
                    trackLinks:true,
                    accurateTrackBounce:true,
                    webvisor:true,
                    ecommerce:"dataLayer"
                });
            } catch(e) { }
        });

        var n = d.getElementsByTagName("script")[0],
            s = d.createElement("script"),
            f = function () { n.parentNode.insertBefore(s, n); };
        s.type = "text/javascript";
        s.async = true;
        s.src = "https://mc.yandex.ru/metrika/watch.js";

        if (w.opera == "[object Opera]") {
            d.addEventListener("DOMContentLoaded", f, false);
        } else { f(); }
    })(document, window, "yandex_metrika_callbacks");
</script>
<noscript><div><img src="https://mc.yandex.ru/watch/14449306" style="position:absolute; left:-9999px;" alt="" /></div></noscript>
<!-- /Yandex.Metrika counter -->


<div id="panel"><?$APPLICATION->ShowPanel();?></div>
<header>
    <div class="header-top hidden-xs">
        <div class="container">
            <?if( $viewedCount ){?>
                <div class="pole1 hidden-sm hidden-xs"><p>Вы просмотрели: <span><?=$viewedCount?></span></p></div>
            <?}?>         
            <button class="btn modal-buttom" data-toggle="modal" data-target="#Modal">Запись на консультацию</button>

            <a href="<?=SITE_DIR?>personal/cart/" class="btn link-buttom" rel="nofollow">ОФОРМИТЬ</a>
            <div class="header-top-right">
                <?$APPLICATION->IncludeComponent("bitrix:sale.basket.basket.small", "top", Array(
                    "COMPONENT_TEMPLATE" => ".default",
                    "PATH_TO_BASKET" => "/personal/cart/",	// Страница корзины
                    "PATH_TO_ORDER" => "/personal/order/make/",	// Страница оформления заказа
                    "SHOW_DELAY" => "Y",	// Показывать отложенные товары
                    "SHOW_NOTAVAIL" => "N",	// Показывать товары, недоступные для покупки
                    "SHOW_SUBSCRIBE" => "N",	// Показывать товары, на которые подписан покупатель
                ),
                    false
                );?>
            </div>

            <div class="pole-lc">
                <?$APPLICATION->IncludeComponent("bitrix:system.auth.form", "top", Array(
                        "REGISTER_URL" => SITE_DIR."login/",	// Страница регистрации
                        "PROFILE_URL" => SITE_DIR."personal/",	// Страница профиля
                        "SHOW_ERRORS" => "N",	// Показывать ошибки
                    ),
                    false
                );?>
            </div>
        </div>
    </div>
    <div class="header-top hidden-sm hidden-md hidden-lg">
        <a class="chart" href='/personal/cart/' rel="nofollow">
            <img src="<?=SITE_DEFAULT_TEMPLATE_PATH?>img/header/cart.png">
            <div class="kolvo">
                <?
                # определено (bitrix/templates/.default/components/bitrix/sale.basket.basket.small/top/template.php)
                $APPLICATION->ShowViewContent("basket_mobile_icon");?>
            </div>
        </a>
        <a class="menu-bar-top" data-toggle="collapse" href="#menu-top"><span class="bars-top"></span></a>
        <div class="collapse menu-col-top" id="menu-top">
            <div>
                <ul class="menu-top-box-top">
                    <li>
                        <?
                        # определено (bitrix/templates/.default/components/bitrix/sale.basket.basket.small/top/template.php)
                        $APPLICATION->ShowViewContent("basket_mobile_menu");
                        ?>
                    </li>
                    <li>
                        <a href="<?=SITE_DIR?>catalog/">
                            <span class="glyphicon-icon3"></span>Каталог
                        </a>
                        <span class="point2 pull-right" data-toggle="collapse" data-target="#toggleDemo" data-parent="#sidenav01"></span>
                        <div class="collapse" id="toggleDemo">
                            <?$APPLICATION->IncludeComponent("bitrix:menu", "mobile.catalog", Array(
                                    "ROOT_MENU_TYPE" => "topleft.".USER_TYPE,	// Тип меню для первого уровня
                                    "MENU_CACHE_TYPE" => "Y",	// Тип кеширования
                                    "MENU_CACHE_TIME" => "36000000",	// Время кеширования (сек.)
                                    "MENU_CACHE_USE_GROUPS" => "Y",	// Учитывать права доступа
                                    "MENU_CACHE_GET_VARS" => "",	// Значимые переменные запроса
                                    "MAX_LEVEL" => "1",	// Уровень вложенности меню
                                    "CHILD_MENU_TYPE" => "",	// Тип меню для остальных уровней
                                    "USE_EXT" => "N",	// Подключать файлы с именами вида .тип_меню.menu_ext.php
                                    "DELAY" => "N",	// Откладывать выполнение шаблона меню
                                    "ALLOW_MULTI_SELECT" => "N",	// Разрешить несколько активных пунктов одновременно
                                ),
                                false
                            );?>
                        </div>
                    </li>
                    <li><a href="<?=SITE_DIR?>all-shares/"><span class="glyphicon-icon7"></span>Акции</a></li>
                    <li><a href="<?=SITE_DIR?>catalog/sale/"><span class="glyphicon-icon1"></span>Распродажа</a></li>
                    <li><a href="<?=SITE_DIR?>contacts/"><span class="glyphicon-icon2"></span>Контакты</a></li>
                    <li><a href="<?=SITE_DIR?>personal/"><span class="glyphicon-icon4"></span>Личный кабинет</a></li>
                </ul>
            </div>
        </div>
    </div>
    <div class="header-middle">
        <div class="container border">
        <div class="logo"><a href="<?=SITE_DIR?>"><img src="/bitrix/templates/.default/img/header/logo.png"></a></div>
            <div class="box-right">
            
            	<div class="search-form hidden-xs">
					<?$APPLICATION->IncludeComponent(
						"bitrix:search.title",
						"",
						Array(
							"CATEGORY_0" => array("iblock_1c_catalog"),
							"CATEGORY_0_TITLE" => "",
							"CATEGORY_0_iblock_1c_catalog" => array("all"),
							"CHECK_DATES" => "N",
							"CONTAINER_ID" => "title-search",
							"INPUT_ID" => "title-search-input",
							"NUM_CATEGORIES" => "1",
							"ORDER" => "date",
							"PAGE" => "#SITE_DIR#catalog/index.php",
							"SHOW_INPUT" => "Y",
							"SHOW_OTHERS" => "N",
							"TOP_COUNT" => "10",
							"USE_LANGUAGE_GUESS" => "Y"
						)
					);?>
            	</div>
                <div class="phone">
                    <?$APPLICATION->IncludeComponent("bitrix:main.include", ".default", array(
                        "AREA_FILE_SHOW" => "file",
                        "PATH" => SITE_DEFAULT_TEMPLATE_PATH."include/phone1.php",
                        "EDIT_TEMPLATE" => ""
                    ),false
                    );?>
                </div>

                <?/*?>
            <div class="info hidden-xs hidden-sm"><div class="tooltip-info">Вы можете сделать заказ в магазине и оплатить его позже в нашем интернет-магазине.</div></div>
            <button type="button" class="btn link-buttom hidden-xs">Оплатить заказ</button>
            <?*/?>
            

                <div class="modal-wrapper-custom">


                <button class="btn modal-buttom hidden-xs" data-toggle="modal" data-target="#Modal2">ЗАДАТЬ ВОПРОС</button>
                <div class="modal fade" id="Modal2" tabindex="-1" role="dialog" aria-labelledby="ModalLabel" aria-hidden="true">
                    <div class="modal-dialog">
                        <div class="modal-content">
                            <div class="modal-header">
                                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                                <div class="modal-title" id="myModalLabel" align="center">Задать вопрос</div>
                            </div>
                            <div class="modal-body">
                                <div class="form-inline header-filter" role="form">
                                    <?$APPLICATION->IncludeComponent(
                                        "bitrix:form",
                                        "question",
                                        array(
                                            "START_PAGE" => "new",
                                            "SHOW_LIST_PAGE" => "N",
                                            "SHOW_EDIT_PAGE" => "N",
                                            "SHOW_VIEW_PAGE" => "N",
                                            "SUCCESS_URL" => "",
                                            "WEB_FORM_ID" => "1",
                                            "RESULT_ID" => $_REQUEST[RESULT_ID],
                                            "SHOW_ANSWER_VALUE" => "N",
                                            "SHOW_ADDITIONAL" => "N",
                                            "SHOW_STATUS" => "N",
                                            "EDIT_ADDITIONAL" => "N",
                                            "EDIT_STATUS" => "N",
                                            "NOT_SHOW_FILTER" => array(
                                                0 => "",
                                                1 => "",
                                            ),
                                            "NOT_SHOW_TABLE" => array(
                                                0 => "",
                                                1 => "",
                                            ),
                                            "IGNORE_CUSTOM_TEMPLATE" => "N",
                                            "USE_EXTENDED_ERRORS" => "N",
                                            "SEF_MODE" => "N",
                                            "AJAX_MODE" => "Y",
                                            "AJAX_OPTION_JUMP" => "N",
                                            "AJAX_OPTION_STYLE" => "N",
                                            "AJAX_OPTION_HISTORY" => "N",
                                            "CACHE_TYPE" => "A",
                                            "CACHE_TIME" => "3600000000",
                                            "CHAIN_ITEM_TEXT" => "",
                                            "CHAIN_ITEM_LINK" => "",
                                            "AJAX_OPTION_ADDITIONAL" => "",
                                            "SEF_FOLDER" => "/about/",
                                            "VARIABLE_ALIASES" => array(
                                                "action" => "action",
                                            )
                                        ),
                                        false
                                    );?>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <button class="btn modal-buttom modal-buttom-custom hidden-xs" data-toggle="modal" data-target="#footer_callback">ЗАКАЗАТЬ ЗВОНОК</button>
                <div class="modal fade" id="footer_callback" tabindex="-1" role="dialog" aria-labelledby="ModalLabel" aria-hidden="true">
                    <div class="modal-dialog">
                        <div class="modal-content">
                            <div class="modal-header">
                                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                                <h4 class="modal-title">Заказать звонок</h4>
                            </div>
                            <div class="modal-body">
                                <div class="form-inline header-filter" role="form">
                                    <?$APPLICATION->IncludeComponent(
    "bitrix:form", 
    "callback", 
    array(
        "START_PAGE" => "new",
        "SHOW_LIST_PAGE" => "N",
        "SHOW_EDIT_PAGE" => "N",
        "SHOW_VIEW_PAGE" => "N",
        "SUCCESS_URL" => "",
        "WEB_FORM_ID" => "3",
        "RESULT_ID" => $_REQUEST[RESULT_ID],
        "SHOW_ANSWER_VALUE" => "N",
        "SHOW_ADDITIONAL" => "N",
        "SHOW_STATUS" => "N",
        "EDIT_ADDITIONAL" => "N",
        "EDIT_STATUS" => "N",
        "NOT_SHOW_FILTER" => array(
            0 => "",
            1 => "",
        ),
        "NOT_SHOW_TABLE" => array(
            0 => "",
            1 => "",
        ),
        "IGNORE_CUSTOM_TEMPLATE" => "N",
        "USE_EXTENDED_ERRORS" => "N",
        "SEF_MODE" => "N",
        "AJAX_MODE" => "Y",
        "AJAX_OPTION_JUMP" => "N",
        "AJAX_OPTION_STYLE" => "N",
        "AJAX_OPTION_HISTORY" => "N",
        "CACHE_TYPE" => "A",
        "CACHE_TIME" => "36000000000",
        "CHAIN_ITEM_TEXT" => "",
        "CHAIN_ITEM_LINK" => "",
        "AJAX_OPTION_ADDITIONAL" => "",
        "SEF_FOLDER" => "/about/",
        "COMPONENT_TEMPLATE" => "callback",
        "VARIABLE_ALIASES" => array(
            "action" => "action",
        )
    ),
    false
);?>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                
                </div>
                
                <?$APPLICATION->IncludeComponent(
    "bitrix:main.include",
    "",
    Array(
        "AREA_FILE_SHOW" => "file",
        "AREA_FILE_SUFFIX" => "inc",
        "EDIT_TEMPLATE" => "",
        "PATH" => SITE_DIR."/include/confirm_text.php"
    )
);?>
                     
            </div>
        </div>
    </div>
    <div class="header-button">
        <div class="container hidden-xs">
            <div class="pole1 hidden-sm hidden-xs">
                <?$countArr = getItemsCollectionsCount();?>
                <p>
                    <span><?=$countArr['SECTIONS']?></span> <?=$countArr['SECTIONS_FORMAT']?>,
                    <span><?=$countArr['ELEMENTS']?></span> <?=$countArr['ELEMENTS_FORMAT']?>
                </p>
            </div>
            <div class="menu-right">
                <?$APPLICATION->IncludeComponent(
                    "bitrix:menu",
                    "top.simple",
                    array(
                        "ROOT_MENU_TYPE" => "topsimple",
                        "MENU_CACHE_TYPE" => "Y",
                        "MENU_CACHE_TIME" => "36000000",
                        "MENU_CACHE_USE_GROUPS" => "Y",
                        "MENU_CACHE_GET_VARS" => array(
                        ),
                        "MAX_LEVEL" => "1",
                        "CHILD_MENU_TYPE" => "",
                        "USE_EXT" => "N",
                        "DELAY" => "N",
                        "ALLOW_MULTI_SELECT" => "N"
                    ),
                    false
                );?>
            </div>
        </div>
        <div class="container hidden-sm hidden-md hidden-lg">
            <div class="search-form">
                <?$APPLICATION->IncludeComponent(
                    "bitrix:search.form", "top",
                    Array(
                        "PAGE" => "#SITE_DIR#catalog/index.php",
                        "USE_SUGGEST" => "N"
                    )
                );?>
            </div>
        </div>
    </div>

    <div class="header-menu hidden-xs">
        <div class="container">

            <a class="menu-bar"  data-toggle="collapse" href="#menu">
                <div class="dropdown-left"><div class="bars"></div>Каталог товаров</div>
            </a>

            <div class="collapse menu-col" id="menu">
                <div class="menu-top-box">
                    <?$APPLICATION->IncludeComponent("bitrix:menu", "multi", Array(
                            "ROOT_MENU_TYPE" => "topleft.".USER_TYPE,	// Тип меню для первого уровня
                            "MENU_CACHE_TYPE" => "Y",	// Тип кеширования
                            "MENU_CACHE_TIME" => "36000000",	// Время кеширования (сек.)
                            "MENU_CACHE_USE_GROUPS" => "Y",	// Учитывать права доступа
                            "MENU_CACHE_GET_VARS" => "",	// Значимые переменные запроса
                            "MAX_LEVEL" => "1",	// Уровень вложенности меню
                            "CHILD_MENU_TYPE" => "",	// Тип меню для остальных уровней
                            "USE_EXT" => "N",	// Подключать файлы с именами вида .тип_меню.menu_ext.php
                            "DELAY" => "N",	// Откладывать выполнение шаблона меню
                            "ALLOW_MULTI_SELECT" => "N",	// Разрешить несколько активных пунктов одновременно
                        ),
                        false
                    );?>
                </div>
            </div>

            <div class="menu-right">
                <?$APPLICATION->IncludeComponent(
                    "bitrix:menu",
                    "topRight",
                    array(
                        "ROOT_MENU_TYPE" => "topright",
                        "MENU_CACHE_TYPE" => "Y",
                        "MENU_CACHE_TIME" => "36000000",
                        "MENU_CACHE_USE_GROUPS" => "Y",
                        "MENU_CACHE_GET_VARS" => array(
                        ),
                        "MAX_LEVEL" => "1",
                        "CHILD_MENU_TYPE" => "",
                        "USE_EXT" => "N",
                        "DELAY" => "N",
                        "ALLOW_MULTI_SELECT" => "N"
                    ),
                    false
                );?>
            </div>
        </div>
    </div>
</header>

    <? # главная
                         
    if( CURPAGE == SITE_DIR ){  
        if( USER_TYPE == 'wholesale' ){?>
            <?$APPLICATION->IncludeComponent("bitrix:main.include", ".default", array(
                    "AREA_FILE_SHOW" => "file",
                    "PATH" => SITE_DEFAULT_TEMPLATE_PATH."include/m_mainpage.".USER_TYPE.".php",
                    "EDIT_TEMPLATE" => ""
                ),false
            );?> 
        <?}
        else{?>
            <?$APPLICATION->IncludeComponent("bitrix:main.include", ".default", array(
                    "AREA_FILE_SHOW" => "file",
                    "PATH" => SITE_DEFAULT_TEMPLATE_PATH."include/m_mainpage.".USER_TYPE.".php",
                    //"PATH" => SITE_DEFAULT_TEMPLATE_PATH."include/m_mainpage.wholesale.php",
                    "EDIT_TEMPLATE" => ""
                ),false
            );?>  
                
            <?}
    }               
    # каталог товаров
    elseif( preg_match('#^/(catalog|sale)/.*#ui', CURPAGE) ){?>
        <div class="<?$APPLICATION->AddBufferContent("setPageClass")?>" >
    <?}
    # бренды
    elseif( preg_match('#^/brands/.*#ui', CURPAGE) ){?>

        <div class="<?$APPLICATION->AddBufferContent("setPageClass")?>" >
            <div class="filtr">
                <div class="container pad">
                    <div class="row">
                        <div class="col-xs-12">
                            <?$APPLICATION->IncludeComponent("bitrix:breadcrumb", "breadcrumbs", array(
                                    "START_FROM" => "0",
                                    "PATH" => "",
                                    "SITE_ID" => "-"
                                ),
                                false
                            );?>
                            <h1 class="title"><?$APPLICATION->ShowTitle(false);?></h1>
                        </div>
                    </div>
                    <div class="row obj">
                        <div class="col-sm-12">
                            <?$APPLICATION->ShowViewContent("brands_filter");?>
                        </div>
                    </div>
                </div>
            </div>
            <div class="container obj">
                <div class="row">
    <?}
    else{?>

        <div class="<?$APPLICATION->AddBufferContent("setPageClass")?>" >

            <div class="container">
                <div class="row">
                    <div class="col-xs-12">
                        <?$APPLICATION->IncludeComponent("bitrix:breadcrumb", "breadcrumbs", array(
                                "START_FROM" => "0",
                                "PATH" => "",
                                "SITE_ID" => "-"
                            ),
                            false
                        );?>

                        <h1 class="title"><?$APPLICATION->ShowTitle();?></h1>

                        <?if( CURPAGE == '/howto/faq/' ){?>
                            <div class="faq_page"><button class="btn modal-buttom-faq" data-toggle="modal" data-target="#Modal-faq">ЗАДАТЬ ВОПРОС</button></div>
                        <?}?>
                    </div>
                </div>
                <div class="row obj">

                    <?$pages = array(
                        '/about/',
                        '/services/',
                        '/vacancies/',
                        '/news/',
                        '/howto/',
                        '/partnership/',
                    );?>

                    <?foreach( $pages as $page ){
                        if(
                            preg_match('#^'.$page.'#ui', CURPAGE) &&
                            !preg_match('#^/howto/(varianty-raskladki|gotovye-proekty)/[a-z0-9_-].*#ui', CURPAGE)
                        ){?>
                            <div class="col-sm-3">
                                <div class="box-left-menu">
                                    <?$APPLICATION->IncludeComponent(
                                        "bitrix:menu",
                                        "leftsimple",
                                        array(
                                            "ROOT_MENU_TYPE" => "leftsimple",
                                            "MENU_CACHE_TYPE" => "Y",
                                            "MENU_CACHE_TIME" => "36000000",
                                            "MENU_CACHE_USE_GROUPS" => "Y",
                                            "MENU_CACHE_GET_VARS" => array(
                                            ),
                                            "MAX_LEVEL" => "1",
                                            "CHILD_MENU_TYPE" => "",
                                            "USE_EXT" => "N",
                                            "DELAY" => "N",
                                            "ALLOW_MULTI_SELECT" => "N"
                                        ),
                                        false
                                    );?>
                                </div>

                                <?if( CURPAGE == '/howto/varianty-raskladki/'){?>
                                    <div class="variant_raskladki_page"><button class="btn link-buttom" data-toggle="modal" data-target="#Modal-var">ЗАКАЖИТЕ РАСКЛАДКУ</button></div>
                                <?}
                                elseif( CURPAGE == '/howto/gotovye-proekty/' ){?>
                                    <div class="projects_page"><button class="btn link-buttom" data-toggle="modal" data-target="#Modal-prj">ЗАКАЖИТЕ ВАШ ПРОЕКТ</button></div>
                                <?}?>
                            </div>
                            <div class="col-sm-9">
                        <?
                            $match++;
                            $break;
                        }
                    }?>

                    <?if( !$match ){?>
                        <div class="col-sm-12">

                        <?if( preg_match('#^/personal/(?!cart|order/make).*#ui', CURPAGE) && $USER->IsAuthorized() ){?>

                            <div class="lc_left">
                                <?$APPLICATION->IncludeComponent(
                                    "bitrix:menu",
                                    "personal",
                                    array(
                                        "ROOT_MENU_TYPE" => "personal",
                                        "MENU_CACHE_TYPE" => "Y",
                                        "MENU_CACHE_TIME" => "36000000",
                                        "MENU_CACHE_USE_GROUPS" => "Y",
                                        "MENU_CACHE_GET_VARS" => array(
                                        ),
                                        "MAX_LEVEL" => "1",
                                        "CHILD_MENU_TYPE" => "",
                                        "USE_EXT" => "N",
                                        "DELAY" => "N",
                                        "ALLOW_MULTI_SELECT" => "N"
                                    ),
                                    false
                                );?>
                            </div>
                        <?}?>
                    <?}?>
    <?}?>


<style>
/*.extendedFilter>div{
    display: block;
}*/


.row .extendedFilter .col-sm-4:not(:first-child) {
    display: block;
}
</style>            
