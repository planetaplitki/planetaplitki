<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>

<?
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);
?>

<?
//$sections = getSections();
//$collections = $sections['COLLECTIONS'];

$idSection = $arResult['ITEMS'][0]['IBLOCK_SECTION_ID'];
if( CModule::IncludeModule('iblock') ){
    $arFilter = Array('IBLOCK_ID'=>$arParams['IBLOCK_ID'], "ID" => $idSection);
    $db_list = CIBlockSection::GetList(Array($by=>$order), $arFilter, true, array('UF_COLLECTION_NAME', 'UF_COLLECTION_LINK'));
    if( $ar_result = $db_list->GetNext() ){
        $collection = $ar_result;
        $collectionName = $ar_result['UF_COLLECTION_NAME'];
        $collectionLink = $ar_result['UF_COLLECTION_LINK'];
    }
}

$description = $arResult['~DESCRIPTION'];
if( $description ){?>
    <div class="row catalog-top-text">
        <div class="col-sm-12">
            <div class="easy"><?=$description;?></div>
        </div>
    </div>
<?}?>

<?if (!empty($arResult['ITEMS'])){?>

    <div class="variant_raskladki_page">

        <?
        $strElementEdit = CIBlock::GetArrayByID($arParams["IBLOCK_ID"], "ELEMENT_EDIT");
        $strElementDelete = CIBlock::GetArrayByID($arParams["IBLOCK_ID"], "ELEMENT_DELETE");
        $arElementDeleteParams = array("CONFIRM" => GetMessage('CT_BCS_TPL_ELEMENT_DELETE_CONFIRM'));
        ?>

        <div class="row">

            <?foreach ($arResult['ITEMS'] as $key => $arItem){

                $this->AddEditAction($arItem['ID'], $arItem['EDIT_LINK'], $strElementEdit);
                $this->AddDeleteAction($arItem['ID'], $arItem['DELETE_LINK'], $strElementDelete, $arElementDeleteParams);
                $strMainID = $this->GetEditAreaId($arItem['ID']);

                $arItemIDs = array(
                    'ID' => $strMainID,
                    'PICT' => $strMainID.'_pict',
                    'SECOND_PICT' => $strMainID.'_secondpict',
                    'STICKER_ID' => $strMainID.'_sticker',
                    'SECOND_STICKER_ID' => $strMainID.'_secondsticker',
                    'QUANTITY' => $strMainID.'_quantity',
                    'QUANTITY_DOWN' => $strMainID.'_quant_down',
                    'QUANTITY_UP' => $strMainID.'_quant_up',
                    'QUANTITY_MEASURE' => $strMainID.'_quant_measure',
                    'BUY_LINK' => $strMainID.'_buy_link',
                    'SUBSCRIBE_LINK' => $strMainID.'_subscribe',

                    'PRICE' => $strMainID.'_price',
                    'DSC_PERC' => $strMainID.'_dsc_perc',
                    'SECOND_DSC_PERC' => $strMainID.'_second_dsc_perc',

                    'PROP_DIV' => $strMainID.'_sku_tree',
                    'PROP' => $strMainID.'_prop_',
                    'DISPLAY_PROP_DIV' => $strMainID.'_sku_prop',
                    'BASKET_PROP_DIV' => $strMainID.'_basket_prop',
                );

                $strObName = 'ob'.preg_replace("/[^a-zA-Z0-9_]/", "x", $strMainID);

                $productTitle = (
                isset($arItem['IPROPERTY_VALUES']['ELEMENT_PAGE_TITLE'])&& $arItem['IPROPERTY_VALUES']['ELEMENT_PAGE_TITLE'] != ''
                    ? $arItem['IPROPERTY_VALUES']['ELEMENT_PAGE_TITLE']
                    : $arItem['NAME']
                );
                $imgTitle = (
                isset($arItem['IPROPERTY_VALUES']['ELEMENT_PREVIEW_PICTURE_FILE_TITLE']) && $arItem['IPROPERTY_VALUES']['ELEMENT_PREVIEW_PICTURE_FILE_TITLE'] != ''
                    ? $arItem['IPROPERTY_VALUES']['ELEMENT_PREVIEW_PICTURE_FILE_TITLE']
                    : $arItem['NAME']
                );?>


                <div class="col-xs-4 usligi-pict" id="<? echo $strMainID; ?>">
                    <?$img = 'http://placehold.it/230x230';
                    if( $arItem['PREVIEW_PICTURE']['ID'] ){
                        #$img = CFile::GetPath($arItem['PREVIEW_PICTURE']['ID']);
                        $img = i($arItem['DETAIL_PICTURE']['ID'], 262, 264, BX_RESIZE_IMAGE_EXACT);
                        $imgBig = $arItem['DETAIL_PICTURE']['SRC'];
                    }?>
                    <a href="<?=$imgBig?>" rel="<?=$arItem['IBLOCK_SECTION_ID']?>" class="fancybox" >
                        <img
                            src="<?=$img?>"
                            title="<? echo $arItem['PREVIEW_PICTURE']['TITLE']; ?>"
                            alt="<? echo $arItem['PREVIEW_PICTURE']['ALT']; ?>"
                            />
                    </a>
                    <p class="text-pict"><p><?=$arItem['NAME']; ?></p></p>
                    <p class="text-fio"><?=$arItem['DETAIL_TEXT']?></p>
                    <?if( $collectionName ){?>
                        <p class="text-coll">Коллекция:</p>
                        <p class="text-coll-link">
                            <?if( $collectionLink ){?>
                                <a href="<?=$collectionLink;?>"><?=$collectionName?></a>
                            <?}
                            else{?>
                                <span><?=$collectionName?></span>
                            <?}?>
                        </p>
                    <?}?>
                </div>

            <?}?>
        </div>

    </div>
<?}?>