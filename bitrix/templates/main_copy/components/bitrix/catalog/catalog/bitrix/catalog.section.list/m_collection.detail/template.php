<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);
?>

<?        
$idSection = $arResult['SECTION']['ID'];
$sectionsHeader = getElementSectionsHTML($idSection, 'section');
$sectionInfo = $arResult['SECTION_INFO'];

$picture = $arResult['SECTION']['PICTURE'];
$morePhoto = $sectionInfo['UF_MORE_PHOTO'];
$topDescription = $arResult['SECTION_INFO']['~UF_SHORT_DESCRIPTION'];
$officialDistr = $arResult['SECTION_INFO']['~UF_OFFICIAL_DISTR'];
$textDescription = $arResult['SECTION_INFO']['~UF_TEXT_DESCRIPTION'];
$chainStore = $arResult['SECTION_INFO']['~UF_SHOW_CHAIN_STORE'];
$showCheaper = $arResult['SECTION_INFO']['~UF_SHOW_CHEAPER'];
?>
 <div class="hidden-xs">
    <div class="container"><div class="menu-link"><?=$sectionsHeader['DESKTOP']?></div></div>
    <div class="container">
        <div class="row obj">
            <div class="col-sm-6 gallery-container-collections"> 
                <?if( $picture || eRU($morePhoto) ){?>
                    <a class="gallery-zoom-link">
                        <div class="zoom"></div>
                    </a>
                <?}?>

                <div class="gallery-container-collections-gallery" data-nav="thumbs" data-arrows="false">
               
                    <?if( $picture ){
                        $s = i($picture, 555,396);
                        $b = CFile::GetPath($picture);
                        ?>
                        <a href="<?=$b;?>" class="gallery-preview-link" data-thumb="<?=$s;?>"><img src="<?=$s;?>" class="gallery-preview-image"></a>
                    <?}
                    else{?>
                        <?$img = "/upload/images/net-foto-detail.jpg";?>
                        <a href="javascript:void(0);" class="gallery-preview-link" ><img src="<?=$img?>" class="gallery-preview-image"></a>
                    <?}?>
                    <?if( $picture || eRU($morePhoto) ){?>
                        <?if( eRU($morePhoto) ){
                            foreach($morePhoto as $photo){
                                $s = i($photo, 115,115, BX_RESIZE_IMAGE_EXACT);
                                $b = CFile::GetPath($photo);
                                ?>
                                <a href="<?=$b;?>" class="gallery-preview-link" data-thumb="<?=$s;?>"><img src="<?=$s;?>" class="gallery-preview-image"></a>
                                <?
                            }
                        }else?>
                    <?}?>
                </div>


                <!--<div id="openModal" class="_modalDialog">
                    <div>
                        <a href="#close" title="Закрыть" class="closed"></a>
                    </div>
                </div>-->
            </div>
            
            <div class="col-sm-6">
                <div class="row obj m-collection-advantages">
                    <?/*$APPLICATION->IncludeComponent("bitrix:main.include", ".default", array(
                            "AREA_FILE_SHOW" => "file",
                            "PATH" => SITE_DEFAULT_TEMPLATE_PATH."include/collection-advantages.php",
                            "EDIT_TEMPLATE" => ""
                        ),false
                    );*/?>
                    <?$APPLICATION->IncludeFile(SITE_DEFAULT_TEMPLATE_PATH."/include/collection-advantages.php", Array(), Array(
                        "MODE"      => "html",                                    
                        "NAME"      => "collection-advantages",     
                        "TEMPLATE"  => ""                  
                        ));?>
                </div>
            </div>
            
            <?if($USER->isAdmin()){?>
            <div class="col-sm-6">
                <div class="row obj company-planeta-plitki">
                    <?/*<img width="65" alt="garantya" src="/upload/medialibrary/a68/garantya_new.png" height="65" title="garantya.jpg" align="left" hspace="10">*/?>
                    <img alt="garantya" src="/upload/medialibrary/a68/garantya_new.png" title="garantya.jpg" align="left" hspace="10" vspace="10">
                
            <?if($officialDistr){?>
                    <?=$officialDistr;?>
            <?}else {?>
                    <?$APPLICATION->IncludeFile(SITE_DEFAULT_TEMPLATE_PATH."/include/company_planeta_plitki.php", Array(), Array(
                        "MODE"      => "html",                                    
                        "NAME"      => "company_planeta_plitki",     
                        "TEMPLATE"  => ""                  
                        ));?>
            <?}?>
                </div>
            </div>
            
            <div class="col-sm-6">
                <div class="row obj m-sample">
                    <?if(!$chainStore){?>
                        <?$APPLICATION->IncludeFile(SITE_DEFAULT_TEMPLATE_PATH."/include/sample-view.php", Array(), Array(
                        "MODE"      => "html",                                    
                        "NAME"      => "sample-view",     
                        "TEMPLATE"  => ""                  
                        ));?>
                        
                    <?}?>
            <?if($chainStore){?>
                <p class="sample-view">
                    <span>Образец представлен во всей&nbsp;</span><a href="http://planetaplitki.ru/contacts/" target="_blank">сети магазинов "Планета Плитки"</a><span style="text-align: right;"></span>
                </p>
                <?if($showCheaper){?>
                    <p class="nashli-deshevle">
                        <?/*<a href="/all-shares/nashli-deshevle/" target="_blank"><img width="354" alt="nashli-deshevle-b.png" src="/upload/medialibrary/8a2/nashli_deshevle_b.png" height="37" title="nashli-deshevle-b.png"></a>*/?>
                        <a href="/all-shares/nashli-deshevle/" target="_blank">Нашли дешевле? Предложим цену ниже!</a>
                    </p>
                <?}?>
            <?}?>
                </div>
            </div>
            <?if($textDescription){?>
                <div class="col-sm-6 col-text-description">
                    <div class="row obj">
                        <p class="text-description">
                            <?=$textDescription;?>
                        </p>
                    </div>
                </div>
            <?}?>
            <?}?>
            
          <?/*if( $topDescription ){?>
                <div class="col-sm-6">
                    <div class="row obj">
                        <?=$topDescription;?>
                    </div>
                </div>
            <?}*/?>
        </div>
    </div>
</div>

