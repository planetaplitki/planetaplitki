<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>

<?if ($arResult["isFormErrors"] == "Y"):?>
    <script>
	    Recaptchafree.reset();
    </script>
<?=$arResult["FORM_ERRORS_TEXT"];?><?endif;?>

<?//reload page after adding form
if($_REQUEST['formresult'] == 'addok' && $arResult['arForm']['ID'] == $_REQUEST['WEB_FORM_ID']){?>
    <script>
	    Recaptchafree.reset();

        setTimeout(function() {
                location.reload();
            }, 3000
        );
        
    </script>
<?}?>
<?echo($arResult['arForm']['DESCRIPTION']);?>
<?echo "<br><br><br>";?>

<?=$arResult["FORM_NOTE"]?>

<?if ($arResult["isFormNote"] != "Y"){?>

<?=$arResult["FORM_HEADER"]?>
     
    <?
    # заказать звонок
    ob_start();
    foreach ($arResult["QUESTIONS"] as $FIELD_SID => $arQuestion){
        $question = $arResult['arQuestions'][$FIELD_SID];
        $idSite = $arQuestion['STRUCTURE'][0]['ID'];
        $idAdminka = $arQuestion['STRUCTURE'][0]['FIELD_ID'];
        $type = $arQuestion['STRUCTURE'][0]['FIELD_TYPE'];

        if( $idAdminka == 11 ){?>
            <div class="col-sm-2 text-left">
                <select class="filters select" id="ModalInput<?=$idSite?>" name="form_dropdown_<?=$FIELD_SID?>">
                    <? $select = $arResult['arDropDown'][$FIELD_SID];
                    foreach( $select['reference'] as $k => $variant ){?>
                        <option <?=$_REQUEST['form_dropdown_'.$FIELD_SID] == $select['reference_id'][$k] ? 'selected' : ''; ?> value="<?=$select['reference_id'][$k]?>"><?=$variant?></option>
                    <?}?>
                </select>
            </div>
        <?}
        elseif( $idAdminka == 12 ){?>
            <div class="col-sm-5">
                <div class="input-group date">
                    <input type="text" name="form_text_<?=$idSite?>" value="<?=htmlspecialchars($_REQUEST['form_text_'.$idSite]);?>" class="filters clear"><span class="input-group-addon"><i class="icon-cal"></i></span>
                </div>
            </div>
        <?}?>
    <?}
    $content = ob_get_contents();
    ob_end_clean();
    ?>

    <?foreach ($arResult["QUESTIONS"] as $FIELD_SID => $arQuestion){?>

        <?
        $question = $arResult['arQuestions'][$FIELD_SID];
        $idSite = $arQuestion['STRUCTURE'][0]['ID'];
        $idAdminka = $arQuestion['STRUCTURE'][0]['FIELD_ID'];
        $type = $arQuestion['STRUCTURE'][0]['FIELD_TYPE'];

        if( $idAdminka == 31 || $idAdminka == 37 ){?>
            <div class="row">
                <div class="col-sm-3 modal-text text-right"><?=$arQuestion["CAPTION"]?> <span><?if ($arQuestion["REQUIRED"] == "Y"):?><?=$arResult["REQUIRED_SIGN"];?><?endif;?></span></div>
                <div class="col-sm-7">
                    <input type="text" readonly class="filters" name="form_text_<?=$idSite?>" id="ModalInput<?=$idSite?>" value="<?=$arParams['UF_ELEMENT_NAME']?>">
                </div>
            </div>
        <?}
        elseif( $type == 'textarea' ){?>
            <div class="row">
                <div class="col-sm-3 modal-text text-right"><?=$arQuestion["CAPTION"]?> <span><?if ($arQuestion["REQUIRED"] == "Y"):?><?=$arResult["REQUIRED_SIGN"];?><?endif;?></span></div>
                <div class="col-sm-7"><textarea name="form_<?=$type?>_<?=$idSite?>" class="filters textarea-fleld"><?=htmlspecialchars($_REQUEST['form_'.$type.'_'.$idSite]);?></textarea></div>
            </div>
        <?}
        elseif(
            ($type == 'text' || $type == 'email') &&
            $idAdminka != 11 &&
            $idAdminka != 12
        ){?>
            <div class="row">
                <div class="col-sm-3 modal-text text-right"><?=$arQuestion["CAPTION"]?> <span><?if ($arQuestion["REQUIRED"] == "Y"):?><?=$arResult["REQUIRED_SIGN"];?><?endif;?></span></div>
                <div class="col-sm-7"><input type="<?=$type;?>" class="filters" name="form_<?=$type;?>_<?=$idSite?>" id="ModalInput<?=$idSite?>" value="<?=htmlspecialchars($_REQUEST['form_'.$type.'_'.$idSite]);?>"></div>
            </div>
        <?}
        elseif($idAdminka == 11){?>
            <div class="row">
                <div class="col-sm-3 modal-text text-right">Удобное время <span>*</span></div>
                <?=$content;?>
            </div>
        <?}?>
    <?}?>
	<input type="hidden" name="hooorai" id="hooorai" />
	
	        <div class="row">
        <div class="col-sm-7 col-sm-offset-3">
         
         <?
$capCode = $GLOBALS["APPLICATION"]->CaptchaGetCode();
?>
<input type="hidden" name="captcha_sid" value="<?= htmlspecialchars($capCode)?>">
<img align="left" src="/bitrix/tools/captcha.php?captcha_sid=<?= htmlspecialchars($capCode)?>" width="" height="40" alt="CAPTCHA" style="display: none;">
<input type="hidden" name="captcha_word" size="20" maxlength="20" value="">

<div class="g-recaptcha" data-sitekey="6LfWlREUAAAAANAT36Nqn9Qc8l6a5-kiLlpXDnyq"></div>
        </div>
    </div>
	    <br>
    <div class="row">
        <div class="col-sm-7 col-sm-offset-3">
	       
            <input type="submit" name="web_form_submit" class="btn modal-buttom-form" value="Отправить">
        </div>
    </div>

    <?=$arResult["FORM_FOOTER"]?>
<?}?>
	   