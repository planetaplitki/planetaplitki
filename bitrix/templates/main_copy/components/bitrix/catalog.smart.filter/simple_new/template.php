<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);

//echo '<pre>'; print_r($arResult['ITEMS']); echo '</pre>';
$prices = array();
$minCount = 1;
//$excludePrices = array(10,12);
$excludePrices = array(11,13);

$show_props = array(
    'BASE' => array(
        'VYSOTA',
        'SHIRINA',
        'POVERKHNOST',
        'PRICE',
        'TSENANASAYTE',
        'kategoriyatovar',
        'S_RESET',
    ),
    'ADVANCES' => array(
        'S_ADVANTAGE_FILTER',
        'STRANA',
        'PROIZVODITEL',
        'KOLLEKTSIYA',
        'tematikatovar',
        'tsvettovar',
        'naznachenietovar'
    ),
);
$colors = getColors();

$statuses = array(
    array(
        'NAME' => 'новинка',
        'VALUE' => 'UF_NEW_COLLECTION'
    ),
    array(
        'NAME' => 'распродажа',
        'VALUE' => 'UF_SALE'
    ),
    array(
        'NAME' => 'эксклюзив',
        'VALUE' => 'UF_EXCLUSIVE'
    ),
);

$sortVariants = array(
    array(
        'NAME' => 'по возрастанию цены',
        'VALUE' => 'asc'
    ),
    array(
        'NAME' => 'по убыванию цены',
        'VALUE' => 'desc'
    ),
);


//pRU($colors,'all');

foreach($arResult['ITEMS'] as $key => $arItem){   
    if( $arItem["PRICE"] ){    
        if(
            //($arItem["VALUES"]["MAX"]["VALUE"] - $arItem["VALUES"]["MIN"]["VALUE"] <= 0) ||
            in_array($arItem['ID'], $excludePrices)
        ){
            continue;
        }
//        $prices[$key] = $arItem;
        $prices = $arItem;
    }
    else{
        continue;
    }
}

foreach($show_props as $groupId => $group){
    foreach($group as $prop){
        foreach($arResult['ITEMS'] as $key => $arItem){
            if( $prop == 'PRICE' ){
                $newItems[$groupId][$prices['CODE']] = $prices;
                break;
            }
            elseif( $prop == 'S_RESET' || $prop == 'S_ADVANTAGE_FILTER' ){
                $newItems[$groupId][$prop] = array(
                    'CODE' => $prop,
                );
            }
            elseif( $prop == $arItem['CODE'] ){
                if( $prop == 'VYSOTA' || $prop == 'SHIRINA' ){
                    foreach( $arItem['VALUES'] as &$value ){
                        $value['VALUE'] = str_replace(',', '.', $value['VALUE']);
                    }
                }
                uasort($arItem['VALUES'], "sortFields");
                $newItems[$groupId][$key] = $arItem;
                break;
            }
        }
    }
}

//pRU($arResult['ITEMS'],'all');
//pRU($newItems,'all');
?>

<form name="<?echo $arResult["FILTER_NAME"]."_form"?>" action="<?echo $arResult["FORM_ACTION"]?>" class="form-inline header-filter hidden-xs" id="catalogFilter">

    <?foreach($arResult["HIDDEN"] as $arItem):?>
        <input type="hidden" name="<?echo $arItem["CONTROL_NAME"]?>" id="<?echo $arItem["CONTROL_ID"]?>" value="<?echo $arItem["HTML_VALUE"]?>" />
    <?endforeach;?>

    <?foreach($newItems as $groupId => $arItems){?>
        <div class="row<?=$groupId == 'BASE' ? '' : ' extendedFilter';?>">
            <?foreach($arItems as $key => $arItem){      

                # prices
                $key = $arItem["ENCODED_ID"];
                if( isset($arItem["PRICE"]) ){        
                    continue;
                    ?>
                    <div class="col-sm-4 col-md-2">
                        <button type="button" class="btn slider-buttom filter-price-button">
                            <span class="filter-price-active-title">Цена: </span>
                            <span class="filter-price-active-from-value"></span> ‒
                            <span class="filter-price-active-to-value"></span>
                            <span class="filter-price-active-small">(руб.)</span>
                        </button>
                        <div class="arrow_box">
                            <div class="range">
                                <span class="range-input-label range-input-label-from">от:</span>
                                <span class="range-input-label range-input-label-to">до:</span>
                                <div class="slider-range">
                                    <input type="text" name="<?echo $arItem["VALUES"]["MIN"]["CONTROL_NAME"]?>" class="ui-slider-handle ui-state-default ui-corner-all" id="min_cost">
                                    <input type="text" name="<?echo $arItem["VALUES"]["MAX"]["CONTROL_NAME"]?>" class="ui-slider-handle ui-state-default ui-corner-all" id="max_cost">
                                </div>
                            </div>
                            <hr class="arrow-line">
                            <button type="button" class="priceFilter btn link-buttom">применить</button>
                        </div>
                    </div>

                    <?
                    $arItem["VALUES"]["MIN"]["VALUE"] = intval($arItem["VALUES"]["MIN"]["VALUE"]);
                    $arItem["VALUES"]["MAX"]["VALUE"] = intval($arItem["VALUES"]["MAX"]["VALUE"]);
                    $ranges = array(
                        'PRICES' => array(
                            'MIN' => $arItem["VALUES"]["MIN"]["VALUE"],
                            'MAX' => $arItem["VALUES"]["MAX"]["VALUE"],
                            'MIN_CURRENT' => $arItem["VALUES"]["MIN"]["HTML_VALUE"] ?: $arItem["VALUES"]["MIN"]["VALUE"],
                            'MAX_CURRENT' => $arItem["VALUES"]["MAX"]["HTML_VALUE"] ?: $arItem["VALUES"]["MAX"]["VALUE"],
                        ),
                    );

                    # массив допустимых цен на коллекции
                    $APPLICATION->AddHeadString('<script>window.ranges='.json_encode($ranges).'</script>',true);
                }
                else{?>
                    <div class="col-sm-4 col-md-2">
                        <?if( $arItem['CODE'] == 'S_RESET' ){?>
                            <a href="<?=CURPAGE?>" type="reset" class="btn reset-buttom">Сбросить фильтр</a>
                        <?}
                        elseif( $arItem['CODE'] == 'S_ADVANTAGE_FILTER' ){?>
                            <div class="text-box"><a>Расширенный поиск</a></div>
                        <?}
                        else{
                            $arCur = current($arItem["VALUES"]);  
                            switch ($arItem["DISPLAY_TYPE"])
                            {              
                                case "A"://NUMBERS TSENA
                                {            
                                    ?>
                                        <button type="button" class="btn slider-buttom filter-price-button">
                                            <span class="filter-price-active-title">Цена: </span>
                                            <span class="filter-price-active-from-value"></span> ‒
                                            <span class="filter-price-active-to-value"></span>
                                            <span class="filter-price-active-small">(руб.)</span>
                                        </button>
                                        <div class="arrow_box">
                                            <div class="range">
                                                <span class="range-input-label range-input-label-from">от:</span>
                                                <span class="range-input-label range-input-label-to">до:</span>
                                                <div class="slider-range">
                                                    <input type="text" name="<?echo $arItem["VALUES"]["MIN"]["CONTROL_NAME"]?>" value="<?//=($arItem["VALUES"]["MIN"]["HTML_VALUE"] > $arItem["VALUES"]["MIN"]["VALUE"])?$arItem["VALUES"]["MIN"]["HTML_VALUE"]:""?>" class="ui-slider-handle ui-state-default ui-corner-all" id="min_cost">
                                                    <input type="text" name="<?echo $arItem["VALUES"]["MAX"]["CONTROL_NAME"]?>" value="<?//=($arItem["VALUES"]["MAX"]["HTML_VALUE"] < $arItem["VALUES"]["MAX"]["VALUE"])?$arItem["VALUES"]["MAX"]["HTML_VALUE"]:""?>" class="ui-slider-handle ui-state-default ui-corner-all" id="max_cost">
                                                </div>
                                            </div>
                                            <hr class="arrow-line">
                                            <button type="button" class="priceFilter btn link-buttom">применить</button>
                                        </div>
                                        <?
                                        $arItem["VALUES"]["MIN"]["VALUE"] = intval($arItem["VALUES"]["MIN"]["FILTERED_VALUE"] ? $arItem["VALUES"]["MIN"]["FILTERED_VALUE"] : $arItem["VALUES"]["MIN"]["VALUE"]);
                                        $arItem["VALUES"]["MAX"]["VALUE"] = intval($arItem["VALUES"]["MAX"]["FILTERED_VALUE"] ? $arItem["VALUES"]["MAX"]["FILTERED_VALUE"] : $arItem["VALUES"]["MAX"]["VALUE"]);
                                        
                                        $ranges = array(
                                            'PRICES' => array(
                                                'MIN' => $arItem["VALUES"]["MIN"]["VALUE"],
                                                'MAX' => $arItem["VALUES"]["MAX"]["VALUE"],
                                                'MIN_CURRENT' => $arItem["VALUES"]["MIN"]["HTML_VALUE"] ?: $arItem["VALUES"]["MIN"]["VALUE"],
                                                'MAX_CURRENT' => $arItem["VALUES"]["MAX"]["HTML_VALUE"] ?: $arItem["VALUES"]["MAX"]["VALUE"],
                                            ),
                                        ); 
                                        //echo '<pre>'; print_r($ranges); echo '</pre>';

                                        # массив допустимых цен на коллекции
                                        $APPLICATION->AddHeadString('<script>window.ranges='.json_encode($ranges).'</script>',true);
                                }  
                                    break;
                                case "N"://NUMBERS   
                                    break;
                                case "G"://CHECKBOXES_WITH_PICTURES
                                    break;
                                case "H"://CHECKBOXES_WITH_PICTURES_AND_LABELS
                                    break;
                                case "P"://DROPDOWN
                                    break;
                                default://CHECKBOXES
                                                 
                                    # при внешней фильтрации по свойствам товаров
                                    $selectedProp = "";
                                    $selectedPropValue = $GLOBALS[$arParams['FILTER_NAME']]['PROPERTY'][$arItem['CODE']];

                                    if( eRU($arItem["VALUES"][$selectedPropValue]) ){
                                        $selectedProp = "Y";
                                    }

                                    $count = count($arItem["VALUES"]);
                                    $minCountFlag = $count == $minCount ? "Y" : "";

                                    $name = $cur_name = $cur_id = $cur_val = "";
                                    $caption = $arItem['NAME'].' (все)';

                                    foreach($arItem["VALUES"] as $val => $ar){
                                        if(
                                            $ar['CHECKED'] ||
                                            $minCountFlag ||
                                            ($selectedProp && $selectedPropValue == $val)
                                        ){
                                            $name = $ar["CONTROL_ID"];
                                            if( $minCountFlag || $selectedPropValue == $val ){
                                                $caption = $ar["VALUE"];
                                                $cur_name = $ar["CONTROL_NAME"];
                                                $cur_id = $ar["CONTROL_ID"];
                                                $cur_val = $ar["HTML_VALUE"];
                                            }
                                            break;
                                        }
                                    }?>

                                    <?if($arItem['CODE'] == 'tsvettovar' ){?>
                                        <?#pRU($arItem,'all')?>
                                        <div class="filter-popup-toggler"><?=$caption?></div>
                                        <div class="filter-popup">
                                            <div class="filter-popup-inner">
                                                <?foreach( $arItem['VALUES'] as $ar ){?>
                                                    <div class="checkbox color-checkbox">
                                                        <input type="checkbox"
                                                           name="<?=$ar["CONTROL_NAME"]?>"
                                                            <?=$ar["CHECKED"]? 'checked': '' ?>
                                                           id="filter-check<?=$ar['CONTROL_ID']?>"
                                                           value="<?=$ar['HTML_VALUE']?>"
                                                        />
                                                        <label for="filter-check<?=$ar['CONTROL_ID']?>">
                                                            <div class="filter-checkbox-color" style="background-image: url(<?=$colors[$ar['URL_ID']]?>)"></div>
                                                            <span class="filter-checkbox-text"><?=$ar['VALUE']?></span>
                                                        </label>
                                                    </div>
                                                <?}?>
                                            </div>
                                            <div class="filter-popup-submit">
                                                <button type="button" class="btn link-buttom filter-popup-button">применить</button>
                                            </div>
                                        </div>
                                    <?}
                                    else{?>
                                        <select class="filters select" id="select_type1" name="<?=$name?>" >
                                            <option <?if($cur_id):?>
                                            value="<?=$cur_val?>"
                                            name="<?=$cur_name?>"
                                            id="<?=$cur_id?>"
                                            selected="selected"
                                            <?endif;?>
                                            ><?=$caption?></option>
                                            <?foreach($arItem["VALUES"] as $val => $ar){
                                                if($ar["DISABLED"] || $selectedProp || $minCountFlag){
                                                    echo '<pre>'; print_r($ar); echo '</pre>';
                                                    continue;
                                                }?>
                                                <option
                                                    value="<? echo $ar["HTML_VALUE"] ?>"
                                                    name="<? echo $ar["CONTROL_NAME"] ?>"
                                                    id="<? echo $ar["CONTROL_ID"] ?>"
                                                    <?=$ar["CHECKED"]? 'selected': '' ?>
                                                    />
                                                    <?=$ar["VALUE"];?>
                                                </option>
                                            <?}?>
                                        </select>
                                    <?}?>

                                <?}
                        }?>
                    </div>
                <?}?>



                <?

                /*# not prices
                foreach($arResult["ITEMS"] as $key=>$arItem)
                {
                    if(
                        empty($arItem["VALUES"])
                        || isset($arItem["PRICE"])
                    )
                        continue;

                    if (
                        $arItem["DISPLAY_TYPE"] == "A"
                        && (
                            $arItem["VALUES"]["MAX"]["VALUE"] - $arItem["VALUES"]["MIN"]["VALUE"] <= 0
                        )
                    )
                        continue;
                    ?>


                <?}*/
            }?>
        </div>
    <?}?>

    <input type="submit" id="set_filter" name="set_filter" value="<?=GetMessage("CT_BCSF_SET_FILTER")?>" />

    <hr class="line-filter">

    <div class="row">

        <?=getStatusHtml($statuses, 'STATUSES', 'Статус');?>


        <div class="row obj hidden-sm hidden-md hidden-lg">
            <div class="col-xs-12">
                <p class="chouse-text">Подходят: <span><?=$countResults?></span> <?=$countResultsFormatted?></p>
            </div>
        </div>

        <?=getSortHtml($sortVariants, 'PRICES', 'Сортировать по');?>

        <div class="col-xs-2 col-sm-2 filter_switch_view_wrap">
            <div class="switch_view_wrap">
                <div class="label-text2 switch_view_l">Коллекции</div>
                <div class="onoffswitch">
                    <input type="checkbox" name="onoffswitch" class="onoffswitch-checkbox" id="myonoffswitch">
                    <label class="onoffswitch-label" for="myonoffswitch"></label>
                </div>
                <div class="label-text2 switch_view_r">Элементы</div>
            </div>
        </div>
        <div class="col-xs-3 col-sm-3 pull-right">

            <div class="text-right hidden-xs">
                <div class="label-text">Показывать по:</div>
                <div class="list-block">

                    <?
                    $pageElementCount = array(
                        array(
                            'VALUE' => '40',
                            'ACTIVE' => '',
                        ),
                        array(
                            'VALUE' => '80',
                            'ACTIVE' => 'Y',
                        ),
                        array(
                            'VALUE' => '120',
                            'ACTIVE' => '',
                        ),
                    );
                    ?>

                    <ul class="list-block-item">
                        <?
                        $showAll = false;
                        ?>
                        <?foreach($pageElementCount as $k => $arItem){
                            $count = $arItem['VALUE'];
                            ?>

                            <?if(
                                !$_REQUEST['PAGE_ELEMENT_COUNT'] && !$_REQUEST['SHOWALL_1'] && $arItem['ACTIVE'] == "Y" ||
                                $count == $_REQUEST['PAGE_ELEMENT_COUNT'] ||
                                $_REQUEST['SHOWALL_1'] && $count == 'ALL'
                            ){?>
                                <li class="active"><a><?=$count == 'ALL' ? 'Все' : $count;?></a></li>
                            <?}
                            else{?>
                                <?if( $count == 'ALL' ){?>
                                    <li><a href="<?=$APPLICATION->GetCurPageParam('SHOWALL_1=1', array('PAGE_ELEMENT_COUNT', 'SHOWALL_1'));?>" >все</a></li>
                                <?}
                                else{?>
                                    <li><a href="<?=$APPLICATION->GetCurPageParam('PAGE_ELEMENT_COUNT='.$count.'&SHOWALL_1=0', array('PAGE_ELEMENT_COUNT', 'SHOWALL_1'));?>" ><?=$count?></a></li>
                                <?}?>
                            <?}?>
                        <?}?>
                    </ul>
                </div>
            </div>
        </div>
    </div>

</form>


<!--mobile-->

<?$this->SetViewTarget("catalog_mobile_filter");?>

<hr class="mobileForm line-filter">
<div class="row obj hidden-sm hidden-md hidden-lg">
    <div class="col-xs-12">
        <p class="chouse-text">Подходят: <span><?=$countResults?></span> <?=$countResultsFormatted?></p>
    </div>
</div>

<a data-toggle="collapse" href="#menu-filtr" class="filter-mobile-toggle">
    <div class="hidden-sm hidden-md hidden-lg chouse-box"><div class="gear"></div></div>
</a>

<?/*?>
<select class="filters select" id="select_mode_view" name="modeView" multiple>
    <option value="collection" <?=$_REQUEST['modeView'] != 'items' ? 'selected' : ''?>>Коллекции</option>
    <option value="items" <?=$_REQUEST['modeView'] == 'items' ? 'selected' : ''?>>Элементы</option>
</select>
<?*/?>

<?
# группируем мобильные свойства
$show_props = array(
    array(
        'NAME' => 'Производитель',
        'ITEMS' => array(
            'STRANA',
            'PROIZVODITEL',
        ),
    ),
    array(
        'NAME' => 'Габариты',
        'ITEMS' => array(
            'VYSOTA',
            'SHIRINA',
        ),
    ),
    array(
        'NAME' => 'Тип',
        'ITEMS' => array(
            'POVERKHNOST',
            'kategoriyatovar',
        ),
    ),
    array(
        'NAME' => 'Коллекция \ тематика',
        'ITEMS' => array(
            'KOLLEKTSIYA',
            'tematikatovar',
        ),
    ),
);

$newItems = array();

foreach($show_props as $groupId => $group){

    $groupParams = array();
    $groupParams['NAME'] = $group['NAME'];

    foreach($group['ITEMS'] as $prop){
        foreach($arResult['ITEMS'] as $key => $arItem){
            if( $prop == 'PRICE' ){
                break;
            }
            elseif( $prop == $arItem['CODE'] ){
                $groupParams['ITEMS'][$key] = $arItem;
                break;
            }
        }
    }

    $newItems[$groupId] = $groupParams;
}

//pRU($newItems,'all');
?>

<div class="mobileForm">

    <form class="form-inline header-filter" role="form" name="<?echo $arResult["FILTER_NAME"]."_form"?>" action="<?echo $arResult["FORM_ACTION"]?>" method="get" id="mobileFilter">

        <div class="row">
            <?=getStatusHtml($statuses, 'STATUSES', 'Статус');?>
            <?=getSortHtml($sortVariants, 'PRICES', 'Сортировать по')?>
        </div>

        <div class="collapse menu-col-mob" id="menu-filtr">
            <ul class="menu-top-box-mob">
                <?foreach($arResult['ITEMS'] as $key => $arItem){
                    if( $arItem["PRICE"] ){
                        if(
                            ($arItem["VALUES"]["MAX"]["VALUE"] - $arItem["VALUES"]["MIN"]["VALUE"] <= 0) ||
                            in_array($arItem['ID'], $excludePrices)
                        ){
                            continue;
                        }?>

                        <li>
                            <a data-toggle="collapse" data-target="#toggleDemo1" data-parent="#sidenav1">
                                Цена
                                <div class="col-mob">
                                    <span class="point"></span>
                                    <div class="text2">ПОКАЗАТЬ</div>
                                </div>
                            </a>
                            <div class="collapse" id="toggleDemo1">
                                <div class="row obj50">
                                    <div class="col-xs-6">
                                        <label class="sr-only" for="price"></label>
                                        <input type="text" class="filters"
                                               name="<?echo $arItem["VALUES"]["MIN"]["CONTROL_NAME"]?>"
                                               id="price"
                                               value="<?=$ranges['PRICES']['MIN_CURRENT']?>"
                                               placeholder="От <?=SaleFormatCurrency($ranges['PRICES']['MIN'], 'RUB')?>"
                                            >
                                    </div>
                                    <div class="col-xs-6">
                                        <label class="sr-only" for="width"></label>
                                        <input type="text" class="filters mnone"
                                               name="<?echo $arItem["VALUES"]["MAX"]["CONTROL_NAME"]?>"
                                               value="<?=$ranges['PRICES']['MAX_CURRENT']?>"
                                               id="width"
                                               placeholder="До <?=SaleFormatCurrency($ranges['PRICES']['MAX'], 'RUB')?>"
                                            >
                                    </div>
                                </div>
                            </div>
                        </li>
                    <?}
                }?>

                <?
                $i = 2;
                foreach( $newItems as $groupId => $group ){?>
                    <?
                    $name = $group['NAME'];
                    $show_props = $group['ITEMS'];
                    ?>
                    <li>
                        <a data-toggle="collapse" data-target="#toggleDemo<?=$i?>" data-parent="#sidenav<?=$i?>">
                            <?=$name;?>
                            <div class="col-mob">
                                <span class="point"></span>
                                <div class="text2">ПОКАЗАТЬ</div>
                            </div>
                        </a>
                        <div class="collapse" id="toggleDemo<?=$i?>">
                            <div class="row obj50">
                                <?foreach($show_props as $arItem) {

                                    # при внешней фильтрации по свойствам товаров
                                    $selectedProp = "";
                                    $selectedPropValue = $GLOBALS['arrSectionsFilter']['PROPERTY'][$arItem['CODE']];
                                    if( eRU($arItem["VALUES"][$selectedPropValue]) ){
                                        $selectedProp = "Y";
                                    }

                                    $count = count($arItem["VALUES"]);
                                    $minCountFlag = $count == $minCount ? "Y" : "";

                                    $name = "";
                                    $caption = $arItem['NAME'].' (все)';

                                    foreach($arItem["VALUES"] as $val => $ar){
                                        if(
                                            $ar['CHECKED'] ||
                                            $minCountFlag ||
                                            ($selectedProp && $selectedPropValue == $val)
                                        ){
                                            $name = $ar["CONTROL_ID"];
                                            if( $minCountFlag ){
                                                $caption = $ar["VALUE"];
                                            }
                                            break;
                                        }
                                    }?>

                                    <div class="col-xs-6">
                                        <select class="filters select" id="select_type1" name="<?=$name?>" >
                                            <option value=""><?=$caption?></option>
                                            <?foreach($arItem["VALUES"] as $val => $ar){
                                                if($ar["DISABLED"] || $selectedProp || $minCountFlag){
                                                    continue;
                                                }?>
                                                <option
                                                    value="<? echo $ar["HTML_VALUE"] ?>"
                                                    name="<? echo $ar["CONTROL_NAME"] ?>"
                                                    id="<? echo $ar["CONTROL_ID"] ?>"
                                                    <?=$ar["CHECKED"]? 'selected': '' ?>
                                                    />
                                                    <?=$ar["VALUE"];?>
                                                </option>
                                            <?}?>
                                        </select>
                                    </div>
                                    <?
                                }?>
                            </div>
                        </div>
                    </li>
                    <?
                    $i++;
                }?>
            </ul>
            <div class="buttons">
                <button type="submit" name="set_filter" class="btn link-buttom">Фильтровать</button>
                <a href="<?=CURPAGE?>" class="btn link-up">Сбросить</a>
            </div>
        </div>
<!--        <input type="hidden" name="setFilter" value="y" />-->
        <?/*?><input type="submit" value="Фильтровать" name="submit" /><?*/?>
    </form>
</div>

<?$this->EndViewTarget("catalog_mobile_filter");?>


<script>                     
    var smartFilter = new JCSmartFilter('<?echo CUtil::JSEscape($arResult["FORM_ACTION"])?>', 'horizontal');
</script>