<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */

# имя
$fio = htmlspecialchars($_REQUEST['1CLICK_NAME']);
if( !$fio ){
    $fio = trim($arResult['USER']['NAME']);
    if( !$fio ){
        $fio = '';
    }
}

# телефон
$phone = htmlspecialchars($_REQUEST['1CLICK_PHONE']);
if( !$phone ){
    $phone = $arResult['USER']['PERSONAL_PHONE'];
    if( !$phone ){
        $phone = '';
    }
}

$price = $arResult['PRICE'];
$status = $arResult['STATUS'];
$idOrder = $arResult['ORDER_ID'];
?>

<?if( $status == 'success' ){?>
    <div class="successWrap">
        <?/*?><p>Спасибо Ваш заказ №<?=$idOrder;?> создан! В ближайшее время с Вами свяжется наш менеджер.</p><?*/?>
        <p>В ближайшее время с Вами свяжется наш менеджер.</p>
    </div>
<?}
else{?>
    <form class="form-inline header-filter" role="form" method="post" action="<?=CURPAGE?>" name="1CLICK_FORM" id="1ClickBuyItemForm">
        <div class="row">
            <div class="col-sm-3 modal-text text-right">Имя <span>*</span></div>
            <div class="col-sm-7">
                <input class="filters" placeholder="ФИО" value="<?=$fio?>" type="text" name="1CLICK_NAME" />
                <?/*?><input class="filters" pattern="[a-zа-я0-9\s-_]{2,}" placeholder="ФИО" value="<?=$fio?>" type="text" name="1CLICK_NAME" data-required="true" /><?*/?>
            </div>
        </div>
        <div class="row">
            <div class="col-sm-3 modal-text text-right">Телефон <span>*</span></div>
            <div class="col-sm-7">
                    <label class="my_before_after_number"></label><input type="tel" class="filters" value="<?=$phone?>" name="1CLICK_PHONE" >
                <?/*?><input type="tel" class="filters" placeholder="+7(ххх) ххх-хххх" pattern="(\+)?[0-9\s-\(\)]{5,}" value="<?=$phone?>" name="1CLICK_PHONE" data-required="true"><?*/?>
            </div>
        </div>
        <div class="row">
            <div class="col-sm-3 modal-text text-right"></div>
            <div class="col-sm-7">
                <div class="fast-block">
                    <div class="row">
                        <div class="col-sm-6">
                            <?$img = i($arResult['PICTURE'], 133, 133, BX_RESIZE_IMAGE_EXACT);?>
                            <img src="<?=$img;?>" alt="<?=$strAlt;?>" title="<?=$strTitle?>" />
                        </div>
                        <div class="col-sm-6">
                            <div class="text1"><?=$arResult['NAME'];?></div>
                            <div class="text2"><span><?=SaleFormatCurrency($price, 'RUB', true)?> Р </span>м2</div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-sm-7 col-sm-offset-3">
            <? if(!in_array('9',$USER-> GetUserGroupArray())):?>
                            <noindex>
                                <div class="col-sm-12 label-text2 confirm-block">
                                    <input type="checkbox" id="select_data3" required="" checked="checked" name="CONFIRM_POLITICS" value="y" <?=$confirmed == 'y' ? "checked" : "" ?> />
                                    <label for="select_data3"><span></span>Настоящим подтверждаю, что я ознакомлен и согласен с условиями политики конфиденциальности<i>* </i> <a href="" class="confirm-trigger" data-toggle="modal" data-target="#Modal3">Узнать больше</a></label>
                                </div>
                            </noindex>
                        <?endif;?>
                <button type="submit" onclick="yaCounter14449306.reachGoal('quick-sale'); return true;" class="btn modal-buttom-form">
                    <input type="hidden" name="act" value="click.buy" />
                    <input type="hidden" name="1CLICK_ITEM_ID" value="<?=$arResult['ID']?>" />
                    <input type="hidden" name="1CLICK_ITEM_COUNT" value="1" />
                    <input type="hidden" name="web_form_submit" value="Заказать" />
                    <input type="hidden" name="is_ajax" value="y" />
                    Отправить заказ
                </button>
            </div>
        </div>
    </form>

<?}?>