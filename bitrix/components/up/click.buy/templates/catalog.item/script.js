$(function(){

    var send_ajax_one_click = null;
    $('#Modalfast').on('show.bs.modal', function (e) {
        var itemId = $(e.relatedTarget).attr('data-itemid'),
            itemCount = $('.box-position.quantity input').val(),
            itemPrice = $('.finalSum .price span').text();
        //$('#1ClickBuyItemForm input[name="1CLICK_ITEM_ID"]').val(itemId);
        $('#1ClickBuyItemForm input[name="1CLICK_ITEM_COUNT"]').val(itemCount);
        $('#1ClickBuyItemForm .fast-block .text2 span').text(itemPrice);
    });

    //формат телефона
    //$.mask.definitions['~'] = "[+-]";
    //$('input[name="1CLICK_PHONE"]').mask("+7 (999) 999-9999");

    //валидация формы перед отправкой
    $("#1ClickBuyItemForm").validate({
        onKeyup : true,
        sendForm : false,
        highlight: function(element, errorClass) {
            $(element).closest('div').removeClass('has-success').addClass('has-error');
        },
        unhighlight: function(element, errorClass, validClass) {
            $(element).closest('div').removeClass('has-error').addClass('has-success');
        },
        rules: {
            '1CLICK_NAME': {
                required: true
            },
            '1CLICK_PHONE': {
                required: true
            }
        },
        messages: {
            '1CLICK_NAME': {
                required: "Введите имя контактного лица"
            },
            '1CLICK_PHONE': {
                required: "Введите номер телефона"
            }
        },
        // отправляем форму
        submitHandler: function() {
            $('#1ClickBuyItemForm button[type="submit"]').addClass('loading');
            if (send_ajax_one_click != null){
                return false;
            }
            send_ajax_one_click = $.ajax({
                //type: "post",
                url: $('#1ClickBuyItemForm').attr('action'),
                data: $('#1ClickBuyItemForm').serialize(),
                beforeSend: function ()
                {
                    //$(".clickBuyWrap").append( "<div class='aj-loader'></div>" );
                },
                success: function(response, status, xhr) {

                    $('#Modalfast .modal-body').html(response);

                    /*$(".aj-loader").remove();
                    var params = $.evalJSON(response);
                    if( params['STATUS'] == 'success' ){
                        $('.clickBuyWrap .status').html('<div class="success">'+params['MESSAGE']+'</div>');
                        $('.clickBuyWrap form').hide()
                        setTimeout(function(){
                            $('#modalClickBuy').modal('hide');
                            $('#1ClickBuyItemForm')[0].reset();
                            $('.clickBuyWrap .status').html('');
                            $('.clickBuyWrap form').show();
                        }, 6000);
                    }
                    else if( params['STATUS'] == 'fail' ){
                        $('.clickBuyWrap .status').html('<div class="fail">'+params['MESSAGE']+'</div>');
                    }*/
                },
            });
            return false;
        }
    });
});
