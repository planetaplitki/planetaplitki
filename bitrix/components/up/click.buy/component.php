<?if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>

<?/**
 * Bitrix vars
 * @var array $arParams
 * @var array $arResult
 * @var CBitrixComponent $this
 * @global CMain $APPLICATION
 * @global CUser $USER
 */
?>

<?
global $USER;
if($USER->IsAuthorized() ){
    $rsUser = CUser::GetByID($USER->GetID());
    $arUser = $rsUser->Fetch();
    $arResult['USER'] = $arUser;
}

$name = htmlspecialchars($_REQUEST['1CLICK_NAME']);
$phone = htmlspecialchars($_REQUEST['1CLICK_PHONE']);
$itemId = intval($_REQUEST['1CLICK_ITEM_ID']);
$quantity = intval($_REQUEST['1CLICK_ITEM_COUNT']) ? intval($_REQUEST['1CLICK_ITEM_COUNT']) : 1;

$arResult['ERRORS'] = array();
$arResult['PICTURE'] = $arParams['PICTURE'];
$arResult['ID'] = $arParams['1CLICK_ITEM_ID'];
$arResult['PRICE'] = $arParams['PRICE'];
$arResult['NAME'] = htmlspecialchars($arParams['NAME']);

if( $name && $phone && $itemId ){


    CModule::IncludeModule('iblock');
    CModule::IncludeModule('sale');
    CModule::IncludeModule('catalog');


    
    if (empty($arResult['USER'])){
        //Проверим есть ли у нас такой пользователь
        
        $password = randString(7);
        $fields = array(
            "NAME"              => $name,
            "LAST_NAME"         => $name,
            "EMAIL"             => $phone.'@oneclick.ru',
            "LOGIN"             => $phone,
            "LID"               => "ru",
            "ACTIVE"            => "N",
            "GROUP_ID"          => array(3,4,5,8),
            "PASSWORD"          => $password,
            "CONFIRM_PASSWORD"  => $password
        );
        $rsUser = CUser::GetByLogin($phone);
        $arUser = $rsUser->Fetch();
        if (empty($arUser)){
            $user = new CUser;
            $ID = $user->Add($fields);
            $rsUser = CUser::GetByID($ID);
            $arUser = $rsUser->Fetch();
            $arResult['USER'] = $arUser;
        } else {
            $arResult['USER'] = $arUser;
        }

    }
    //////////////////////////////////////////

    //получаем параметры товара
    $arLoadFields = array();
    $res = CIBlockElement::GetList(Array(), array("IBLOCK_ID" => IBLOCK_ID_CATALOG, "ACTIVE"=>"Y", "ID" => $itemId), false, false, array());
    while($ob = $res->GetNextElement()){
        $arFieldsItem = $ob->GetFields();

        // XML_ID товара
        if( !empty($arFieldsItem["XML_ID"]) ) {
            $arLoadFields["PRODUCT_XML_ID"] = $arFieldsItem["XML_ID"];
        }
    }

    // XML_ID каталога
    $resIb = CIBlock::GetByID(CATALOG_IBLOCK_ID);
    if($ar_resIb = $resIb->GetNext()) {
        if( !empty($ar_resIb["XML_ID"]) ) {
            $arLoadFields["CATALOG_XML_ID"] = $ar_resIb["XML_ID"];
        }
    }

    //////////////////////////////////////////

    //получаем корзину текущего пользователя и обнуляем ее
    $arBasketItems = array();

    $dbBasketItems = CSaleBasket::GetList(
        array(),
        array(
            "FUSER_ID" => CSaleBasket::GetBasketUserID(),
            "LID" => SITE_ID,
            "ORDER_ID" => "NULL"
        ),
        false,
        false,
        array()
    );
    while ($arItems = $dbBasketItems->Fetch()){
        $arBasketItems[] = $arItems;
    }

    //удаляем все товары из корзины текущего пользователя
    CSaleBasket::DeleteAll(CSaleBasket::GetBasketUserID());

    //////////////////////////////////////////

    $renewal = 'N';
    $arPrice = CCatalogProduct::GetOptimalPrice($itemId, $quantity, $USER->GetUserGroupArray(), $renewal);
    if (!$arPrice || count($arPrice) <= 0){
        if ($nearestQuantity = CCatalogProduct::GetNearestQuantityPrice($itemId, $quantity, $USER->GetUserGroupArray())){
            $quantity = $nearestQuantity;
            $arPrice = CCatalogProduct::GetOptimalPrice($itemId, $quantity, $USER->GetUserGroupArray(), $renewal);
        }
    }
    
    //Запишем заказ в инфоблок
    $element = new CIBlockElement;
    $properites = [
        'IBLOCK_ID' => 39,
        'NAME' => $name,
        'PROPERTY_VALUES' => [
            'PHONE' => $phone,
            'PRODUCT' => $arFieldsItem['NAME'],
            'QUANTITY' => $quantity,
            'PRICE' => $arPrice['DISCOUNT_PRICE']
        ]
    ];
    $element->Add($properites);
    //добавляем в корзину новый товар
    Add2BasketByProductID($itemId,$quantity, array(), array());
    /*$arFieldsBasket = array(
        "PRODUCT_ID" => $itemId,
        "PRODUCT_PRICE_ID" => $arPrice['PRICE']['CATALOG_GROUP_ID'],
        "PRICE" => $arPrice['DISCOUNT_PRICE'],
        "CURRENCY" => "RUB",
        "QUANTITY" => $quantity,
        "LID" => LANG,
        "DELAY" => "N",
        "CAN_BUY" => "Y",
        "NAME" => $arFieldsItem['NAME'],
        "DETAIL_PAGE_URL" => $arFieldsItem['DETAIL_PAGE_URL'],
        "PRODUCT_XML_ID" => $arLoadFields["PRODUCT_XML_ID"],
        "CATALOG_XML_ID" => $arLoadFields["CATALOG_XML_ID"],
    );

    CSaleBasket::Add($arFieldsBasket);*/


    $arResult['STATUS'] = 'success';
    $arResult['MESSAGE'] = 'Ваш заказ принят! Номер заказа <b style="font-size:18px;">'.$ORDER_ID.'</b>. Наш менеджер в ближайшее время обязательно с вами свяжется!';

    $strOrderList = "";
    $arBasketList = array();
    $dbBasketItems = CSaleBasket::GetList(
        array("ID" => "ASC"),
        array(
            "FUSER_ID" => CSaleBasket::GetBasketUserID(),
            "LID" => SITE_ID,
            "ORDER_ID" => "NULL"
        ),
        false,
        false,
        array("ID", "PRODUCT_ID", "NAME", "QUANTITY", "PRICE", "CURRENCY", "TYPE", "SET_PARENT_ID")
    );
    while ($arItem = $dbBasketItems->Fetch()){
        if (CSaleBasketHelper::isSetItem($arItem))
            continue;

        $arBasketList[] = $arItem;
    }

    $arBasketList = getMeasures($arBasketList);

    $sum = 0;

    if (!empty($arBasketList) && is_array($arBasketList))
    {
        foreach ($arBasketList as $arItem)
        {
            $measureText = (isset($arItem["MEASURE_TEXT"]) && strlen($arItem["MEASURE_TEXT"])) ? $arItem["MEASURE_TEXT"] : GetMessage("SOA_SHT");
            $strOrderList .= $arItem["NAME"]." - ".$arItem["QUANTITY"]." ".$measureText.": ".SaleFormatCurrency($arItem["PRICE"], $arItem["CURRENCY"]);
            $strOrderList .= "\n";
            $sum += $arItem['PRICE'] * $arItem['QUANTITY'];
        }
    }

    global $DB;
    $arEventFields = array(
        "ORDER_ID" => $ORDER_ID,
        "ORDER_DATE" => Date($DB->DateFormatToPHP(CLang::GetDateFormat("SHORT", SITE_ID))),
        "ORDER_USER" => $name,
        "PRICE" => SaleFormatCurrency($sum, "RUB"),
        "BCC" => "nahim@planetaplitki.ru",
        "EMAIL" => $email,
        "PHONE" => $phone,
        "FIO" => $name,
        "ORDER_LIST" => $strOrderList,
        "SALE_EMAIL" => "nahim@planetaplitki.ru",
        "DELIVERY_PRICE" => "",
        "ICE_USER_ID" => $USER->GetID()
    );
    
    //lead to B24
    /*$queryUrl = 'https://planetaplitki.bitrix24.ru/rest/18/lcvyhlg8tyyhtc8c/crm.lead.add.json';
    $queryData = http_build_query(array(
     'fields' => array(
     "TITLE" => '1click_'.$name,
     "NAME" => $name,
     "OPENED" => "Y",
     "OPPORTUNITY" => $sum,
     "COMMENTS" => $strOrderList."\n".SaleFormatCurrency($sum, "RUB"),
     "ASSIGNED_BY_ID" => 1,
     "PHONE" => array(array("VALUE" => $phone, "VALUE_TYPE" => "WORK" )),
     "EMAIL" => array(array("VALUE" => $email, "VALUE_TYPE" => "WORK" )),
     ),
     'params' => array("REGISTER_SONET_EVENT" => "Y")
    ));

    $curl = curl_init();
    curl_setopt_array($curl, array(
     CURLOPT_SSL_VERIFYPEER => 0,
     CURLOPT_POST => 1,
     CURLOPT_HEADER => 0,
     CURLOPT_RETURNTRANSFER => 1,
     CURLOPT_URL => $queryUrl,
     CURLOPT_POSTFIELDS => $queryData,
    ));

    $result = curl_exec($curl);
    curl_close($curl);

    $result = json_decode($result, 1);
    AddMessage2Log('b24_result = '.print_r($result, true),'');*/
    //lead end

    $eventName = "ONE_CLICK";
    $event = new CEvent;
    $event->Send($eventName, SITE_ID, $arEventFields, "Y");


    //удаляем все товары из корзины текущего пользователя
    //CSaleBasket::DeleteAll(CSaleBasket::GetBasketUserID());

    //возвращаем корзину пользователя до "покупки в 1 клик"
    foreach($arBasketItems as $arBasketItem){
        //CSaleBasket::Add($arBasketItem);
    }


    
    //////////////////////////////////////////

    if( empty($arResult['USER']) ) {
        $newUserID = 25;
    }

    //////////////////////////////////////////

    //создаём новый заказ
    $arFieldsOrder = array(
        "LID" => "s1",
        "PERSON_TYPE_ID" => 1,
        "PAYED" => "N",
        "CANCELED" => "N",
        "STATUS_ID" => "N",
        "PRICE" => $quantity * $arPrice['DISCOUNT_PRICE'],
        "CURRENCY" => "RUB",
        "USER_ID" => $arResult['USER']['ID'] ? $arResult['USER']['ID'] : $newUserID,
        "PAY_SYSTEM_ID" => "",
        "PRICE_DELIVERY" => "",
        "DELIVERY_ID" => "",
        "COMMENTS" => "покупка в 1 клик",
    );

    // add Guest ID
    if (CModule::IncludeModule("statistic"))
        $arFields["STAT_GID"] = CStatistic::GetEventParam();

    $ORDER_ID = CSaleOrder::Add($arFieldsOrder);
    $ORDER_ID = IntVal($ORDER_ID);

    if( $ORDER_ID ){

        //добавляем к заказу св-ва покупателя
        $props = array(
            'FIO' => array(
                'ORDER_PROPS_ID' => 1,
                'NAME' => 'ФИО',
                'CODE' => 'FIO',
                'VALUE' => $name,
            ),
            'PHONE' => array(
                'ORDER_PROPS_ID' => 3,
                'NAME' => 'Телефон',
                'CODE' => 'PHONE',
                'VALUE' => $phone,
            ),
        );

        foreach($props as $prop){
            $arFieldsOrderProp = array(
                "ORDER_ID" => $ORDER_ID,
                "ORDER_PROPS_ID" => $prop['ORDER_PROPS_ID'],
                "NAME" => $prop['NAME'],
                "CODE" => $prop['CODE'],
                "VALUE" => $prop['VALUE'],
            );

            CSaleOrderPropsValue::Add($arFieldsOrderProp);
        }

        //привязываем текущую корзину "покупки в 1 клик" к заказу
        CSaleBasket::OrderBasket($ORDER_ID, $_SESSION["SALE_USER_ID"], SITE_ID);
        $arResult['ORDER_ID'] = $ORDER_ID;
        $arResult['STATUS'] = 'success';
        $arResult['MESSAGE'] = 'Ваш заказ принят! Номер заказа <b style="font-size:18px;">'.$ORDER_ID.'</b>. Наш менеджер в ближайшее время обязательно с вами свяжется!';

        //удаляем все товары из корзины текущего пользователя
        CSaleBasket::DeleteAll(CSaleBasket::GetBasketUserID());

        //возвращаем корзину пользователя до "покупки в 1 клик"
        foreach($arBasketItems as $arBasketItem){
            CSaleBasket::Add($arBasketItem);
        }

        $strOrderList = "";
        $arBasketList = array();
        $dbBasketItems = CSaleBasket::GetList(
                            array("ID" => "ASC"),
                            array("ORDER_ID" => $ORDER_ID),
                            false,
                            false,
                            array("ID", "PRODUCT_ID", "NAME", "QUANTITY", "PRICE", "CURRENCY", "TYPE", "SET_PARENT_ID")
                        );
        while ($arItem = $dbBasketItems->Fetch()){
            if (CSaleBasketHelper::isSetItem($arItem))
                continue;

            $arBasketList[] = $arItem;
        }

        $arBasketList = getMeasures($arBasketList);

        if (!empty($arBasketList) && is_array($arBasketList))
        {
            foreach ($arBasketList as $arItem)
            {
                $measureText = (isset($arItem["MEASURE_TEXT"]) && strlen($arItem["MEASURE_TEXT"])) ? $arItem["MEASURE_TEXT"] : GetMessage("SOA_SHT");
                $strOrderList .= $arItem["NAME"]." - ".$arItem["QUANTITY"]." ".$measureText.": ".SaleFormatCurrency($arItem["PRICE"], $arItem["CURRENCY"]);
                $strOrderList .= "\n";
            }
        }

        global $DB;
        $arEventFields = array(
            "ORDER_ID" => $ORDER_ID,
            "ORDER_DATE" => Date($DB->DateFormatToPHP(CLang::GetDateFormat("SHORT", SITE_ID))),
            "ORDER_USER" => $name,
            "PRICE" => SaleFormatCurrency($arPrice['DISCOUNT_PRICE'], "RUB"),
            "BCC" => "nahim@planetaplitki.ru",
            "EMAIL" => $email,
            "PHONE" => $phone,
            "FIO" => $name,
            "ORDER_LIST" => $strOrderList,
            "SALE_EMAIL" => "nahim@planetaplitki.ru",
            "DELIVERY_PRICE" => "",
        );

        $eventName = "ONE_CLICK";

        $arOrder = CSaleOrder::GetByID($ORDER_ID);
        COrderMakeRU::OnOrderAddHandler($ORDER_ID);

        $event = new CEvent;
        $event->Send($eventName, SITE_ID, $arEventFields, "N");
    }
}
else{
    $arResult['ERRORS'][] = 'Не заполнено одно из обязательных полей!';
}

if( count($arResult['ERRORS']) >= 1 ){
    $arResult['STATUS'] = 'fail';
}

$this->IncludeComponentTemplate();