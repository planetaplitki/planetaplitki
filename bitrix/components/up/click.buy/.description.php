<?
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();

$arComponentDescription = array(
	"NAME" => GetMessage("COMP_RU_CLICK_BUY_TITLE"),
	"DESCRIPTION" => GetMessage("COMP_RU_CLICK_BUY_DESCR"),
	"ICON" => "/images/sale_order_full.gif",
	"PATH" => array(
		"ID" => "e-store",
		"CHILD" => array(
			"ID" => "sale_order",
			"NAME" => GetMessage("SOF_NAME")
		)
	),
);
?>