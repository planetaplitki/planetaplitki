<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>

<?
$statuses = array(
	array(
		'NAME' => 'новинка',
		'VALUE' => 'UF_NEW_COLLECTION'
	),
	array(
		'NAME' => 'распродажа',
		'VALUE' => 'UF_SALE'
	),
	array(
		'NAME' => 'акция',
		'VALUE' => 'UF_AKCIYA'
	),
	array(
		'NAME' => 'эксклюзив',
		'VALUE' => 'UF_EXCLUSIVE'
	),
);

$sortVariants = array(
	array(
		'NAME' => 'по возрастанию цены',
		'VALUE' => 'asc'
	),
	array(
		'NAME' => 'по убыванию цены',
		'VALUE' => 'desc'
	),
);

$show_props = array(
	'VYSOTA',
	'SHIRINA',
	'POVERKHNOST',
	'PRICE',
	'kategoriyatovar',
	'S_RESET',
	'S_ADVANTAGE_FILTER',
	'STRANA',
	'PROIZVODITEL',
	'KOLLEKTSIYA',
	'tematikatovar',
	'tsvettovar',
	'naznachenietovar'
);
if($USER->IsAdmin()){
	$show_props[] = 'modeView';
}
?>

<?
$props_ = $arResult['FILTER']['PROPS_'];
$sections_ = $arResult['FILTER']['SECTIONS'];
$countries = $sections_['COUNTRIES'];
$brands = $sections_['BRANDS'];
$collections = $sections_['COLLECTIONS'];
$idSection = $arParams['SECTION_ID'];
$itemCollectionsParent = "";
$ufType = $arParams['UF_TYPE'];
$seoPageProps = $arParams['UF_SEO_PAGE_PROPS'];

if( eRU($countries[$idSection]) ){
	$country = $countries[$idSection];
	$itemsCollectionsParent = $country['ID'];
}
elseif( eRU($brands[$idSection]) ){
	$brand = $brands[$idSection];
	$country = $sections_[$brand['IBLOCK_SECTION_ID']];
	$itemsCollectionsParent = $brand['ID'];
}

$itemsBrands = $component->getItemsSections($itemsCollectionsParent, 'PROIZVODITEL');
$itemsCollections = $component->getItemsSections($itemsCollectionsParent, 'KOLLEKTSIYA');
$prices = $arResult['FILTER']['PRICE'];
$countResults = $arResult['FILTER']['COUNT'];
$countResultsFormatted = plural($arResult['FILTER']['COUNT'], 'коллекция', 'коллекции', 'коллекций');

$action = $arResult['FILTER']['ACTION'];
//pRU($itemsCollectionsParent, 'all');
//pRU($sections_, 'all');
//echo $itemsCollectionsParent;
$colors = $arResult['FIELDS']['COLORS'];


//pRU($props_, 'all');
//pRU($colors, 'all');
//pRU($prices);
$optionsInStr = 6;
?>

<form class="form-inline header-filter hidden-xs" action="<?=CURPAGE?>" name="FILTER_FORM" method="get" id="catalogFilter" >
	<input type="hidden"name="view" value="<?=htmlspecialchars($_REQUEST['view']);?>" />
	<div class="row">
		<?
		$i = 1;
		$countProps = count($show_props);
		foreach($show_props as $show_prop){
			$prop = $props_[$show_prop];
			?>

			<div class="col-sm-4 col-md-2">
				<?if( $show_prop == 'S_ADVANTAGE_FILTER' ){?>
					<div class="text-box"><a>Расширенный поиск</a></div>
				<?}
				elseif( $show_prop == 'PRICE' ){?>
					<button type="button" class="btn slider-buttom filter-price-button">
                        <span class="filter-price-active-title">Цена: </span>
                        <span class="filter-price-active-from-value"></span> ‒
                        <span class="filter-price-active-to-value"></span>
                        <span class="filter-price-active-small">(руб.)</span>
                    </button>
					<div class="arrow_box">
						<div class="range">
                            <span class="range-input-label range-input-label-from">от:</span>
                            <span class="range-input-label range-input-label-to">до:</span>
							<div class="slider-range">
								<input type="text" name="PRICE_MIN" class="ui-slider-handle ui-state-default ui-corner-all" id="min_cost">
								<input type="text" name="PRICE_MAX" class="ui-slider-handle ui-state-default ui-corner-all" id="max_cost">
							</div>
						</div>
						<hr class="arrow-line">
						<button type="button" class="priceFilter btn link-buttom">применить</button>
					</div>

				<?}/*
				elseif( $show_prop == 'VYSOTA' ){?>
					<button type="button" class="btn slider-buttom filter-price-button">
						<span class="filter-price-active-title">Высота: </span>
						<span class="filter-price-active-from-value"></span> ‒
						<span class="filter-price-active-to-value"></span>
						<span class="filter-price-active-small">(см)</span>
					</button>
					<div class="arrow_box">
						<div class="range">
							<span class="range-input-label range-input-label-from">от:</span>
							<span class="range-input-label range-input-label-to">до:</span>
							<div class="slider-range">
								<input type="text" name="VISOTA_MIN" class="ui-slider-handle ui-state-default ui-corner-all" id="min_shirina">
								<input type="text" name="VISOTA_MAX" class="ui-slider-handle ui-state-default ui-corner-all" id="max_shirina">
							</div>
						</div>
						<hr class="arrow-line">
						<button type="button" class="priceFilter btn link-buttom">применить</button>
					</div>
				<?}
				elseif( $show_prop == 'SHIRINA' ){?>
					<button type="button" class="btn slider-buttom filter-price-button">
						<span class="filter-price-active-title">Ширина: </span>
						<span class="filter-price-active-from-value"></span> ‒
						<span class="filter-price-active-to-value"></span>
						<span class="filter-price-active-small">(см)</span>
					</button>
					<div class="arrow_box">
						<div class="range">
							<span class="range-input-label range-input-label-from">от:</span>
							<span class="range-input-label range-input-label-to">до:</span>
							<div class="slider-range">
								<input type="text" name="SHIRINA_MIN" class="ui-slider-handle ui-state-default ui-corner-all" id="min_shirina">
								<input type="text" name="SHIRINA_MAX" class="ui-slider-handle ui-state-default ui-corner-all" id="max_shirina">
							</div>
						</div>
						<hr class="arrow-line">
						<button type="button" class="priceFilter btn link-buttom">применить</button>
					</div>
				<?}*/
				elseif( $show_prop == 'STRANA' ){?>
					<?if( eRU($country) ){?>
						<select class="filters select" id="select_type1">
							<option value=""><?=$country['NAME']?></option>
						</select>
					<?}
					else{?>
						<select class="filters select" id="select_type1" name="PROPERTY[<?=$show_prop;?>]" multiple>
							<option value=""><?=$prop['PROPERTY']['NAME']?> (все)</option>
							<?foreach( $prop['VARIANTS'] as $variant ){?>
								<option <?=in_array($variant['ID'], $_REQUEST['PROPERTY'][$show_prop]) ? 'selected' : '';?> value="<?=$variant['ID']?>"><?=$variant['VALUE']?></option>
							<?}?>
						</select>
					<?}?>
				<?}
				elseif( $show_prop == 'PROIZVODITEL' ){?>
					<?if( eRU($brand) ){?>
						<select class="filters select" id="select_type1">
							<option value=""><?=$brand['NAME']?></option>
						</select>
					<?}
					else{?>
						<select class="filters select" id="select_type1" name="PROPERTY[<?=$show_prop;?>]" multiple>
							<option value=""><?=$prop['PROPERTY']['NAME']?> (все)</option>
							<?if( eRU($itemsBrands) ){
								foreach( $prop['VARIANTS'] as $variant ){
									if( in_array($variant['ID'], $itemsBrands) ){?>
										<option <?=in_array($variant['ID'], $_REQUEST['PROPERTY'][$show_prop]) ? 'selected' : '';?> value="<?=$variant['ID']?>"><?=$variant['VALUE']?></option>
									<?}?>
								<?}
							}
							else{
								foreach( $prop['VARIANTS'] as $variant ){?>
									<option <?=$_REQUEST['PROPERTY'][$show_prop] == $variant['ID'] ? 'selected' : '';?> value="<?=$variant['ID']?>"><?=$variant['VALUE']?></option>
								<?}
							}?>
						</select>
					<?}?>
				<?}
				elseif( $show_prop == 'KOLLEKTSIYA' ){?>
					<select class="filters select" id="select_type1" name="PROPERTY[<?=$show_prop;?>][]" multiple>
						<option value=""><?=$prop['PROPERTY']['NAME']?> (все)</option>
						<?if( eRU($itemsCollections) ){
							foreach( $prop['VARIANTS'] as $variant ){
								if( in_array($variant['ID'], $itemsCollections) ){?>
									<option <?=$_REQUEST['PROPERTY'][$show_prop] == $variant['ID'] ? 'selected' : '';?> value="<?=$variant['ID']?>"><?=$variant['VALUE']?></option>
								<?}?>
							<?}
						}
						else{
							foreach( $prop['VARIANTS'] as $variant ){?>
								<option <?=in_array($variant['ID'], $_REQUEST['PROPERTY'][$show_prop]) ? 'selected' : '';?> value="<?=$variant['ID']?>"><?=$variant['VALUE']?></option>
							<?}
						}?>
					</select>
				<?}
				elseif( $show_prop == 'S_RESET' ){?>
					<a href="<?=CURPAGE?>" type="reset" class="btn reset-buttom">Сбросить фильтр</a>
				<?}
				else{?>
					<?# для страниц с предустановленным фильтром
					if( $seoPageProps[$show_prop] ){?>
						<select class="filters select" name="PROPERTY[<?=$show_prop;?>]">
							<?foreach( $prop['VARIANTS'] as $variant ){
								if( $variant['ID'] == $seoPageProps[$show_prop] ){?>
									<option value="<?=$variant['ID']?>" selected><?=$variant['VALUE']?></option>
								<?}?>
							<?}?>
						</select>
					<?}
					elseif( eRU($prop) ){?>
						<?if( in_array($show_prop, array('tsvettovar')) ){?>
							<!--<select class="filters select" id="select_type1" name="PROPERTY[<?=$show_prop;?>][]">
								<option value=""><?=$prop['PROPERTY']['NAME']?> (все)</option>
								<?foreach( $prop['VARIANTS'] as $variant ){?>
									<option <?=in_array($variant['ID'], $_REQUEST['PROPERTY'][$show_prop]) ? 'selected' : '';?> value="<?=$variant['ID']?>">
										<?=$variant['VALUE']?>
										<div class="color"><img src="<?=$colors[$variant['XML_ID']]?>" alt=""></div>
									</option>
								<?}?>
							</select>-->
                            <div class="filter-popup-toggler"><?=$prop['PROPERTY']['NAME']?> (все)</div>
                            <div class="filter-popup">
                                <div class="filter-popup-inner">
                                    <?foreach( $prop['VARIANTS'] as $variant ){?>
                                        <div class="checkbox color-checkbox">
                                            <input type="checkbox"
												   name="PROPERTY[<?=$show_prop;?>][]"
													<?=in_array($variant['ID'], $_REQUEST['PROPERTY'][$show_prop]) ? 'checked' : '';?>
												   id="filter-check<?=$variant['ID']?>"
												   value="<?=$variant['ID']?>"
												/>
                                            <label for="filter-check<?=$variant['ID']?>">
                                                <div class="filter-checkbox-color" style="background-image: url(<?=$colors[$variant['XML_ID']]?>)"></div>
                                                <span class="filter-checkbox-text"><?=$variant['VALUE']?></span>
                                            </label>
                                        </div>
                                    <?}?>
                                </div>
                                <div class="filter-popup-submit">
                                    <button type="button" class="btn link-buttom filter-popup-button">применить</button>
                                </div>
                            </div>
						<?}
						elseif( in_array($show_prop, array('tematikatovar',)) ){?>
							<select class="filters select" id="select_type1" name="PROPERTY[<?=$show_prop;?>][]">
								<option value=""><?=$prop['PROPERTY']['NAME']?> (все)</option>
								<?foreach( $prop['VARIANTS'] as $variant ){?>
									<option <?=in_array($variant['ID'], $_REQUEST['PROPERTY'][$show_prop]) ? 'selected' : '';?> value="<?=$variant['ID']?>">
										<?=$variant['VALUE']?>
										<div class="color"><img src="<?=$colors[$variant['XML_ID']]?>" alt=""></div>
									</option>
								<?}?>
							</select>
						<?}
						else{?>
							<select class="filters select" id="select_type1" name="PROPERTY[<?=$show_prop;?>]">
								<?
								$all = "";
								if( !in_array($show_prop, array('VYSOTA', 'SHIRINA')) ){
									$all = " (все)";
								}?>
								<option value=""><?=$prop['PROPERTY']['NAME']?><?=$all;?></option>
								<?foreach( $prop['VARIANTS'] as $variant ){?>
									<option <?= $_REQUEST['PROPERTY'][$show_prop] == $variant['ID'] ? 'selected' : '';?> value="<?=$variant['ID']?>"><?=$variant['VALUE']?></option>
								<?}?>
							</select>
						<?}?>
					<?}?>
				<?}?>
			</div>

			<?if( $i%$optionsInStr == 0 && $i <= $optionsInStr && $i != $countProps ){?>
				</div><div class="row extendedFilter">
			<?}?>

			<?$i++;?>
		<?}?>
	</div>
	<input type="submit" value="Фильтровать" name="submit" />
	<input type="hidden" name="setFilter" value="y" />

	<hr class="line-filter">
	<div class="row obj hidden-sm hidden-md hidden-lg">
		<div class="col-xs-12">
			<p class="chouse-text">Подходят: <span><?=$countResults?></span> <?=$countResultsFormatted?></p>
		</div>
	</div>
	<?if( !$ufType ){?>
		<?=$component->getStatusHtml($statuses, 'STATUSES', 'Статус')?>
	<?}?>
			<?if($USER->IsAdmin()){
					?>
					<div class="col-xs-3 col-sm-3">
						<div class="label-text2">Показать</div>
						<a href="#" class="btn link-up modeViewSetter <?=$_REQUEST['modeView'] != 'items' ? 'modeViewSetterActive' : ''?>" data-mode-view="collection">Коллекции</a>
						<a href="#" class="btn link-up modeViewSetter <?=$_REQUEST['modeView'] == 'items' ? 'modeViewSetterActive' : ''?>" data-mode-view="items">Элементы</a> 
						<input type='text' name='modeView' style='display:none;' value='<?=$_REQUEST['modeView']?>'>						
					</div>
				<!--<div class="col-xs-3 col-sm-3">
						<div class="label-text2">Показать</div>
						<select class="filters2" id="select_mode_view" name="modeView" multiple>
							<option value="collection" <?=$_REQUEST['modeView'] != 'items' ? 'selected' : ''?>>Коллекции</option>
							<option value="items" <?=$_REQUEST['modeView'] == 'items' ? 'selected' : ''?>>Элементы</option>
						</select>
				</div> -->
					<?				
				}?>
	<?=$component->getSortHtml($sortVariants, 'PRICES', 'Сортировать по')?>


	<div class="col-xs-3 col-sm-3 pull-right">

    	<?/*?>
    	<?if( !$ufType ){?>
    		<?=$component->getStatusHtml($statuses, 'STATUSES', 'Статус')?>
    	<?}?>
    	<?=$component->getSortHtml($prices, 'PRICES', 'Сортировать по')?>
    	<?*/?>

    	<div class="text-right hidden-xs">
    		<div class="label-text">Показывать по:</div>
    		<div class="list-block">

    			<?
    			$pageElementCount = array(40, 80, 'ALL');
    			?>

    			<ul class="list-block-item">
    				<?
    				$showAll = false;
    				?>
    				<?foreach($pageElementCount as $k => $count){?>

    					<?if(
    						!$k && !$_REQUEST['PAGE_ELEMENT_COUNT'] && !$_REQUEST['SHOWALL_1'] ||
    						$count == $_REQUEST['PAGE_ELEMENT_COUNT'] ||
    						$_REQUEST['SHOWALL_1'] && $count == 'ALL'
    					){?>
    						<li class="active"><a><?=$count == 'ALL' ? 'Все' : $count;?></a></li>
    					<?}
    					else{?>
    						<?if( $count == 'ALL' ){?>
    							<li><a href="<?=$APPLICATION->GetCurPageParam('SHOWALL_1=1', array('PAGE_ELEMENT_COUNT', 'SHOWALL_1'));?>" >все</a></li>
    						<?}
    						else{?>
    							<li><a href="<?=$APPLICATION->GetCurPageParam('PAGE_ELEMENT_COUNT='.$count.'&SHOWALL_1=0', array('PAGE_ELEMENT_COUNT', 'SHOWALL_1'));?>" ><?=$count?></a></li>
    						<?}?>
    					<?}?>
    				<?}?>
    			</ul>
    		</div>
    	</div>
    </div>
</form>

<?/*?>
<hr class="line-filter">
<div class="row obj hidden-sm hidden-md hidden-lg">
	<div class="col-xs-12">
		<p class="chouse-text">Подходят: <span><?=$countResults?></span> <?=$countResultsFormatted?></p>
	</div>
</div>
<?*/?>

<!--mobile-->

<?$this->SetViewTarget("catalog_mobile_filter");?>

<hr class="mobileForm line-filter">
<div class="row obj hidden-sm hidden-md hidden-lg">
	<div class="col-xs-12">
		<p class="chouse-text">Подходят: <span><?=$countResults?></span> <?=$countResultsFormatted?></p>
	</div>
</div>

<a data-toggle="collapse" href="#menu-filtr" class="filter-mobile-toggle">
	<div class="hidden-sm hidden-md hidden-lg chouse-box"><div class="gear"></div></div>
</a>

<div class="mobileForm">
	<form class="form-inline header-filter" role="form" action="<?=CURPAGE?>" name="FILTER_FORM" method="get" id="mobileFilter">
		<div class="row">
			<?if( !$ufType ){?>
				<?=$component->getStatusHtml($statuses, 'STATUSES', 'Статус')?>
			<?}?>
			<?if($USER->IsAdmin()){
					?>
					<div class="col-xs-3 col-sm-3">
						<select class="filters select" id="select_mode_view" name="modeView" multiple>
							<option value="collection" <?=$_REQUEST['modeView'] != 'items' ? 'selected' : ''?>>Коллекции</option>
							<option value="items" <?=$_REQUEST['modeView'] == 'items' ? 'selected' : ''?>>Элементы</option>
						</select>
					</div>
					<?				
				}?>
			<?=$component->getSortHtml($sortVariants, 'PRICES', 'Сортировать по')?>
		</div>
		<div class="collapse menu-col-mob" id="menu-filtr">
			<ul class="menu-top-box-mob">
				<li>
					<a data-toggle="collapse" data-target="#toggleDemo1" data-parent="#sidenav1">
						Цена
						<div class="col-mob">
							<span class="point"></span>
							<div class="text2">ПОКАЗАТЬ</div>
						</div>
					</a>
					<div class="collapse" id="toggleDemo1">
						<div class="row obj50">
							<div class="col-xs-6">
								<label class="sr-only" for="price"></label>
								<input type="text" class="filters" name="PRICE_MIN" id="price" value="<?=$prices['MIN_CURRENT'] ? $prices['MIN_CURRENT'] : ""?>" placeholder="От <?=SaleFormatCurrency($prices['MIN'], 'RUB')?>">
							</div>
							<div class="col-xs-6">
								<label class="sr-only" for="width"></label>
								<input type="text" class="filters mnone" name="PRICE_MAX" value="<?=$prices['MAX_CURRENT'] ? $prices['MAX_CURRENT'] : ""?>" id="width" placeholder="До <?=SaleFormatCurrency($prices['MAX'], 'RUB')?>">
							</div>
						</div>
					</div>
				</li>
				<li>
					<a data-toggle="collapse" data-target="#toggleDemo2" data-parent="#sidenav2">
						Производитель
						<div class="col-mob">
							<span class="point"></span>
							<div class="text2">ПОКАЗАТЬ</div>
						</div>
					</a>
					<div class="collapse" id="toggleDemo2">
						<div class="row obj50">
							<div class="col-xs-6">
								<?
								$show_prop = 'STRANA';
								$prop = $props_[$show_prop];
								if( eRU($country) ){?>
									<select class="filters select" id="select_type1">
										<option value=""><?=$country['NAME']?></option>
									</select>
								<?}
								else{?>
									<select class="filters select" id="country" name="PROPERTY[<?=$show_prop;?>]">
										<option value=""><?=$prop['PROPERTY']['NAME']?> (все)</option>
										<?foreach( $prop['VARIANTS'] as $variant ){?>
											<option <?=$_REQUEST['PROPERTY'][$show_prop] == $variant['ID'] ? 'selected' : '';?> value="<?=$variant['ID']?>"><?=$variant['VALUE']?></option>
										<?}?>
									</select>
								<?}?>
							</div>
							<div class="col-xs-6">
								<?
								$show_prop = 'PROIZVODITEL';
								$prop = $props_[$show_prop];

								if( eRU($brand) ){?>
									<select class="filters select" id="brend">
										<option value=""><?=$brand['NAME']?></option>
									</select>
								<?}
								else{?>
									<select class="filters select" id="brend" name="PROPERTY[<?=$show_prop;?>]">
										<option value=""><?=$prop['PROPERTY']['NAME']?> (все)</option>
										<?if( eRU($itemsBrands) ){
											foreach( $prop['VARIANTS'] as $variant ){
												if( in_array($variant['ID'], $itemsBrands) ){?>
													<option <?=$_REQUEST['PROPERTY'][$show_prop] == $variant['ID'] ? 'selected' : '';?> value="<?=$variant['ID']?>"><?=$variant['VALUE']?></option>
												<?}?>
											<?}
										}
										else{
											foreach( $prop['VARIANTS'] as $variant ){?>
												<option <?=$_REQUEST['PROPERTY'][$show_prop] == $variant['ID'] ? 'selected' : '';?> value="<?=$variant['ID']?>"><?=$variant['VALUE']?></option>
											<?}
										}?>
									</select>
								<?}?>
							</div>
						</div>
					</div>
				</li>

				<?$show_props_ = array(
					'Габариты' => array(
						'VYSOTA',
						'SHIRINA',
					),
					'Тип' => array(
						'POVERKHNOST',
						'kategoriyatovar',
					),
					'Коллекция \ тематика' => array(
						'KOLLEKTSIYA',
						'tematikatovar',
					),
				);?>

				<?
				$i = 3;
				foreach( $show_props_ as $name => $show_props ){?>
					<li>
						<a data-toggle="collapse" data-target="#toggleDemo<?=$i?>" data-parent="#sidenav<?=$i?>">
							<?=$name;?>
							<div class="col-mob">
								<span class="point"></span>
								<div class="text2">ПОКАЗАТЬ</div>
							</div>
						</a>
						<div class="collapse" id="toggleDemo<?=$i?>">
							<div class="row obj50">
								<?foreach($show_props as $show_prop) {
									$prop = $props_[$show_prop];?>
									<div class="col-xs-6">
										<? # для страниц с предустановленным фильтром
										if( $seoPageProps[$show_prop] ){?>
											<select class="filters select" name="PROPERTY[<?=$show_prop;?>]">
												<?foreach( $prop['VARIANTS'] as $variant ){
													if( $variant['ID'] == $seoPageProps[$show_prop] ){?>
														<option value="<?=$variant['ID']?>" selected><?=$variant['VALUE']?></option>
													<?}?>
												<?}?>
											</select>
										<?}
										elseif( eRU($prop) ){?>
											<select class="filters select" id="select_type1" name="PROPERTY[<?=$show_prop;?>]">
												<option value=""><?=$prop['PROPERTY']['NAME']?></option>
												<?foreach( $prop['VARIANTS'] as $variant ){?>
													<option <?=$_REQUEST['PROPERTY'][$show_prop] == $variant['ID'] ? 'selected' : '';?> value="<?=$variant['ID']?>"><?=$variant['VALUE']?></option>
												<?}?>
											</select>
										<?}?>
									</div>
									<?
								}?>
							</div>
						</div>
					</li>
					<?
					$i++;
				}?>
			</ul>
			<div class="buttons">
				<?/*?><a href="<?=SITE_DIR?>catalog/" class="btn link-buttom">ПЕРЕЙТИ В КАТАЛОГ</a>
				<button type="button" class="btn link-up">НАВЕРХ<span class="icon-up"></span></button><?*/?>
				<button type="submit" name="submit" class="btn link-buttom">Фильтровать</button>
				<a href="<?=CURPAGE?>" class="btn link-up">Сбросить</a>
			</div>
		</div>
		<input type="hidden" name="setFilter" value="y" />
		<?/*?><input type="submit" value="Фильтровать" name="submit" /><?*/?>
	</form>
</div>
<?$this->EndViewTarget("catalog_mobile_filter");?>
