$(function(){
    var ranges = [/*window.ranges.SHIRINA, window.ranges.VISOTA,*/window.ranges.PRICES];

    $( ".slider-range" ).each(function(n){
        if(ranges[n].MAX_CURRENT > ranges[n].MAX){
            ranges[n].MAX_CURRENT = ranges[n].MAX;
        }
        $(this).closest('.col-sm-4').find('.filter-price-active-from-value').text(ranges[n].MIN_CURRENT);
        $(this).closest('.col-sm-4').find('.filter-price-active-to-value').text(ranges[n].MAX_CURRENT);
        $(this).slider({
            range: true,
            min: ranges[n].MIN,
            max: ranges[n].MAX,
            values: [ranges[n].MIN_CURRENT, ranges[n].MAX_CURRENT],
            slide: function() {
                $(this).find('input').eq(0).val($(this).slider("values",0));
                $(this).find('input').eq(1).val($(this).slider("values",1));
            }
        });
        $(this).find('input').eq(0).val($(this).slider("values",0));
        $(this).find('input').eq(1).val($(this).slider("values",1));
    });

    //
    $(".select").selectbox().change(function () {
        var form = $(this).parents('form');
        if( $(form).length ){
            $(form).find('[type="submit"]').click();
        }
    });

    $(".modeViewSetter").click(function () {
        var form = $(this).parents('form');
		  $('input[name="modeView"]').attr('value', $(this).attr('data-mode-view'));
        if( $(form).length ){
            $(form).find('[type="submit"]').click();
        }
		  return false;
    });

    $(".filters2").selectbox({
        classHolder: 'sbHolder2'
    }).change(function () {
        var form = $(this).parents('form');
        if( $(form).length ){
            $(form).find('[type="submit"]').click();
        }
    });

    $('.priceFilter, .filter-popup-button').on('click', function () {
        var form = $(this).parents('#catalogFilter');
        $(form).find('input[type="submit"]').click();
    });

    //$('#mobileFilter').on('change', 'input, select', function(){
    //    var form = $(this).parents('#mobileFilter');
    //    $(form).find('input[type="submit"]').click();
    //});

    $('.filter-popup-toggler').click(function(){
        $(this).closest('.col-sm-4').find('.filter-popup').toggle();
    })
});
