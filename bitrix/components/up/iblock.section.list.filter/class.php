<?php

class IblockSectionListRU extends CBitrixComponent {

	private $_nav = null;

	/**
	 * Устанавливает дополнительные параметры вызова компонента
	 */
	public function setParams() {
		$params = &$this->arParams;

		if(!$params['IBLOCK_ID']) {
			ShowError('Не указан ID инфоблока');
		}

		# поля раздела
		is_array($params['SECTION_FIELDS']) or $params['SECTION_FIELDS'] = array();
		is_array($params['SECTION_USER_FIELDS']) or $params['SECTION_USER_FIELDS'] = array();
		in_array('ID', $params['SECTION_FIELDS']) or $params['SECTION_FIELDS'][] = 'ID';
		in_array('IBLOCK_ID', $params['SECTION_FIELDS']) or $params['SECTION_FIELDS'][] = 'IBLOCK_ID';

		# поля элементов
		is_array($params['ELEMENT_FIELDS']) or $params['ELEMENT_FIELDS'] = array();
		is_array($params['ELEMENT_PROPERTIES']) or $params['ELEMENT_PROPERTIES'] = array();
		in_array('ID', $params['ELEMENT_FIELDS']) or $params['ELEMENT_FIELDS'][] = 'ID';
		in_array('IBLOCK_ID', $params['ELEMENT_FIELDS']) or $params['ELEMENT_FIELDS'][] = 'IBLOCK_ID';
		
		# навигация
		$params['DISPLAY_TOP_PAGER'] = $params['DISPLAY_TOP_PAGER']=='Y';
		$params['DISPLAY_BOTTOM_PAGER'] = $params['DISPLAY_BOTTOM_PAGER']!='N';
		$params['PAGER_TITLE'] = trim($params['PAGER_TITLE']);
		$params['PAGER_SHOW_ALWAYS'] = $params['PAGER_SHOW_ALWAYS']=='Y';
		$params['PAGER_TEMPLATE'] = trim($params['PAGER_TEMPLATE']);
		$params['PAGER_DESC_NUMBERING'] = $params['PAGER_DESC_NUMBERING']=='Y';
		$params['PAGER_DESC_NUMBERING_CACHE_TIME'] = intval($params['PAGER_DESC_NUMBERING_CACHE_TIME']);
		$params['PAGER_SHOW_ALL'] = $params['PAGER_SHOW_ALL']=='Y';

//		if(!$params['URL_404']) {
//			ShowError('Не указана 404 страница');
//		}
	}

	/**
	 * Возвращает массив для сортировки элементов
	 * @return array
	 */
	public function getSortArray() {
		return array(
			$this->arParams['SORT_FIELD'] => $this->arParams['SORT_ORDER'],
			$this->arParams['SORT_FIELD2'] => $this->arParams['SORT_ORDER2']
		);
	}

	/**
	 * Массив навигации для выборки элементов
	 * @return array
	 */
	public function getNavArray() {
		if($this->_nav === null) {
			$params = &$this->arParams;
			if($params['DISPLAY_TOP_PAGER'] || $params['DISPLAY_BOTTOM_PAGER']) {
				$this->_nav = array(
					'nPageSize' => $params['PAGE_ELEMENT_COUNT'],
					'bDescPageNumbering' => $params['PAGER_DESC_NUMBERING'],
					'bShowAll' => $params['PAGER_SHOW_ALL'],
				);
			}
			else {
				$this->_nav = array(
					'nTopCount' => $params['PAGE_ELEMENT_COUNT'],
					'bDescPageNumbering' => $params['PAGER_DESC_NUMBERING'],
				);
			}
		}
		return $this->_nav;
	}

	/**
	 * Массив навигации для кеша
	 * @return array|bool
	 */
	public function getNavForCache() {
		$params = &$this->arParams;
		if($params['DISPLAY_TOP_PAGER'] || $params['DISPLAY_BOTTOM_PAGER']) {
			$nav = $this->getNavArray();
			$dbResult = new CDBResult;
			$nc = $dbResult->GetNavParams($nav);
			if ($nc['PAGEN'] == 0 && $params['PAGER_DESC_NUMBERING_CACHE_TIME'] > 0) {
				$params['CACHE_TIME'] = $params['PAGER_DESC_NUMBERING_CACHE_TIME'];
			}
		}
		else {
			$nc = false;
		}
		return $nc;
	}

	/**
	 * Устанавливает 404 страницу
	 */
	public function set404() {
		AddEventHandler('main', 'OnEpilog', function() {
			if(defined('ERROR_404') && ERROR_404=='Y' && !defined('ADMIN_SECTION')) {
				global $APPLICATION;
				global $USER;
				$APPLICATION->RestartBuffer();
				require $_SERVER['DOCUMENT_ROOT'].'/bitrix/templates/'.SITE_TEMPLATE_ID.'/header.php';
				require $_SERVER['DOCUMENT_ROOT'].'/404.php';
				require $_SERVER['DOCUMENT_ROOT'].'/bitrix/templates/'.SITE_TEMPLATE_ID.'/footer.php';
			}
		});
		defined('ERROR_404') or define('ERROR_404', 'Y');
	}

	/**
	 * Возвращает сортировку
	 * @param array $variants
	 * @param string $name
	 * @param string $text
	 * @return string
	 */
	public function getSortHtml($variants = array(), $name = "", $text = ""){

		$str = "";
		$options = "";

		if( eRU($variants) ){
			foreach($variants as $variant){
				$options .= '<option value="'.$variant['VALUE'].'">'.$variant['NAME'].'</option>';
			}

			$str = '
				<div class="col-xs-3 col-sm-3">
					<div class="label-text2">'.$text.'</div>
					<select class="filters2" id="'.$name.'" name="SORT['.$name.']">'.$options.'</select>
				</div>
			';
		}

		return $str;
	}

	/**
	 * Возвращает фильтр по статусу (флагу)
	 * @param array $variants
	 * @param string $name
	 * @param string $text
	 * @return string
	 */
	public function getStatusHtml($variants = array(), $name = "", $text = ""){

		$str = "";
		$options = "";
		$rStatus = $_REQUEST['STATUSES'];

		if( eRU($variants) ){
			$options = '<option value="">не выбран</option>';
			foreach($variants as $variant){
				$options .= '<option '.($rStatus == $variant['VALUE'] ? "selected" : "").' value="'.$variant['VALUE'].'">'.$variant['NAME'].'</option>';
			}

			$str = '
				<div class="col-xs-3 col-sm-3">
					<div class="label-text2">'.$text.'</div>
					<select class="filters2" id="'.$name.'" name="'.$name.'">'.$options.'</select>
				</div>
			';
		}

		return $str;
	}

	/**
	 * Список цветов плитки для отображения в фильтре
	 * @return array
	 */
	public function getColors (){

		$colors = array();

		# кешируем результат
		$phpCache = new CPHPCache;
		$cacheTime = 3600*30*24; # время кеширования - 1 месяц
		$cacheID = 'colors'; # формируем уникальный id кеша
		$cachePath = '/colors'; # папка с кешем

		if($phpCache->InitCache($cacheTime, $cacheID, $cachePath)) {
			$colors = $phpCache->GetVars();
		}
		else {
			# помечаем кеш тегом, связанным с инфоблоком
			$tagCache = $GLOBALS['CACHE_MANAGER'];
			$tagCache->StartTagCache($cachePath);
			$tagCache->RegisterTag('iblock_id_'.IBLOCK_ID_COLORS);
			$tagCache->EndTagCache();

			if( CModule::IncludeModule('iblock') ){
				$arSelect = Array("ID", "NAME", "DETAIL_PICTURE", "XML_ID");
				$arFilter = Array("IBLOCK_ID" => IBLOCK_ID_COLORS, "ACTIVE"=>"Y",);
				$res = CIBlockElement::GetList(Array(), $arFilter, false, false, $arSelect);
				while( $ob = $res->GetNextElement() ){
					$arFields = $ob->GetFields();
					$arFields['ICON'] = i($arFields['DETAIL_PICTURE'], 50, 50);
					$colors[$arFields['XML_ID']] = $arFields['ICON'];
				}
			}

			$phpCache->StartDataCache();
			$phpCache->EndDataCache($colors);
		}

		return $colors;
	}

	/**
	 * Возвращает свойства товара для фильтра
	 * @return array
	 */
	public function getFilterProps(){

		$props = array();

		$props_ = array(
			'kategoriyatovar',
			'naznachenietovar',
			'tematikatovar',
			'tsvettovar',
			'POVERKHNOST',
			'MATERIAL',
			'VYSOTA',
			'SHIRINA',
			'STRANA',
			'PROIZVODITEL',
			'KOLLEKTSIYA',
		);

		# кешируем результат
		$phpCache = new CPHPCache;
		$cacheTime = 3600*30*24; # время кеширования - 1 месяц
		$cacheID = 'catalog_items_props_filter_'.USER_TYPE; # формируем уникальный id кеша
		$cachePath = '/cached_sections'; # папка с кешем

		if($phpCache->InitCache($cacheTime, $cacheID, $cachePath)) {
			$props = $phpCache->GetVars();
		}
		else {
			# помечаем кеш тегом, связанным с инфоблоком
//			$tagCache = $GLOBALS['CACHE_MANAGER'];
//			$tagCache->StartTagCache($cachePath);
//			$tagCache->RegisterTag('iblock_id_'.IBLOCK_ID_CATALOG);
//			$tagCache->EndTagCache();

			if( CModule::IncludeModule('iblock') ){

				$sections = array();
//				$arFilter = Array('IBLOCK_ID'=>IBLOCK_ID_CATALOG, 'UF_USER_GROUP' => ((USER_TYPE == 'retail')? 6 : 7));
				$arFilter = Array('IBLOCK_ID'=>IBLOCK_ID_CATALOG);

				if( USER_TYPE == 'wholesale' ){
					$arFilter['UF_USER_GROUP'] = 7;
				}

				$db_list = CIBlockSection::GetList(Array($by=>$order), $arFilter, true, array('ID'));
				while($ar_result = $db_list->GetNext()){
					$sections[] = $ar_result['ID'];
				}

				$arSelect = Array("ID");
				$arFilter = Array("IBLOCK_ID"=>IBLOCK_ID_CATALOG, "SECTION_ID" => $sections, "ACTIVE"=>"Y");
				$res = CIBlockElement::GetList(Array(), $arFilter, false, false, $arSelect);
				while($ob = $res->GetNextElement()){

					$arFields = $ob->GetFields();

					foreach( $props_ as $prop ){
						$res_ = CIBlockElement::GetProperty(IBLOCK_ID_CATALOG, $arFields['ID'], "name", "asc", array("CODE" => $prop));
						while ($ob_ = $res_->GetNext()){

							if( !eRU($props[$prop]['PROPERTY']) ){
								$props[$prop]['PROPERTY'] = array('NAME' => $ob_['NAME'], 'CODE' => $ob_['CODE']);
							}

							if( !is_array($props[$prop][$ob_['VALUE']]) && $ob_['VALUE'] ){
								$props[$prop]['VARIANTS'][$ob_['VALUE']] = array('ID' => $ob_['VALUE'], 'VALUE' =>$ob_['VALUE_ENUM'], 'XML_ID' => $ob_['VALUE_XML_ID']);
							}
						}
					}

//					echo '########################################';
//					foreach( $props_ as $prop ){
//						$ar_res = $ob->GetProperty($prop);
//						pRU($ar_res, 'all');
//					}
				}

//				pRU($props, 'all');

				/*
				 * todo : старый вариант выбора свойств
				 * foreach( $props_ as $propCode ){

					$propsVariants = array();

					$property_enums = CIBlockPropertyEnum::GetList(Array("name"=>"asc"), Array("IBLOCK_ID"=>IBLOCK_ID_CATALOG, "CODE"=>$propCode));
					while($enum_fields = $property_enums->GetNext()){
						$propsVariants[] = $enum_fields;
					}

					$props[$propCode] = $propsVariants;
				}*/
			}

			foreach( $props as $code => &$prop ){
				if( $code == 'PROIZVODITEL' || $code == 'KOLLEKTSIYA' ){
					uasort($prop['VARIANTS'], "sortFields");
				}
				if( $code == 'VYSOTA' || $code == 'SHIRINA' ){
					foreach( $prop['VARIANTS'] as &$value ){
						$value['VALUE'] = str_replace(',', '.', $value['VALUE']);
					}
					uasort($prop['VARIANTS'], "sortFields");
				}
			}

			$phpCache->StartDataCache();
			$phpCache->EndDataCache($props);
		}

		return $props;
	}

	
	/**
	 * Возвращает id значений св-в товаров, которые есть в указанных разделов
	 * @param $idParent
	 * @param $codeProp
	 * @return array
	 */
	public function getItemsSections($idParent, $codeProp){

		$itemCollections = array();

		# кешируем результат
		$phpCache = new CPHPCache;
		$cacheTime = 3600*30*24; # время кеширования - 1 месяц
		$cacheID = 'items_sections_'.$idParent.'_'.$codeProp; # формируем уникальный id кеша
		$cachePath = '/cached_sections'; # папка с кешем

		if($phpCache->InitCache($cacheTime, $cacheID, $cachePath)) {
			$itemCollections = $phpCache->GetVars();
		}
		else {
			# помечаем кеш тегом, связанным с инфоблоком
//			$tagCache = $GLOBALS['CACHE_MANAGER'];
//			$tagCache->StartTagCache($cachePath);
//			$tagCache->RegisterTag('iblock_id_'.IBLOCK_ID_CATALOG);
//			$tagCache->EndTagCache();

			if( CModule::IncludeModule('iblock') ){

				$arFilter = Array("IBLOCK_ID"=>IBLOCK_ID_CATALOG, "ACTIVE"=>"Y", "INCLUDE_SUBSECTIONS" => "Y");
				if( $idParent ){
					$arFilter['SECTION_ID'] = $idParent;
				}

				$res = CIBlockElement::GetList(Array(), $arFilter, false, false, array('ID', 'PROPERTY_'.$codeProp));
				while($ob = $res->GetNextElement()){
					$arFields = $ob->GetFields();
					$itemCollections[$arFields['PROPERTY_'.$codeProp.'_ENUM_ID']] = $arFields['PROPERTY_'.$codeProp.'_ENUM_ID'];
				}
//				asort($itemCollections);
			}

			$phpCache->StartDataCache();
			$phpCache->EndDataCache($itemCollections);
		}

		return $itemCollections;
	}

	/**
	 * Возвращает актуальные значения полей для раздела (фикс бага с выгрузкой)
	 * @param $idSection
	 * @return array
	 */
	public function getSectionMargin($idSection){
		$margin = array();

		if( $idSection ){
			$res = CIBlockSection::GetByID($idSection);
			if($ar_res = $res->GetNext()){
				$margin['LEFT_MARGIN'] = $ar_res['LEFT_MARGIN'];
				$margin['RIGHT_MARGIN'] = $ar_res['RIGHT_MARGIN'];
			}

		}
		return $margin;
	}
}

/**
 * Сортировка по производителю и коллекции по возрастанию по алфавиту
 * @param $a
 * @param $b
 * @return int
 */
function sortFields ($a, $b){
	if ( $a['VALUE'] == $b['VALUE'] ){
		return 0;
	}
	return ($a['VALUE'] < $b['VALUE']) ? -1 : 1;
}
