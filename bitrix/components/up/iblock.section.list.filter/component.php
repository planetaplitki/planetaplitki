<?php

use Bitrix\Main\Loader;

/**
 * @var JoriqueIblockElementList $this
 * @var array $arParams
 * @var array $arResult
 * @global CMain $APPLICATION
 * @global CUser $USER
 */

defined('B_PROLOG_INCLUDED') or die;

Loader::includeModule('iblock');

$this->setParams();

$sections_ = getSections();
$countries = $sections_['COUNTRIES'];
$brands = $sections_['BRANDS'];
$collections = $sections_['COLLECTIONS'];
$idSection = $arParams['SECTION_ID'];

# фильтр
global $filterName;
$filterName = $arParams['FILTER_NAME'];
$filterName_ = $filterName;

if( !preg_match('/^[A-Za-z_][A-Za-z01-9_]*$/', $filterName) || !isset($GLOBALS[$filterName]) ){
	$extFilter = array();
}
else {
	$extFilter = (array)$GLOBALS[$filterName];
}

$filter = array(
	//'SECTION_ID' => $arParams['SECTION_ID'] ? $arParams['SECTION_ID'] : false,
	'IBLOCK_ID' => $arParams['IBLOCK_ID'],
	'IBLOCK_ACTIVE' => 'Y',
	'ACTIVE' => 'Y',
	'CHECK_PERMISSIONS' => 'Y',
	'MIN_PERMISSION' => 'R',
	'DEPTH_LEVEL' => $arParams['DEPTH_LEVEL'],
);


# фильтр по типу пользователя: для розницы не используем фильтр
if( USER_TYPE == "wholesale" ){
//	$filter['UF_USER_GROUP'] = USER_TYPE == "retail" ? 6 : 7;
	$filter['UF_USER_GROUP'] = 7;
}

$filter = array_merge($filter, $extFilter);

$ufType = $arParams['UF_TYPE'];
if( $ufType ){
	switch($ufType){
		case 'sale' : $filter['UF_SALE'] = 1;
			break;
		case 'new' : $filter['UF_NEW_COLLECTION'] = 1;
			break;
		case 'exclusive' : $filter['UF_EXCLUSIVE'] = 1;
			break;
	}
}

$status = htmlspecialchars($_REQUEST['STATUSES']);
$priceMin = intval($_REQUEST['PRICE_MIN']);
$priceMax = intval($_REQUEST['PRICE_MAX']);

$shirinaMin = intval($_REQUEST['SHIRINA_MIN']);
$shirinaMax = intval($_REQUEST['SHIRINA_MAX']);

$visotaMin = intval($_REQUEST['VISOTA_MIN']);
$visotaMax = intval($_REQUEST['VISOTA_MAX']);

if( eRU($countries[$idSection]) ){

	$country = $countries[$idSection];
	$margin = $this->getSectionMargin($country['ID']);
	$filter['LEFT_MARGIN'] = $margin['LEFT_MARGIN'];
	$filter['RIGHT_MARGIN'] = $margin['RIGHT_MARGIN'];

}
elseif( eRU($brands[$idSection]) ){

	$brand = $brands[$idSection];
	$country = $sections_[$brand['IBLOCK_SECTION_ID']];

	$margin = $this->getSectionMargin($brand['ID']);
	$filter['LEFT_MARGIN'] = $margin['LEFT_MARGIN'];
	$filter['RIGHT_MARGIN'] = $margin['RIGHT_MARGIN'];

//	$filter['LEFT_MARGIN'] = $brand['LEFT_MARGIN'];
//	$filter['RIGHT_MARGIN'] = $brand['RIGHT_MARGIN'];
//	$filter['SECTION_ID'] = $brand['ID'];
}

if( $_REQUEST['setFilter'] ){

	if( eRU($_REQUEST['PROPERTY']) ){
		foreach( $_REQUEST['PROPERTY'] as $code => $value ){
			$el_props_filter[$code] = $value;
		}
		$filter['PROPERTY'] = $el_props_filter;
	}
	if( $status ){
		$filter[$status] = 1;
	}

	if( $visotaMax ){
		$filter[] = array(
			"LOGIC" => "AND",
			array("<=PROPERTY_VYSOTA" => $visotaMax),
			array(">=PROPERTY_VYSOTA" => $visotaMin),
		);
	}
	if( $shirinaMax ){
		$filter[] = array(
			"LOGIC" => "AND",
			array("<=PROPERTY_SHIRINA" => $shirinaMax),
			array(">=PROPERTY_SHIRINA" => $shirinaMin),
		);
	}
	if( USER_TYPE == 'wholesale' ){
		if( !$priceMin ){
			$filter['<=UF_PRICE_WHOLESALE'] = $priceMax;
		}
		else{
			$filter[] = array(
				"LOGIC" => "AND",
				array("<=UF_PRICE_WHOLESALE" => $priceMax),
				array(">=UF_PRICE_WHOLESALE" => $priceMin),
			);
		}
	}
	else{
		if( !$priceMin ){
			$filter['<=UF_PRICE_RETAIL'] = $priceMax;
		}
		else{
			$filter[] = array(
				"LOGIC" => "AND",
				array("<=UF_PRICE_RETAIL" => $priceMax),
				array(">=UF_PRICE_RETAIL" => $priceMin),
			);
		}
	}
}

# получаем цены коллекций для фильтрации
$prices = array();
$priceType = 'UF_PRICE_RETAIL';
if( USER_TYPE == 'wholesale' ){
	$priceType = 'UF_PRICE_WHOLESALE';
}
foreach( $collections as $collection ){
	$price = intval($collection[$priceType]);
	if( !in_array($price, $prices) ){
		$prices[] = $price;
	}
}
sort($prices);
$minPrice = min($prices);
$maxPrice = max($prices);

if( !$priceMax ){
	$priceMax = $maxPrice;
}

$ranges = array(
	'PRICES' => array(
		'MIN' => $minPrice,
		'MAX' => $maxPrice,
		'MIN_CURRENT' => $priceMin,
		'MAX_CURRENT' => $priceMax,
	),
	'SHIRINA' => array(
		'MIN' => $min,
		'MAX' => $max,
		'MIN_CURRENT' => $visotaMin,
		'MAX_CURRENT' => $visotaMax,
	),
	'VISOTA' => array(
		'MIN' => $min,
		'MAX' => $max,
		'MIN_CURRENT' => $shirinaMin,
		'MAX_CURRENT' => $shirinaMax,
	),
);

# массив допустимых цен на коллекции
$APPLICATION->AddHeadString('<script>window.ranges='.json_encode($ranges).'</script>',true);

$GLOBALS[$filterName_] = $filter;

$arResult['FILTER']['SECTIONS'] = $sections_;
$arResult['FILTER']['PRICE'] = $ranges['PRICES'];
$arResult['FILTER']['PROPS_'] = $this->getFilterProps();
//
$arResult['FIELDS']['COLORS'] = $this->getColors();
$arResult['FILTER']['ACTION'] = $action;

$this->includeComponentTemplate();

//pRU($arResult['FILTER']['PROPS_'], 'all');
//pRU($GLOBALS[$filterName_], 'all');
//$db_list = CIBlockSection::GetList(Array($by=>$order), $filter, true);
//$arResult['FILTER']['COUNT'] = intval($db_list->SelectedRowsCount());

return $arResult['ID'];
