<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>

<?       
CModule::IncludeModule('sale');     
?>

<?
global $USER;
$arGroups = CUser::GetUserGroup($USER->GetID());
?>

<?    
$sections = getSections();
$countries = $sections['COUNTRIES'];
$brands = $sections['BRANDS'];
$type = $arParams['UF_TYPE'];
$userType = $arParams['UF_USER_TYPE'];

if($arResult['MODE_VIEW'] == 'items'){
    $type = 'items';
}
?>

<?
$class = 'col-xs-6 col-sm-4 col-md-15 usligi-pict';
if( $type == 'similar' ){
    $class = 'col-xs-6 col-sm-3 col-md-3 usligi-pict';
}
elseif( $type == 'similar.mobile' ){
    $class = 'col-xs-4 usligi-pict2';
}
?>

<?
//pRU($arParams, 'all');
//pRU($arResult, 'all');


if( $view == 'brands' ){?>  
    <?foreach($arResult['ITEMS'] as $item) {?>

        <div class="container obj50">
            <div class="row">
                <div class="col-sm-12">
                    <p class="t2"><?=$item['NAME']?></p>

                    <?$APPLICATION->IncludeComponent(
                        "bitrix:catalog.section",
                        "catalog",
                        array(
                            "TEMPLATE_THEME" => "blue",
                            "PRODUCT_DISPLAY_MODE" => "N",
                            "ADD_PICT_PROP" => "-",
                            "LABEL_PROP" => "-",
                            "OFFER_ADD_PICT_PROP" => "FILE",
                            "OFFER_TREE_PROPS" => "-",
                            "PRODUCT_SUBSCRIPTION" => "N",
                            "SHOW_DISCOUNT_PERCENT" => "N",
                            "SHOW_OLD_PRICE" => "N",
                            "SHOW_CLOSE_POPUP" => "N",
                            "MESS_BTN_BUY" => "Купить",
                            "MESS_BTN_ADD_TO_BASKET" => "В корзину",
                            "MESS_BTN_SUBSCRIBE" => "Подписаться",
                            "MESS_BTN_DETAIL" => "Подробнее",
                            "MESS_NOT_AVAILABLE" => "Нет в наличии",
                            "AJAX_MODE" => "N",
                            "SEF_MODE" => "N",
                            "IBLOCK_TYPE" => "1c_catalog",
                            "IBLOCK_ID" => "34",
                            "SECTION_ID" => $item['ID'],
                            "SECTION_CODE" => "",
                            "SECTION_USER_FIELDS" => array(
                                0 => "",
                                1 => "",
                            ),
                            "ELEMENT_SORT_FIELD" => "sort",
                            "ELEMENT_SORT_ORDER" => "asc",
                            "ELEMENT_SORT_FIELD2" => "name",
                            "ELEMENT_SORT_ORDER2" => "asc",
                            "FILTER_NAME" => "arrFilterPoverhost",
                            "INCLUDE_SUBSECTIONS" => "Y",
                            "SHOW_ALL_WO_SECTION" => "Y",
                            "SECTION_URL" => "#SITE_DIR#catalog/#SECTION_CODE_PATH#/",
                            "DETAIL_URL" => "#SITE_DIR#catalog/#SECTION_CODE_PATH#/#ELEMENT_CODE#/",
                            "BASKET_URL" => "/personal/basket.php",
                            "ACTION_VARIABLE" => "action",
                            "PRODUCT_ID_VARIABLE" => "id",
                            "PRODUCT_QUANTITY_VARIABLE" => "quantity",
                            "ADD_PROPERTIES_TO_BASKET" => "Y",
                            "PRODUCT_PROPS_VARIABLE" => "prop",
                            "PARTIAL_PRODUCT_PROPERTIES" => "N",
                            "SECTION_ID_VARIABLE" => "SECTION_ID",
                            "ADD_SECTIONS_CHAIN" => "N",
                            "DISPLAY_COMPARE" => "N",
                            "SET_TITLE" => "N",
                            "SET_BROWSER_TITLE" => "N",
                            "BROWSER_TITLE" => "-",
                            "SET_META_KEYWORDS" => "N",
                            "META_KEYWORDS" => "",
                            "SET_META_DESCRIPTION" => "N",
                            "META_DESCRIPTION" => "",
                            "SET_LAST_MODIFIED" => "N",
                            "USE_MAIN_ELEMENT_SECTION" => "N",
                            "SET_STATUS_404" => "N",
                            "PAGE_ELEMENT_COUNT" => "100",
                            "LINE_ELEMENT_COUNT" => "3",
                            "PROPERTY_CODE" => array(
                                0 => "VYSOTA",
                                1 => "SHIRINA",
                                2 => "",
                            ),
                            "OFFERS_FIELD_CODE" => "",
                            "OFFERS_PROPERTY_CODE" => "",
                            "OFFERS_SORT_FIELD" => "sort",
                            "OFFERS_SORT_ORDER" => "asc",
                            "OFFERS_SORT_FIELD2" => "active_from",
                            "OFFERS_SORT_ORDER2" => "desc",
                            "OFFERS_LIMIT" => "5",
                            "BACKGROUND_IMAGE" => "-",
                            "PRICE_CODE" => $arParams['PRICE_CODE'],
                            "USE_PRICE_COUNT" => "",
                            "SHOW_PRICE_COUNT" => "1",
                            "PRICE_VAT_INCLUDE" => "Y",
                            "PRODUCT_PROPERTIES" => array(
                            ),
                            "USE_PRODUCT_QUANTITY" => "Y",
                            "CACHE_TYPE" => "A",
                            "CACHE_TIME" => "36000000",
                            "CACHE_FILTER" => "Y",
                            "CACHE_GROUPS" => "Y",
                            "DISPLAY_TOP_PAGER" => "N",
                            "DISPLAY_BOTTOM_PAGER" => "N",
                            "PAGER_TITLE" => "Товары",
                            "PAGER_SHOW_ALWAYS" => "N",
                            "PAGER_TEMPLATE" => "infinity",
                            "PAGER_DESC_NUMBERING" => "N",
                            "PAGER_DESC_NUMBERING_CACHE_TIME" => "36000",
                            "PAGER_SHOW_ALL" => "N",
                            "HIDE_NOT_AVAILABLE" => "N",
                            "OFFERS_CART_PROPERTIES" => "",
                            "AJAX_OPTION_JUMP" => "N",
                            "AJAX_OPTION_STYLE" => "Y",
                            "AJAX_OPTION_HISTORY" => "N",
                            "CONVERT_CURRENCY" => "Y",
                            "CURRENCY_ID" => "RUB",
                            "ADD_TO_BASKET_ACTION" => "ADD",
                            "PAGER_BASE_LINK_ENABLE" => "Y",
                            "SHOW_404" => "N",
                            "MESSAGE_404" => "",
                            "PAGER_BASE_LINK" => "",
                            "PAGER_PARAMS_NAME" => "arrPager",
                            "COMPONENT_TEMPLATE" => "catalog",
                            "MESS_BTN_COMPARE" => "Сравнить",
                            "UF_TYPE" => "recommended",
                            "AJAX_OPTION_ADDITIONAL" => "",
                            "FILE_404" => "",
                            "DISABLE_INIT_JS_IN_COMPONENT" => "N",
                            "UF_USER_TYPE" => USER_TYPE
                        ),
                        false
                    );?>
                </div>
            </div>
        </div>
        <?}?>
    <?}
elseif( $type == 'items' ){

    $isShow = true;
    foreach($arResult['ITEMS'] as $item) {   
        if($isShow){
            //print_r($item);
            $isShow = false;        
        }
        $this->AddEditAction($item['ID'], $item['EDIT_LINK'], CIBlock::GetArrayByID($arParams['IBLOCK_ID'], 'ELEMENT_EDIT'));
        $this->AddDeleteAction($item['ID'], $item['DELETE_LINK'], CIBlock::GetArrayByID($arParams['IBLOCK_ID'], 'ELEMENT_DELETE'), array('CONFIRM' => 'Удалить?'));



        $img = i($item['DETAIL_PICTURE'], 198, 0, BX_RESIZE_IMAGE_EXACT);
        $img = $img ? $img : "/upload/images/net-foto-kollektsia.jpg";

        $brand = $brands[$item['IBLOCK_SECTION_ID']];
        echo empty($brand);
        $country = $countries[$brand['IBLOCK_SECTION_ID']];
        $imgFlag = i($country['UF_FLAG'], 16, 11, BX_RESIZE_IMAGE_EXACT);
        $price = $item['UF_PRICE_RETAIL'];
        if( $userType == 'wholesale' ){
            $price = $item['UF_PRICE_WHOLESALE'];
        }
        $price = SaleFormatCurrency($price, 'RUB', true);

        $marker = getMarkerSection($item);

        if( eRU($item['UF_MORE_PHOTO']) ){
            $morePhoto = $item['UF_MORE_PHOTO'];
        }
        else{
            $morePhoto = unserialize($item['UF_MORE_PHOTO']);
        }
        //        pRU($morePhoto, 'all');
        //        pRU($item, 'all');
        ?>



        <div class="<?=$class;?>" id="<?=$this->GetEditAreaId($item['ID'])?>">
            <?=$marker['HTML']['LIST']?>

            <a href="<?=$item['DETAIL_PAGE_URL']?>" style="max-width: 100%;max-height: 198px;" class="collections-catalog-placeholder">
                <img src="<?=$img;?>"  style="max-width: 100%;max-height: 198px;" >
            </a>
            <div class="collections-catalog-gallery">
                <a href="<?=$img;?>" class="collections-catalog-image-link"></a>
                <?foreach($morePhoto as $onePhoto){
                    $imgPhoto = i($onePhoto, 198, 198, BX_RESIZE_IMAGE_EXACT);?>
                    <a href="<?=$imgPhoto?>" class="collections-catalog-image-link"></a>
                    <?}?>
            </div>

            <div class="box_under">
                <a href="<?=$item['DETAIL_PAGE_URL']?>" class="link"><?=$item["PROPERTIES"]['NAZVANIE_DLYA_SAYTA']['VALUE']?></a>
                <p class="easy"><?=$brand['NAME']?> (<?=$country['NAME']?>)
                    <span class="flag"><img src="<?=$imgFlag;?>" alt="<?=$item["PROPERTIES"]['NAZVANIE_DLYA_SAYTA']['VALUE']?>" /></span>
                </p>
                <?if( $price ){?>
                    <p class="metr">Цена от <span><?=$price;?> Р</span>/м2 </p>
                    <?}else{?>
                    <p class="metr"></p>
                    <?}?>
            </div>
        </div>

        <?}?>

    <div class="row">
        <div class="col-xs-12">
            <?if($arParams['DISPLAY_BOTTOM_PAGER']) {?>
                <?=$arResult['NAV_STRING'];?>
                <?}?>
        </div>
    </div>
    <?
} 


# похожие коллекции в карточке товара: мобильные
elseif( $type == 'similar.mobile' ){?>
    <div class="panel">
        <a data-toggle="collapse" data-parent="#accordion" href="#collapse5">
            <div class="title-heading">
                <h4 class="title">Похожие коллекции</h4>
                <div class="point"></div>
            </div>
        </a>
        <div id="collapse5" class="panel-collapse collapse">
            <div class="container">
                <div class="row">

                    <?foreach($arResult['ITEMS'] as $item) {?>

                        <?                    
                        $img = i($item['PICTURE'], 198, 198, BX_RESIZE_IMAGE_EXACT);
                        $img = $img ? $img : "/upload/images/net-foto-kollektsia.jpg";

                        $brand = $brands[$item['IBLOCK_SECTION_ID']];
                        $country = $countries[$brand['IBLOCK_SECTION_ID']];
                        $price = $item['UF_PRICE_RETAIL'];
                        if( $userType == 'wholesale' ){
                            $price = $item['UF_PRICE_WHOLESALE'];
                        }

                        $price = SaleFormatCurrency($price, 'RUB', true);
                        $marker = getMarkerSection($item);


                        ?>

                        <div class="<?=$class;?>">
                            <?=$marker['HTML']['LIST']?>
                            <a href="<?=$item['SECTION_PAGE_URL']?>"><img src="<?=$img;?>"></a>
                            <div class="box_under">
                                <a href="<?=$item['SECTION_PAGE_URL']?>" class="link"><?=$item['NAME']?></a>
                                <p class="easy"><?=$brand['NAME']?> (<?=$country['NAME']?>)</p>
                                <?if( $price ){?>
                                    <p class="metr">Цена от <span><?=$price;?> Р</span>/м2</p>
                                    <?}
                                else{?>
                                    <p class="metr"></p>
                                    <?}?>
                            </div>
                        </div>
                        <?}?>
                </div>
            </div>
        </div>
    </div>
    <?}
else{        
    ?>

    <?foreach($arResult['ITEMS'] as $item) {?>

        <?                        
        $this->AddEditAction($item['ID'], $item['EDIT_LINK'], CIBlock::GetArrayByID($arParams['IBLOCK_ID'], 'ELEMENT_EDIT'));
        $this->AddDeleteAction($item['ID'], $item['DELETE_LINK'], CIBlock::GetArrayByID($arParams['IBLOCK_ID'], 'ELEMENT_DELETE'), array('CONFIRM' => 'Удалить?'));
        ?>

        <?        
        $img = i($item['PICTURE'], 198, 198, BX_RESIZE_IMAGE_EXACT);
        $img = $img ? $img : "/upload/images/net-foto-kollektsia.jpg";

        $brand = $brands[$item['IBLOCK_SECTION_ID']];
        $country = $countries[$brand['IBLOCK_SECTION_ID']];
        $imgFlag = i($country['UF_FLAG'], 16, 11, BX_RESIZE_IMAGE_EXACT);
        $price = $item['UF_PRICE_RETAIL'];
        if( $userType == 'wholesale' ){
            $price = $item['UF_PRICE_WHOLESALE'];
        }
        $price = SaleFormatCurrency($price, 'RUB', true);

        if(!empty($item['OLD_PRICE'])){
            $oldPrice = SaleFormatCurrency($item['OLD_PRICE'], 'RUB', true);
        }

        $marker = getMarkerSection($item);

        if( eRU($item['UF_MORE_PHOTO']) ){
            $morePhoto = $item['UF_MORE_PHOTO'];
        }
        else{
            $morePhoto = unserialize($item['UF_MORE_PHOTO']);
        }
        //        pRU($morePhoto, 'all');
        //        pRU($item, 'all');
        ?>



        <div class="<?=$class;?>" id="<?=$this->GetEditAreaId($item['ID'])?>">
            <?=$marker['HTML']['LIST']?>

            <a href="<?=$item['SECTION_PAGE_URL']?>" class="collections-catalog-placeholder">
                <img src="<?=$img;?>">
            </a>
            <div class="collections-catalog-gallery">
                <a href="<?=$img;?>" class="collections-catalog-image-link"></a>
                <?foreach($morePhoto as $onePhoto){
                    $imgPhoto = i($onePhoto, 198, 198, BX_RESIZE_IMAGE_EXACT);?>
                    <a href="<?=$imgPhoto?>" class="collections-catalog-image-link"></a>
                    <?}?>
            </div>

            <div class="box_under" data-price="<?=$price;?>">
                <a href="<?=$item['SECTION_PAGE_URL']?>" class="link"><?=$item['NAME']?></a>
                <p class="easy"><?=$brand['NAME']?> (<?=$country['NAME']?>)
                    <span class="flag"><img src="<?=$imgFlag;?>" alt="Керамическая плитка <?=$item['NAME']?>" /></span>
                </p>
                <?if($item["UF_CATEGORY"]):?><p class="easy category"><?=$item["UF_CATEGORY"]?></p><?endif;?>

                <?$dirz = $APPLICATION->GetCurDir();
                $dir = explode("/", $dirz);
                ?>

                <?if( $price ){?>

                    <?/*global $USER;
                    if($USER->IsAdmin()){
                    echo "---------price------<pre>";
                    var_dump($price);
                    echo "</pre>-------END-------";//todo for debug
                    echo "--------OLD_PRICE-------<pre>";
                    var_dump($oldPrice);
                    echo "</pre>-------END-------";//todo for debug
                    echo "-------bool--------<pre>";
                    var_dump(intval($oldPrice) > intval($price));
                    echo "</pre>--------------";//todo for debug
                    }*/?>

                    <?if( $item['OLD_PRICE'] ):?>
                        <p itemprop="offers" itemscope itemtype="http://schema.org/Offer" class="metr priceDiscount" data-test="barmaley_1">
                            от <span itemprop="price" content="<?=$price;?>" style="color: #d60303; font-size: 14px;"><?=$price;?> <span itemprop="priceCurrency" content="RUR">р/</span>м2</span>
                        </p>

                        <?if(!in_array(9, $arGroups)):?>
                            <p class="metr priceBase" style="padding-top: 2px;"><span style="font-size: 13px;"><?=SaleFormatCurrency($item['OLD_PRICE'], 'RUB', true)?> р</span>/м2</p>
                            <?endif;?>
                        <?else:?>
                        <p class="metr" data-test="barmaley_2">Цена от <span><?=$price;?> Р</span>/м2 </p>
                        <?endif;?>

                    <?}else{?>

                    <? CModule::IncludeModule("iblock");

                    $userType = USER_TYPE;

                    $m2PriceId = 10;
                    if ($userType == 'wholesale') {
                        $m2PriceId = 12;
                    }            

                    $arSelect = Array("ID", "NAME", "CATALOG_GROUP_11", "CATALOG_GROUP_".$m2PriceId);
                    $arFilter = Array("IBLOCK_ID"=>34, "SECTION_ID"=>$item["ID"], "ACTIVE"=>"Y");
                    $res = CIBlockElement::GetList(Array("catalog_PRICE_".$m2PriceId=>"asc,nulls", "catalog_PRICE_11"=>"asc,nulls"), $arFilter, false, Array("nPageSize" => 1), $arSelect);
                    if($ob = $res->Fetch())
                    {
                        $retailM2 = '';
                        $db_res = CPrice::GetList(array(), array("PRODUCT_ID" => $ob['ID'], "CATALOG_GROUP_ID" => $m2PriceId));
                        if ($ar_res = $db_res->Fetch()) {                           
                            $retailM2 = $ar_res['PRICE'];
                        }

                        if(!empty($retailM2)){?>
                            <?if( $item['OLD_PRICE'] && (intval($oldPrice) > intval($retailM2)) ):?>
                                <p itemprop="offers" itemscope itemtype="http://schema.org/Offer" class="metr priceDiscount" data-test="barmaley_3">
                                    от <span itemprop="price" content="<?=$retailM2?>" style="color: #d60303; font-size: 14px;"><?=$retailM2;?> <span itemprop="priceCurrency" content="RUR">р/</span>м2</span>
                                </p>

                                <?if(!in_array(9, $arGroups)):?>
                                    <p class="metr priceBase" style="padding-top: 2px;"><span style="font-size: 13px;"><?=SaleFormatCurrency($item['OLD_PRICE'], 'RUB', true)?> р</span>/м2</p>
                                    <?endif;?>
                                <?else:?>
                                <p class="metr" data-test="barmaley_4">Цена от <span><?=$retailM2;?> Р</span>/м2 </p>
                                <?endif;?>
                            <!--old version-->
                            <!--<p class="metr">Цена от <span><?/*echo $retailM2;*/?> Р</p>/м2 -->
                            <?}
                        elseif(!empty($ob['CATALOG_PRICE_11']) && empty($price) && ($dir[1] != "all-shares")) {?>
                            <p class="metr" data-test="barmaley_5">Цена от <span><?echo $ob['CATALOG_PRICE_11'];?> Р</p>/шт
                            <?}else {?><p class="metr" data-test="barmaley_6"></p><?}?>

                        <?}?>    
                    <?}?>
            </div>
        </div>

        <?}?>

    <div class="js-pagination">
        <div class="row">
            <div class="col-xs-12">
                <?if($arParams['DISPLAY_BOTTOM_PAGER'] && strlen($arResult['NAV_STRING'])>1) {?>
                        
                        <?
                        $ch = explode("PAGEN_",$arResult['NAV_STRING']);
                        $nma=explode("=",$ch[1]);
                        $nm=$nma[0];
                        ?>
                        <div class="show-more-block">
                            <a href="<?=$APPLICATION->GetCurPageParam('PAGEN_'.$nm.'='.(intval($_REQUEST['PAGEN_'.$nm]) > 0 ? intval($_REQUEST['PAGEN_'.$nm]) + 1 : 2), array('PAGEN_'.$nm));?>" class="show_more js-show-more-col">Показать еще</a>
                            <img class="show-more-block__preloader" src="<?=SITE_DEFAULT_TEMPLATE_PATH?>img/preloader.gif">
                        </div>
                    

                    <?=$arResult['NAV_STRING'];?>
                <?}?>
            </div>
        </div>
    </div>
 
    <?
    $description = $arResult['DESCRIPTION'];
    if( $arResult['SECTION']['~DESCRIPTION'] ){
        $description = $arResult['SECTION']['~DESCRIPTION'];
    }
    if (\Bitrix\Main\Loader::includeModule('synpaw.seofilter') &&
    $model = CSynPawSeoFilter::getInstance()->getModel()) {
        if(!empty($model))
            $description = $model['CONTENT'];
    }
    ?>

    <?if( $description ){?>
        <hr class="line-buttom">
        <div class="t2"><?=$arResult['SECTION']['NAME']?></div>
        <div class="easy">
            <?=$description;?>
        </div>
        <?}?>

    <?}?>

<?
//pRU($arResult['ITEMS'], 'all');
//pRU($arResult, 'all');
/*
?>
<div style="display: none">

<?
pRU($arResult, 'all');?>
</div>
<?
*/?>
