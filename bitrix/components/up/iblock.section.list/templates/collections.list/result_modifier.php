<?php
/**
 * Created by PhpStorm.
 * User: master
 * Date: 28.02.2018
 * Time: 12:16
 */

if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();
if($arParams["IS_ACTION"] === "Y"){
    global $APPLICATION;
    $userType = USER_TYPE;

    $m2PriceId = 10;
    $priceType = 'UF_PRICE_RETAIL';
    if ($userType == 'wholesale') {
        $m2PriceId = 12;
        $priceType = 'UF_PRICE_WHOLESALE';
    }

    if(!empty($arResult['ITEMS'])){
        foreach ($arResult['ITEMS'] as $key => $arSection){
            $filter = array('IBLOCK_ID'=>$arSection["IBLOCK_ID"], 'SECTION_ID'=>$arSection['ID'], 'ACTIVE'=>'Y', '>CATALOG_PRICE_'.$m2PriceId=>1);
            $dbRes = CIBlockElement::GetList(array('catalog_PRICE_'.$m2PriceId=>'asc'), $filter, false, false, array('IBLOCK_ID', 'ID', 'NAME', 'CODE', 'CATALOG_GROUP_10'));
            if($resElement = $dbRes->fetch()){
                $arResult['ITEMS'][$key]['OLD_PRICE'] = $resElement['CATALOG_PRICE_10'];

                $dbPrice = CPrice::GetList(array(), array("PRODUCT_ID" => $resElement['ID'], "CATALOG_GROUP_ID" => $m2PriceId));
                while ($arPrice = $dbPrice->Fetch())
                {
                    $arDiscounts = CCatalogDiscount::GetDiscountByPrice(
                        $arPrice["ID"],
                        $USER->GetUserGroupArray(),
                        "N",
                        SITE_ID
                    );
                    $discountPrice = CCatalogProduct::CountPriceWithDiscount(
                        $arPrice["PRICE"],
                        $arPrice["CURRENCY"],
                        $arDiscounts
                    );
                    $arPrice["DISCOUNT_PRICE"] = $discountPrice;

                    $arResult['ITEMS'][$key][$priceType] = $arPrice['DISCOUNT_PRICE'];
                }

            }
        }
    }
}
