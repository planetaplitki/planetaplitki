<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>

<?if( !count($arResult['ITEMS']) ){
    return;
}?>

<?
CModule::IncludeModule('sale');

$type = $arParams['UF_TYPE'];

if( $type == 'new' ){
    $name = "Новые коллекции";
    $carousel = "carousel_1";
}
elseif( $type == 'popular' ){
    $name = "Популярные коллекции";
    $carousel = "carousel_2";
}
elseif( $type == 'pop_collections' ){
    $name = "Популярные коллекции";
    $carousel = "carousel_3";
}

if($type == 'pop_collections') {
    $inStr = 3;    
} else {
    $inStr = 5;    
}

$count = count($arResult['ITEMS']);
$parts = ceil($count/$inStr); 
?>

<div class="container">
    <div class="row">
        <div class="col-sm-12">
            <div class="carousel-line">
                <div class="title"><?=$name?></div>
                <div class="control hidden-xs">
                    <a class="control-prev" href="#<?=$carousel;?>" data-slide="prev"></a>
                    <a class="control-next" href="#<?=$carousel;?>" data-slide="next"></a>
                </div>
            </div>
        </div>
    </div>
    <div class="row obj">
        <div class="col-sm-12">
            <div id="<?=$carousel;?>" class="carousel slide">

                <?if( $count > $inStr ){?>
                    <ol class="carousel-indicators">
                        <?for( $i = 0; $i<$parts; $i++ ){?>
                            <li data-target="#<?=$carousel;?>" data-slide-to="<?=$i;?>" class="<?=!$i ? "active" : "";?>"></li>
                        <?}?>
                    </ol>
                <?}?>

                <div class="carousel-inner">
                    <div class="item active">
                        <div class="row-fluid">
                            <?
                            $i = 1;
                            $j = 0;

                            foreach($arResult['ITEMS'] as $item) {?>
                            
                                <?
                                $nav = CIBlockSection::GetNavChain(false, $item["IBLOCK_SECTION_ID"]);
                                while ($arNav=$nav->GetNext()) {
                                    if($arNav["DEPTH_LEVEL"] == 1) {
                                        $country_name = $arNav["NAME"];    
                                    }
                                }
                                ?>
                                
                                <?
                                $this->AddEditAction($item['ID'], $item['EDIT_LINK'], CIBlock::GetArrayByID($arParams['IBLOCK_ID'], 'ELEMENT_EDIT'));
                                $this->AddDeleteAction($item['ID'], $item['DELETE_LINK'], CIBlock::GetArrayByID($arParams['IBLOCK_ID'], 'ELEMENT_DELETE'), array('CONFIRM' => 'Удалить?'));
                                ?>

                                <?
                                if($type == 'pop_collections') {
                                    $img = i($item['PICTURE'], 365, 195, BX_RESIZE_IMAGE_EXACT);   
                                } else {
                                    $img = i($item['PICTURE'], 170, 170, BX_RESIZE_IMAGE_EXACT);    
                                }
                                
                                $img = $img ? $img : "https://placeholdit.imgix.net/~text?txtsize=33&txt=350%C3%97150&w=170&h=170";
                                ?>

                                <div class="span3<?if($type != 'pop_collections'):?><?=!$j ? ' hidden-xs hidden-sm' : ''?><?endif;?>" id="<?=$this->GetEditAreaId($item['ID'])?>">
                                    <a href="<?=$item['SECTION_PAGE_URL']?>" class="thumbnail">
                                        <img src="<?=$img;?>" alt="Image" style="max-width:100%;" />
                                    </a>
                                    <?if($type == 'pop_collections'):?>
                                        <span><?=$item["NAME"]?> (<?=$country_name?>)</span>    
                                    <?endif;?>
                                </div>

                                <?$j++;?>

                                <?if( $i%$inStr == 0 && $i != $count ){?>
                                    </div></div><div class="item"><div class="row-fluid">
                                    <?$j=0?>
                                <?}?>

                                <?$i++;?>
                            <?}?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>