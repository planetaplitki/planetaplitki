<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>

<?$this->SetViewTarget("brands_filter");?>

<?
$sections = getSectionsByUserType();
$countries = $sections['COUNTRIES'];
$userType = $arParams['UF_USER'];
?>

<div class="checkbox-mnf">
	<form action="<?=CURPAGE?>" method="get" id="brands">
		<?foreach($countries as $country){?>
			<input type="checkbox" id="<?=$country['ID']?>" value="<?=$country['ID']?>" name="select_country[]" <?=in_array($country['ID'], $_REQUEST['select_country']) ? "checked" : "";?> />
			<label for="<?=$country['ID']?>"><span></span><?=$country['NAME']?></label>
		<?}?>
	</form>
</div>

<?$this->EndViewTarget("brands_filter");?>

<div class="clearfix">
	<?$i = 1;?>
	<?foreach($arResult['ITEMS'] as $item) {?>

		<?
		$this->AddEditAction($item['ID'], $item['EDIT_LINK'], CIBlock::GetArrayByID($arParams['IBLOCK_ID'], 'ELEMENT_EDIT'));
		$this->AddDeleteAction($item['ID'], $item['DELETE_LINK'], CIBlock::GetArrayByID($arParams['IBLOCK_ID'], 'ELEMENT_DELETE'), array('CONFIRM' => 'Удалить?'));
		?>

		<?
		$img = i($item['PICTURE'], 138, 138);
		$img = $img ? $img : "/upload/images/net-foto-proizvoditel.jpg";
		if( $userType == 'wholesale' ){
			$item['SECTION_PAGE_URL'] = $item['SECTION_PAGE_URL'].'?view=brands';
		}?>

		<div class="col-xs-6 col-sm-4 col-md-15" id="<?=$this->GetEditAreaId($item['ID'])?>">
			<a href="<?=$item['SECTION_PAGE_URL']?>" class="pict_mnf_link">
				<div class="pict_mnf">
					<img src="<?=$img;?>">
				</div>
			</a>
		</div>

		<?if( $i%5 == 0 ){?>
			</div><div class="clearfix">
		<?}?>

		<?$i++;?>
	<?}?>
</div>

<?if($arParams['DISPLAY_BOTTOM_PAGER']) {
	echo $arResult['NAV_STRING'];
}?>
