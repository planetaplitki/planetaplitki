<?php
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die;

$arComponentDescription = array(
    "NAME" => "Список разделов",
    "DESCRIPTION" => "Список разделов",
    "ICON" => "/images/cat_list.gif",
    "CACHE_PATH" => "Y",
    "SORT" => 30,
    "PATH" => array(
        "ID" => "Jorique"
    ),
);

?>