<?php

use Bitrix\Main\Loader;

/**
* @var JoriqueIblockElementList $this
* @var array $arParams
* @var array $arResult
* @global CMain $APPLICATION
* @global CUser $USER
*/

defined('B_PROLOG_INCLUDED') or die;

Loader::includeModule('iblock');

$this->setParams();



# фильтр
$filterName = $arParams['FILTER_NAME'];
if (!preg_match('/^[A-Za-z_][A-Za-z01-9_]*$/', $filterName) || !isset($GLOBALS[$filterName])) {
    $extFilter = array();
}
else {
    $extFilter = (array)$GLOBALS[$filterName];
}      

$arNavParams = $this->getNavArray();
$arNavigation = $this->getNavForCache();



//$params = array(($arParams["CACHE_GROUPS"]==="N"? false: $USER->GetGroups()), $arNavigation, $arParams['UF_VIEW'], $arParams['UF_USER']);
//pRU($params, 'all');
//echo md5(serialize($params));

//pRU($arParams, 'all');

//if( $this->StartResultCache(false) ){
//if( $this->StartResultCache(false, md5(serialize(array(($arParams["CACHE_GROUPS"]==="N"? false: $USER->GetGroups()), $arNavigation, $arParams['UF_VIEW'], $arParams['UF_USER']))) )){
//if( $this->StartResultCache(false,  ($arParams["CACHE_GROUPS"]==="N"? false: $USER->GetGroups()), $arNavigation, $arParams['UF_VIEW'], $arParams['UF_USER']) ){
if($this->startResultCache(false, array($extFilter, ($arParams['CACHE_GROUPS']=='N' ? false: $USER->GetGroups()), $arNavigation))) {
    //if($this->startResultCache(false, array($extFilter, ($arParams['CACHE_GROUPS']=='N' ? false: $USER->GetGroups()), $arNavigation, $arParams['UF_VIEW'], $arParams['UF_USER']))) {
    $isObject = new CIBlockSection;
    //$ieObject = new CIBlockElement;

    # ищем сам раздел
    $filter = array(
        'IBLOCK_ID' => $arParams['IBLOCK_ID'],
        'IBLOCK_ACTIVE' => 'Y',
        'ACTIVE' => 'Y',
        'GLOBAL_ACTIVE' => 'Y',
    );

    $select = array_merge($arParams['SECTION_FIELDS'], $arParams['SECTION_USER_FIELDS']);
    $sectionFound = false;
    if($arParams['SECTION_ID']) {
        $filter['ID'] = $arParams['SECTION_ID'];    
        $rsResult = $isObject->GetList(false, $filter, false, $select);
        $arParams['SECTION_URL'] && $rsResult->SetUrlTemplates('', $arParams['SECTION_URL']);
        $arResult = $rsResult->GetNext(true, false);
    }
    elseif($arParams['SECTION_CODE']) {
        $filter['=CODE'] = $arParams['SECTION_CODE'];                 
        $rsResult = $isObject->GetList(false, $filter, false, $select);
        $arParams['SECTION_URL'] && $rsResult->SetUrlTemplates('', $arParams['SECTION_URL']);
        $arResult = $rsResult->GetNext(true, false);
    }
    else {
        $arResult = array(
            'ID' => 0,
            'IBLOCK_ID' => $arParams['IBLOCK_ID']
        );
    }

    //    pRU($filter, 'all');


    if( !$arResult ) {
        $this->AbortResultCache();
        $this->set404();
        return;
    }

    # seo
    if($arResult['ID']) {
        $ipropValues = new \Bitrix\Iblock\InheritedProperty\SectionValues($arParams['IBLOCK_ID'], $arResult['ID']);
        $arResult['IPROPERTY_VALUES'] = $ipropValues->getValues();
    }
    else {
        $arResult['IPROPERTY_VALUES'] = array();
    }

    # массив для кнопок эрмитажа
    if($arParams['CACHE_GROUPS']!='Y' || $USER->IsAuthorized()) {
        $arResult['BUTTONS'] = CIBlock::GetPanelButtons($arParams['IBLOCK_ID'], 0, $arResult['ID'], array(
            'SECTION_BUTTONS' => true,
            'SESSID' => false
        ));
    }

    # массив для breadcrumbs
    if ($arResult['ID'] && $arParams['ADD_SECTIONS_CHAIN']) {
        $arResult['PATH'] = array();
        $rsPath = CIBlockSection::GetNavChain($arResult['IBLOCK_ID'], $arResult['ID']);
        $rsPath->SetUrlTemplates('', $arParams['SECTION_URL']);
        while($arPath = $rsPath->GetNext()) {
            $ipropValues = new \Bitrix\Iblock\InheritedProperty\SectionValues($arParams['IBLOCK_ID'], $arPath['ID']);
            $arPath['IPROPERTY_VALUES'] = $ipropValues->getValues();
            $arResult['PATH'][] = $arPath;
        }
    }

    # ищем элементы
    $filter = array(
        //        'SECTION_ID' => $arResult['ID'] ?: false,
        'IBLOCK_ID' => $arResult['IBLOCK_ID'],
        'IBLOCK_ACTIVE' => 'Y',
        'ACTIVE' => 'Y',
        'CHECK_PERMISSIONS' => 'Y',
        'MIN_PERMISSION' => 'R',
    );
    $filter = array_merge($filter, $extFilter);

    //    pRU($filter, 'all');

    $sort = $this->getSortArray();   

    //    pRU($sort, 'all');  
    
    if( isset($_GET['PAGEN_1']) ){
        $page = intval($_GET['PAGEN_1']);
    }else{
        $page = 1;
    }
    $arNavParams['iNumPage'] = $page;  
    
    $rsSections = $isObject->GetList($sort, $filter, false, $select, $arNavParams);      
    $arSections = array();
    while($arSection = $rsSections->GetNext(true, false)) {   
        if($arParams['ELEMENT_CONTROLS'] == 'Y') {
            $arButtons = CIBlock::GetPanelButtons(
                $arSection['IBLOCK_ID'],
                0,
                $arSection['ID'],
                array('SESSID' => false, 'CATALOG' => true, 'SECTION_BUTTONS' => true)
            );
            $arSection["EDIT_LINK"] = $arButtons["edit"]["edit_section"]["ACTION_URL"];
            $arSection["DELETE_LINK"] = $arButtons["edit"]["delete_section"]["ACTION_URL"];
        }
        $arSections[] = $arSection;
    }                        
    $arResult['ITEMS'] = $arSections;

    if($arParams['DISPLAY_TOP_PAGER'] || $arParams['DISPLAY_BOTTOM_PAGER']) {
        $arResult['NAV_STRING'] = $rsSections->GetPageNavStringEx($navComponentObject, $arParams['PAGER_TITLE'], $arParams['PAGER_TEMPLATE'], $arParams['PAGER_SHOW_ALWAYS']);
        /** @var CBitrixComponent $navComponentObject */
        $arResult['NAV_CACHED_DATA'] = $navComponentObject->GetTemplateCachedData();
    }

    $this->SetResultCacheKeys(array(
        'ID',
        'NAV_CACHED_DATA',
        'NAME',
        'PATH',
        'IBLOCK_SECTION_ID',
        'IPROPERTY_VALUES',
        'BUTTONS'
    ));

    $this->includeComponentTemplate();     
}

# breadcrumbs
if ($arParams['ADD_SECTIONS_CHAIN'] && isset($arResult['PATH']) && is_array($arResult['PATH'])) {
    foreach($arResult['PATH'] as $arPath) {
        if ($arPath['IPROPERTY_VALUES']['SECTION_PAGE_TITLE'] != '') {
            $APPLICATION->AddChainItem($arPath['IPROPERTY_VALUES']['SECTION_PAGE_TITLE'], $arPath['~SECTION_PAGE_URL']);
        }
        else {
            $APPLICATION->AddChainItem($arPath['NAME'], $arPath['~SECTION_PAGE_URL']);
        }
    }
}

# кнопки в панели
if($arResult['BUTTONS']) {
    $this->AddIncludeAreaIcons(CIBlock::GetComponentMenu($APPLICATION->GetPublicShowMode(), $arResult['BUTTONS']));
}



# хз, зачем это
/*$this->SetTemplateCachedData($arResult['NAV_CACHED_DATA']);*/

return $arResult['ID'];