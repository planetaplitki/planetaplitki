<?if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();?><?

$paymentSystem = CSalePaySystem::GetByID($vval["ORDER"]["PAY_SYSTEM_ID"], $vval["ORDER"]["PERSON_TYPE_ID"]);

/*if ($USER->isAdmin()):
echo"<pre>";print_r($vval);echo"</pre>"; 
endif;*/

$paramsCustom = unserialize($paymentSystem["PSA_PARAMS"]);
//echo"<pre>";print_r($paramsCustom);echo"</pre>"; 
//echo"<pre>";print_r($arResult["ORDER_PROPS"]);echo"</pre>";

 
$Sum = CSalePaySystemAction::GetParamValue("SHOULD_PAY") ? CSalePaySystemAction::GetParamValue("SHOULD_PAY") : $vval["ORDER"]["PRICE"]; 
$ShopID = CSalePaySystemAction::GetParamValue("SHOP_ID") ? CSalePaySystemAction::GetParamValue("SHOP_ID") : $paramsCustom["SHOP_ID"]["VALUE"];
$scid = CSalePaySystemAction::GetParamValue("SCID") ? CSalePaySystemAction::GetParamValue("SCID") : $paramsCustom["SCID"]["VALUE"];
$customerNumber = CSalePaySystemAction::GetParamValue("ORDER_ID") ? CSalePaySystemAction::GetParamValue("ORDER_ID") : $vval["ORDER"]["ID"];
$orderDate = CSalePaySystemAction::GetParamValue("ORDER_DATE") ? CSalePaySystemAction::GetParamValue("ORDER_DATE") : $vval["ORDER"]["DATE_INSERT_FORMATED"];
$orderNumber = CSalePaySystemAction::GetParamValue("ORDER_ID") ? CSalePaySystemAction::GetParamValue("ORDER_ID") : $vval["ORDER"]["ID"];
$paymentType = CSalePaySystemAction::GetParamValue("PAYMENT_VALUE") ? CSalePaySystemAction::GetParamValue("PAYMENT_VALUE") : $paramsCustom["PAYMENT_VALUE"]["VALUE"];


/* echo"<pre>";print_r($Sum);echo"</pre>"; echo "<br/>"; 
echo"<pre>";print_r($ShopID);echo"</pre>"; echo "<br/>"; 
echo"<pre>";print_r($scid);echo"</pre>"; echo "<br/>"; 
echo"<pre>";print_r($customerNumber);echo"</pre>"; echo "<br/>"; 
echo"<pre>";print_r($orderDate);echo"</pre>"; echo "<br/>"; 
echo"<pre>";print_r($orderNumber);echo"</pre>"; echo "<br/>"; 
echo"<pre>";print_r($paymentType);echo"</pre>"; echo "<br/>";  */


/*if(!$USER->isAdmin()): 
$Sum = CSalePaySystemAction::GetParamValue("SHOULD_PAY"); 
$ShopID = CSalePaySystemAction::GetParamValue("SHOP_ID");
$scid = CSalePaySystemAction::GetParamValue("SCID");
$customerNumber = CSalePaySystemAction::GetParamValue("ORDER_ID");
$orderDate = CSalePaySystemAction::GetParamValue("ORDER_DATE");
$orderNumber = CSalePaySystemAction::GetParamValue("ORDER_ID");
$paymentType = CSalePaySystemAction::GetParamValue("PAYMENT_VALUE"); 
endif;   */


/*if($USER->isAdmin()):

echo  $Sum;  echo"<br/>";
echo  $ShopID;   echo"<br/>";
echo  $scid;     echo"<br/>";
echo  $customerNumber;    echo"<br/>";
echo  $orderDate;   echo"<br/>";
echo  $orderNumber;   echo"<br/>";
echo  $paymentType;  echo"<br/>";

endif;  */

$fio = "";
if( CModule::IncludeModule('sale') ){
    if( $arOrder = CSaleOrder::GetByID($orderNumber) ){
        $Sum = $arOrder['PRICE'] - (($arOrder["PAY_SYSTEM_ID"] == 3) ? 0 : $arOrder['PRICE_DELIVERY']);
    }
}

$Sum = number_format($Sum, 2, ',', '');

?>
<?foreach($arResult["ORDER_PROPS"] as $prop):?>
<?php if ($prop['ORDER_PROPS_ID'] == 1) {
    $fio = $prop['VALUE'];
}
?>    
<?endforeach?>

<?
if($fio == "") {
    $fio = $vval["ORDER"]["USER_NAME"];
}
?>
<font class="tablebodytext">

<?if($arOrder['PRICE_DELIVERY'] > 0):?>

<hr class="pay-line"/>
<p>Сумма заказа: <?=$Sum - $arOrder['PRICE_DELIVERY']?> руб.</p>
<p>Стоимость доставки: <?=$arOrder['PRICE_DELIVERY']?> руб.</p>

<?endif;?>



<span class="visual-price">Сумма к оплате по счету (<?=$arOrder["PAY_SYSTEM_ID"] == 3 && $arOrder['PRICE_DELIVERY'] > 0 ? "с учетом доставки" : "без учета доставки" ?>) : <b><?=$Sum?> р.</b></span><br />
<br />
</font>
<?if(strlen(CSalePaySystemAction::GetParamValue("IS_TEST")) > 0 || strlen($paramsCustom["IS_TEST"]["VALUE"]) > 0 ):
    ?>
    <form name="ShopForm" class="shop-form" action="https://demomoney.yandex.ru/eshop.xml" method="post" target="_blank">
<?else:
    ?>
    <form name="ShopForm" class="shop-form" action="https://money.yandex.ru/eshop.xml" method="post">
<?endif;?>
<font class="tablebodytext">
<input name="ShopID" value="<?=$ShopID?>" type="hidden">
<input name="scid" value="<?=$scid?>" type="hidden">
<input name="customerNumber" value="<?=$customerNumber?>" type="hidden">
<input name="orderNumber" value="<?=$orderNumber?>:<?=$fio?>" type="hidden">
<input name="Sum" value="<?=$Sum?>" type="hidden">
<input name="paymentType" value="<?=$paymentType?>" type="hidden">
<input name="cms_name" value="1C-Bitrix" type="hidden">
<input name="fio" value="<?=$fio?>" type="hidden">

<!-- <br /> -->
<!-- Детали заказа:<br /> -->
<!-- <input name="OrderDetails" value="заказ №<?=$orderNumber?> (<?=$orderDate?>)" type="hidden"> -->
<input name="BuyButton" class="b-button" value="Оплатить" type="submit">

</font><!-- <p><font class="tablebodytext"><b>Обратите внимание:</b> если вы откажетесь от покупки, для возврата денег вам придется обратиться в магазин.</font></p> -->
</form>