<?                        
@require_once 'include/autoload.php';
define("RE_SITE_KEY","6Lc0wycTAAAAAE0SfCy8qz_hpWZ1bCBFBJyFEOSZ");
define("RE_SEC_KEY","6Lc0wycTAAAAAFpXqqkqpJOeMTzL6J8ly3VxcIRX"); 
define("LOG_FILENAME",$_SERVER['DOCUMENT_ROOT']."/logg.txt"); 


include 'init/constant.php';
include 'init/classes.php';
include 'init/functions.php';
include 'init/events.php';

// перехват события отправки письма о новом заказе
//AddEventHandler("sale", "OnOrderNewSendEmail", "MyOnOrderNewSendEmailHandler");
//function MyOnOrderNewSendEmailHandler($arFields){
//    global $USER;
//    if(in_array(9, $USER->GetUserGroupArray())){ // если пользователь в групе Опт, сейчас = 9
//        $arFields['SALE_EMAIL'] = 'ivkazachenko@gmail.com';
//    }
//}
// перехват события отправка письма с сайта
AddEventHandler('main', 'OnBeforeEventSend', Array("MyForm", "myOnBeforeEventSend"));

class MyForm
{
    function myOnBeforeEventSend($arFields, $arTemplate)
    {
        global $APPLICATION, $USER;
        CModule::IncludeModule("sale");

        $USER = new CUser();
        if (!empty($arFields['RS_USER_ID'])) {
            $uid = $arFields['RS_USER_ID'];
        }
        if (!empty($arFields['ICE_USER_ID'])) {
            $uid = $arFields['ICE_USER_ID'];
        }
        if (!empty($arFields['ORDER_ID'])) {
            $order = CSaleOrder::GetByID($arFields['ORDER_ID']);
            $uid = $order['USER_ID'];
        }

        //Bitrix\Main\Diag\Debug::writeToFile(CUser::GetUserGroup($uid), '', 'opt-log.txt');
        //Bitrix\Main\Diag\Debug::writeToFile($arFields, '', 'opt-log.txt');
        //Bitrix\Main\Diag\Debug::writeToFile($arTemplate, '', 'opt-log.txt');

        $tids_opt = array(105, 106, 107, 108, 109, 110, 111, 112, 113, 114, 115, 116, 117, 118, 119, 120, 121, 122); // шаблоны которы должны заменить стандартные для оптовых пользователей.
        $tids_replace = array(33, 34, 35, 36, 37, 38, 39, 40, 41, 69, 70, 71, 72, 73, 76, 77, 100, 104); // те шаблоны которые нужно заменять для оптовых пользователей

        if (in_array($arTemplate['ID'], $tids_opt)) {
            if (in_array(9, CUser::GetUserGroup($uid))) {
                //Bitrix\Main\Diag\Debug::writeToFile('RETURN TRUE user opt, shablon opt', '', 'opt-log.txt');
                $arTemplate['BCC'] = implode(", ", 'elya_18_87@mail.ru');
            } else {
                //Bitrix\Main\Diag\Debug::writeToFile('RETURN FALSE user ne opt, shablon opt', '', 'opt-log.txt');
                return false;
            }
        } else {
            if (in_array(9, CUser::GetUserGroup($uid))) {
                if (in_array($arTemplate['ID'], $tids_replace)) {
                    //Bitrix\Main\Diag\Debug::writeToFile('RETURN FALSE user - opt, shablon - ne opt', '', 'opt-log.txt');
                    return false;
                } else {
                    $arTemplate['BCC'] = implode(", ", 'elya_18_87@mail.ru');
                    //Bitrix\Main\Diag\Debug::writeToFile('RETURN TRUE user - opt, shablon - ne zameschat', '', 'opt-log.txt');
                }
            } else {
                //Bitrix\Main\Diag\Debug::writeToFile('RETURN TRUE user - ne opt, shablon ne opt', '', 'opt-log.txt');
            }
        }
    }
}

function clean_expire_cache()
{
    BXClearCache(true);
    return "clean_expire_cache();";
}

if (!empty($_REQUEST['hooorai']))die;

/*$f = @fopen($_SERVER["DOCUMENT_ROOT"].'/upload/access.txt', 'ab');
@fwrite($f, date("Y-m-d H:i:s ").$_SERVER["REQUEST_URI"].' -- '.$_SERVER["REMOTE_ADDR"]."\n");
@fclose($f);*/

AddEventHandler("iblock", "OnBeforeIBlockElementUpdate", "OnBeforeIBlockElementUpdateHandler");
function OnBeforeIBlockElementUpdateHandler(&$arFields)
{
    AddMessage2Log($arFields["ID"].' - active = '.$arFields["ACTIVE"]);
}


function onBeforeWebformAdd($WEB_FORM_ID, &$arFields, &$arrVALUES){
    /*
    * Проблема:
    * Из веб-форм на сайте "Задать вопрос по плитке" и "Задать вопрос по  коллекции" 
    * нужно что бы поле с коллекцией или плиткой, заполненное по  умолчанию записывалось в комментарий.
    * Потому что в битрикс24 не передаётся по какой именно плитки\коллекции задали вопрос.
    */
    
    global $APPLICATION;
    // Если вебформа "Вопрос по плитке"
    if ($WEB_FORM_ID == 7){
        $arrVALUES['form_textarea_37'] .= "\nВопрос по плитке ".$arrVALUES['form_text_38'];

        if (empty($arrVALUES['form_text_36']) && empty($arrVALUES['form_text_70'])){
            $APPLICATION->ThrowException('Введите e-mail или телефон.');
        }
    }
    //Если вебформа "Вопрос по коллекции"
    if ($WEB_FORM_ID == 8){
        $arrVALUES['form_textarea_43'] .= "\nВопрос по коллекции ".$arrVALUES['form_text_44'];

        if (empty($arrVALUES['form_text_41']) && empty($arrVALUES['form_text_71'])){
            $APPLICATION->ThrowException('Введите e-mail или телефон.');
        }
    }
}
AddEventHandler('form', 'onBeforeResultAdd', 'onBeforeWebformAdd');
    $whatToClean = array("(",")");
    $_REQUEST['form_text_12'] = str_replace($whatToClean, "", $_REQUEST['form_text_12']);
    $_POST['form_text_12'] = str_replace($whatToClean, "", $_POST['form_text_12']);
    
    $_REQUEST['form_text_70'] = str_replace($whatToClean, "", $_REQUEST['form_text_70']);
    $_POST['form_text_70'] = str_replace($whatToClean, "", $_POST['form_text_70']);
    
    $_REQUEST['form_text_71'] = str_replace($whatToClean, "", $_REQUEST['form_text_71']);
    $_POST['form_text_71'] = str_replace($whatToClean, "", $_POST['form_text_71']);
    
    $_REQUEST['form_text_5'] = str_replace($whatToClean, "", $_REQUEST['form_text_5']);
    $_POST['form_text_5'] = str_replace($whatToClean, "", $_POST['form_text_5']);
    
    $_REQUEST['1CLICK_PHONE'] = str_replace($whatToClean, "", $_REQUEST['1CLICK_PHONE']);
    $_POST['1CLICK_PHONE'] = str_replace($whatToClean, "", $_POST['1CLICK_PHONE']);
    
    $_REQUEST['ORDER_PROP_3'] = str_replace($whatToClean, "", $_REQUEST['ORDER_PROP_3']);
    $_POST['ORDER_PROP_3'] = str_replace($whatToClean, "", $_POST['ORDER_PROP_3']);
    
    $_REQUEST['PERSONAL_MOBILE'] = str_replace($whatToClean, "", $_REQUEST['PERSONAL_MOBILE']);
    $_POST['PERSONAL_MOBILE'] = str_replace($whatToClean, "", $_POST['PERSONAL_MOBILE']);
    
    $_REQUEST['REGISTER[PERSONAL_MOBILE]'] = str_replace($whatToClean, "", $_REQUEST['REGISTER[PERSONAL_MOBILE]']);
    $_POST['REGISTER[PERSONAL_MOBILE]'] = str_replace($whatToClean, "", $_POST['REGISTER[PERSONAL_MOBILE]']);
