<?
CModule::addAutoloadClasses(
    '',
    array(
        'COrderMakeRU'                        => '/bitrix/php_interface/init/classes/COrderMakeRU.php',
        'CViewedCollectionsRU'                => '/bitrix/php_interface/init/classes/CViewedCollectionsRU.php',
        'CUserAuthorizeRU'                    => '/bitrix/php_interface/init/classes/CUserAuthorizeRU.php',
        'CUserProfileRU'                    => '/bitrix/php_interface/init/classes/CUserProfileRU.php',
        'CImportRU'                            => '/bitrix/php_interface/init/classes/CImportRU.php',
    )
);

/**
 * Добавляет пользовательское поле "описание" для разделов
 * Class MyHtmlRedactorType
 */
class MyHtmlRedactorType extends CUserTypeString{

    function GetUserTypeDescription(){
        return array(
            "USER_TYPE_ID" => "c_string",
            "CLASS_NAME" => "MyHtmlRedactorType",
            "DESCRIPTION" => "Строка в html редакторе",
            "BASE_TYPE" => "string",
        );
    }

    function GetEditFormHTML($arUserField, $arHtmlControl){
        ob_start();
        CFileMan::AddHTMLEditorFrame($arHtmlControl["NAME"],
            $arHtmlControl["VALUE"],
            "html",
            "html",
            440,
            "N",
            0,
            "",
            "",
            CATALOG_IBLOCK_ID) ;
        $b=ob_get_clean();
        return $b;
    }
}