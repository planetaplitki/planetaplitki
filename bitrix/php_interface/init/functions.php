<?
/**
 * пережатие картинок
 * @param $id
 * @param $width
 * @param $height
 * @param array $filter
 * @return mixed
 */
function i($id,$width,$height, $mode = ""){
    $small=CFile::ResizeImageGet($id,Array("height"=>$height,"width"=>$width), $mode, false);
    return $small["src"];
}

/**
 * распечатываем массив
 * @param $arr
 * @param bool $break
 */
function pRU ( $arr, $show, $break = false ){
    global $USER;

    if( $USER->IsAdmin() || $show == 'all' ){
        echo "<pre>";
        print_r($arr);
        echo "</pre>";

        $break ? die() : '';
    }
}

/**
 * проверяем на непустой массив
 * @param $arr
 * @return bool
 */
function eRU($arr){
    if( is_array($arr) && !empty($arr) ){
        return true;
    }
    return;
}

function plural($n, $one, $two, $many) {
    return $n%10==1&&$n%100!=11?$one:($n%10>=2&&$n%10<=4&&($n%100<10||$n%100>=20)?$two:$many);
}

/**
 * возвращает список ссылок для личного кабинета
 * @return string
 */
function personalCabinetLink (){

    global $USER;
    global $APPLICATION;

    $str = '';
    $str = '
        <div class="lc_left">
            <div class="lc_left_box">
                <a class="lc_left_box_p">Личная информация<span></span></a>
                <ul>
                    <li><a href="/personal/profile/">Личные данные</a></li>
                </ul>
            </div>
            <div class="lc_left_box">
                <a class="lc_left_box_p">Заказы<span></span></a>
                <ul>
                    <li><a href="/personal/order/?show_all=Y">Состояние заказов</a></li>
                    <li><a href="/personal/cart/">Содержание корзины</a></li>
                    <li><a href="/personal/order/?filter_history=Y">История заказов</a></li>
                </ul>
            </div>
            <div class="lc_left_box">
                <a class="lc_left_box_p">Подписка<span></span></a>
                <ul>
                    <li><a href="/personal/subscribe/">Изменить подписку</a></li>
                </ul>
            </div>
    ';

    # если пользователь авторизован
    if( $USER->IsAuthorized() ){
        $str .= '<div class=""><a href="?logout=yes">Выйти</a></div>';
    }

    $str .= '</div>';

    return $str;
}

/**
 * импорт csv
 * @param $f_handle
 * @param $length
 * @param string $delimiter
 * @param string $enclosure
 * @return array|bool
 */
function fgetcsv2($f_handle, $length, $delimiter=',', $enclosure='"'){
    if (!strtoupper(substr(PHP_OS, 0, 3)) === 'WIN')
        return fgetcsv($f_handle, $length, $delimiter, $enclosure);
    if (!$f_handle || feof($f_handle))
        return false;

    if (strlen($delimiter) > 1)
        $delimiter = substr($delimiter, 0, 1);
    elseif (!strlen($delimiter))          // There _MUST_ be a delimiter
        return false;

    if (strlen($enclosure) > 1)         // There _MAY_ be an enclosure
        $enclosure = substr($enclosure, 0, 1);

    $line = fgets($f_handle, $length);
    if (!$line)
        return false;
    $result = array();
    $csv_fields = explode($delimiter, trim($line));
    $csv_field_count = count($csv_fields);
    $encl_len = strlen($enclosure);
    for ($i=0; $i<$csv_field_count; $i++)
    {
        // Removing possible enclosures
        if ($encl_len && $csv_fields[$i]{0} == $enclosure)
            $csv_fields[$i] = substr($csv_fields[$i], 1);
        if ($encl_len && $csv_fields[$i]{strlen($csv_fields[$i])-1} == $enclosure)
            $csv_fields[$i] = substr($csv_fields[$i], 0, strlen($csv_fields[$i])-1);
        // Double enclosures are just original symbols
        $csv_fields[$i] = str_replace($enclosure.$enclosure, $enclosure, $csv_fields[$i]);
        $result[] = $csv_fields[$i];
    }
    return $result;
}

/*
 * генерируем произвольный пароль
 * */
function generatePassword(){

    // Символы, которые будут использоваться в пароле.
    $chars="qazxswedcvfrtgbnhyujmkiolp1234567890QAZXSWEDCVFRTGBNHYUJMKIOLP";

    // Количество символов в пароле.
    $max=10;

    // Определяем количество символов в $chars
    $size=StrLen($chars)-1;

    // Определяем пустую переменную, в которую и будем записывать символы.
    $password=null;

    // Создаём пароль.
    while($max--){
        $password.=$chars[rand(0,$size)];
    }

    return $password;
}

/**
 * вывод в файл результатов работы чего-либо
 * @param $var
 */
function save2FileVariable($var){
    ob_start();
    echo '<pre>';
    if(eRU($var)){
        print_r($var);
    }
    else{
        echo $var;
    }
    echo '</pre>';
    $out = ob_get_contents();
    ob_end_clean();
    file_put_contents($_SERVER['DOCUMENT_ROOT'].'/variables.txt', $out);
}

/**
 * возвращает маркеры текущего товара
 * @param $item
 * @return array
 */
function getMarker($item){

    $arr = array();

    if( $item['PROPERTIES']['NEW']['VALUE'] ){
        $arr['TYPE'] = 'new';
        $arr['HTML']['LIST'] = '<div class="new-icon"></div>'.$arr['TYPE'].'</div>';
        $arr['HTML']['DETAIL'] = '<div class="new-icon"></div>'.$arr['TYPE'].'</div>';
    }
    elseif( $item['PROPERTIES']['SALE']['VALUE'] ){
        $arr['TYPE'] = 'sale';
        $arr['HTML']['LIST'] = '<div class="sales"></div>'.$arr['TYPE'].'</div>';
        $arr['HTML']['DETAIL'] = '<div class="sales"></div>'.$arr['TYPE'].'</div>';
    }
    elseif( $item['PROPERTIES']['HIT']['VALUE'] ){
        $arr['TYPE'] = 'hit';
        $arr['HTML']['LIST'] = '<div class="hit-icon"></div>'.$arr['TYPE'].'</div>';
        $arr['HTML']['DETAIL'] = '<div class="hit-icon"></div>'.$arr['TYPE'].'</div>';
    }

    return $arr;
}

/**
 * возвращает маркеры текущей коллекции
 * @param $item
 * @return array
 */
function getMarkerSection($section){

    $arr = array();

    if( $section['UF_NEW_COLLECTION'] ){
        $arr['TYPE'] = 'new';
        $arr['HTML']['LIST'] = '<div class="new-icon"></div>';
        $arr['HTML']['DETAIL'] = '<div class="new-icon"></div>';
    }
    elseif( $section['UF_SALE'] ){
        $arr['TYPE'] = 'sale';
        $arr['HTML']['LIST'] = '<div class="sales"></div>';
        $arr['HTML']['DETAIL'] = '<div class="sales"></div>';
    }
    elseif( $section['UF_HIT'] ){
        $arr['TYPE'] = 'hit';
        $arr['HTML']['LIST'] = '<div class="hit-icon"></div>';
        $arr['HTML']['DETAIL'] = '<div class="hit-icon"></div>';
    }
    elseif( $section['UF_EXCLUSIVE'] ){
        $arr['TYPE'] = 'exclusive';
        $arr['HTML']['LIST'] = '<div class="exclusive-icon"></div>';
        $arr['HTML']['DETAIL'] = '<div class="exclusive-icon"></div>';
    }

    return $arr;
}

/**
 * Выставляет класс странице
 * @return string
 */
function setPageClass(){
    global $pageClass;
    global $APPLICATION;

    $pageClass = $APPLICATION->GetPageProperty("pageclass");

    if( $pageClass ){
        $class = $pageClass;
    }
    else{
        $class = "main";
    }

    return $class;
}

/**
 * возвращает кол-во подписчиков в группе vk.com
 * @return mixed
 */
function getVKMembers (){

    $url = "http://api.vk.com/method/groups.getById?gid=71410646&fields=members_count";

    $IsCurl=curl_init();
    curl_setopt($IsCurl, CURLOPT_FAILONERROR, 1);
    curl_setopt($IsCurl, CURLOPT_FOLLOWLOCATION, 1);
    curl_setopt($IsCurl, CURLOPT_RETURNTRANSFER,1);
    curl_setopt($IsCurl, CURLOPT_COOKIEJAR, $_SERVER['DOCUMENT_ROOT'].'/1f928592a33b2316cc33c78340157ba3');
    curl_setopt($IsCurl, CURLOPT_COOKIEFILE, $_SERVER['DOCUMENT_ROOT'].'/1f928592a33b2316cc33c78340157ba3');
    curl_setopt($IsCurl, CURLOPT_TIMEOUT, 5);
    curl_setopt($IsCurl, CURLOPT_URL, $url);
    $ResUrl=curl_exec($IsCurl);
    curl_close($IsCurl);

    $result = json_decode($ResUrl, true);

    $membersCount = $result['response'][0]['members_count'];
    return $membersCount;
}

/**
 * возвращает колв-во подписчиков в группе fb
 */
function getFBMembers (){
    $pageId = "295170290589151";
    $xml = @simplexml_load_file("http://api.facebook.com/restserver.php?method=facebook.fql.query&query=SELECT%20fan_count%20FROM%20page%20WHERE%20page_id=".$pageId."") or die ("много");
    $likes = $xml->page->fan_count;
    echo($likes[0]);
}

/**
 * Вовзращает структуру с разбивкой разделов на страны, фабрики, коллекции
 * @return array
 */
function getSections(){

    $sections = array();

    # кешируем результат
    $phpCache = new CPHPCache;
    $cacheTime = 3600*30*24; # время кеширования - 1 месяц
    $cacheID = 'catalog_sections_arr'; # формируем уникальный id кеша
    $cachePath = '/cached_sections'; # папка с кешем

    if($phpCache->InitCache($cacheTime, $cacheID, $cachePath)) {
        $sections = $phpCache->GetVars();
    }
    else {
        # помечаем кеш тегом, связанным с инфоблоком
//        $tagCache = $GLOBALS['CACHE_MANAGER'];
//        $tagCache->StartTagCache($cachePath);
//        $tagCache->RegisterTag('iblock_id_'.IBLOCK_ID_CATALOG);
//        $tagCache->EndTagCache();

        if( CModule::IncludeModule('iblock') ){

            $arFilter = Array('IBLOCK_ID'=>IBLOCK_ID_CATALOG, 'GLOBAL_ACTIVE'=>'Y',);
            $db_list = CIBlockSection::GetList(Array('name'=>'asc'), $arFilter, false, array('ID', 'NAME', 'DEPTH_LEVEL', 'IBLOCK_SECTION_ID', 'SECTION_PAGE_URL', 'UF_*', 'LEFT_MARGIN', 'RIGHT_MARGIN'));
            while($ar_result = $db_list->GetNext()){

                $depth = $ar_result['DEPTH_LEVEL'];

                if( $depth == 1 ){
                    $sections['COUNTRIES'][$ar_result['ID']] = $ar_result;
                }
                elseif( $depth == 2 ){
                    $sections['BRANDS'][$ar_result['ID']] = $ar_result;
                }
                elseif( $depth == 3 ){
                    $sections['COLLECTIONS'][$ar_result['ID']] =  $ar_result;
                }
                else{
                    $sections['OTHER'][$ar_result['ID']] = $ar_result;
                }

                $sections[$ar_result['ID']] = $ar_result;
            }
        }

        $phpCache->StartDataCache();
        $phpCache->EndDataCache($sections);
    }

    return $sections;
}

/**
 * Возвращает разделы с учетом группы текущего пользователя: опт, розница
 * @return mixed
 */
function getSectionsByUserType(){

    # кешируем результат
    $phpCache = new CPHPCache;
    $cacheTime = 3600*30*24; # время кеширования - 1 месяц
    $cacheID = 'catalog_sections_'.USER_TYPE; # формируем уникальный id кеша
    $cachePath = '/cached_sections'; # папка с кешем

    if($phpCache->InitCache($cacheTime, $cacheID, $cachePath)) {
        $sections = $phpCache->GetVars();
    }
    else {
        # помечаем кеш тегом, связанным с инфоблоком
//        $tagCache = $GLOBALS['CACHE_MANAGER'];
//        $tagCache->StartTagCache($cachePath);
//        $tagCache->RegisterTag('iblock_id_'.IBLOCK_ID_CATALOG);
//        $tagCache->EndTagCache();

        if( CModule::IncludeModule('iblock') ){

            $sections_ = getSections();
            if( USER_TYPE == 'retail' ){
                $sectionUserGroup = array(6, 7);
            }
            elseif( USER_TYPE == 'wholesale' ){
                $sectionUserGroup = array(7);
            }
                     
            foreach( $sections_['COLLECTIONS'] as $item ){     
                if( in_array($item['UF_USER_GROUP'], $sectionUserGroup) ){    
                    $sections['COLLECTIONS'][$item['ID']] = $item;
                    $idBrand = $item['IBLOCK_SECTION_ID'];
                    $idBrandItem = $sections_['BRANDS'][$idBrand];
                    $sections['BRANDS'][$idBrand] = $idBrandItem;
                    $idCountry = $idBrandItem['IBLOCK_SECTION_ID'];
                    $idCountryItem = $sections_['COUNTRIES'][$idCountry];
                    $sections['COUNTRIES'][$idCountry] = $idCountryItem;
                }
            }
        }

        $phpCache->StartDataCache();
        $phpCache->EndDataCache($sections);
    }

    return $sections;
}

/**
 * Возвращает коллекции для опта
 * @return array
 */
function getWholesaleSectionsID (){
    $sections = array();

    # кешируем результат
    $phpCache = new CPHPCache;
    $cacheTime = 3600*30*24; # время кеширования - 1 месяц
    $cacheID = 'wholesale_sections_id'; # формируем уникальный id кеша
    $cachePath = '/cached_sections'; # папка с кешем

    if($phpCache->InitCache($cacheTime, $cacheID, $cachePath)) {
        $sections = $phpCache->GetVars();
    }
    else {
        # помечаем кеш тегом, связанным с инфоблоком
//        $tagCache = $GLOBALS['CACHE_MANAGER'];
//        $tagCache->StartTagCache($cachePath);
//        $tagCache->RegisterTag('iblock_id_'.IBLOCK_ID_CATALOG);
//        $tagCache->EndTagCache();

        $sections_ = getSections();

        foreach( $sections_['COLLECTIONS'] as $item ){
            if( $item['UF_USER_GROUP'] == 7 ){
                $sections['COLLECTIONS'][] = $item['ID'];
                $idBrand = $item['IBLOCK_SECTION_ID'];
                $idBrandItem = $sections_['BRANDS'][$idBrand];
                $sections['BRANDS'][$idBrand] = $idBrandItem['ID'];
                $idCountry = $idBrandItem['IBLOCK_SECTION_ID'];
                $idCountryItem = $sections_['COUNTRIES'][$idCountry];
                $sections['COUNTRIES'][$idCountry] = $idCountryItem['ID'];
            }
        }

        $phpCache->StartDataCache();
        $phpCache->EndDataCache($sections);
    }

    return $sections;
}

/**
 * Возвращает описание разделов в карточке товара
 * @param $idSection
 * @return string
 */
function getElementSectionsHTML($idSection, $type = 'section'){

    $sections = getSections();
    $collection = $sections['COLLECTIONS'][$idSection];
    $brand = $sections['BRANDS'][$collection['IBLOCK_SECTION_ID']];
    $country = $sections['COUNTRIES'][$brand['IBLOCK_SECTION_ID']];
    $imgFlag = i($country['UF_FLAG'], 16, 11, BX_RESIZE_IMAGE_EXACT);

    $html['DESKTOP'] = '
        <p class="easy-text">Фабрика:</p>
        <a href="'.$brand['SECTION_PAGE_URL'].'">'.$brand['NAME'].'</a>
        <div class="sepa"></div>
        <p class="easy-text">Страна:</p>
        <a href="'.$country['SECTION_PAGE_URL'].'">'.$country['NAME'].'
            <span class="flag"><img src="'.$imgFlag.'" alt="" /></span>
        </a>
    ';
    $html['MOBILE'] = '
        <div class="item1">
            <p class="easy-text">Фабрика:<a href="'.$brand['SECTION_PAGE_URL'].'">'.$brand['NAME'].'</a></p>
            <div class="sepa"></div>
        </div>
        <div class="item1">
            <p class="easy-text">Страна:<a href="'.$country['SECTION_PAGE_URL'].'">'.$country['NAME'].'<span class="flag"><img src="'.$imgFlag.'" alt="" /></span></a></p>
            <div class="sepa"></div>
        </div>
    ';

    if( $type == 'element' ){
        $html['DESKTOP'] .= '
            <div class="sepa"></div>
            <p class="easy-text">Коллекция:</p>
            <a href="'.$collection['SECTION_PAGE_URL'].'">'.$collection['NAME'].'</a>
        ';
        $html['MOBILE'] .= '
            <div class="item1">
                <p class="easy-text">Коллекция:<a href="'.$collection['SECTION_PAGE_URL'].'">'.$collection['NAME'].'</a></p>
            </div>
        ';
    }

    return $html;
}

/**
 * Вовзращает ID товаров, разбитые по типу поверхности в коллекции
 * @param $idSection
 * @param string $idElement
 * @return array
 */
function getPoverhnostItems($idSection, $idElement = ""){

    $idItems = array();

    # кешируем результат
    $phpCache = new CPHPCache;
    $cacheTime = 3600*30*24; # время кеширования - 1 месяц
    $cacheID = 'catalog_section_'.$idSection.'_items_poverhost'; # формируем уникальный id кеша
    $cachePath = '/cached_sections'; # папка с кешем

    if($phpCache->InitCache($cacheTime, $cacheID, $cachePath)) {
        $idItems = $phpCache->GetVars();
    }
    else {
        # помечаем кеш тегом, связанным с инфоблоком
//        $tagCache = $GLOBALS['CACHE_MANAGER'];
//        $tagCache->StartTagCache($cachePath);
//        $tagCache->RegisterTag('iblock_id_'.IBLOCK_ID_CATALOG);
//        $tagCache->EndTagCache();

        if( CModule::IncludeModule('iblock') ){

            $arSelect = Array("ID", "PROPERTY_POVERKHNOST", "PROPERTY_ZAGOLOVOK_NOVOY_STROKI");
            $arFilter = Array("IBLOCK_ID"=>IBLOCK_ID_CATALOG, "ACTIVE"=>"Y", "SECTION_ID" => $idSection/*, "!ID" => $idElement*/);
            $res = CIBlockElement::GetList(Array(), $arFilter, false, false, $arSelect);
            while( $ob = $res->GetNextElement() ){
                $arFields = $ob->GetFields();
//                # todo не удалять - старый алгоритм разбивки по поверхности
//                $idItems[$arFields['PROPERTY_POVERKHNOST_VALUE']][] = $arFields['ID'];
                $idItems[$arFields['PROPERTY_ZAGOLOVOK_NOVOY_STROKI_VALUE']][] = $arFields['ID'];
            }
        }

        $phpCache->StartDataCache();
        $phpCache->EndDataCache($idItems);
    }

    return $idItems;
}

/**
 * Возвращает связь между разелом и глубиной вложенности
 * 1-й уровень - страны
 * 2-й уровень - бренды
 * 3-й уровень - коллекции
 * @return array
 */
function getDepthIdSection(){

    $arr = array();

    # кешируем результат
    $phpCache = new CPHPCache;
    $cacheTime = 3600*30*24; # время кеширования - 1 месяц
    $cacheID = 'catalog_section_depth'; # формируем уникальный id кеша
    $cachePath = '/cached_sections'; # папка с кешем

    if($phpCache->InitCache($cacheTime, $cacheID, $cachePath)) {
        $arr = $phpCache->GetVars();
    }
    else {
        # помечаем кеш тегом, связанным с инфоблоком
//        $tagCache = $GLOBALS['CACHE_MANAGER'];
//        $tagCache->StartTagCache($cachePath);
//        $tagCache->RegisterTag('iblock_id_'.IBLOCK_ID_CATALOG);
//        $tagCache->EndTagCache();

        if( CModule::IncludeModule('iblock') ){
            $arFilter = Array('IBLOCK_ID'=>IBLOCK_ID_CATALOG, 'GLOBAL_ACTIVE'=>'Y',);
            $db_list = CIBlockSection::GetList(Array($by=>$order), $arFilter, false, array('ID','DEPTH_LEVEL'));
            while($ar_result = $db_list->GetNext()){
                $arr[$ar_result['ID']] = $ar_result['DEPTH_LEVEL'];
            }
        }

        $phpCache->StartDataCache();
        $phpCache->EndDataCache($arr);
    }

    return $arr;
}

/**
 * Возвращает кол-во элементов и разделов (наименований и коллекций)
 * @return array
 */
function getItemsCollectionsCount(){

    $arr = array();

    # кешируем результат
    $phpCache = new CPHPCache;
    $cacheTime = 3600*24; # время кеширования - 1 месяц
    $cacheID = 'catalog_items_collections_count'; # формируем уникальный id кеша
    $cachePath = '/cached_sections'; # папка с кешем

    if($phpCache->InitCache($cacheTime, $cacheID, $cachePath)) {
        $arr = $phpCache->GetVars();
    }
    else {
        # помечаем кеш тегом, связанным с инфоблоком
/*        $tagCache = $GLOBALS['CACHE_MANAGER'];
        $tagCache->StartTagCache($cachePath);
        $tagCache->RegisterTag('iblock_id_'.IBLOCK_ID_CATALOG);
        $tagCache->EndTagCache();   */

        if( CModule::IncludeModule('iblock') ){

            $res = CIBlockElement::GetList(Array(), array("IBLOCK_ID" => IBLOCK_ID_CATALOG), false, false, array('ID'));
            $elements = $res->SelectedRowsCount();

            $db_list = CIBlockSection::GetList(Array($by=>$order), array('IBLOCK_ID' => IBLOCK_ID_CATALOG, 'DEPTH_LEVEL' => 3), false, array('ID'));
            $sections = $db_list->SelectedRowsCount();

            $arr['ELEMENTS'] = $elements;
            $arr['ELEMENTS_FORMAT'] = plural($elements, 'наименование', 'наименования', 'наименований');
            $arr['SECTIONS'] = $sections;
            $arr['SECTIONS_FORMAT'] = plural($sections, 'коллекция', 'коллекции', 'коллекций');
        }

        $phpCache->StartDataCache();
        $phpCache->EndDataCache($arr);
    }

    return $arr;
}

/**
 * Возвращает ID товаров, актуальных для текущего типа пользователя
 * @return array
 */
function getUserTypeItemsId(){

    $idItems = array();

    # кешируем результат
    $phpCache = new CPHPCache;
    $cacheTime = 3600*30*24; # время кеширования - 1 месяц
    $cacheID = USER_TYPE.'_items_id'; # формируем уникальный id кеша
    $cachePath = '/cached_items'; # папка с кешем

    if($phpCache->InitCache($cacheTime, $cacheID, $cachePath)) {
        $idItems = $phpCache->GetVars();
    }
    else {
        # помечаем кеш тегом, связанным с инфоблоком
//        $tagCache = $GLOBALS['CACHE_MANAGER'];
//        $tagCache->StartTagCache($cachePath);
//        $tagCache->RegisterTag('iblock_id_'.IBLOCK_ID_CATALOG);
//        $tagCache->EndTagCache();

        $sections = getSections();
        $collections = $sections['COLLECTIONS'];
        foreach($collections as $collection){
            if( ($collection['UF_USER_GROUP'] == 6 || $collection['UF_USER_GROUP'] == 7) && USER_TYPE == 'retail' ){
                $idSections[] = $collection['ID'];
            }
            elseif( $collection['UF_USER_GROUP'] == 7 && USER_TYPE == 'wholesale' ){
                $idSections[] = $collection['ID'];
            }
        }

        if( CModule::IncludeModule('iblock') ){
            $res = CIBlockElement::GetList(Array(), array("IBLOCK_ID" => IBLOCK_ID_CATALOG, "SECTION_ID" => $idSections), false, false, array('ID'));
            while( $ob = $res->GetNextElement() ){
                $arFields = $ob->GetFields();
                $idItems[] = $arFields['ID'];
            }
        }

        $phpCache->StartDataCache();
        $phpCache->EndDataCache($idItems);
    }

    return $idItems;
}

/**
 * Определяет константы сайта для текущего пользователя
 */
function currentUserSiteParams(){
    global $USER;
    global $APPLICATION;
    if( CSite::InGroup(array(9)) ){
        define('USER_TYPE', 'wholesale');
    }
    else{
        define('USER_TYPE', 'retail');
    }
}

/**
 * Возврщает массив xml_id похожих коллекций
 * @param $strCollections
 * @return array
 */
function getSimilarCollections ($strCollections){
    $arr =  array_diff(explode(';', $strCollections), array(''));
    return $arr;
}

/**
 * присваиваем логину значение e-mail
 */
function OnBeforeUserUpdateHandler(&$arFields){
    global $USER;
    $arFields["LOGIN"] = $arFields["EMAIL"];
    return $arFields;
}

# проверка работы отправки писем из bitrix
//function custom_mail($to,$subject,$body,$headers) {
//    $f=fopen($_SERVER["DOCUMENT_ROOT"]."/maillog.txt", "a+");
//    fwrite($f, print_r(array('TO' => $to, 'SUBJECT' => $subject, 'BODY' => $body, 'HEADERS' => $headers),1)."\n========\n");
//    fclose($f);
//
//    #return mail($to,$subject,$body,$headers);
//    $to .= ',ulyanov@100up.ru';
//    return mail($to,$subject,$body,$headers);
//}

function BeforeIndexHandler($arFields)
{              
    //для каталога заменим название в поиске
    if($arFields["PARAM1"] == '1c_catalog' && $arFields["PARAM2"] == '34')
    {
        $el = CIBlockElement::GetList(array(), array("ID" => $arFields["ITEM_ID"]), false, false, array("PROPERTY_NAZVANIE_DLYA_SAYTA", "PROPERTY_VYSOTA", "PROPERTY_SHIRINA", "PROPERTY_PROIZVODITEL"))->GetNext();  
        $arFields["TITLE"] = $el["PROPERTY_PROIZVODITEL_VALUE"].' '.$el["PROPERTY_NAZVANIE_DLYA_SAYTA_VALUE"].' '.$el["PROPERTY_VYSOTA_VALUE"].'x'.$el["PROPERTY_SHIRINA_VALUE"];
        return $arFields;
    }                                                                                                                                                                                  
}