<?
/**
 * Определяет параметры каталога для текущего пользователя
 */
AddEventHandler("main", "OnBeforeProlog", "currentUserSiteParams", 50);

/**
 * Подписывает пользователя на рассылку
 */
AddEventHandler("sale", "OnSaleComponentOrderOneStepComplete", array('COrderMakeRU', 'addSubscribe'), 100);
/**
 * Сохраняет пользователю личные данные
 */
AddEventHandler("sale", "OnSaleComponentOrderOneStepComplete", array('COrderMakeRU', 'addUserData'), 101);

/**
 * присваиваем логину значение e-mail
 */
AddEventHandler("main", "OnBeforeUserRegister", "OnBeforeUserUpdateHandler");
AddEventHandler("main", "OnBeforeUserUpdate", "OnBeforeUserUpdateHandler");

/**
 * После авторизации пользователя перенаправляем его в заказы
 */
AddEventHandler("main", "OnAfterUserAuthorize", Array("CUserAuthorizeRU", "OnAfterUserAuthorizeHandler"));

/**
 * определяем тип профилей текущего пользователя и возвращаем в компонент для корректного подключения профилей (и их св-в) и служб доставок и спсобов оплаты
 */
AddEventHandler("sale", "OnSaleComponentOrderOneStepPersonType", array('COrderMakeRU', 'setRightProfileID4CurrentUser'));

AddEventHandler("sale", "OnBeforeOrderAdd", array('COrderMakeRU', 'updateJurOrder'));

/**
 * добавляем новое пользовательское св-во для создания у разделов
 */
AddEventHandler("main", "OnUserTypeBuildList", array("MyHtmlRedactorType", "GetUserTypeDescription" ) );

/**
 * После выгрузки товаров из 1С запускаем обновление разделов
 */
//AddEventHandler("catalog", "OnSuccessCatalogImport1C", Array("CImportRU", "updateSectionCode"));

/**
 * После выгрузки товаров из 1С запускаем обновление параметров разделов
 */
//AddEventHandler("catalog", "OnSuccessCatalogImport1C", Array("CImportRU", "updateCollectionsParams"));
AddEventHandler("sale", "OnOrderSave", Array("COrderMakeRU", "OnOrderAddHandler"));

AddEventHandler("search", "BeforeIndex", "BeforeIndexHandler");
