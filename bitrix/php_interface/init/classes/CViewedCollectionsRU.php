<?

/**
 * Просмотренные коллекции
 * Class CViewedCollectionsRU
 */
class CViewedCollectionsRU{
    public $app;
    public function __construct(){
        global $APPLICATION;
        $this->app = $APPLICATION;
    }

    /**
     * Получает просмотренные коллекции
     * @return mixed
     */
    public function getViewedCollections(){
        $viewedCollections = $this->app->get_cookie("VIEWED_COLLECTIONS");
        $viewedCollections = json_decode($viewedCollections, true);
        return $viewedCollections;
    }

    /**
     * Устанавливает просмотренные коллекции
     * @param $idSection
     */
    public function setViewedCollections ($idSection){

        $viewedCollections = $this->getViewedCollections();
        if( !eRU($viewedCollections) ){
            $viewedCollections[] = $idSection;
        }
        elseif( !in_array($idSection, $viewedCollections) ){
            $viewedCollections[] = $idSection;
        }
        $this->app->set_cookie("VIEWED_COLLECTIONS", json_encode($viewedCollections), time()+60*60*24*30*1);
    }

    /**
     * @param $count
     * @return string
     */
    protected function formatCount($count){
        $str = "";
        if( $count ){
            $str = $count.' '.plural($count, 'коллекция', 'коллекции', 'коллекций');
            $link = '<a href="/catalog/last-view/">'.$str.'</a>';
        }

        return $link;
    }

    /**
     * @return int|string
     */
    public function getCount(){
        $viewedCollections = $this->getViewedCollections();
        $count = count($viewedCollections);
        $count = $this->formatCount($count);
        return $count;
    }
}
?>