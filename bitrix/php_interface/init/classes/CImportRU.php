<?
class CImportRU {

    public function __construct(){

    }

    /**
     * Обновляет символьный код раздела (удаляет лишние пробельные символы с концов)
     */
    public function updateSectionCode(){

        if( CModule::IncludeModule('iblock') ){

            $bs = new CIBlockSection;

            $arFilter = Array('IBLOCK_ID'=>IBLOCK_ID_CATALOG);
            $db_list = CIBlockSection::GetList(Array($by=>$order), $arFilter, true, array('ID', 'NAME', 'CODE'));
            while($ar_result = $db_list->GetNext()){

                $name = trim($ar_result['NAME']);
                $arParams = array("replace_space"=>"-","replace_other"=>"-");
                $code = Cutil::translit($name, "ru", $arParams);

                $arFields = Array(
                    "NAME" => $name,
                    "CODE" => $code
                );

                if($ar_result["CODE"] != $code)
                {
                    $bs->Update($ar_result['ID'], $arFields);    
                }
            }
        }
    }

    /**
     * Обновление параметров коллекций
     */
    public function updateCollectionsParams(){
        $_SERVER["DOCUMENT_ROOT"] = '/var/www/www-root/data/www/new.planetaplitki.ru';
        include $_SERVER["DOCUMENT_ROOT"].'/import/import.sections.php';
    }
}
?>