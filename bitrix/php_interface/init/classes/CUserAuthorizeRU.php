<?

/**
 * Работа с авторизацией пользователей
 * Class CUserAuthorizeRU
 */
class CUserAuthorizeRU
{
    /**
     * Отправляет пользователя в список заказов
     * @param $arUser
     */
    function OnAfterUserAuthorizeHandler( $arUser ){
        global $USER;
        if( !($USER->IsAdmin()) && CURPAGE != '/personal/order/make/' ){
            LocalRedirect('/personal/order/?show_all=Y');
        }
    }
}