<?

/**
 * работа с профилями покупателя
 * Class CUserProfileRU
 */
class CUserProfileRU {

    public $idUser;
    public $idUserType;
    public $typeRegistr;

    public function __construct(){
        global $USER;
        if( $USER->IsAuthorized() ){
            $this->idUser = $USER->GetID();
            $this->idUserType = $this->getUserProfileTypeId();
        }
    }

    /**
     * возвращает тип учетной записи текущего пользователя
     * @return mixed
     */
    public function getUserProfileTypeId(){

        $idType = 1;

        if( CModule::IncludeModule('sale') ){

            $db_sales = CSaleOrderUserProps::GetList(
                array("ID" => "ASC"),
                array("USER_ID" => $this->idUser),
                false,
                false,
                array('ID', 'PERSON_TYPE_ID')
            );

            while ($ar_sales = $db_sales->Fetch()){
                if( $ar_sales['PERSON_TYPE_ID'] ){

                    $idType = $ar_sales['PERSON_TYPE_ID'];
                }
            }
        }
        return $idType;
    }
}
?>