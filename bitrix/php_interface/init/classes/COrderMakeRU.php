<?
/**
 * Работа с заказами
 * Class COrderMakeRU
 */
Class COrderMakeRU{

    /**
     * Подписываем на рассылку после оформления заказа, если выбрана галочка
     * @param $ID
     * @param $arOrder
     * @param $arParams
     */
    public function addSubscribe($ID, $arOrder, $arParams){

        $addSubscribe = htmlspecialchars($_REQUEST['ADD_SUBSCRIBE']);

        if( $addSubscribe == 'y' ){

            $idUser = $arOrder["USER_ID"];

            if( $idUser > 0 ){

                $rsUser = CUser::GetByID($idUser);
                $arUser = $rsUser->Fetch();
                $userEmail = $arUser['EMAIL'];

                if(CModule::IncludeModule("subscribe")) {

                    $arFilter = array(
                        "ACTIVE" => "Y",
                        "LID" => "s1",
                        "VISIBLE"=>"Y",
                    );

                    $rsRubrics = CRubric::GetList(array(), $arFilter);
                    $arRubrics = array();
                    while($arRubric = $rsRubrics->GetNext()) $arRubrics[] = $arRubric["ID"];

                    if( eRU($arRubrics) ){

                        $obSubscription = new CSubscription;

                        $ID = $obSubscription->Add(array(
                            "USER_ID" => $idUser,
                            "ACTIVE" => "Y",
                            "EMAIL" => $userEmail,
                            "FORMAT" => "html",
                            "CONFIRMED" => "Y",
                            "SEND_CONFIRM" => "N",
                            "RUB_ID" => $arRubrics,
                        ));
                    }
                }
            }
        }
    }

    /**
     * Добавляет поля пользователя "Имя", "Телефон"
     * @param $ID
     * @param $arOrder
     * @param $arParams
     */
    public function addUserData($ID, $arOrder, $arParams){

        global $USER;
        global $APPLICATION;

        $fields = array();

        $idUser = $arOrder["USER_ID"];
        $rsUser = CUser::GetByID($idUser);
        $arUser = $rsUser->Fetch();
        if( !$arUser['NAME'] ){
            $fields['NAME'] = $_REQUEST['ORDER_PROP_1'];
        }
        if( !$arUser['PERSONAL_PHONE'] ){
            $fields['PERSONAL_PHONE'] = $_REQUEST['ORDER_PROP_3'];
        }

        if( eRU($fields) ){
            $user = new CUser;
            $user->Update($idUser, $fields);
        }
    }

    /**
     * выставляет корректный ID типа профилей для текущего пользователя
     * @param $arResult
     * @param $arUserResult
     * @param $arParams
     */
    function setRightProfileID4CurrentUser(&$arResult, &$arUserResult, $arParams){

        # получаем ID типа профиля
        $profile = new CUserProfileRU();
        $idProfile = $profile->getUserProfileTypeId();

        $arUserResult['PERSON_TYPE_ID'] = $idProfile;
    }

    /**
     * Выставляет исходные данные профиля, чтобы их нельзяя было сменить при оформление заказа
     * @param $arFields
     */
    function updateJurOrder($arFields){

        # получаем поля юр.лица
        if( $_REQUEST['PERSON_TYPE'] == 2 ){

            $excludeProps = array(18, 19);

            global $USER;
            $idUser = $USER->GetID();

            if( CModule::IncludeModule('sale') ){

                $db_sales = CSaleOrderUserProps::GetList(
                    array("ID" => "ASC"),
                    array("USER_ID" => $idUser),
                    false,
                    false,
                    array('ID', 'PERSON_TYPE_ID')
                );

                while ($ar_sales = $db_sales->Fetch()){
                    if( $ar_sales['PERSON_TYPE_ID'] ){

                        $idType = $ar_sales['PERSON_TYPE_ID'];
                        $ID = $ar_sales['ID'];

                        $db_propVals = CSaleOrderUserPropsValue::GetList(array("ID" => "ASC"), Array("USER_PROPS_ID"=>$ID));
                        while ($arPropVals = $db_propVals->Fetch()){
                            if( !in_array($arPropVals['ORDER_PROPS_ID'], $excludeProps) ){
                                $_REQUEST['ORDER_PROP_'.$arPropVals['ORDER_PROPS_ID']] = $arPropVals['VALUE'];
                            }
                        }
                    }
                }
            }
        }
    }

    /**
     * Добавляет в заказ тип покупателя
     * @param $ID
     * @param $arFields
     */
    public function OnOrderAddHandler($ID, $arFields, $arOrder){

        if(
            CModule::IncludeModule('sale') &&
            CModule::IncludeModule('catalog')
        ){
            # получаем св-ва заказа
            $db_props = CSaleOrderPropsValue::GetOrderProps($ID);

            if( $arProps = $db_props->Fetch() ){

                # определяем тип плательщика
                $orderPropsId = $arProps['ORDER_PROPS_ID'];

                if( $ar = CSaleOrderProps::GetByID($orderPropsId) ){
                    $idUserType = $ar['PERSON_TYPE_ID'];

                    $buyerTypePropsId = 35;
                    $value = "розница";

                    if( $idUserType == 2 ){
                        $buyerTypePropsId = 37;
                    }

                    if( CSite::InGroup(array(9)) ){
                        $value = "опт";
                    }
                }

                $arFields_ = array(
                    "ORDER_ID" => $ID,
                    "ORDER_PROPS_ID" => $buyerTypePropsId,
                    "NAME" => "Тип покупателя",
                    "CODE" => "BUYER_TYPE",
                    "VALUE" => $value
                );

                CSaleOrderPropsValue::Add($arFields_);

                save2FileVariable(array($ID, $arFields, $_REQUEST, $buyerTypePropsId, $value, $orderPropsId, $idUserType, $arProps, $arOrder));
            }
        }
    }
}