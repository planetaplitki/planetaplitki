<?
include_once($_SERVER['DOCUMENT_ROOT'].'/bitrix/modules/main/include/urlrewrite.php');

CHTTP::SetStatus("404 Not Found");
@define("ERROR_404","Y");

require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php"); ?>
<?$curPage = explode('/', $APPLICATION->GetCurDir());?>
<?if($curPage[1] == 'catalog'):?>
	<div class="container">
		<?$APPLICATION->SetPageProperty('title', "Страница не найдена");?>
		<h1>Страница не найдена</h1>
		<?require($_SERVER["DOCUMENT_ROOT"].SITE_DEFAULT_TEMPLATE_PATH."/include/sitemap.php");?>
	</div>
<?elseif($curPage[1] == 'all-shares'):?>
	<div class="container">
		<?$APPLICATION->SetPageProperty('title', "Страница не найдена");?>
		<?require($_SERVER["DOCUMENT_ROOT"].SITE_DEFAULT_TEMPLATE_PATH."/include/sitemap.php");?>
	</div>
<?else:?>
	<?$APPLICATION->SetTitle("Страница не найдена");?>
	<?require($_SERVER["DOCUMENT_ROOT"].SITE_DEFAULT_TEMPLATE_PATH."/include/sitemap.php");?>
<?endif?>
<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>