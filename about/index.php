<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetPageProperty("description", "Информация о компании \"Планета Плитки\" - официальном поставщике плитки из Испании и Италии");
$APPLICATION->SetPageProperty("keywords", "Планета Плитки");
$APPLICATION->SetPageProperty("title", "О компании \"Планета Плитки\"");
$APPLICATION->SetTitle("О компании");
?><p>
</p>
<p style="text-align: justify;">
 <strong style="line-height: 1.6em;"><br>
	<span style="font-size: 11pt;">
	Уважаемые господа!</span></strong><span style="font-size: 11pt;"> </span>
</p>
<span style="font-size: 11pt;"> </span>
<p style="text-align: justify;">
	<span style="font-size: 11pt;"> </span>
</p>
<span style="font-size: 11pt;"> </span>
<p style="text-align: justify;">
	<span style="font-size: 11pt;">
	Компания </span><strong><span style="font-size: 11pt;">"Планета Плитки"</span></strong><span style="font-size: 11pt;"> предлагает Вам взаимовыгодное сотрудничество. Наша специализация - </span><strong><span style="font-size: 11pt;">керамическая плитка</span></strong><span style="font-size: 11pt;"> в интерьерном представлении. Чтобы Вам было проще оценить преимущества работы с нами, несколько слов о нашей компании. </span>
</p>
<span style="font-size: 11pt;"> </span>
<p style="text-align: justify;">
	<span style="font-size: 11pt;"> </span>
</p>
<span style="font-size: 11pt;"> </span>
<p style="text-align: justify;">
	<span style="font-size: 11pt;"> </span><strong><span style="font-size: 11pt;">"Планета Плитки"</span></strong><span style="font-size: 11pt;"> была основана в 1995 году и на сегодняшний момент является крупнейшим импортером керамической плитки на рынке отделочных материалов. Компания имеет сеть магазинов в Москве и регулярно участвует на специализированных строительных выставках, выставляя на своих стендах последние коллекции керамической плитки и </span><a href="/catalog/keramogranit/" title="керамогранит"><span style="font-size: 11pt;">керамогранита</span></a><span style="font-size: 11pt;"> от лучших мировых производителей. Постоянно растущая дилерская сеть сегодня представлена компаниями из Москвы, Владивостока, Омска, Вологды, Воронежа, Екатеринбурга, Перми. </span>
</p>
<span style="font-size: 11pt;"> </span>
<p style="text-align: justify;">
	<span style="font-size: 11pt;"> </span>
</p>
<span style="font-size: 11pt;"> </span>
<p style="text-align: justify;">
	<span style="font-size: 11pt;">
	Ассортимент керамической плитки очень широк: от недорогих коллекций плитки до коллекций элитного класса.</span><br>
	<span style="font-size: 11pt;"> </span><br>
	<span style="font-size: 11pt;">
	Наши поставщики - ведущие испанские и итальянские фабрики по производству керамической плитки:</span><br>
	<span style="font-size: 11pt;"> </span><strong><strong><a href="/catalog/ispaniya/ape/"><span style="font-size: 11pt;">APE</span></a></strong></strong><br>
	<span style="font-size: 11pt;"> </span><strong><strong><a href="/catalog/ispaniya/alaplana/" target="_blank"><span style="font-size: 11pt;">A</span></a><a target="_blank" href="/catalog/ispaniya/alaplana/"><span style="font-size: 11pt;">LAPLANA</span></a></strong></strong><br>
	<span style="font-size: 11pt;"> </span><strong><a href="/catalog/ispaniya/azulev/"><span style="font-size: 11pt;">AZULEV</span></a></strong><br>
	<span style="font-size: 11pt;"> </span><strong><a href="/catalog/ispaniya/azteca/" target="_blank"><span style="font-size: 11pt;">AZTECA</span></a></strong><br>
	<span style="font-size: 11pt;"> </span><strong><a href="/catalog/ispaniya/bellavista/" target="_blank"><span style="font-size: 11pt;">BELLAVISTA</span></a></strong><br>
	<span style="font-size: 11pt;"> </span><strong><a href="/catalog/ispaniya/navarti/" target="_blank"><span style="font-size: 11pt;">NAVARTI</span></a></strong><br>
	<span style="font-size: 11pt;"> </span><a href="/catalog/ispaniya/latina-ceramica/"><strong><span style="font-size: 11pt;">LATINA&nbsp;CERAMICA</span></strong></a><br>
	<span style="font-size: 11pt;"> </span><strong><a href="/catalog/ispaniya/el-molino/" target="_blank"><span style="font-size: 11pt;">EL MOLINO</span></a></strong><br>
	<span style="font-size: 11pt;"> </span><strong><a href="/catalog/ispaniya/emigres/" target="_blank"><span style="font-size: 11pt;">EMIGRES</span></a></strong><br>
	<span style="font-size: 11pt;"> </span><b><a target="_blank" href="/catalog/ispaniya/hdc-porcelanicos/"><span style="font-size: 11pt;">HDC PORCELANICOS</span></a></b><br>
	<span style="font-size: 11pt;"> </span><strong><a href="/catalog/ispaniya/roca/"><span style="font-size: 11pt;">ROCA</span></a></strong><br>
	<span style="font-size: 11pt;"> </span><strong><a href="/catalog/ispaniya/ibero/"><span style="font-size: 11pt;">IBERO</span></a></strong><br>
	<span style="font-size: 11pt;"> </span><strong><a href="/catalog/ispaniya/itt-ceramic/" target="_blank"><span style="font-size: 11pt;">ITT CERAMICA</span></a></strong><br>
	<span style="font-size: 11pt;"> </span><strong><a href="/catalog/ispaniya/ceracasa/"><span style="font-size: 11pt;">CERACASA</span></a></strong><br>
	<span style="font-size: 11pt;"> </span><strong><a href="/catalog/ispaniya/el-molino/"><span style="font-size: 11pt;">EL MOLINO</span></a></strong><br>
	<span style="font-size: 11pt;"> </span><strong><a href="/catalog/ispaniya/gaya-fores/" target="_blank"><span style="font-size: 11pt;">GAYAFORES</span></a></strong><br>
	<span style="font-size: 11pt;"> </span><strong><a href="/catalog/portugaliya/gresart/"><span style="font-size: 11pt;">GRESART</span></a></strong><br>
	<span style="font-size: 11pt;"> </span><strong><a href="/catalog/ispaniya/plaza/plaza-freedom/"><span style="font-size: 11pt;">PLAZA</span></a></strong><a href="/catalog/ispaniya/plaza/plaza-freedom/"><br>
	<span style="font-size: 11pt;"> </span></a><strong><a href="/catalog/ispaniya/venus/"><span style="font-size: 11pt;">VENUS</span></a></strong><br>
	<span style="font-size: 11pt;"> </span><a href="/catalog/italiya/ariana-ceramica/"><b style="line-height: 1.6em;"><span style="font-size: 11pt;">ARIANA CERAMICA</span></b></a><br>
	<span style="font-size: 11pt;"> </span><a href="/catalog/italiya/domus-linea/"><b><span style="font-size: 11pt;">DOMUS LINEA</span></b></a><br>
	<span style="font-size: 11pt;"> </span><strong><a href="/catalog/ispaniya/tau/" target="_blank"><span style="font-size: 11pt;">TAU</span></a></strong><br>
	<span style="font-size: 11pt;"> </span><br>
	<span style="font-size: 11pt;">
	Считаем необходимым выделить в отдельную группу такие фабрики, как: </span><br>
	<span style="font-size: 11pt;"> </span><strong><a href="/catalog/ispaniya/cas-ceramica/"><span style="font-size: 11pt;">CERAMICA CAS</span></a></strong><br>
	<span style="font-size: 11pt;"> </span><strong><a href="/catalog/ispaniya/mainzu/"><span style="font-size: 11pt;">MAINZU</span></a></strong><br>
	<span style="font-size: 11pt;"> </span><strong><span style="font-size: 11pt;"> </span><a href="/catalog/ispaniya/pamesa/"><span style="font-size: 11pt;">PAMESA</span></a></strong><br>
	<span style="font-size: 11pt;"> </span><strong><a href="/catalog/ispaniya/rocersa/"><span style="font-size: 11pt;">ROCERSA</span></a></strong><span style="font-size: 11pt;"> </span>
</p>
<span style="font-size: 11pt;"> </span>
<p style="text-align: justify;">
	<span style="font-size: 11pt;"> </span>
</p>
<span style="font-size: 11pt;"> </span>
<p style="text-align: justify;">
	<span style="font-size: 11pt;">
	которые наравне с керамической плиткой, предлагают широкий выбор керамического гранита, настолько востребованного в наши дни. </span>
</p>
<span style="font-size: 11pt;"> </span>
<p style="text-align: justify;">
	<span style="font-size: 11pt;"> </span>
</p>
<span style="font-size: 11pt;"> </span>
<p style="text-align: justify;">
	<span style="font-size: 11pt;">
	Компания </span><strong><span style="font-size: 11pt;">"Планета Плитки"</span></strong><span style="font-size: 11pt;"> вышла на рынок с принципиально новым для России направлением - </span><a href="/catalog/vostochnaya-kollektsiya/"><span style="font-size: 11pt;">керамическая плитка в восточном стиле</span></a><span style="font-size: 11pt;"> с характерным орнаментом. </span>
</p>
<span style="font-size: 11pt;"> </span>
<p style="text-align: justify;">
	<span style="font-size: 11pt;"> </span>
</p>
<span style="font-size: 11pt;"> </span>
<p style="text-align: justify;">
	<span style="font-size: 11pt;">
	Предлагаем различные схемы сотрудничества для торговых, архитектурных и строительных организаций, дающие возможность покупать высококачественные отделочные материалы по специальным ценам.</span><br>
	<span style="font-size: 11pt;">
	Приглашаем Вас посетить наш магазин, располагающий двумя торговыми залами: Европа и Восток, где&nbsp;керамическая плитка представлена в виде готовых интерьерных решений, это позволит Вам легко определиться с выбором плитки. </span>
</p>
<span style="font-size: 11pt;"> </span>
<p style="text-align: justify;">
	<span style="font-size: 11pt;"> </span>
</p>
<span style="font-size: 11pt;"> </span>
<p style="text-align: justify;">
	<span style="font-size: 11pt;">
	&nbsp; </span>
</p>
<span style="font-size: 11pt;"> </span>
<p style="text-align: justify;">
	<span style="font-size: 11pt;"> </span>
</p>
<span style="font-size: 11pt;"> </span>
<p style="text-align: justify;">
	<span style="font-size: 11pt;"> </span><strong><span style="font-size: 11pt;">Наши координаты, а также контактную информацию Вы можете посмотреть </span><a href="/contacts/"><span style="font-size: 11pt;">здесь</span></a><span style="font-size: 11pt;">.</span></strong><strong><span style="font-size: 11pt;">&nbsp;</span></strong>
</p>
<p style="text-align: justify;">
</p><?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>