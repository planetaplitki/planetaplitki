<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetPageProperty("title", "Распродажа плитки для ванной, кухни, напольная, керамогранит");
$APPLICATION->SetPageProperty("keywords", "Распродажа плитки, распродажа керамической плитки");
$APPLICATION->SetPageProperty("description", "Распродажа плитки со скидками до 90%. Плитка для ванной со склада, кухни, напольная, керамогранит");
$APPLICATION->SetTitle("Распродажа");
$APPLICATION->SetPageProperty('pageclass', 'main2 rasprodaja_page');
?><?
$ufType = "sale";
$arParams["IBLOCK_TYPE"] = '1c_catalog';
$arParams["IBLOCK_ID"] = IBLOCK_ID_CATALOG;
?> 
<?//include_once $_SERVER['DOCUMENT_ROOT'].'/bitrix/templates/.default/components/bitrix/catalog/catalog/filter_no_elem.php';?>
<?include_once $_SERVER['DOCUMENT_ROOT'].'/bitrix/templates/.default/components/bitrix/catalog/catalog/filter.php';?>

<div class="text-content">
    <div class="container">
<br>
<h2><span style="font-size: 14pt;">Распродажа плитки для пола, ванной и кухни</span></h2>
<p style="text-align: justify;">
	 Компания "Планета Плитки" предлагает Вам приобрести плитку и <a href="/catalog/keramogranit/" title="керамогранит">керамогранит</a> на выгодных условиях. В нашем разделе "Распродажа" представлены коллекции керамической плитки от ведущих Испанских и Итальянских фабрик. Это качественная керамическая плитка и керамогранит, которые есть на складе в ограниченном количестве. В ассортименте распродажи вы можете подобрать <a href="/catalog/plitka-dlya-vannoy/" title="плитка для ванной">плитку для ванной</a>, для кухни или коридора со скидками до 70%.
</p>
<p style="text-align: justify;">
 <strong>Внимание!</strong> На керамическую плитку из раздела "Распродажа" не действуют дополнительные скидки и акции.
</p>
        </div></div>
        <?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>