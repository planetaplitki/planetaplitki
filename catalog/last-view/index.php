<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetTitle("Последние просмотренные коллекции");
$APPLICATION->SetPageProperty('pageclass', 'main2 rasprodaja_page');
?>

<?
$lastView = json_decode($APPLICATION->get_cookie("VIEWED_COLLECTIONS"), true);
if( eRU($lastView) ){
    $GLOBALS['arrSectionsFilter'] = array();
    $GLOBALS['arrSectionsFilter']['ID'] = $lastView;
    $arParams["IBLOCK_TYPE"] = '1c_catalog';
    $arParams["IBLOCK_ID"] = IBLOCK_ID_CATALOG;
    $ufSeoPageProps = $GLOBALS['arrSectionsFilter']['PROPERTY'];
    include_once $_SERVER['DOCUMENT_ROOT'].'/bitrix/templates/.default/components/bitrix/catalog/catalog/filter.php';
}
else{?>
<div class="container empty-page">
    <p>Нет ни одной просмотренной коллекции.</p>
</div>
<?}?>

<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>