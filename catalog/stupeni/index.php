<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetPageProperty("keywords_inner", "Ступени купить");
$APPLICATION->SetPageProperty("title", "Ступени - купить в Москве, каталог, цены;  интернет-магазин керамических ступеней, ступеней из клинкера и керамогранита в магазине Планета Плитки");
$APPLICATION->SetPageProperty("keywords", "Ступени, клинкерные ступени, клинкер, керамические ступени");
$APPLICATION->SetPageProperty("description", "Ступени в ассортименте – сеть магазинов в Москве. Клинкерные ступени, керамические ступени в интернет-магазине с доставкой по Москве и области. Официальный поставщик керамической плитки с 1995 года. Салон-магазин с образцами 310 кв.м.");
$APPLICATION->SetTitle("Ступени");
$APPLICATION->SetPageProperty('pageclass', 'main2 rasprodaja_page');
?><?
$GLOBALS['arrSectionsFilter'] = array();
$GLOBALS['arrSectionsFilter']['PROPERTY']['kategoriyatovar'] = 573374;
$arParams["IBLOCK_TYPE"] = '1c_catalog';
$arParams["IBLOCK_ID"] = IBLOCK_ID_CATALOG;
$ufSeoPageProps = $GLOBALS['arrSectionsFilter']['PROPERTY'];
?> <?global $USER?> <?//if($_SERVER['HTTP_X_FORWARDED_FOR'] == '109.110.66.119'):?> <?if($USER->IsAdmin()):?> <?include_once $_SERVER['DOCUMENT_ROOT'].'/bitrix/templates/main_copy/components/bitrix/catalog/catalog/filter.php';?> <?else:?> <?include_once $_SERVER['DOCUMENT_ROOT'].'/bitrix/templates/.default/components/bitrix/catalog/catalog/filter.php';?> <?endif?>
<div class="text-content">
	<div class="container">
 <br>
		<hr>
		<h2><span style="font-size: 16pt;">Ступени: клинкерные, керамические, из керамогранита</span></h2>
		<p>
			Мы предлагаем приобрести клинкерные ступени и ступени из керамогранита от ведущих производителей Испании и Италии, а так же других европейских фабрик. Начиная с 1995 года компания заботиться о каждом клиенте и предоставляет исключительно высококлассную продукцию, соответствующую международным стандартам качества и безопасности. Купить ступени Вы можете в интернет-магазине planetaplitki.ru либо в одном из наших салонов-магазинов в Москве.
		</p>
	</div>
</div>
 <br><?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>