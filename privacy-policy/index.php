<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetTitle("Политика конфиденциальности");
?><p style="text-align: justify;">
 <span style="font-size: 11pt;">Целью создания настоящего сайта ООО «ЭЛЬБА» (Компания «Планета Плитки») является предоставление физическим и&nbsp;юридическим лицам (пользователям) необходимых сведений о&nbsp;деятельности Компании и&nbsp;информирование о&nbsp;товарах и&nbsp;услугах (продуктах), предоставляемых ООО «ЭЛЬБА». Сведения на&nbsp;сайте в&nbsp;большей степени носят информационный характер. </span><span style="font-size: 11pt;"> </span>
</p>
 <span style="font-size: 11pt;"> </span><span style="font-size: 11pt;"> </span><span style="font-size: 11pt;"> </span>
<p style="text-align: justify;">
 <span style="font-size: 11pt;"> </span><span style="font-size: 11pt;"> </span><span style="font-size: 11pt;"> </span>
</p>
 <span style="font-size: 11pt;"> </span><span style="font-size: 11pt;"> </span><span style="font-size: 11pt;"> </span>
<p style="text-align: justify;">
 <span style="font-size: 11pt;"> </span><span style="font-size: 11pt;">
	Все содержимое сайта является собственностью ООО «</span>ЭЛЬБА<span style="font-size: 11pt;">» и&nbsp;защищено действующим законодательством, регулирующим вопросы авторского права. В&nbsp;связи с&nbsp;чем, пользователи сайта могут использовать его содержание в&nbsp;личных и&nbsp;некоммерческих целях. Использование содержания сайта в&nbsp;иных случаях не&nbsp;допускается. </span><span style="font-size: 11pt;"> </span>
</p>
 <span style="font-size: 11pt;"> </span><span style="font-size: 11pt;"> </span><span style="font-size: 11pt;"> </span>
<p style="text-align: justify;">
 <span style="font-size: 11pt;"> </span><span style="font-size: 11pt;"> </span><span style="font-size: 11pt;"> </span>
</p>
 <span style="font-size: 11pt;"> </span><span style="font-size: 11pt;"> </span><span style="font-size: 11pt;"> </span>
<p style="text-align: justify;">
 <span style="font-size: 11pt;"> </span><span style="font-size: 11pt;">
	Компания не&nbsp;разрешает внесение в&nbsp;содержание данного сайта каких-либо изменений, а&nbsp;также последующее воспроизведение его содержания. </span><span style="font-size: 11pt;"> </span>
</p>
 <span style="font-size: 11pt;"> </span><span style="font-size: 11pt;"> </span><span style="font-size: 11pt;"> </span>
<p style="text-align: justify;">
 <span style="font-size: 11pt;"> </span><span style="font-size: 11pt;"> </span><span style="font-size: 11pt;"> </span>
</p>
 <span style="font-size: 11pt;"> </span><span style="font-size: 11pt;"> </span><span style="font-size: 11pt;"> </span>
<p style="text-align: justify;">
 <span style="font-size: 11pt;"> </span><span style="font-size: 11pt;">
	Обращаем Ваше внимание на&nbsp;то, что на&nbsp;сайте Компании имеются ссылки на&nbsp;другие веб-сайты и&nbsp;в&nbsp;данном случае Компания не&nbsp;несет ответственность за&nbsp;конфиденциальность информации на&nbsp;других ресурсах. </span><span style="font-size: 11pt;"> </span>
</p>
 <span style="font-size: 11pt;"> </span><span style="font-size: 11pt;"> </span><span style="font-size: 11pt;"> </span>
<p style="text-align: justify;">
 <span style="font-size: 11pt;"> </span><span style="font-size: 11pt;"> </span><span style="font-size: 11pt;"> </span>
</p>
 <span style="font-size: 11pt;"> </span><span style="font-size: 11pt;"> </span><span style="font-size: 11pt;"> </span>
<p style="text-align: justify;">
 <span style="font-size: 11pt;"> </span><span style="font-size: 12pt;"><span style="font-size: 11pt;">
	Компания оставляет за&nbsp;собой право изменения Политики конфиденциальности в&nbsp;любое время с&nbsp;целью дальнейшего совершенствования системы защиты от&nbsp;несанкционированного доступа к&nbsp;сообщаемым Вами персональным данным</span>. </span>
</p>
 <span style="font-size: 12pt;"> </span>
<p style="text-align: justify;">
 <span style="font-size: 12pt;"> </span>
</p>
 <span style="font-size: 12pt;"> </span>
<h2 style="text-align: justify;"><span style="font-size: 12pt;"> </span><span style="font-size: 14pt;"><b>КАК, КОГДА И&nbsp;КАКУЮ ИНФОРМАЦИЮ МЫ&nbsp;СОБИРАЕМ И&nbsp;КАК ЕЕ&nbsp;ИСПОЛЬЗУЕМ</b></span><span style="font-size: 12pt;"> </span></h2>
 <span style="font-size: 12pt;"> </span>
<p style="text-align: justify;">
 <span style="font-size: 12pt;"> </span>
</p>
 <span style="font-size: 12pt;"> </span>
<p style="text-align: justify;">
 <span style="font-size: 12pt;"> <span style="font-size: 11pt;">Компания собирает личную информацию о&nbsp;Вас (персональные данные) всякий раз, когда&nbsp;Вы ее&nbsp;предоставляете в&nbsp;магазинах Компании, указываете на&nbsp;сайте, отправляете по&nbsp;электронной почте или сообщаете лично. Передавая Компании свои персональные данные, Вы&nbsp;соглашаетесь с&nbsp;условиями, приведенными здесь. В&nbsp;соответствии с&nbsp;действующим законодательством и&nbsp;Политикой ООО «ЭЛЬБА» о&nbsp;защите персональных данных, Вы&nbsp;можете в&nbsp;любое время их&nbsp;изменить, обновить или попросить об&nbsp;удалении. </span></span><span style="font-size: 11pt;"> </span>
</p>
 <span style="font-size: 11pt;"> </span><span style="font-size: 11pt;"> </span><span style="font-size: 11pt;"> </span>
<p style="text-align: justify;">
 <span style="font-size: 11pt;"> </span><span style="font-size: 11pt;"> </span><span style="font-size: 11pt;"> </span>
</p>
 <span style="font-size: 11pt;"> </span><span style="font-size: 11pt;"> </span><span style="font-size: 11pt;"> </span>
<p style="text-align: justify;">
 <span style="font-size: 11pt;"> </span><span style="font-size: 11pt;"> </span><b><span style="font-size: 11pt;">При регистрации на&nbsp;сайте, оформлении заказа, а&nbsp;также при заполнении других документов, в&nbsp;том числе в&nbsp;магазинах Компании, Вы&nbsp;можете предоставить следующую информацию:</span></b><span style="font-size: 11pt;"> </span><span style="font-size: 11pt;"> </span>
</p>
 <span style="font-size: 11pt;"> </span><span style="font-size: 11pt;"> </span><span style="font-size: 11pt;"> </span>
<p style="text-align: justify;">
 <span style="font-size: 11pt;"> </span><span style="font-size: 11pt;"> </span><span style="font-size: 11pt;"> </span>
</p>
 <span style="font-size: 11pt;"> </span><span style="font-size: 11pt;"> </span><span style="font-size: 11pt;"> </span>
<p style="text-align: justify;">
 <span style="font-size: 11pt;"> </span><span style="font-size: 11pt;"> </span><span style="font-size: 11pt;"> </span>
</p>
 <span style="font-size: 11pt;"> </span><span style="font-size: 11pt;"> </span><span style="font-size: 11pt;"> </span>
<ul>
	<li><span style="font-size: 11pt;"> </span><span style="font-size: 11pt;"> </span><span style="font-size: 11pt;">
	Фамилия, Имя, Отчество; </span><span style="font-size: 11pt;"> </span><span style="font-size: 11pt;"> </span></li>
	<li><span style="font-size: 11pt;"> </span><span style="font-size: 11pt;"> </span><span style="font-size: 11pt;">
	Номер контактного телефона и&nbsp;адрес электронной почты, по&nbsp;которым мы&nbsp;можем связаться с&nbsp;Вами; </span><span style="font-size: 11pt;"> </span><span style="font-size: 11pt;"> </span></li>
	<li><span style="font-size: 11pt;"> </span><span style="font-size: 11pt;"> </span><span style="font-size: 11pt;">
	Логин и&nbsp;пароль, которые&nbsp;Вы будете использовать для доступа к&nbsp;Интернет-ресурсам Компании; </span><span style="font-size: 11pt;"> </span><span style="font-size: 11pt;"> </span></li>
	<li><span style="font-size: 11pt;"> </span><span style="font-size: 11pt;"> </span><span style="font-size: 12pt;"><span style="font-size: 11pt;">
	Информацию об&nbsp;адресе доставки Вашего заказа;</span> </span> <span style="font-size: 12pt;"> </span></li>
</ul>
 <span style="font-size: 12pt;"> </span><b><span style="font-size: 11pt;">Также Компания собирает некоторую статистическую информацию, например:</span></b><span style="font-size: 11pt;"> </span><span style="font-size: 11pt;"> </span>
<ol>
	<li><span style="font-size: 11pt;"> </span><span style="font-size: 11pt;"> </span><span style="font-size: 11pt;">
	IP-адрес пользователя; </span><span style="font-size: 11pt;"> </span><span style="font-size: 11pt;"> </span></li>
	<li><span style="font-size: 11pt;"> </span><span style="font-size: 11pt;"> </span><span style="font-size: 11pt;">
	Тип браузера; </span><span style="font-size: 11pt;"> </span><span style="font-size: 11pt;"> </span></li>
	<li><span style="font-size: 11pt;"> </span><span style="font-size: 11pt;"> </span><span style="font-size: 11pt;">
	Дата, время и&nbsp;количество посещений; </span><span style="font-size: 11pt;"> </span><span style="font-size: 11pt;"> </span></li>
	<li><span style="font-size: 11pt;"> </span><span style="font-size: 11pt;"> </span><span style="font-size: 11pt;">
	Адрес сайта, с&nbsp;которого пользователь осуществил переход на&nbsp;сайт Компании; </span><span style="font-size: 11pt;"> </span><span style="font-size: 11pt;"> </span></li>
	<li><span style="font-size: 11pt;"> </span><span style="font-size: 11pt;"> </span><span style="font-size: 11pt;">
	Сведения о&nbsp;местоположении; </span><span style="font-size: 11pt;"> </span><span style="font-size: 11pt;"> </span></li>
	<li><span style="font-size: 11pt;"> </span><span style="font-size: 11pt;"> </span><span style="font-size: 11pt;">
	Сведения о&nbsp;посещенных страницах; </span><span style="font-size: 11pt;"> </span><span style="font-size: 11pt;"> </span></li>
	<li><span style="font-size: 11pt;"> </span><span style="font-size: 11pt;"> </span><span style="font-size: 11pt;">
	Информация, предоставляемая Вашим браузером (тип устройства, тип и&nbsp;версия браузера,&nbsp;</span><br>
 <span style="font-size: 11pt;"> </span><span style="font-size: 11pt;">
	операционная система и&nbsp;т.п.). </span><span style="font-size: 11pt;"> </span><span style="font-size: 11pt;"> </span></li>
</ol>
 <span style="font-size: 11pt;"> </span>
<p style="text-align: justify;">
 <span style="font-size: 11pt;"> </span><span style="font-size: 11pt;"> </span><b><span style="font-size: 11pt;">Компания использует персональную информацию, предоставленную Вами в соответствии с действующим законодательством в целях:</span></b><span style="font-size: 11pt;"> </span><span style="font-size: 11pt;"> </span>
</p>
 <span style="font-size: 11pt;"> </span>
<ul>
	<li><span style="font-size: 11pt;"> </span><span style="font-size: 11pt;"> </span><span style="font-size: 11pt;">
	повышения качества предоставляемых товаров и&nbsp;услуг (продуктов); </span><span style="font-size: 11pt;"> </span><span style="font-size: 11pt;"> </span></li>
	<li><span style="font-size: 11pt;"> </span><span style="font-size: 11pt;"> </span><span style="font-size: 11pt;">
	обработки заказа на&nbsp;сайте Компании или для предоставления Вам доступа к&nbsp;специальной информации; </span><span style="font-size: 11pt;"> </span><span style="font-size: 11pt;"> </span></li>
	<li><span style="font-size: 11pt;"> </span><span style="font-size: 11pt;"> </span><span style="font-size: 11pt;">
	анализа данных и&nbsp;проведения различных исследований; </span><span style="font-size: 11pt;"> </span><span style="font-size: 11pt;"> </span></li>
	<li><span style="font-size: 11pt;"> </span><span style="font-size: 11pt;"> </span><span style="font-size: 11pt;">
	осуществления деятельности по&nbsp;продвижению товаров и&nbsp;услуг; </span><span style="font-size: 11pt;"> </span><span style="font-size: 11pt;"> </span></li>
	<li><span style="font-size: 11pt;"> </span><span style="font-size: 11pt;"> </span><span style="font-size: 11pt;">
	подготовки индивидуальных предложений, которые подходят именно Вам; </span><span style="font-size: 11pt;"> </span><span style="font-size: 11pt;"> </span></li>
	<li><span style="font-size: 11pt;"> </span><span style="font-size: 11pt;"> </span><span style="font-size: 11pt;">
	информирования Вас об&nbsp;акциях, скидках и&nbsp;специальных предложениях, присылая на&nbsp;указанный при регистрации номер телефона и&nbsp;адрес электронный почты информационные сообщения. </span><span style="font-size: 11pt;"> </span><span style="font-size: 11pt;"> </span></li>
</ul>
 <span style="font-size: 11pt;"> </span>
<p style="text-align: justify;">
 <span style="font-size: 11pt;"> </span><span style="font-size: 11pt;"> </span><b><span style="font-size: 11pt;">Обращаем Ваше внимание на&nbsp;то, что в&nbsp;любой момент&nbsp;Вы можете отказаться от&nbsp;указанных сообщений, обратившись к&nbsp;Компании.</span></b><span style="font-size: 12pt;"> </span>
</p>
<h2 style="text-align: justify;"><span style="font-size: 12pt;"> <span style="font-size: 14pt;"><b>ЧТО МЫ&nbsp;ГАРАНТИРУЕМ</b></span> </span></h2>
<p style="text-align: justify;">
 <span style="font-size: 12pt;"> <span style="font-size: 11pt;">ООО «ЭЛЬБА» ответственно относится к&nbsp;вопросу конфиденциальности своих пользователей и&nbsp;уважает право каждого пользователя сайта на&nbsp;конфиденциальность. </span></span><span style="font-size: 11pt;"> </span>
</p>
 <span style="font-size: 11pt;"> </span>
<p style="text-align: justify;">
 <span style="font-size: 11pt;"> </span><span style="font-size: 11pt;">
	Компания гарантирует, что никакая полученная от&nbsp;Вас информация никогда и&nbsp;ни&nbsp;при каких условиях не&nbsp;будет предоставлена третьим лицам без Вашего согласия, за&nbsp;исключением случаев, предусмотренных действующим законодательством Российской Федерации. </span><span style="font-size: 11pt;"> </span>
</p>
 <span style="font-size: 11pt;"> </span>
<p style="text-align: justify;">
 <span style="font-size: 11pt;"> </span><span style="font-size: 11pt;">
	Компания гарантирует, что персональные данные, передаваемые Вами Компании, будут обрабатываться в&nbsp;строгом соответствии с&nbsp;действующим законодательством. </span><span style="font-size: 11pt;"> </span>
</p>
 <span style="font-size: 11pt;"> </span>
<p style="text-align: justify;">
 <span style="font-size: 11pt;"> </span><span style="font-size: 11pt;"> </span><br>
 <span style="font-size: 11pt;"> </span><span style="font-size: 11pt;"> </span><strong><span style="font-size: 11pt;">Информация о&nbsp;гарантиях конфиденциальности при использовании платежных банковских карт на сайте:</span></strong><span style="font-size: 11pt;"> </span><span style="font-size: 11pt;"> </span>
</p>
 <span style="font-size: 11pt;"> </span>
<p style="text-align: justify;">
 <span style="font-size: 11pt;"> </span><span style="font-size: 12pt;"><span style="font-size: 11pt;">
	Компания обращает Ваше внимание на&nbsp;то, что при совершении Вами Интернет-платежей&nbsp; с&nbsp;использованием платежных банковских карт, безопасность гарантируется ООО НКО «Яндекс.Деньги». Для передачи конфиденциальной информации от клиента на сервер системы для дальнейшей обработки используется SSL протокол. Дальнейшая передача информации осуществляется по закрытым банковским сетям высшей степени защиты. Обработка полученных в зашифрованном виде конфиденциальных данных клиента (реквизиты карты, регистрационные данные и т. д.) производится в процессинговом центре. Таким образом, никто, даже продавец, не может получить персональные и банковские данные клиента, включая информацию о его покупках, сделанных в других магазинах.</span> </span>
</p>
<h2 style="text-align: justify;"><span style="font-size: 12pt;"> <span style="font-size: 14pt;"><b>ИЗМЕНЕНИЯ И&nbsp;ОБНОВЛЕНИЯ</b></span> </span></h2>
<p style="text-align: justify;">
 <span style="font-size: 12pt;"> <span style="font-size: 11pt;">Компания оставляет за&nbsp;собой право вносить необходимые изменения на&nbsp;сайте, заменять или удалять любые части его содержания и&nbsp;ограничивать доступ к&nbsp;сайту в&nbsp;любое время по&nbsp;своему усмотрению. </span></span><span style="font-size: 11pt;"> </span>
</p>
 <span style="font-size: 11pt;"> </span>
<p style="text-align: justify;">
 <span style="font-size: 11pt;"> </span><span style="font-size: 11pt;">
	Компания также оставляет за&nbsp;собой право изменения Политики конфиденциальности в&nbsp;любое время с&nbsp;целью дальнейшего совершенствования системы защиты от&nbsp;несанкционированного доступа к&nbsp;сообщаемым Вами персональным данным.</span>
</p><?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>