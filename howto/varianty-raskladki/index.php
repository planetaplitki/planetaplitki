<? require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetPageProperty("description", "Раскладка плитки от компании \"Планета Плитки\", варианты раскладки для ванной, кухни, коридора, хаммамов, бань.");
$APPLICATION->SetPageProperty("keywords", "варианты раскладки плитки, раскладка плитки");
$APPLICATION->SetPageProperty("title", "Варианты раскладки плитки. Раскладка плитки дизайнером.");
$APPLICATION->SetTitle("Варианты раскладки плитки");
?><?if( CURPAGE == '/howto/varianty-raskladki/' ){?>
<div class="row">
    <p class="col-sm-12">
    </p>
    <p style="text-align: justify;">
 <span style="font-size: 11pt;">Мы предлагем Вам готовые дизайнерские проекты оформления интерьера керамической плиткой, керамогранитом, мозаикой. По желанию Вы можете воспользоваться услугами нашего дизайнера, который выполнит раскладку выбранной плитки, с учетом Ваших пожеланий.</span><br>
        <span style="font-size: 11pt;"> </span>
    </p>
    <span style="font-size: 11pt;"> </span>
    <p style="text-align: justify;">
        <span style="font-size: 11pt;">
        Заказать индивидуальную раскладку, а так же получить помощь в подборе плитки Вы можете в салоне-магазине "Планета Плитки", по адресу Нахимовский пр-т, 50. Желательна предварительная запись. Стоимость дизайн-проекта одного помещения 2500 руб.</span>
    </p>
    <p>
 <b><span style="color: #9d0039;">Подробности и запись по тел.: 8 (495) 646-16-90, доб.955, 957 или e-mail: <a href="mailto:3d@planetaplitki.ru">3d@planetaplitki.ru</a>&nbsp;и <a href="mailto:dokshina@planetaplitki.ru">dokshina@planetaplitki.ru</a>&nbsp;</span></b><br>
    </p>
    <p>
 <b><br>
 </b>
    </p>
    <p>
    </p>
</div>
<?}?>
<?$APPLICATION->IncludeComponent(
    "bitrix:catalog",
    "varianti-raskladki",
    Array(
        "1CLICK_PHONE" => "",
        "ACTION_VARIABLE" => "action",
        "ADD_ELEMENT_CHAIN" => "Y",
        "ADD_PICT_PROP" => "-",
        "ADD_PROPERTIES_TO_BASKET" => "Y",
        "ADD_SECTIONS_CHAIN" => "Y",
        "AJAX_MODE" => "N",
        "AJAX_OPTION_ADDITIONAL" => "",
        "AJAX_OPTION_HISTORY" => "N",
        "AJAX_OPTION_JUMP" => "N",
        "AJAX_OPTION_STYLE" => "Y",
        "BASKET_URL" => "/personal/basket.php",
        "BIG_DATA_RCM_TYPE" => "bestsell",
        "CACHE_FILTER" => "N",
        "CACHE_GROUPS" => "Y",
        "CACHE_TIME" => "36000000",
        "CACHE_TYPE" => "A",
        "COMMON_ADD_TO_BASKET_ACTION" => "ADD",
        "COMMON_SHOW_CLOSE_POPUP" => "N",
        "COMPONENT_TEMPLATE" => "varianti-raskladki",
        "CONVERT_CURRENCY" => "N",
        "DETAIL_ADD_DETAIL_TO_SLIDER" => "N",
        "DETAIL_ADD_TO_BASKET_ACTION" => "BUY",
        "DETAIL_BACKGROUND_IMAGE" => "-",
        "DETAIL_BRAND_USE" => "N",
        "DETAIL_BROWSER_TITLE" => "-",
        "DETAIL_CHECK_SECTION_ID_VARIABLE" => "N",
        "DETAIL_DETAIL_PICTURE_MODE" => "IMG",
        "DETAIL_DISPLAY_NAME" => "Y",
        "DETAIL_DISPLAY_PREVIEW_TEXT_MODE" => "H",
        "DETAIL_META_DESCRIPTION" => "-",
        "DETAIL_META_KEYWORDS" => "-",
        "DETAIL_PROPERTY_CODE" => array(0=>"",1=>"",),
        "DETAIL_SET_CANONICAL_URL" => "N",
        "DETAIL_SET_VIEWED_IN_COMPONENT" => "N",
        "DETAIL_SHOW_MAX_QUANTITY" => "N",
        "DETAIL_USE_COMMENTS" => "N",
        "DETAIL_USE_VOTE_RATING" => "N",
        "DISABLE_INIT_JS_IN_COMPONENT" => "N",
        "DISPLAY_BOTTOM_PAGER" => "Y",
        "DISPLAY_TOP_PAGER" => "N",
        "ELEMENT_SORT_FIELD" => "sort",
        "ELEMENT_SORT_FIELD2" => "shows",
        "ELEMENT_SORT_ORDER" => "",
        "ELEMENT_SORT_ORDER2" => "asc",
        "FILTER_VIEW_MODE" => "VERTICAL",
        "HIDE_NOT_AVAILABLE" => "N",
        "IBLOCK_ID" => "11",
        "IBLOCK_TYPE" => "data",
        "INCLUDE_SUBSECTIONS" => "Y",
        "LABEL_PROP" => "-",
        "LINE_ELEMENT_COUNT" => "3",
        "LINK_ELEMENTS_URL" => "link.php?PARENT_ELEMENT_ID=#ELEMENT_ID#",
        "LINK_IBLOCK_ID" => "2",
        "LINK_IBLOCK_TYPE" => "catalog",
        "LINK_PROPERTY_SID" => "",
        "LIST_BROWSER_TITLE" => "-",
        "LIST_META_DESCRIPTION" => "-",
        "LIST_META_KEYWORDS" => "-",
        "LIST_PROPERTY_CODE" => array(0=>"",1=>"UF_3D_PRICE",2=>"UF_RASKLADKA",),
        "MESSAGE_404" => "",
        "MESS_BTN_ADD_TO_BASKET" => "В корзину",
        "MESS_BTN_BUY" => "Купить",
        "MESS_BTN_COMPARE" => "Сравнение",
        "MESS_BTN_DETAIL" => "Подробнее",
        "MESS_NOT_AVAILABLE" => "Нет в наличии",
        "ORDER_PROP_3" => "",
        "PAGER_BASE_LINK_ENABLE" => "N",
        "PAGER_DESC_NUMBERING" => "N",
        "PAGER_DESC_NUMBERING_CACHE_TIME" => "36000",
        "PAGER_SHOW_ALL" => "N",
        "PAGER_SHOW_ALWAYS" => "N",
        "PAGER_TEMPLATE" => ".default",
        "PAGER_TITLE" => "Товары",
        "PAGE_ELEMENT_COUNT" => "30",
        "PARTIAL_PRODUCT_PROPERTIES" => "N",
        "PERSONAL_MOBILE" => "",
        "PRICE_CODE" => array(),
        "PRICE_VAT_INCLUDE" => "Y",
        "PRICE_VAT_SHOW_VALUE" => "N",
        "PRODUCT_ID_VARIABLE" => "id",
        "PRODUCT_PROPERTIES" => array(),
        "PRODUCT_PROPS_VARIABLE" => "prop",
        "PRODUCT_QUANTITY_VARIABLE" => "quantity",
        "REGISTER[PERSONAL_MOBILE]" => "",
        "SECTIONS_SHOW_PARENT_NAME" => "Y",
        "SECTIONS_VIEW_MODE" => "LIST",
        "SECTION_ADD_TO_BASKET_ACTION" => "BUY",
        "SECTION_BACKGROUND_IMAGE" => "-",
        "SECTION_COUNT_ELEMENTS" => "Y",
        "SECTION_ID_VARIABLE" => "SECTION_ID",
        "SECTION_TOP_DEPTH" => "2",
        "SEF_FOLDER" => "/howto/varianty-raskladki/",
        "SEF_MODE" => "Y",
        "SEF_URL_TEMPLATES" => array("sections"=>"","section"=>"#SECTION_CODE#/","element"=>"#SECTION_CODE#/#ELEMENT_CODE#/","compare"=>"compare.php?action=#ACTION_CODE#","smart_filter"=>"#SECTION_CODE#/filter/#SMART_FILTER_PATH#/apply/",),
        "SET_LAST_MODIFIED" => "N",
        "SET_STATUS_404" => "Y",
        "SET_TITLE" => "Y",
        "SHOW_404" => "N",
        "SHOW_DEACTIVATED" => "N",
        "SHOW_DISCOUNT_PERCENT" => "N",
        "SHOW_OLD_PRICE" => "N",
        "SHOW_PRICE_COUNT" => "1",
        "SHOW_TOP_ELEMENTS" => "N",
        "TEMPLATE_THEME" => "site",
        "TOP_ADD_TO_BASKET_ACTION" => "BUY",
        "USE_ALSO_BUY" => "N",
        "USE_BIG_DATA" => "Y",
        "USE_COMMON_SETTINGS_BASKET_POPUP" => "N",
        "USE_COMPARE" => "N",
        "USE_ELEMENT_COUNTER" => "Y",
        "USE_FILTER" => "N",
        "USE_MAIN_ELEMENT_SECTION" => "N",
        "USE_PRICE_COUNT" => "N",
        "USE_PRODUCT_QUANTITY" => "N",
        "USE_REVIEW" => "N",
        "USE_SALE_BESTSELLERS" => "N",
        "USE_STORE" => "N",
        "VARIABLE_ALIASES" => array("compare"=>array("ACTION_CODE"=>"action",),),
        "form_text_12" => "",
        "form_text_5" => "",
        "form_text_70" => "",
        "form_text_71" => ""
    )
);?><?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>