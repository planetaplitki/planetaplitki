<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetTitle("Информация для покупателей");
?><p class="bx_page">
</p>
<h2>Оформление заказа</h2>
<p>
 <span style="font-size: 11pt;">Уважаемые клиенты! Вы можете осуществить покупку любым из предложенных Вам способов:</span><br>
</p>
<h3>1. Оформление заказа в интернет-магазине</h3>
<p>
</p>
<p style="text-align: justify;">
</p>
<p>
	<span style="font-size: 11pt;">Найдите товар на сайте, который хотите заказать, укажите нужное количество и нажмите кнопку «Корзина». </span>
</p>
<span style="font-size: 11pt;"> </span>
<p>
	<span style="font-size: 11pt;">
	После того, как вы добавите весь необходимый товар в корзину, перейдите по ссылке «Оформить заказ». Или «Продолжить покупки», если Вы хотите продолжить выбор товара. </span>
</p>
<span style="font-size: 11pt;"> </span>
<p>
	<span style="font-size: 11pt;">
	После изменения количества товара необходимо нажать кнопку&nbsp;«Пересчитать» для пересчета итоговой суммы заказа. Если выбор товара окончен, нажмите кнопку «Оформить заказ». </span>
</p>
<span style="font-size: 11pt;"> </span>
<p>
	<span style="font-size: 11pt;">
	Далее выберите способ оплаты, заполните контактную информацию, укажите адрес и способ доставки и нажмите кнопку «Оформить заказ». Информация о вашем заказе будет выслана вам на указанный e-mail. </span>
</p>
<span style="font-size: 11pt;"> </span>
<p>
	<span style="font-size: 11pt;"> </span><i><b><span style="font-size: 11pt;">Внимание!&nbsp;Неправильно указанный номер телефона, неточный или неполный адрес могут привести к дополнительной задержке! Пожалуйста, внимательно проверяйте ваши персональные данные при регистрации и оформлении заказа.</span></b></i><br>
	<span style="font-size: 11pt;"> </span>
</p>
<span style="font-size: 11pt;">
Наш менеджер свяжется с Вами в течении суток после оформления заявки.</span><br>
<span style="font-size: 11pt;"> </span><br>
<span style="font-size: 11pt;">
Пожалуйста,&nbsp;</span><b><a href="/login/?register=yes"><span style="font-size: 11pt;">зарегистрируйтесь</span></a><span style="font-size: 11pt;">,&nbsp;</span></b><span style="font-size: 11pt;">для того, чтобы вы могли просматривать содержимое корзины, историю своих заказов, повторить или отказаться от заказа,&nbsp;а также&nbsp;подписаться на рассылку новостей магазина.</span>
<p>
</p>
<p class="bx_page">
</p>
<p>
</p>
<h3>2. Оформление заказа по телефону</h3>
 <span style="font-size: 11pt;">Вы так же можете оформить заказ по телефону&nbsp;+7 (495) 646-16-90, доб. 1</span><br>
<h3>3. Оформление заказа в ближайшем магазине</h3>
<p style="text-align: justify;">
	<span style="font-size: 11pt;">Вы можете посетить один из наших магазинов и оформить заказ на месте.</span><br>
	<span style="font-size: 11pt;"> </span><span style="font-size: 11pt;"><a href="/contacts/">Выберите ближайший к Вам магазин</a></span>
</p>
 <br>
<hr class="line-block">
<h2>Оплата</h2>
<p>
 <b></b>
</p>
<p style="text-align: justify;">
	<b><span style="font-size: 11pt;">Внимание!</span></b><span style="font-size: 11pt;"> Для каждого отдельного заказа возможен только один способ оплаты на ваш выбор. Оплата заказа по частям различными способами невозможна. </span>
</p>
<p style="text-align: justify;">
	<span style="font-size: 11pt;">
	Возможные способы оплаты указаны на странице </span><a href="/delivery/"><span style="font-size: 11pt;">Доставка и оплата</span></a><span style="font-size: 11pt;">.</span>
</p>
 <br>
<hr class="line-block">
<h2>Возврат товара</h2>
<p style="text-align: justify;">
	<span style="font-size: 11pt;">Правила возврата товара:</span><br>
	<span style="font-size: 11pt;"> </span><br>
	<span style="font-size: 11pt;"> </span>
</p>
<ul style="text-align: justify;">
	<li><span style="font-size: 11pt;">Возврат Товара надлежащего качества возможен в случае, если сохранены его товарный вид, потребительские свойства, а также документ, подтверждающий факт и условия покупки Товара.</span></li>
	<li><span style="font-size: 11pt;">
	Покупатель вправе не позднее 7 (семи) дней после получения Товара известить Продавца о нарушениях условий Договора, касающихся количества, ассортимента, качества, комплектности, упаковки и (или) тары Товара.</span></li>
	<li><span style="font-size: 11pt;">
	При отказе Покупателя от Товара Продавец должен возвратить ему денежную сумму, уплаченную Покупателем по настоящему Договору, за исключением расходов Продавца на доставку от Покупателя возвращенного Товара, не позднее чем через 10 (десять) дней со дня предъявления Покупателем соответствующего требования.</span></li>
</ul>
<span style="font-size: 11pt;"> </span><br>
<span style="font-size: 11pt;">
Пожалуйста, ознакомьтесь более подробно с </span><a href="/vozvrat/"><span style="font-size: 11pt;">правилами возврата товара</span></a><span style="font-size: 11pt;">, а так же со всеми </span><a href="/upload/documents/Оферта физ. лица ООО Ронда.docx"><span style="font-size: 11pt;">правилами покупки товара</span></a><a href="/upload/documents/Оферта физ. лица ООО Ронда.docx"></a><span style="font-size: 11pt;">.</span>
<p>
</p>
<br>
 <br><?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>