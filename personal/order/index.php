<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetTitle("Заказы");


//обрабатываем переход по кнопке оплатить(письмо)
if($_GET["utoken"] && $_GET["token"] && CModule::IncludeModule('sale'))
{                          
    $order_id = $_GET["token"] / 3;    
    //найдем пользователя по айдишнику заказа
    $db_sales = CSaleOrder::GetList(array("DATE_INSERT" => "DESC"), array("ID" => $order_id));
    if ($ar_sales = $db_sales->Fetch())
    {         
        $USER_ID = $ar_sales["USER_ID"]; 

        //проверим, не админ ли этот пользователь
        $arGroups = CUser::GetUserGroup($USER_ID);
          
        if(in_array(1, $arGroups)) 
        {
            die('access denided');      
        }       
        
        //если код доступа совпал, авторизуем пользователя
        if($USER_ID == $_GET["utoken"])
        {
            $USER->Authorize($USER_ID);    
        }       
    }       
}
?>

<div class="right">
	<?$APPLICATION->IncludeComponent("bitrix:sale.personal.order", "order", Array(
			"SEF_MODE" => "Y",	// Включить поддержку ЧПУ
			"SEF_FOLDER" => "/personal/order/",	// Каталог ЧПУ (относительно корня сайта)
			"ORDERS_PER_PAGE" => "10",	// Количество заказов на одной странице
			"PATH_TO_PAYMENT" => "/personal/order/payment/",	// Страница подключения платежной системы
			"PATH_TO_BASKET" => "/personal/cart/",	// Страница с корзиной
			"SET_TITLE" => "Y",	// Устанавливать заголовок страницы
			"SAVE_IN_SESSION" => "N",	// Сохранять установки фильтра в сессии пользователя
			"NAV_TEMPLATE" => "arrows",	// Имя шаблона для постраничной навигации
			"SEF_URL_TEMPLATES" => array(
				"list" => "index.php",
				"detail" => "detail/#ID#/",
				"cancel" => "cancel/#ID#/",
			),
			"SHOW_ACCOUNT_NUMBER" => "Y"
		),
		false
	);?>
</div>

<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>