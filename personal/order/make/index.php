<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetTitle("Заказы");
?>

<div class="cart_page">
    <?$idOrder = intval($_REQUEST['ORDER_ID']);?>
    <div class="col-sm-12">
        <ul class="nav nav-tabs" role="tablist">
            <li ><a href="/personal/cart/" role="tab" >1. Корзина</a></li>
            <li class="<?=!$idOrder ? 'active' : '';?>"><a href="/personal/order/make/" role="tab" >2. Оформление</a></li>
            <?if( $idOrder ){?>
                <li class="active"><a href="#zakaz" role="tab">3. Заказ сформирован</a></li>
            <?}?>
        </ul>

        <div class="tab-content">
            <div class="tab-pane fade active in" id="oformlenie">
                <div class="box-inner">
					<?
//					$_REQUEST['ORDER_PROP_29'] = 66666666;
					?>
                    <?$APPLICATION->IncludeComponent(
						"bitrix:sale.order.ajax",
						"simple",
						array(
							"PAY_FROM_ACCOUNT" => "N",
							"COUNT_DELIVERY_TAX" => "N",
							"COUNT_DISCOUNT_4_ALL_QUANTITY" => "N",
							"ONLY_FULL_PAY_FROM_ACCOUNT" => "N",
							"ALLOW_AUTO_REGISTER" => "Y",
							"SEND_NEW_USER_NOTIFY" => "Y",
							"DELIVERY_NO_AJAX" => "N",
							"TEMPLATE_LOCATION" => "popup",
							"PROP_1" => array(
							),
							"PATH_TO_BASKET" => "/personal/cart/",
							"PATH_TO_PERSONAL" => "/personal/order/",
							"PATH_TO_PAYMENT" => "/personal/order/payment/",
							"PATH_TO_ORDER" => "/personal/order/make/",
							"SET_TITLE" => "Y",
							"DELIVERY2PAY_SYSTEM" => "",
							"SHOW_ACCOUNT_NUMBER" => "Y",
							"DELIVERY_NO_SESSION" => "N",
							"DELIVERY_TO_PAYSYSTEM" => "d2p",
							"USE_PREPAYMENT" => "N",
							"PROP_2" => array(
							),
							"ALLOW_NEW_PROFILE" => "N",
							"SHOW_PAYMENT_SERVICES_NAMES" => "Y",
							"SHOW_STORES_IMAGES" => "N",
							"PATH_TO_AUTH" => "/auth/",
							"DISABLE_BASKET_REDIRECT" => "N",
							"PRODUCT_COLUMNS" => array(
							),
							"COMPONENT_TEMPLATE" => "simple"
						),
						false
					);?>
                </div>
            </div>
        </div>
    </div>
</div>

<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>