<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetTitle("Настройки пользователя");
?>

<div class="regblock_right">
	<div role="tabpanel">

		<!-- Tab panes -->
		<div class="regblock__tabs__content tab-content">
			<div role="tabpanel" class="tab-pane active" id="regdata">

				<?$APPLICATION->IncludeComponent(
					"bitrix:main.profile",
					"personal",
					array(
						"SET_TITLE" => "Y",
						"COMPONENT_TEMPLATE" => "personal",
						"AJAX_MODE" => "N",
						"AJAX_OPTION_JUMP" => "N",
						"AJAX_OPTION_STYLE" => "Y",
						"AJAX_OPTION_HISTORY" => "N",
						"AJAX_OPTION_ADDITIONAL" => "",
						"USER_PROPERTY" => array(
						),
						"SEND_INFO" => "N",
						"CHECK_RIGHTS" => "N",
						"USER_PROPERTY_NAME" => ""
					),
					false
				);?>




			</div>
		</div>
	</div>
</div>

<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>