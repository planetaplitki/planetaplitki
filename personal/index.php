<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetTitle("Персональный раздел");
?><?
global $USER;
?>
<div class="bx_page">
    <p>
         В личном кабинете Вы можете проверить текущее состояние корзины, ход выполнения Ваших заказов, просмотреть или изменить личную информацию, а также подписаться на новости и другие информационные рассылки.
    </p>
    <p>
 <br>
    </p>
    <p>
        <br>
    </p>
     <?if( !$USER->IsAuthorized() ){?>
    <div class="registerLinks">
 <a href="/login/" rel="nofollow">Войти</a> <a href="/login/?register=yes" rel="nofollow">Зарегистрироваться</a>
    </div>
     <?}?>
</div>
 <br><?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>