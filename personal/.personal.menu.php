<?
$aMenuLinks = Array(
	Array(
		"Личные данные", 
		"/personal/profile/", 
		Array(), 
		Array("key"=>"Личная информация"), 
		"" 
	),
	Array(
		"Состояние заказов", 
		"/personal/order/state/", 
		Array(), 
		Array("key"=>"Заказы"), 
		"" 
	),
	Array(
		"Содержание корзины", 
		"/personal/cart/", 
		Array(), 
		Array("key"=>"Заказы"), 
		"" 
	),
	Array(
		"История заказов", 
		"/personal/order/history/", 
		Array(), 
		Array("key"=>"Заказы"), 
		"" 
	),
	Array(
		"Изменить подписку", 
		"/personal/subscribe/", 
		Array(), 
		Array("key"=>"Подписка"), 
		"" 
	),
);

if( CSite::InGroup(array(9)) ){
	$aMenuLinks[] = array(
		"Документы",
		"/personal/documents/",
		Array(),
		Array("key"=>"Документы"),
		""
	);
}

?>