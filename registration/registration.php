<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetTitle("Регистрация");
?>

    <div class="lc_left">
        <div class="lc_left_box">
            <a class="lc_left_box_p">Личная информация<span></span></a>
            <ul>
                <li><a>Личные данные</a></li>
            </ul>
        </div>
        <div class="lc_left_box">
            <a class="lc_left_box_p">Заказы<span></span></a>
            <ul>
                <li><a>Состояние заказов</a></li>
                <li><a>Содержание корзины</a></li>
                <li><a>История заказов</a></li>
            </ul>
        </div>
        <div class="lc_left_box">
            <a class="lc_left_box_p">Подписка<span></span></a>
            <ul>
                <li><a>Изменить подписку</a></li>
            </ul>
        </div>
    </div>

    <div class="regblock_right_all">
        <div role="tabpanel">

            <div class="regblock__tabs__fon regblock__tabs__fon2">
                <!-- Nav tabs -->
                <ul class="nav nav-tabs" role="tablist">
                    <li role="presentation" class=" active"><a href="#individual" aria-controls="individual" role="tab" data-toggle="tab">Физическое лицо</a></li>
                    <li role="presentation" class=""><a href="#individual_entrepreneur" aria-controls="individual_entrepreneur" role="tab" data-toggle="tab">Индивидуальный предприниматель</a></li>
                    <li role="presentation" class=""><a href="#legal_person" aria-controls="legal_person" role="tab" data-toggle="tab">Юридическое лицо</a></li>
                </ul>
            </div>


            <!-- Tab panes -->
            <div class="regblock__tabs__content tab-content">
                <div role="tabpanel" class="tab-pane active" id="individual">

                    <form class="regblock__tabs__content__form2">

                        <hr class="line-form-sep">
                        <p class="form__title">Контактные данные</p>

                        <div class="form-group regblock__form-group">
                            <div class="row">
                                <div class="col-xs-5"><label for="">Фамилия<span>*</span></label></div>
                                <div class="col-xs-7"><input type="text" class="form-control regblock__form_control" id=""></div>
                            </div>
                        </div>
                        <div class="form-group regblock__form-group">
                            <div class="row">
                                <div class="col-xs-5"><label for="">Имя<span>*</span></label></div>
                                <div class="col-xs-7"><input type="text" class="form-control regblock__form_control" id=""></div>
                            </div>
                        </div>
                        <div class="form-group regblock__form-group">
                            <div class="row">
                                <div class="col-xs-5"><label for="">Отчество</label></div>
                                <div class="col-xs-7"><input type="text" class="form-control regblock__form_control" id=""></div>
                            </div>
                        </div>
                        <div class="form-group regblock__form-group">
                            <div class="row">
                                <div class="col-xs-5"><label for="">Электронная почта<span>*</span></label></div>
                                <div class="col-xs-7"><input type="text" class="form-control regblock__form_control" id=""></div>
                            </div>
                        </div>
                        <div class="form-group regblock__form-group">
                            <div class="row">
                                <div class="col-xs-5"><label for="">Телефон<span>*</span></label></div>
                                <div class="col-xs-7"><input type="text" class="form-control regblock__form_control" id=""></div>
                            </div>
                        </div>

                        <hr class="line-form-sep">
                        <p class="form__title">Паспортные данные</p>

                        <div class="form-group regblock__form-group">
                            <div class="row">
                                <div class="col-xs-5"><label for="">Дата рождения<span>*</span></label></div>
                                <div class="col-xs-7"><input type="text" class="form-control regblock__form_control" id=""></div>
                            </div>
                        </div>
                        <div class="form-group regblock__form-group">
                            <div class="row">
                                <div class="col-xs-5"><label for="">Серия и номер<span>*</span></label></div>
                                <div class="col-xs-7"><input type="text" class="form-control regblock__form_control" id=""></div>
                            </div>
                        </div>
                        <div class="form-group regblock__form-group">
                            <div class="row">
                                <div class="col-xs-5"><label for="">Кем выдан<span>*</span></label></div>
                                <div class="col-xs-7"><input type="text" class="form-control regblock__form_control" id=""></div>
                            </div>
                        </div>
                        <div class="form-group regblock__form-group">
                            <div class="row">
                                <div class="col-xs-5"><label for="">Дата выдачи<span>*</span></label></div>
                                <div class="col-xs-7"><input type="text" class="form-control regblock__form_control" id=""></div>
                            </div>
                        </div>

                        <hr class="line-form-sep">
                        <p class="form__title">Адрес регистрации</p>

                        <div class="form-group regblock__form-group">
                            <div class="row">
                                <div class="col-xs-5"><label for="">Адрес</label></div>
                                <div class="col-xs-7"><input type="text" class="form-control regblock__form_control" id=""></div>
                            </div>
                        </div>

                        <hr class="line-form-sep">

                        <button type="submit" class="btn regblock__form_btn2">Добавить<span></span></button>
                    </form>

                </div>
                <div role="tabpanel" class="tab-pane" id="individual_entrepreneur">
                    <form class="regblock__tabs__content__form2">

                        <hr class="line-form-sep">
                        <p class="form__title">Контактные данные</p>

                        <div class="form-group regblock__form-group">
                            <div class="row">
                                <div class="col-xs-5"><label for="">Фамилия<span>*</span></label></div>
                                <div class="col-xs-7"><input type="text" class="form-control regblock__form_control" id=""></div>
                            </div>
                        </div>
                        <div class="form-group regblock__form-group">
                            <div class="row">
                                <div class="col-xs-5"><label for="">Имя<span>*</span></label></div>
                                <div class="col-xs-7"><input type="text" class="form-control regblock__form_control" id=""></div>
                            </div>
                        </div>
                        <div class="form-group regblock__form-group">
                            <div class="row">
                                <div class="col-xs-5"><label for="">Отчество</label></div>
                                <div class="col-xs-7"><input type="text" class="form-control regblock__form_control" id=""></div>
                            </div>
                        </div>
                        <div class="form-group regblock__form-group">
                            <div class="row">
                                <div class="col-xs-5"><label for="">Электронная почта<span>*</span></label></div>
                                <div class="col-xs-7"><input type="text" class="form-control regblock__form_control" id=""></div>
                            </div>
                        </div>
                        <div class="form-group regblock__form-group">
                            <div class="row">
                                <div class="col-xs-5"><label for="">Телефон<span>*</span></label></div>
                                <div class="col-xs-7"><input type="text" class="form-control regblock__form_control" id=""></div>
                            </div>
                        </div>

                        <hr class="line-form-sep">
                        <p class="form__title">Данные об индивидуальном предпринимателе</p>

                        <div class="form-group regblock__form-group">
                            <div class="row">
                                <div class="col-xs-5"><label for="">ИНН<span>*</span></label></div>
                                <div class="col-xs-7"><input type="text" class="form-control regblock__form_control" id=""></div>
                            </div>
                        </div>
                        <div class="form-group regblock__form-group">
                            <div class="row">
                                <div class="col-xs-5"><label for="">Св-во о регистрации ИП<span>*</span></label></div>
                                <div class="col-xs-7"><input type="text" class="form-control regblock__form_control" id=""></div>
                            </div>
                        </div>

                        <hr class="line-form-sep">
                        <p class="form__title">Адрес регистрации</p>

                        <div class="form-group regblock__form-group">
                            <div class="row">
                                <div class="col-xs-5"><label for="">Адрес</label></div>
                                <div class="col-xs-7"><input type="text" class="form-control regblock__form_control" id=""></div>
                            </div>
                        </div>

                        <hr class="line-form-sep">
                        <p class="form__title">Почтовый адрес</p>

                        <div class="form-group regblock__form-group">
                            <div class="row">
                                <div class="col-xs-5"><label for="">Адрес</label></div>
                                <div class="col-xs-7"><input type="text" class="form-control regblock__form_control" id=""></div>
                            </div>
                        </div>

                        <hr class="line-form-sep">

                        <button type="submit" class="btn regblock__form_btn2">Добавить<span></span></button>
                    </form>
                </div>

                <div role="tabpanel" class="tab-pane" id="legal_person">
                    <form class="regblock__tabs__content__form2">

                        <hr class="line-form-sep">
                        <p class="form__title">Контактные данные</p>

                        <div class="form-group regblock__form-group">
                            <div class="row">
                                <div class="col-xs-5"><label for="">Фамилия<span>*</span></label></div>
                                <div class="col-xs-7"><input type="text" class="form-control regblock__form_control" id=""></div>
                            </div>
                        </div>
                        <div class="form-group regblock__form-group">
                            <div class="row">
                                <div class="col-xs-5"><label for="">Имя<span>*</span></label></div>
                                <div class="col-xs-7"><input type="text" class="form-control regblock__form_control" id=""></div>
                            </div>
                        </div>
                        <div class="form-group regblock__form-group">
                            <div class="row">
                                <div class="col-xs-5"><label for="">Отчество</label></div>
                                <div class="col-xs-7"><input type="text" class="form-control regblock__form_control" id=""></div>
                            </div>
                        </div>
                        <div class="form-group regblock__form-group">
                            <div class="row">
                                <div class="col-xs-5"><label for="">Электронная почта<span>*</span></label></div>
                                <div class="col-xs-7"><input type="text" class="form-control regblock__form_control" id=""></div>
                            </div>
                        </div>
                        <div class="form-group regblock__form-group">
                            <div class="row">
                                <div class="col-xs-5"><label for="">Телефон<span>*</span></label></div>
                                <div class="col-xs-7"><input type="text" class="form-control regblock__form_control" id=""></div>
                            </div>
                        </div>

                        <hr class="line-form-sep">
                        <p class="form__title">Данные о компании</p>

                        <div class="form-group regblock__form-group">
                            <div class="row">
                                <div class="col-xs-5"><label for="">Название организации<span>*</span></label></div>
                                <div class="col-xs-7"><input type="text" class="form-control regblock__form_control" id=""></div>
                            </div>
                        </div>
                        <div class="form-group regblock__form-group">
                            <div class="row">
                                <div class="col-xs-5"><label for="">ИНН<span>*</span></label></div>
                                <div class="col-xs-7"><input type="text" class="form-control regblock__form_control" id=""></div>
                            </div>
                        </div>
                        <div class="form-group regblock__form-group">
                            <div class="row">
                                <div class="col-xs-5"><label for="">КПП<span>*</span></label></div>
                                <div class="col-xs-7"><input type="text" class="form-control regblock__form_control" id=""></div>
                            </div>
                        </div>
                        <div class="form-group regblock__form-group">
                            <div class="row">
                                <div class="col-xs-5"><label for="">ФИО руководителя</label></div>
                                <div class="col-xs-7"><input type="text" class="form-control regblock__form_control" id=""></div>
                            </div>
                        </div>
                        <div class="form-group regblock__form-group">
                            <div class="row">
                                <div class="col-xs-5"><label for="">Должность руководителя</label></div>
                                <div class="col-xs-7"><input type="text" class="form-control regblock__form_control" id=""></div>
                            </div>
                        </div>
                        <div class="form-group regblock__form-group">
                            <div class="row">
                                <div class="col-xs-5"><label for="">Расчетный счет</label></div>
                                <div class="col-xs-7"><input type="text" class="form-control regblock__form_control" id=""></div>
                            </div>
                        </div>
                        <div class="form-group regblock__form-group">
                            <div class="row">
                                <div class="col-xs-5"><label for="">Наименование банка</label></div>
                                <div class="col-xs-7"><input type="text" class="form-control regblock__form_control" id=""></div>
                            </div>
                        </div>
                        <div class="form-group regblock__form-group">
                            <div class="row">
                                <div class="col-xs-5"><label for="">БИК</label></div>
                                <div class="col-xs-7"><input type="text" class="form-control regblock__form_control" id=""></div>
                            </div>
                        </div>
                        <div class="form-group regblock__form-group">
                            <div class="row">
                                <div class="col-xs-5"><label for="">Корреспондентский счет</label></div>
                                <div class="col-xs-7"><input type="text" class="form-control regblock__form_control" id=""></div>
                            </div>
                        </div>

                        <hr class="line-form-sep">
                        <p class="form__title">Юридический адрес</p>

                        <div class="form-group regblock__form-group">
                            <div class="row">
                                <div class="col-xs-5"><label for="">Адрес</label></div>
                                <div class="col-xs-7"><input type="text" class="form-control regblock__form_control" id=""></div>
                            </div>
                        </div>

                        <hr class="line-form-sep">
                        <p class="form__title">Почтовый адрес</p>

                        <div class="form-group regblock__form-group">
                            <div class="row">
                                <div class="col-xs-5"><label for="">Адрес</label></div>
                                <div class="col-xs-7"><input type="text" class="form-control regblock__form_control" id=""></div>
                            </div>
                        </div>

                        <hr class="line-form-sep">

                        <button type="submit" class="btn regblock__form_btn2">Добавить<span></span></button>
                    </form>
                </div>
            </div>

        </div>
    </div>

<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>