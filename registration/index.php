<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetTitle("Регистрация");
?>

<div class="regblock">
    <form class="regblock__content__form">
        <div class="form-group">
            <label for="">ФИО</label>
            <input type="text" class="form-control regblock__form_control" id="">
        </div>
        <div class="form-group">
            <label for="">E-mail</label>
            <input type="email" class="form-control regblock__form_control" id="">
        </div>
        <div class="form-group">
            <label for="">Телефон</label>
            <input type="text" class="form-control regblock__form_control" id="">
        </div>
        <div class="form-group">
            <label for="">Логин</label>
            <input type="text" class="form-control regblock__form_control" id="">
        </div>
        <div class="form-group">
            <label for="">Пароль</label>
            <input type="password" class="form-control regblock__form_control" id="">
        </div>
        <div class="form-group">
            <label for="">Повторите пароль</label>
            <input type="password" class="form-control regblock__form_control" id="">
        </div>
        <button type="submit" class="btn regblock__form_btn">Зарегистрироваться<span></span></button>
    </form>
</div>


<div class="regblock_main">
    <div class="lc_left">
        <div class="lc_left_box">
            <a class="lc_left_box_p">Личная информация<span></span></a>
            <ul>
                <li><a>Личные данные</a></li>
            </ul>
        </div>
        <div class="lc_left_box">
            <a class="lc_left_box_p">Заказы<span></span></a>
            <ul>
                <li><a>Состояние заказов</a></li>
                <li><a>Содержание корзины</a></li>
                <li><a>История заказов</a></li>
            </ul>
        </div>
        <div class="lc_left_box">
            <a class="lc_left_box_p">Подписка<span></span></a>
            <ul>
                <li><a>Изменить подписку</a></li>
            </ul>
        </div>
    </div>

    <div class="regblock_right">
        <div role="tabpanel">

            <div class="regblock__tabs__fon">
                <!-- Nav tabs -->
                <ul class="nav nav-tabs" role="tablist">
                    <li role="presentation" class="tabs_one active"><a href="#regdata" aria-controls="regdata" role="tab" data-toggle="tab">Регистрационные данные</a></li>
                    <li role="presentation" class="tabs_two"><a href="#kontragets" aria-controls="kontragets" role="tab" data-toggle="tab">Контрагенты</a></li>
                </ul>
            </div>


            <!-- Tab panes -->
            <div class="regblock__tabs__content tab-content">
                <div role="tabpanel" class="tab-pane active" id="regdata">

                    <form class="regblock__tabs__content__form">
                        <div class="form-group">
                            <label for="">ФИО</label>
                            <input type="text" class="form-control regblock__form_control" id="">
                        </div>
                        <div class="form-group">
                            <label for="">E-mail</label>
                            <input type="email" class="form-control regblock__form_control" id="">
                        </div>
                        <div class="form-group">
                            <label for="">Телефон</label>
                            <input type="text" class="form-control regblock__form_control" id="">
                        </div>
                        <div class="form-group">
                            <label for="">Логин</label>
                            <input type="text" class="form-control regblock__form_control" id="">
                        </div>
                        <div class="form-group">
                            <label for="">Пароль</label>
                            <input type="password" class="form-control regblock__form_control" id="">
                        </div>
                        <div class="form-group">
                            <label for="">Повторите пароль</label>
                            <input type="password" class="form-control regblock__form_control" id="">
                        </div>
                        <button type="submit" class="btn regblock__form_btn">Сохранить данные<span></span></button>
                    </form>

                </div>
                <div role="tabpanel" class="tab-pane" id="kontragets">
                    <p class="regblock__tabs__text">Для удобства Вы можете указать несколько профилей покупателей с адресами доставки (например, домашний и рабочий.)</p>
                    <table class="regblock__tabs__table">
                        <tr class="regblock__tabs__table__head">
                            <td style="width: 170px;">Название</td>
                            <td style="width: 170px;">Тип плательщика</td>
                            <td style="width: 65px;">Действия</td>
                        </tr>
                        <tr>
                            <td>Иванов</td>
                            <td>Физическое лицо</td>
                            <td>
                                <a href="#">Изменить</a>
                                <a href="#">Удалить</a>
                            </td>
                        </tr>
                    </table>
                    <a href="#" class="regblock__tabs__link">Добавить<span></span></a>
                </div>
            </div>

        </div>
    </div>
</div>


<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>