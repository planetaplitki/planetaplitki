<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetTitle("Избранное");
?>
<?global $USER;?>

<?if( USER_TYPE == 'retail' ){
    $price = array(
        'Розничные руб. шт для сайта',
        'Розничные руб. м2 для сайта',
    );
}
elseif( USER_TYPE == 'wholesale' ){
    $price = array(
        'Оптовые руб. шт для сайта',
        'Оптовые руб. м2 для сайта',
    );
}
?>

<?
$sortPrice = htmlspecialchars($_REQUEST['SORT']['PRICES']);
$sortOrder = $sortPrice ? $sortPrice : 'asc';
$sortField = USER_TYPE == 'retail' ? "UF_PRICE_RETAIL" : "UF_PRICE_WHOLESALE";
?>
<?

// получим куки и если сессия не определена, то запишем данные из кук в сессию
$FAVORITES_COOKIE = json_decode($APPLICATION->get_cookie("FAVORITES"), true);

$arElements = Array();
if(is_array($_SESSION['FAVORITES']) && !empty($_SESSION['FAVORITES'])) {
    foreach($_SESSION['FAVORITES'] as $value) {
        $arElements[] = $value;
    }
}else if(is_array($FAVORITES_COOKIE) && !empty($FAVORITES_COOKIE)) {
    foreach($FAVORITES_COOKIE as $value) {
        $arElements[] = $value;
    }
}
?>
<?if(!empty($arElements)) {?>
<div class="main2 rasprodaja_page">
    <div class="container obj">
        <div class="collections-page-list">
            <div class="row">
                <?if (!empty($arElements) && is_array($arElements))
                {
                    global $searchFilter;
                    $searchFilter = array(
                        "=ID" => $arElements,
                    ); ?>
                    <?$APPLICATION->IncludeComponent(
                        "up:iblock.section.list", 
                        "collections.list.fav", 
                        array(
                            "PRICE_CODE" => $price,
                            "SHOW_PARENT_NAME" => "Y",
                            "IBLOCK_TYPE" => "catalog",
                            "IBLOCK_ID" => "34",
                            "SECTION_CODE" => "",
                            "SECTION_URL" => "",
                            "COUNT_ELEMENTS" => "Y",
                            "TOP_DEPTH" => "3",
                            "SECTION_FIELDS" => array(
                                0 => "ID",
                                1 => "NAME",
                                2 => "DESCRIPTION",
                                3 => "PICTURE",
                                4 => "SECTION_PAGE_URL",
                                5 => "DEPTH_LEVEL",
                                6 => "IBLOCK_SECTION_ID",
                                7 => "",
                            ),
                            "SECTION_USER_FIELDS" => array(
                                0 => "UF_CATEGORY",
                                1 => "UF_NEW_COLLECTION",
                                2 => "UF_EXCLUSIVE",
                                3 => "UF_SALE",
                                4 => "UF_PRICE_RETAIL",
                                5 => "UF_PRICE_WHOLESALE",
                                6 => "UF_MORE_PHOTO",
                                7 => "",
                            ),
                            "ADD_SECTIONS_CHAIN" => "Y",
                            "CACHE_TYPE" => "N",
                            "CACHE_TIME" => "36000000",
                            "CACHE_NOTES" => "",
                            "CACHE_GROUPS" => "Y",
                            "COMPONENT_TEMPLATE" => "collections.list",
                            "FILTER_NAME" => "searchFilter",
                            "SORT_FIELD2" => $sortField,
                            "SORT_ORDER2" => $sortOrder,
                            "SORT_FIELD" => "UF_NEW_COLLECTION",
                            "SORT_ORDER" => "desc",
                            "URL_404" => "/404.php",
                            "ELEMENT_CONTROLS" => "N",
                            "AJAX_MODE" => "N",
                            "AJAX_OPTION_JUMP" => "N",
                            "AJAX_OPTION_STYLE" => "N",
                            "AJAX_OPTION_HISTORY" => "N",
                            "AJAX_OPTION_ADDITIONAL" => "",
                            "CACHE_FILTER" => "N",
                            "PAGER_TEMPLATE" => "modern",
                            "DISPLAY_TOP_PAGER" => "N",
                            "DISPLAY_BOTTOM_PAGER" => "Y",
                            "PAGER_TITLE" => "",
                            "PAGER_SHOW_ALWAYS" => "N",
                            "PAGER_DESC_NUMBERING" => "N",
                            "PAGER_DESC_NUMBERING_CACHE_TIME" => "36000",
                            "PAGE_ELEMENT_COUNT" => "80",
                            "PAGER_SHOW_ALL" => "N",
                            "UF_USER_TYPE" => USER_TYPE
                        ),
                        false
                    );?>
                    <?}?>
            </div>
        </div>
    </div>
</div>
<?}else {?>
   <h4 class="main__fav-empty">Вы еще ничего не добавили в избранное</h4>
<?}?>
<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>