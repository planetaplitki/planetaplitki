<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_before.php");

global $APPLICATION;

// получим куки
$FAVORITES_COOKIE = json_decode($APPLICATION->get_cookie("FAVORITES"), true);

// если сессия пустая, то скопируем куки в сессию
if($_SESSION['FAVORITES'] === NULL || empty($_SESSION['FAVORITES'])) {
    if(is_array($FAVORITES_COOKIE) && !empty($FAVORITES_COOKIE)) {
        foreach($FAVORITES_COOKIE as $value) {
            $_SESSION['FAVORITES'][] = $value;        
        }
    } else {
        $_SESSION['FAVORITES'] = Array();    
    }     
}

// обработка добавления и удаления элементов в избранном
$fav = htmlspecialchars($_REQUEST["FAV_ID"]);
$response["ELEMENT_IN_FAV"] = 0;

$index_fav = array_search($fav, $_SESSION['FAVORITES']);

if($index_fav === false || $index_fav === NULL) {
    $_SESSION['FAVORITES'][] = $fav;
    $response["ELEMENT_IN_FAV"] = 1;
} else {
    unset($_SESSION['FAVORITES'][$index_fav]); 
}

// установим куки, равными сессии
$APPLICATION->set_cookie("FAVORITES", json_encode($_SESSION['FAVORITES']), time() + 2592000*3, "/");

// счетчик для хедера
$count = count($_SESSION['FAVORITES']);
$response["COUNT"] = $count;

echo json_encode($response);
