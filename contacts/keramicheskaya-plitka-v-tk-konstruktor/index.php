<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetPageProperty("description", "Керамическая плитка в ТК Конструктор, купить плитку в Конструкторе, торговый центр \"Конструктор\" купить керамическую плитку");
$APPLICATION->SetPageProperty("title", "Керамическая плитка в ТК Конструктор");
$APPLICATION->SetTitle("Керамическая плитка в ТК Конструктор ");
?><div style="line-height: 1.6em;">
	<table>
	<tbody>
	<tr>
		<td>
			<p>
 <span style="font-size: small;"><strong><strong>5. ТК "Конструктор", 1-й этаж главного корпуса</strong></strong></span><br>
 <span style="color: #333333;">25-й км МКАД (внешняя сторона)</span><br>
 <img width="19" alt="телефон Планета Плитки контакты" src="https://planetaplitki.ru/upload/medialibrary/754/tel_icon.png" height="19" title="телефон Планета Плитки контакты" align="left" style="color: #333333;"><span style="color: #333333;">&nbsp; +7 (495) 280-78-57</span><br>
 <img width="17" alt="e-mail почта контакты Планета Плитки" src="https://planetaplitki.ru/upload/medialibrary/53b/mail_icon.png" height="17" title="e-mail почта контакты Планета Плитки" align="left" style="color: #333333;"><span style="color: #333333;">&nbsp;&nbsp;</span><a href="https://e.mail.ru/compose/?mailto=mailto%3akonstryktor@planetaplitki.ru" target="_blank" style="background: 0px 0px #ffffff;">konstryktor@planetaplitki.ru</a><br>
 <img width="19" alt="скайп контакты Планета Плитки" src="https://planetaplitki.ru/upload/medialibrary/d7d/skype_icon.png" height="19" title="скайп контакты Планета Плитки" align="left" style="color: #333333;"><span style="color: #333333;">&nbsp;&nbsp;&nbsp;</span><a href="skype:p_konstryktor" style="background: 0px 0px #ffffff;">p_konstryktor</a><br>
 <span style="color: #333333;">Ежедневно с 10:00 до 21:00</span><br>
			</p>
			<p>
 <span style="color: #555555;">GPS координаты:&nbsp;55.588069, 37.724300</span>
			</p>
			<p>
 <strong><span style="font-size: small;">Как доехать на общественном транспорте:</span> </strong><br>
 <span style="font-size: small;">от ст.м. «Домодедовская»&nbsp;</span> <br>
 <span style="font-size: small;">1. Маршрутное такси «Конструктор» (остановка напротив магазина «Снежная королева»)</span> <br>
 <span style="font-size: small;">2. Любое маршрутное такси до «ТРЦ Вегас»</span>
			</p>
		</td>
		<td>
 <a href="javascript:(print());"><img width="22" alt="pechat.png" src="/upload/medialibrary/87c/87cbaf10c2a637dc335558dd8ab744ca.png" height="22" title="pechat.png"></a>
		</td>
	</tr>
	<tr>
		<td colspan="2">
			<p>
 <span style="font-size: small;"><img width="749" alt="Керамическая плитка в ТК Конструктор" src="/upload/medialibrary/410/4100833e6038faf9d35e730a209c4d5d.jpg" height="545" title="Керамическая плитка в ТК Конструктор"></span>
			</p>
		</td>
	</tr>
	</tbody>
	</table>
</div>
 <br><?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>