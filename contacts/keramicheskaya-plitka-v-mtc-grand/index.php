<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetPageProperty("title", "Керамическая плитка \"Планета Плитки\" в МТЦ ГРАНД (Химки, Планерная)");
$APPLICATION->SetTitle("Керамическая плитка в МТЦ ГРАНД");
?><div style="line-height: 1.6em;">
	<table>
	<tbody>
	<tr>
		<td>
			<p>
 <span style="font-size: small;"><strong><strong>МТЦ "ГРАНД", 1 этаж Гранд-1</strong><br>
 </strong><span style="color: #333333;">м. Речной вокзал, м. Планерная, г.Химки, Ленинградское шоссе</span><br>
 <img width="19" alt="телефон Планета Плитки контакты" src="https://planetaplitki.ru/upload/medialibrary/754/tel_icon.png" height="19" title="телефон Планета Плитки контакты" align="left" style="color: #333333;"><span style="color: #333333;">&nbsp; +7 (495) 780-36-12&nbsp;</span><br>
 <img width="17" alt="e-mail почта контакты Планета Плитки" src="https://planetaplitki.ru/upload/medialibrary/53b/mail_icon.png" height="17" title="e-mail почта контакты Планета Плитки" align="left" style="color: #333333;"><span style="color: #333333;">&nbsp;&nbsp;</span><a href="https://e.mail.ru/compose/?mailto=mailto%3agrand@planetaplitki.ru" target="_blank" style="background: 0px 0px #ffffff;">grand@planetaplitki.ru</a><span style="color: #333333;"> </span><br>
 <img width="19" alt="скайп контакты Планета Плитки" src="https://planetaplitki.ru/upload/medialibrary/d7d/skype_icon.png" height="19" title="скайп контакты Планета Плитки" align="left" style="color: #333333;"><span style="color: #333333;">&nbsp;&nbsp;&nbsp;</span><a href="skype:tccgrand" style="background: 0px 0px #ffffff;">tccgrand</a><br>
 <span style="color: #333333;">Ежедневно с 10:00 до 21:00</span><br>
 </span>
			</p>
			<p>
 <span style="color: #555555;">GPS координаты:&nbsp;55.886691, 37.436557</span>
			</p>
		</td>
		<td>
 <a href="javascript:(print());"><img width="22" alt="pechat.png" src="/upload/medialibrary/87c/87cbaf10c2a637dc335558dd8ab744ca.png" height="22" title="pechat.png"></a>
		</td>
	</tr>
	<tr>
		<td colspan="2">
			<p>
 <img width="100%" alt="Керамическая плитка в МТЦ ГРАНД" src="/upload/medialibrary/456/456e4f0a5bd9f30a7cdd4a54d6656da2.jpg" title="Керамическая плитка в МТЦ ГРАНД">
			</p>
			<p>
				 &nbsp;
			</p>
		</td>
	</tr>
	</tbody>
	</table>
</div>
 <br><?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>