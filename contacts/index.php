<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetPageProperty("description", "Москва, Нахимовский пр-т, 50. Тел. +7 (495) 646-16-90");
$APPLICATION->SetPageProperty("keywords", "Адреса и телефоны магазинов \"Планета Плитки\"");
$APPLICATION->SetTitle("Контакты");
?><div style="font-size: 15px;">
	<table style="line-height: 1.6em; width: 95%;" cellspacing="10" cellpadding="10" class="easy">
	<tbody>
	<tr>
		<td colspan="2">
			 &nbsp; &nbsp; &nbsp;&nbsp;
		</td>
	</tr>
	<tr>
		<td colspan="2" style="width: 50%; text-align: left;" valign="top">
			<p style="line-height: 1.2em;">
 <span style="font-size: 18pt; color: #000000;"><span class="mgo-number-13206">+7 (495) 646-16-90</span><br>
				<br>
				 &nbsp; 8 (800) 500-82-29 <br>
				<span style="font-size: 10pt; color: #707070;">&nbsp; &nbsp; Звонок по России бесплатный</span></span>
			</p>
			<p style="line-height: 1.2em;">
 <span style="font-size: 18pt; color: #000000;"><br>
 </span>
			</p>
		</td>
	</tr>
	<tr>
		<td colspan="2" style="width: 50%; text-align: left;" valign="top">
 <b><span style="color: #450e61;">Viber</span><span style="color: #959595;">/</span></b><b><span style="color: #37b44a;">WhatsApp</span><span style="color: #959595;">/</span><span style="color: #00aeef;">Telegram</span>:</b> <span style="color: #262626;">+7 (966) 389-25-38</span><br>
		</td>
	</tr>
	<tr>
		<td colspan="2">
			<hr>
		</td>
	</tr>
	<tr>
		<td colspan="2">
 <br>
		</td>
	</tr>
	<tr>
		<td style="width: 50%; text-align: left;" valign="top">
			<h4>
			Салон-магазин "Планета Плитки" </h4>
			 г. Москва, Нахимовский проспект, д. 50<br>
 <img width="19" alt="телефон Планета Плитки контакты" src="/upload/medialibrary/754/tel_icon.png" height="19" title="телефон Планета Плитки контакты" align="left">&nbsp; &nbsp;+7 (495) 646-16-90, доб. 1<br>
 <img width="17" alt="e-mail почта контакты Планета Плитки" src="/upload/medialibrary/53b/mail_icon.png" height="17" title="e-mail почта контакты Планета Плитки" align="left">&nbsp; &nbsp;<a href="mailto:nahim@planetaplitki.ru" class="email">nahim@planetaplitki.ru</a><br>
 <img width="19" alt="скайп контакты Планета Плитки" src="/upload/medialibrary/d7d/skype_icon.png" height="19" title="скайп контакты Планета Плитки" align="left">&nbsp; &nbsp; 
			<!--noindex--><a href="skype:zalnaxim" rel="nofollow" class="skype">zalnaxim</a><!--/noindex--><br>
			 Пн-Вс&nbsp;10:00 - 20:00<br>
 <a href="/contacts/keramicheskaya-plitka-na-nahimovskom-prospekte/">Схема проезда</a><br>
 <b><br>
 </b><b>Заказ <a href="/howto/varianty-raskladki/">3d&nbsp;дизайн-проекта</a>:</b><br>
			 Елена&nbsp; +7 (495) 646-16-90, доб. 970<br>
			 Ольга&nbsp; +7 (495) 646-16-90, доб. 955<br>
			 <?$APPLICATION->IncludeComponent(
	"coffeediz:schema.org.OrganizationAndPlace",
	"",
	Array(
		"ADDRESS" => "Нахимовский пр-т, 50",
		"COUNTRY" => "Россия",
		"DESCRIPTION" => "Официальный поставщик керамической плитки из Испании и Италии. Магазин 310 кв.м.",
		"EMAIL" => array("nahim@planetaplitki.ru"),
		"FAX" => "",
		"ITEMPROP" => "",
		"LOCALITY" => "Москва",
		"LOGO" => "",
		"NAME" => "Планета Плитки",
		"PARAM_RATING_SHOW" => "N",
		"PHONE" => array("+7 (495) 646-16-90"),
		"POST_CODE" => "",
		"REGION" => "",
		"SHOW" => "Y",
		"SITE" => "planetaplitki.ru",
		"TAXID" => "",
		"TYPE" => "Organization",
		"TYPE_2" => ""
	)
);?><br>
 <span style="font-size: small;"><span style="color: #3366ff;"><span style="color: #000000;"><a href="mailto:designer@planetaplitki.ru"></a></span></span></span>
		</td>
		<td style="width: 50%;" valign="top">
			<h4> <a href="/dizayneram-intererov/" target="_blank">Отдел по работе с архитекторами и дизайнерами:</a><br>
 </h4>
			<p>
				 г. Москва, Нахимовский проспект, д.&nbsp;50<br>
 <img width="19" alt="телефон Планета Плитки контакты" src="/upload/medialibrary/754/tel_icon.png" height="19" title="телефон Планета Плитки контакты" align="left">&nbsp; +7 (495) 646-16-90, доб. 956<br>
 <img width="17" alt="e-mail почта контакты Планета Плитки" src="/upload/medialibrary/53b/mail_icon.png" height="17" title="e-mail почта контакты Планета Плитки" align="left">&nbsp; &nbsp;<a href="mailto:gavrikova@planetaplitki.ru">gavrikova@planetaplitki.ru</a><br>
			</p>
		</td>
	</tr>
	<tr>
		<td colspan="2">
			<hr>
			<h4><span style="line-height: 1.6em;">Выставки:</span></h4>
			<p>
 <strong>Менеджер выставок</strong><br>
 <img width="19" alt="телефон Планета Плитки контакты" src="/upload/medialibrary/754/tel_icon.png" height="19" title="телефон Планета Плитки контакты" align="left">&nbsp;+7 (495) 646-16-90, доб. 958<br>
 <img width="17" alt="e-mail почта контакты Планета Плитки" src="/upload/medialibrary/53b/mail_icon.png" height="17" title="e-mail почта контакты Планета Плитки" align="left">&nbsp; &nbsp;<a href="mailto:dispetpp@planetaplitki.ru">dispetpp@planetaplitki.ru</a><br>
 <img width="19" alt="скайп контакты Планета Плитки" src="/upload/medialibrary/d7d/skype_icon.png" height="19" title="скайп контакты Планета Плитки">&nbsp;dispetpp
			</p>
		</td>
	</tr>
	<tr>
		<td valign="top">
 <b>1. ТК "ТВОЙ ДОМ", 1 этаж,&nbsp;14 ряд,&nbsp;<b>отдел плитки и сантехники</b><br>
 </b>
			м. Домодедовская, на 24км МКАД, внешняя сторона<br>
 <img width="19" alt="телефон Планета Плитки контакты" src="/upload/medialibrary/754/tel_icon.png" height="19" title="телефон Планета Плитки контакты" align="left">&nbsp; +7 (495) 926-37-39<br>
			 &nbsp; &nbsp; &nbsp; +7 (966) 389-25-37<br>
			 &nbsp; &nbsp; &nbsp; +7 (495) 646-16-90, доб. 943<br>
 <img width="17" alt="e-mail почта контакты Планета Плитки" src="/upload/medialibrary/53b/mail_icon.png" height="17" title="e-mail почта контакты Планета Плитки" align="left">&nbsp;&nbsp;<a href="https://e.mail.ru/compose/?mailto=mailto%3atdom@planetaplitki.ru" target="_blank">tdom@planetaplitki.ru</a><br>
 <img width="19" alt="скайп контакты Планета Плитки" src="/upload/medialibrary/d7d/skype_icon.png" height="19" title="скайп контакты Планета Плитки" align="left">&nbsp;&nbsp; 
			<!--noindex--><a href="skype:tvoyidom" rel="nofollow" class="skype">tvoyidom</a><!--/noindex--><br>
			 Ежедневно с 11:00 до 21:00<br>
 <a href="/contacts/keramicheskaya-plitka-m-domodedovskaya">Схема проезда</a>
		</td>
		<td>
 <b>2. МТЦ "ГРАНД", 1 этаж Гранд-1</b><br>
			 м. Речной вокзал, м. Планерная, г.Химки, Ленинградское шоссе<br>
 <img width="19" alt="телефон Планета Плитки контакты" src="/upload/medialibrary/754/tel_icon.png" height="19" title="телефон Планета Плитки контакты" align="left">&nbsp; +7 (495) 780-36-12 <br>
			 &nbsp; &nbsp; &nbsp; +7 (966) 389-25-29<br>
			 &nbsp; &nbsp; &nbsp; +7 (495) 646-16-90, доб. 942<br>
 <img width="17" alt="e-mail почта контакты Планета Плитки" src="/upload/medialibrary/53b/mail_icon.png" height="17" title="e-mail почта контакты Планета Плитки" align="left">&nbsp;&nbsp;<a href="https://e.mail.ru/compose/?mailto=mailto%3agrand@planetaplitki.ru" target="_blank">grand@planetaplitki.ru</a> <br>
 <img width="19" alt="скайп контакты Планета Плитки" src="/upload/medialibrary/d7d/skype_icon.png" height="19" title="скайп контакты Планета Плитки" align="left">&nbsp;&nbsp; 
			<!--noindex--><a href="skype:tccgrand" rel="nofollow" class="skype">tccgrand</a><!--/noindex--><br>
			 Ежедневно с 10:00 до 21:00<br>
 <a href="/contacts/keramicheskaya-plitka-v-mtc-grand">Схема проезда</a>
		</td>
	</tr>
	<tr>
		<td valign="top">
			<p>
			</p>
			<p>
 <strong>3.ТСК "Каширский Двор", 3 этаж, пав. 3-А12<br>
 </strong>м. Коломенская, м. Каширская, Каширское шоссе, д. 19, корпус 1&nbsp;<br>
 <img width="19" alt="телефон Планета Плитки контакты" src="/upload/medialibrary/754/tel_icon.png" height="19" title="телефон Планета Плитки контакты" align="left">&nbsp; +7 (985) 648-14-95<br>
				 &nbsp; &nbsp; &nbsp; +7 (495) 646-16-90, доб. 940&nbsp;&nbsp; <br>
 <img width="17" alt="e-mail почта контакты Планета Плитки" src="/upload/medialibrary/53b/mail_icon.png" height="17" title="e-mail почта контакты Планета Плитки" align="left">&nbsp;&nbsp;<a href="https://e.mail.ru/compose/?mailto=mailto%3akadvor1@planetaplitki.ru" target="_blank">kadvor1@planetaplitki.ru</a><br>
 <img width="19" alt="скайп контакты Планета Плитки" src="/upload/medialibrary/d7d/skype_icon.png" height="19" title="скайп контакты Планета Плитки" align="left">&nbsp;&nbsp; 
				<!--noindex--><a href="skype:live:kadvor1" rel="nofollow" class="skype">live:kadvor1</a><!--/noindex--><br>
				 Ежедневно с 09:00 до 21:00<strong><br>
 </strong>
			</p>
		</td>
		<td>
			<p>
 <strong>4. ТК "Конструктор", 1-й этаж главного корпуса, пав. 1.35.4</strong><br>
				 25-й км МКАД (внешняя сторона)<br>
 <img width="19" alt="телефон Планета Плитки контакты" src="/upload/medialibrary/754/tel_icon.png" height="19" title="телефон Планета Плитки контакты" align="left">&nbsp; +7 (495) 280-78-57<br>
				 &nbsp; &nbsp; &nbsp; &nbsp;+7 (495) 646-16-90, доб. 941<br>
 <img width="17" alt="e-mail почта контакты Планета Плитки" src="/upload/medialibrary/53b/mail_icon.png" height="17" title="e-mail почта контакты Планета Плитки" align="left">&nbsp;&nbsp;<a href="https://e.mail.ru/compose/?mailto=mailto%3akonstryktor@planetaplitki.ru" target="_blank">konstryktor@planetaplitki.ru</a><br>
 <img width="19" alt="скайп контакты Планета Плитки" src="/upload/medialibrary/d7d/skype_icon.png" height="19" title="скайп контакты Планета Плитки" align="left">&nbsp;&nbsp; 
				<!--noindex--><a href="skype:p_konstryktor" rel="nofollow" class="skype">p_konstryktor</a><!--/noindex--><br>
				 Ежедневно с 10:00 до 21:00<br>
 <a href="/contacts/keramicheskaya-plitka-v-tk-konstruktor">Схема проезда</a>
			</p>
		</td>
	</tr>
	</tbody>
	</table>
	<table style="line-height: 1.6em; width: 95%;" cellspacing="10" cellpadding="10">
	<tbody>
	<tr>
		<td style="width: 50%; text-align: left;" valign="top">
			<p>
			</p>
			<hr>
			<p>
 <b>"Планета Плитки" в г. Омске:</b><br>
				 г. Омск, ул. Лермонтова, д.57<br>
				 Тел.<em>:&nbsp;</em>(3812) 532-165
			</p>
			 г. Омск,&nbsp;ул. 10-летия Октября, д.182 корп.3<br>
			 Тел.: (3812) 729-358
		</td>
	</tr>
	</tbody>
	</table>
	<p>
	</p>
 <br>
	<h3 style="text-align: center;">Магазины "Планета Плитки" на карте в г. Москве</h3>
	<table cellspacing="10" cellpadding="0" align="center" width="100%">
	<tbody>
	<tr>
		<td align="center" valign="middle">
			 <iframe src="https://yandex.ru/map-widget/v1/?um=constructor%3A15906e828930675af0d775a39a468d14738b02225522a00b889a8b31251cdaba&amp;source=constructor" width="100%" height="386" frameborder="0"></iframe>
		</td>
	</tr>
	</tbody>
	</table>
	<p style="text-align: center;">
 <img width="100%" alt="Планета Плитки адрес" src="/upload/medialibrary/dcd/planetaplitki_adres.jpg" title="Планета Плитки адрес"><br>
 <span style="color: #555555;"><i>Вход в салон-магазин "Планета Плитки" по адресу Нахимовский проспект д. 50 </i></span>
	</p>
 <span style="color: #555555;"><i> </i></span><br>
	<h4 style="text-align: center;">Отдел оптовых продаж:<br>
 </h4>
	<p style="text-align: center;">
 <img width="19" alt="телефон Планета Плитки контакты" src="/upload/medialibrary/754/tel_icon.png" height="19" title="телефон Планета Плитки контакты">&nbsp;+7 (495) 215-21-87 <br>
		 &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; 8 (800) 500-82-29, доб. 2<br>
		 +7 (926) 018-90-25 (работает по субботам)<br>
 <img width="17" alt="e-mail почта контакты Планета Плитки" src="/upload/medialibrary/53b/mail_icon.png" height="17" title="e-mail почта контакты Планета Плитки">&nbsp;<a href="mailto:opt@planetaplitki.ru">opt@planetaplitki.ru</a>
	</p>
 <a href="mailto:opt@planetaplitki.ru"></a>
	<p>
	</p>
	<p class="easy">
 <a href="mailto:opt@planetaplitki.ru"></a><br>
	</p>
	<h3 style="text-align: center;">Наши представители в регионах</h3>
 <br>
	<table cellpadding="15" cellspacing="15" align="center" style="width: 80%;">
	<tbody>
	<tr>
		<td>
 <b>г. Санкт-Петербург</b>
			<p>
				 Магазины «КЕРОМАГ»<br>
				 Суворовский пр, д.38<br>
				 +7 (812) 6480208/09<br>
				 Богатырский пр, 14 ТЦ Интерио, секция 232<br>
				 +7 (812) 6480207
			</p>
			<p>
				 Салон «Элит Керамика» ул. 2-я Советская, 14<br>
				 +7 (812) 7175813, 9264996
			</p>
		</td>
		<td>
 <b>г. Тамбов</b> <br>
			 «Мир плитки»<br>
			 ул. Советская, д.191<br>
			 (4752) 504&nbsp;353, 533&nbsp;395;<br>
			 «Ареал Декор»<br>
			 ул. Кронштадтская, д.4А, корп 1<br>
			 (4752) 72 93 28
			<p>
			</p>
 <b>
			<p>
			</p>
 </b><br>
		</td>
		<td>
 <b>г. Воронеж</b>
			<p>
				 Салон плитки “СПАРК”<br>
				 ул.Хользунова, д.60Б<br>
				 (473) 227-21-57,&nbsp;227-21-55
			</p>
			<p>
				 Магазин “INTER ceramics”<br>
				 ул. Конструкторов, д.1&nbsp;<br>
				 (473) 233-36-86
			</p>
 <br>
		</td>
	</tr>
	<tr>
		<td>
 <b>г. Кострома</b>
			<p>
				 "Интерьерный дом "<br>
				 ул. Скворцова, д.7<br>
				 (4942) 30-09- 69
			</p>
			<p>
 <br>
			</p>
		</td>
		<td>
 <b> г. Белгород</b>
			<p>
				 "Керамическая Плитка"&nbsp;&nbsp;<br>
				 ул.Щорса, д.48<br>
				 (4722) 37-22-37
			</p>
			<p>
 <br>
			</p>
		</td>
		<td>
 <b>г. Иркутск</b>
			<p>
				 "Студия Интерьера"&nbsp;<br>
				 ул. Софьи Перовской, д.39<br>
				 (3952) 20-49- 20
			</p>
			<p>
 <br>
			</p>
		</td>
	</tr>
	<tr>
		<td>
 <b>г. Киров</b>
			<p>
				 Центр эксклюзивного интерьера "Динтера"<br>
				 ул. Труда, д.71<br>
				 (8332) 41-55- 77
			</p>
		</td>
		<td>
 <b>г. Оренбург</b>
			<p>
				 "Имола Керамика"&nbsp;&nbsp;<br>
				 ул.Одесская, д.114&nbsp;<br>
				 (3532)75-16-92
			</p>
		</td>
		<td>
 <b>г. Новокузнецк</b>
			<p>
				 "Салон Тепломир"&nbsp;<br>
				 ул.Кирова, д.3<br>
				 (3843) 74-03-74
			</p>
		</td>
	</tr>
	<tr>
		<td>
 <b>г. Нижний Новгород</b>
			<p>
				 "Салон Плитки"<br>
				 ул. Невзоровых, д.49<br>
				 (831) 419-58-51
			</p>
			<p>
			</p>
		</td>
		<td>
 <b>г. Сергиев Посад</b>
			<p>
				 "АКВАТИКА ХХI ВЕК"<br>
				 пр-т Красной Армии, д.218<br>
				 (496) 551-31-10<br>
				 (916) 100-05-21<br>
				 (915) 004-34-44
			</p>
			<p>
			</p>
		</td>
	</tr>
	</tbody>
	</table>
 <br>
	<p>
	</p>
	<p>
	</p>
 <br>
</div>
 <br><?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php")?>