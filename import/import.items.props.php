<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_before.php");
?>

<?
$file = $_SERVER['DOCUMENT_ROOT'].'/upload/1c_items/import.xml';

if(file_exists($file))
{
    $objXML = new CDataXML();
    $objXML->Load($file);
    $arResult = $objXML->GetArray();



    $items = $arResult['КоммерческаяИнформация']['#']['Каталог'][0]['#']['Товары'][0]['#']['Товар'];
//    $items = $arResult['КоммерческаяИнформация']['#']['Каталог'][0]['#']['Товары'];

//    pRU($items, 'all');
//    pRU($arResult, 'all');


    foreach( $items as $item ){
        $props = $item['#']['ЗначенияСвойств'][0]['#']['ЗначенияСвойства'];

        pRU($props, 'all');
    }


    /**
     * Возвращает список инфу по разделам
     * @param $arr
     * @return array
     */
    function getSectionsInfo( $arr, $sections = array() ){

        /*if( !eRU($sections) ){
            $sections = array();
        }*/

        if( eRU($arr) ){
            foreach( $arr as $data ){
                $groups = $data['#']['Группы'][0]['#']['Группа'];

                if( eRU($groups) ){

//                    if( !eRU($sections) ){
//                        $sections = getSectionsInfo($groups, $sections);
//                    }
//                    else{
//                        $sections = getSectionsInfo($groups, $sections);
//                    }
                    $section = array();
                    $section['ID'] = $data['#']['Ид'][0]['#'];
                    $section['NAME'] = $data['#']['Наименование'][0]['#'];
                    $section['DESCRIPTION'] = $data['#']['Описание'][0]['#'];
                    $section['PREVIEW_PICTURE'] = $data['#']['БитриксКартинка'][0]['#'];
                    $section['DETAIL_PICTURE'] = $data['#']['БитриксКартинкаДетальная'][0]['#'];

                    $sections[$section['ID']] = $section;

                    $sections = getSectionsInfo($groups, $sections);
                }
                else{

                    $section = array();
                    $props = array();
                    $section['ID'] = $data['#']['Ид'][0]['#'];
                    $section['NAME'] = $data['#']['Наименование'][0]['#'];
                    $section['DESCRIPTION'] = $data['#']['Описание'][0]['#'];
                    $section['PREVIEW_PICTURE'] = $data['#']['БитриксКартинка'][0]['#'];
                    $section['DETAIL_PICTURE'] = $data['#']['БитриксКартинкаДетальная'][0]['#'];

                    foreach( $data['#']['ЗначенияСвойства'] as $dataProp ){

                        $photoArr = array();
                        $propCode = $dataProp['#']['Ид'][0]['#'];
                        $propValue = $dataProp['#']['Значение'][0]['#'];

                        if( $propCode == 'UF_MORE_PHOTO' ){
                            foreach( $dataProp['#']['Значение'] as $photo ){
                                //$provValue[] = $photo['#'];
                                $photoArr[] = $photo['#'];
                            }
                            $propValue = $photoArr;
                        }

                        $props[$propCode] = $propValue;
                    }

                    $section['PROPS'] = $props;

                    $sections[$section['ID']] = $section;
                }
            }
        }
        return $sections;
    }

//    $sections = getSectionsInfo($groups, array());

    echo '<pre>';
    print_r($sections);
    echo '</pre>';

}
?>

<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/epilog_after.php");
?>
