<?
$_SERVER["DOCUMENT_ROOT"] = '/srv/www/planetaplitki.ru/htdocs/';
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_before.php");
?>

<?
$fileName = 'kontragent.csv';
$filePath = $_SERVER['DOCUMENT_ROOT'].DIRECTORY_SEPARATOR.'upload'.DIRECTORY_SEPARATOR.'1c_users'.DIRECTORY_SEPARATOR.$fileName;
?>

<?
$row = 1;
if( ($handle = fopen($filePath, "r")) !== FALSE ){

    global $USER;

    CModule::IncludeModule('sale');

    $cUser = new CUser;
    $sort_by = "ID";
    $sort_ord = "ASC";
    $arFilter = array("ACTIVE" => 'Y',);
    $dbUsers = $cUser->GetList($sort_by, $sort_ord, $arFilter, array('FIELDS' => array('ID', 'EMAIL')));
    while ($arUser = $dbUsers->Fetch()){
        $users[$arUser['EMAIL']] = $arUser;
    }

    $user = new CUser;

    $j = 1;

    while (($data = fgetcsv2($handle, 10000, ";")) !== FALSE) {

//        pRU($data, 'all');
//        echo $data[0];
//        echo $row;
//        echo $row;

        $email = $data[3];
        if( !$email ){
            continue;
        }

        $personTypeParams = array();
        $personTypeParams['NAME'] = $data[1];
        $personTypeParams['EMAIL'] = $email;

        if( $data[2] == 'Юридическое лицо' ){
            $personTypeId = 2;
            $personTypeParams['INN'] = $data[4];
            $personTypeParams['KPP'] = $data[5];
            $personTypeParams['RASCHETNII_SCHET'] = $data[6];
            $personTypeParams['BIK'] = $data[7];
            $personTypeParams['KORR_SCHET'] = $data[8];
            $personTypeParams['JUR_ADDRESS'] = $data[9];
            $personTypeParams['COMPANY'] = $data[1];
        }
        else{
            $personTypeId = 2;
            $personTypeParams['INN'] = $data[4];
            $personTypeParams['KPP'] = $data[5];
            $personTypeParams['RASCHETNII_SCHET'] = $data[6];
            $personTypeParams['BIK'] = $data[7];
            $personTypeParams['KORR_SCHET'] = $data[8];
            $personTypeParams['JUR_ADDRESS'] = $data[9];
        }

        # обновление
        if( eRU($users[$email]) ){

            $arFields = Array("NAME" => $data[1],"ACTIVE" => "Y",);

            $ID = $users[$email]['ID'];

            if( intval($ID) > 0 ){

                $user->Update($ID, $arFields);

                $db_sales = CSaleOrderUserProps::GetList(
                    array("DATE_UPDATE" => "DESC"),
                    array("USER_ID" => $ID)
                );

                while ($ar_sales = $db_sales->Fetch()){

                    $db_propVals = CSaleOrderUserPropsValue::GetList(array("ID" => "ASC"), Array("USER_PROPS_ID" => $ar_sales['ID']));
                    while ($arPropVals = $db_propVals->Fetch()){

                        $value = $personTypeParams[$arPropVals['PROP_CODE']];

                        # обновляем параметры контрагентов
                        if( $value ){
                            CSaleOrderUserPropsValue::Update(
                                $arPropVals['ID'],
                                array('VALUE' => $value,)
                            );
                        }
                    }
                }
            }
            else{
                echo $user->LAST_ERROR;
            }

            $j++;
        }
        # создание
        else{

            $xmlId = $data[10];
            $password = generatePassword();

            if( $data[0] == 1 ){
                $group = array(8);
            }
            elseif( $data[0] == 2 ){
                $group = array(9);
            }
            else{
                $group = array(5);
            }

            $user = new CUser;
            $arFields = Array(
                "NAME"              => $data[1],
                "EMAIL"             => $email,
                "LOGIN"             => $email,
                "LID"               => "s1",
                "ACTIVE"            => "Y",
                "GROUP_ID"          => $group,
                "PASSWORD"          => $password,
                "CONFIRM_PASSWORD"  => $password,
            );

            $ID = $user->Add($arFields);

            if (intval($ID) > 0){

                $arEventFields = array(
                    "EMAIL" => $email,
                    "LOGIN" => $email,
                    "PASSWORD" => $password,
                );

                CEvent::Send("USER_INFO_UP", 's1', $arEventFields, 'N', 102);
//                file_put_contents($_SERVER['DOCUMENT_ROOT'].'/newusers.txt', $ID, FILE_APPEND);

                #############################################
                //добавляем контрагента у пользователя

                //регистрируем профиль розничного покупателя

                # PERSON_TYPE_ID - идентификатор типа плательщика, для которого создаётся профиль
                $arProfileFields = array(
                    "NAME" => $personTypeParams['NAME'],
                    "USER_ID" => $ID,
                    "PERSON_TYPE_ID" => $personTypeId
                );

                $PROFILE_ID = CSaleOrderUserProps::Add($arProfileFields);

                //если профиль создан
                if ($PROFILE_ID){

                    if( $personTypeId == 1 ){
                        $PROPS[$personTypeId] = Array(
                            array(
                                "USER_PROPS_ID" => $PROFILE_ID,
                                "ORDER_PROPS_ID" => 1,
                                "NAME" => "ФИО",
                                "VALUE" => $personTypeParams['NAME']
                            ),
                            array(
                                "USER_PROPS_ID" => $PROFILE_ID,
                                "ORDER_PROPS_ID" => 2,
                                "NAME" => "Email",
                                "VALUE" => $personTypeParams['EMAIL']
                            ),
                            array(
                                "USER_PROPS_ID" => $PROFILE_ID,
                                "ORDER_PROPS_ID" => 3,
                                "NAME" => "Телефон",
                                "VALUE" => $personTypeParams['PHONE']
                            ),
                        );
                    }
                    //юридическое лицо
                    elseif( $personTypeId == 2 ){
                        $PROPS[$personTypeId] = Array(
                            array(
                                "USER_PROPS_ID" => $PROFILE_ID,
                                "ORDER_PROPS_ID" => 34,
                                "NAME" => "Название компании",
                                "VALUE" => $personTypeParams['NAME']
                            ),
                            array(
                                "USER_PROPS_ID" => $PROFILE_ID,
                                "ORDER_PROPS_ID" => 9,
                                "NAME" => "Юридический адрес",
                                "VALUE" => $personTypeParams['JUR_ADDRESS']
                            ),
                            array(
                                "USER_PROPS_ID" => $PROFILE_ID,
                                "ORDER_PROPS_ID" => 10,
                                "NAME" => "ИНН",
                                "VALUE" => $personTypeParams['INN']
                            ),
                            array(
                                "USER_PROPS_ID" => $PROFILE_ID,
                                "ORDER_PROPS_ID" => 11,
                                "NAME" => "КПП",
                                "VALUE" => $personTypeParams['KPP']
                            ),
                            array(
                                "USER_PROPS_ID" => $PROFILE_ID,
                                "ORDER_PROPS_ID" => 12,
                                "NAME" => "Контактное лицо",
                                "VALUE" => $personTypeParams['NAME']
                            ),
                            array(
                                "USER_PROPS_ID" => $PROFILE_ID,
                                "ORDER_PROPS_ID" => 13,
                                "NAME" => "Email",
                                "VALUE" => $personTypeParams['EMAIL']
                            ),
                            array(
                                "USER_PROPS_ID" => $PROFILE_ID,
                                "ORDER_PROPS_ID" => 14,
                                "NAME" => "Телефон",
                                "VALUE" => $personTypeParams['PHONE']
                            ),
                            array(
                                "USER_PROPS_ID" => $PROFILE_ID,
                                "ORDER_PROPS_ID" => 28,
                                "NAME" => "Расчетный счет",
                                "VALUE" => $personTypeParams['RASCHETNII_SCHET']
                            ),
                            array(
                                "USER_PROPS_ID" => $PROFILE_ID,
                                "ORDER_PROPS_ID" => 29,
                                "NAME" => "БИК",
                                "VALUE" => $personTypeParams['BIK']
                            ),
                            array(
                                "USER_PROPS_ID" => $PROFILE_ID,
                                "ORDER_PROPS_ID" => 30,
                                "NAME" => "Корреспондентский счет",
                                "VALUE" => $personTypeParams['KORR_SCHET']
                            ),

                            array(
                                "USER_PROPS_ID" => $PROFILE_ID,
                                "ORDER_PROPS_ID" => 18,
                                "NAME" => "Местоположение",
                                "VALUE" => 19 //Россия
                            ),
                            array(
                                "USER_PROPS_ID" => $PROFILE_ID,
                                "ORDER_PROPS_ID" => 19,
                                "NAME" => "Адрес доставки",
                                "VALUE" => "Адрес доставки"
                            ),
                            array(
                                "USER_PROPS_ID" => $PROFILE_ID,
                                "ORDER_PROPS_ID" => 33,
                                "NAME" => "Согласен с правилами покупки",
                                "VALUE" => 1
                            ),
                        );
                    }
                }

                //добавляем значения свойств к созданному ранее профилю
                foreach ($PROPS[$personTypeId] as $prop){
                    CSaleOrderUserPropsValue::Add($prop);
                }

//                die;
            }
            else{
                echo $user->LAST_ERROR;
            }
        }

        $row++;
    }
    fclose($handle);

    echo $j.'<br/>';
    echo $i.'<br/>';
}
?>

<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/epilog_after.php");
?>
