<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
?>

<?
# загружает цвета товаров

if( CModule::IncludeModule('iblock') ){

    $el = new CIBlockElement;

    $arSelect = Array("ID", "XML_ID");
    $arFilter = Array("IBLOCK_ID"=>IBLOCK_ID_CATALOG,"ACTIVE"=>"Y");
    $res = CIBlockElement::GetList(Array(), $arFilter, false, false, $arSelect);
    while($ob = $res->GetNextElement()){
        $arFields = $ob->GetFields();
        $elements[$arFields['ID']] = $arFields['XML_ID'];
    }

    $property_enums = CIBlockPropertyEnum::GetList(Array("DEF"=>"DESC", "SORT"=>"ASC"), Array("IBLOCK_ID"=>IBLOCK_ID_CATALOG, "CODE"=>"tsvettovar"));
    while($enum_fields = $property_enums->GetNext())
    {
        if( !in_array($enum_fields['XML_ID'], $elements) ){
            $arLoadProductArray = Array(
                "NAME" => $enum_fields['VALUE'],
                "XML_ID" => $enum_fields['XML_ID'],
                "IBLOCK_ID"      => IBLOCK_ID_COLORS,
            );
            if( $PRODUCT_ID = $el->Add($arLoadProductArray) ){
                //
            }
        }
    }
}
?>

<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>