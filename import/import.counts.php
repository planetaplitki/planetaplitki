<?
$DOCUMENT_ROOT = $_SERVER["DOCUMENT_ROOT"];
define("LANG", "ru");
define("NO_KEEP_STATISTIC", true);
define("NOT_CHECK_PERMISSIONS",true);

ini_set("max_execution_time","36000");
ini_set("memory_limit", "500M");
set_time_limit(0);
?>

<?
require($DOCUMENT_ROOT."/bitrix/modules/main/include/prolog_before.php");
?>

<?
# структура csv
# id;;name;retail_price;opt_price;1storage;2storage;all
$fileName = '1c_data.csv';
$filePath = $_SERVER['DOCUMENT_ROOT'] . DIRECTORY_SEPARATOR . $fileName;
$idStorage = 2;
?>

<?
$row = 1;
if (($handle = fopen($filePath, "r")) !== FALSE) {

    while (($data = fgetcsv2($handle, 10000, ";")) !== FALSE) {

        $item = array(
            'ID' => $data[0],
            'PARAM_1' => $data[1],
            'NAME' => $data[2],
            'PRICE_RETAIL' => $data[3],
            'PRICE_OPT' => $data[4],
            'STORAGE_1' => $data[5],
            #'STORAGE_2' => $data[6],
            #'STORAGE_ALL' => $data[7],
        );
        $items[$item['ID']] = $item;
    }
    fclose($handle);
}?>

<?if(
    CModule::IncludeModule('iblock') &&
    CModule::IncludeModule('catalog')
){

    $count = 0;

    $arSelect = Array("ID", "XML_ID");
    $arFilter = Array("IBLOCK_ID"=> IBLOCK_ID_CATALOG, "ACTIVE"=>"Y");
    $res = CIBlockElement::GetList(Array(), $arFilter, false, false, $arSelect);
    while($ob = $res->GetNextElement())
    {
        $arFields = $ob->GetFields();

        $rsStore = CCatalogStoreProduct::GetList(array(), array('PRODUCT_ID' =>$arFields['ID'], 'STORE_ID' => $idStorage), false, false, array());
        if ($arStore = $rsStore->Fetch()){

            $item = $items[$arFields['XML_ID']];

            if( eRU($item) ){
                $arFields = Array(
                    "STORE_ID" => $idStorage,
                    "AMOUNT" => $item['STORAGE_'.$idStorage] ? $item['STORAGE_'.$idStorage] : 0,
                );

                $ID = CCatalogStoreProduct::Update($arStore['ID'], $arFields);

                if($ID){
                    $count++;
                }
            }
        }
    }
}?>

<?if($count){?>
    <p>Обновлено <?=$count;?> записей.</p>
<?}?>

<?
#pRU($items, 'all');
?>

<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/epilog_after.php");
?>