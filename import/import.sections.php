<?
$_SERVER["DOCUMENT_ROOT"] = '/srv/www/planetaplitki.ru/htdocs/';
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_before.php");
?>

<?
CModule::IncludeModule('iblock');
CModule::IncludeModule('catalog');
AddMessage2Log('import.sections.php');

$zip = new ZipArchive;
$zip->open($_SERVER['DOCUMENT_ROOT'].'/upload/1c_sections/sections.zip');
$zip->extractTo($_SERVER['DOCUMENT_ROOT'].'/upload/1c_sections');
$zip->close();

$file = $_SERVER['DOCUMENT_ROOT'].'/upload/1c_sections/sections.xml';

if(file_exists($file))
{
    $objXML = new CDataXML();
    $objXML->Load($file);
    $arResult = $objXML->GetArray();

    $groups = $arResult['КоммерческаяИнформация']['#']['Классификатор'][0]['#']['Группы'][0]['#']['Группа'];

    /**
     * Возвращает список инфу по разделам
     * @param $arr
     * @return array
     */
    function getSectionsInfo( $arr, $sections = array() ){

        /*if( !eRU($sections) ){
            $sections = array();
        }*/

        if( eRU($arr) ){
            foreach( $arr as $data ){
                $groups = $data['#']['Группы'][0]['#']['Группа'];

                if( eRU($groups) ){

//                    if( !eRU($sections) ){
//                        $sections = getSectionsInfo($groups, $sections);
//                    }
//                    else{
//                        $sections = getSectionsInfo($groups, $sections);
//                    }
                    $section = array();
                    $section['ID'] = $data['#']['Ид'][0]['#'];
                    $section['NAME'] = $data['#']['Наименование'][0]['#'];
                    $section['DESCRIPTION'] = $data['#']['Описание'][0]['#'];
                    $section['PREVIEW_PICTURE'] = $data['#']['БитриксКартинка'][0]['#'];
                    $section['DETAIL_PICTURE'] = $data['#']['БитриксКартинкаДетальная'][0]['#'];

                    $sections[$section['ID']] = $section;

                    $sections = getSectionsInfo($groups, $sections);
                }
                else{

                    $section = array();
                    $props = array();
                    $section['ID'] = $data['#']['Ид'][0]['#'];
                    $section['NAME'] = $data['#']['Наименование'][0]['#'];
                    $section['DESCRIPTION'] = $data['#']['Описание'][0]['#'];
                    $section['PREVIEW_PICTURE'] = $data['#']['БитриксКартинка'][0]['#'];
                    $section['DETAIL_PICTURE'] = $data['#']['БитриксКартинкаДетальная'][0]['#'];

                    foreach( $data['#']['ЗначенияСвойства'] as $dataProp ){

                        $photoArr = array();
                        $propCode = $dataProp['#']['Ид'][0]['#'];
                        $propValue = $dataProp['#']['Значение'][0]['#'];

                        if( $propCode == 'UF_MORE_PHOTO' ){
                            foreach( $dataProp['#']['Значение'] as $photo ){
                                //$provValue[] = $photo['#'];
                                $photoArr[] = $photo['#'];
                            }
                            $propValue = $photoArr;
                        }

                        $props[$propCode] = $propValue;
                    }

                    $section['PROPS'] = $props;

                    $sections[$section['ID']] = $section;
                }
            }
        }
        return $sections;
    }

    $sections = getSectionsInfo($groups, array());

    # обновляем коллекции    
    if( eRU($sections) ){   

        $bs = new CIBlockSection;
        $i = 0;
        foreach( $sections as $section ){

            $arFilter = Array(
                'IBLOCK_ID'=>IBLOCK_ID_CATALOG,
                'XML_ID' => $section['ID'],
                'DEPTH_LEVEL' => 3
            );
            $db_list = CIBlockSection::GetList(Array($by=>$order), $arFilter, true);
            if( $ar_result = $db_list->GetNext() ){  

                $morePhoto = array();
                if( eRU($section['PROPS']['UF_MORE_PHOTO']) ){
                    foreach( $section['PROPS']['UF_MORE_PHOTO'] as $photo ){
                        $morePhoto[] = CFile::MakeFileArray($_SERVER["DOCUMENT_ROOT"].'/upload/1c_sections/'.$photo);
                    }
                }

                # получаем товары в разделе и отыскиваем минимальную цену в м2
                $minPrice = 0;
                $minPriceArr = array();
                $arSelect = Array("ID", "IBLOCK_ID_CATALOG", "CATALOG_GROUP_10", "CATALOG_GROUP_12");
                $arFilter = Array("IBLOCK_ID"=>IBLOCK_ID_CATALOG, "ACTIVE"=>"Y", "SECTION_ID" => $ar_result['ID'], "PROPERTY_TSENA_V_SHT" => false);
                $res = CIBlockElement::GetList(Array(), $arFilter, false, false, $arSelect);
                while($ob = $res->GetNextElement()){
                    $arFields = $ob->GetFields();

                    if( $arFields['CATALOG_PRICE_10'] ){
                        $minPriceArr['RETAIL'][$arFields['ID']] = $arFields['CATALOG_PRICE_10'];
                    }
                    
                    $rsPrices = CPrice::GetListEx(array(), array('PRODUCT_ID' => $arFields['ID'], 'CATALOG_GROUP_ID' => 10, 'CAN_BUY' => 'Y'));
                    if ($arPrice = $rsPrices->Fetch()) {
                        if ($arOptimalPrice = CCatalogProduct::GetOptimalPrice($arFields['ID'], 1, array(), 'N', array($arPrice), 's1')) {
                            if($arOptimalPrice["DISCOUNT_PRICE"])
                            {
                                $minPriceArr['RETAIL'][$arFields['ID']] = $arOptimalPrice["DISCOUNT_PRICE"];
                            }
                        }
                    }   
                                        
                    if( $arFields['CATALOG_PRICE_12'] ){
                        $minPriceArr['WHOLESALE'][$arFields['ID']] = $arFields['CATALOG_PRICE_12'];
                    }
                }

//                if( $ar_result['ID'] == 7953 ){
//                    pRU($ar_result, 'all');
//                    pRU($minPriceArr, 'all');
//                    die;
//                }

                $group = "";
                $sGroup = $section['PROPS']['UF_USER_GROUP'];
                if( $sGroup == 'opt' ){
                    $group = 7;
                    $section['PROPS']['UF_PRICE_WHOLESALE'] = min($minPriceArr['WHOLESALE']);
                    $section['PROPS']['UF_PRICE_RETAIL'] = min($minPriceArr['RETAIL']);
                }

                if( $sGroup == 'rozn' ){
                    $group = 6;
                    $section['PROPS']['UF_PRICE_RETAIL'] = min($minPriceArr['RETAIL']);
                }

                if( !$section['PROPS']['UF_PRICE_RETAIL'] ){
                    $section['PROPS']['UF_PRICE_RETAIL'] = "0";
                }

                if( !$section['PROPS']['UF_PRICE_WHOLESALE'] ){
                    $section['PROPS']['UF_PRICE_WHOLESALE'] = "0";
                }

                $arFields = Array(
                    "PICTURE" => CFile::MakeFileArray($_SERVER["DOCUMENT_ROOT"].'/upload/1c_sections/'.$section['PREVIEW_PICTURE']),
                    "DETAIL_PICTURE" => CFile::MakeFileArray($_SERVER["DOCUMENT_ROOT"].'/upload/1c_sections/'.$section['DETAIL_PICTURE']),
                    "UF_USER_GROUP" => $group,
                    "UF_MORE_PHOTO" => $morePhoto,
                    "UF_PRICE_RETAIL" => $section['PROPS']['UF_PRICE_RETAIL'],
                    "UF_PRICE_WHOLESALE" => $section['PROPS']['UF_PRICE_WHOLESALE'],
                    "UF_SIMILAR_COLLECTIO" => $section['PROPS']['UF_SIMILAR_COLLECTIO'],
                    "UF_EXCLUSIVE" => $section['PROPS']['UF_EXCLUSIVE'] == "true" ? "1" : "0",
                    "UF_NEW_COLLECTION" => $section['PROPS']['UF_NEW_COLLECTION'] == "true" ? "1" : "0",
                    "UF_SALE" => $section['PROPS']['UF_SALE'] == "true" ? "1" : "0",
                    "UF_AKCIYA" => $section['PROPS']['UF_AKCIYA'] == "true" ? "1" : "0",
//                    "UF_MIN_PRICE" => $section['PROPS']['UF_MIN_PRICE'] ? str_replace(',', '.', $section['PROPS']['UF_MIN_PRICE']) : "0",
                );

                # получаем для раздела старые картинки и удаляем их
                $photos = array();
                $db_listPh = CIBlockSection::GetList(array($by=>$order), array('IBLOCK_ID'=>IBLOCK_ID_CATALOG, 'ID' => $ar_result['ID'],), true, array('ID', 'UF_MORE_PHOTO'));

                if( $ar_resultPh = $db_listPh->GetNext() ){

                    if( eRU($ar_resultPh['UF_MORE_PHOTO']) ){

                        foreach($ar_resultPh['UF_MORE_PHOTO'] as $onePhoto){

                            $photoInfo = array();
                            $photoInfo['del'] = 1;
                            $photoInfo['old_id'] = $onePhoto;

                            $photos[] = $photoInfo;
                        }
                        AddMessage2Log('update = '.print_r($ar_result['ID'], true),'');
                        $bs->Update($ar_result['ID'], array('UF_MORE_PHOTO' => $photos));
                    }
                }
                AddMessage2Log('update = '.print_r($ar_result['ID'], true),'');
                                                        
                $res_ = $bs->Update($ar_result['ID'], $arFields);
            }
            $i++;
        }
    }

//    echo '<pre>';
//    print_r($sections);
//    echo '</pre>';
}

?>

<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/epilog_after.php");
?>
