<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetTitle("Оптовым покупателям");
?><p style="text-align: justify;">
 <span style="font-size: 12pt;">Компания </span><b><span style="font-size: 12pt;">«Планета Плитки»</span></b><span style="font-size: 12pt;"> была основана в 1995 году и на сегодняшний момент является крупным импортером керамической плитки на рынке отделочных материалов. Компания имеет сеть магазинов в Москве и регулярно участвует в специализированных строительных выставках (CERSAIE, CEVISAMA, BATIMAT). </span><span style="font-size: 12pt;"> </span><span style="font-size: 12pt;"> </span>
</p>
 <span style="font-size: 12pt;"> </span><span style="font-size: 12pt;"> </span><span style="font-size: 12pt;"> </span><span style="font-size: 12pt;"> </span><span style="font-size: 12pt;"> </span>
<p style="text-align: justify;">
 <span style="font-size: 12pt;"> </span><span style="font-size: 12pt;"> </span><span style="font-size: 12pt;"> </span><span style="font-size: 12pt;"> </span><span style="font-size: 12pt;"> </span>
</p>
 <span style="font-size: 12pt;"> </span><span style="font-size: 12pt;"> </span><span style="font-size: 12pt;"> </span><span style="font-size: 12pt;"> </span><span style="font-size: 12pt;"> </span>
<p style="text-align: justify;">
 <span style="font-size: 12pt;"> </span><span style="font-size: 12pt;"> </span><span style="font-size: 12pt;">
	Постоянно растущая дилерская сеть сегодня представлена компаниями из Москвы, Санкт-Петербурга, Омска, Оренбурга, Казани, Воронежа, Екатеринбурга, Перми, Тюмени, Новосибирска и других городов России.</span><br>
 <span style="font-size: 12pt;"> </span><span style="font-size: 12pt;"> </span><span style="font-size: 12pt;"> </span><br>
 <span style="font-size: 12pt;"> </span><span style="font-size: 12pt;"> </span><span style="font-size: 12pt;">
	Ассортимент керамической плитки очень широк: от недорогих коллекций до коллекций элитного класса. </span><br>
 <span style="font-size: 12pt;"> </span><span style="font-size: 12pt;"> </span><span style="font-size: 12pt;"> </span><br>
 <span style="font-size: 12pt;"> </span><span style="font-size: 12pt;"> </span><span style="font-size: 12pt;">
	Наши поставщики - ведущие европейские производители из Испании, Италии и Португалии.</span>
</p>
<p style="text-align: justify;">
 <span style="font-size: 12pt;">Керамическая плитка в ассортименте представлена 35 фабриками: PAMESA, MAINZU, GAYA FORES, CERACASA, IBERO, ROCERSA, ROCA, CAS CERAMICA, ALAPLANA и другими.&nbsp;</span>
</p>
<p style="text-align: justify;">
 <span style="font-size: 12pt;">Одной из первых </span><b><span style="font-size: 12pt;">«Планета Плитки»</span></b><span style="font-size: 12pt;"> вышла на рынок с принципиально новым для России, направлением керамической плитки - это <a target="_blank" href="/catalog/vostochnaya-kollektsiya/">плитка в восточном стиле</a> с необыкновенно красивыми арабскими орнаментами от производителей MAINZU (Испания),&nbsp; CAS (Испания). </span><span style="font-size: 12pt;"> </span><span style="font-size: 12pt;"> </span>
</p>
 <span style="font-size: 12pt;"> </span><span style="font-size: 12pt;"> </span><span style="font-size: 12pt;"> </span><span style="font-size: 12pt;"> </span><span style="font-size: 12pt;"> </span>
<hr>
 <span style="font-size: 12pt;"> </span><span style="font-size: 12pt;"> </span><span style="font-size: 12pt;"> </span><span style="font-size: 12pt;"> </span><span style="font-size: 12pt;"> </span>
<p style="text-align: justify;">
 <span style="font-size: 12pt;"> </span><span style="font-size: 12pt;"> </span><span style="font-size: 12pt;"> </span><b><span style="font-size: 12pt;">Подробнее ознакомиться с ассортиментом Вы можете в </span></b><a target="_blank" href="/upload/documents/Планета%20Плитки%20Каталог%202018_new.pdf"></a><b><a href="/upload/documents/Планета%20Плитки%20Каталог%202018_new.pdf" target="_blank"></a><a href=""><span style="font-size: 12pt;">Генеральном каталоге компании "Планета Плитки" 2018-2019</span></a></b><b><a href="/upload/documents/Планета%20Плитки%20Каталог%202018_new.pdf" target="_blank"></a><a target="_blank" href="/upload/documents/Планета%20Плитки%20Каталог%202018_new.pdf"></a><span style="font-size: 12pt;"> </span></b><span style="font-size: 12pt;">(ссылка на скачивание каталога в PDF формате).</span><span style="font-size: 12pt;"> </span><span style="font-size: 12pt;"> </span><span style="font-size: 12pt;"> </span><span style="font-size: 12pt;"> </span>
</p>
<span style="font-size: 12pt;"> </span><span style="font-size: 12pt;"> </span><span style="font-size: 12pt;"> </span><span style="font-size: 12pt;"> </span><span style="font-size: 12pt;"> </span><span style="font-size: 12pt;"> </span><span style="font-size: 12pt;"> </span>
<hr>
 <span style="font-size: 12pt;"> </span><span style="font-size: 12pt;"> </span><span style="font-size: 12pt;"> </span><span style="font-size: 12pt;"> </span><span style="font-size: 12pt;"> </span>
<p style="text-align: justify;">
 <span style="font-size: 12pt;">Компания </span><b><span style="font-size: 12pt;">«Планета Плитки» </span></b><span style="font-size: 12pt;">предлагает различные схемы сотрудничества, дающие возможность покупать высококачественные отделочные материалы по специальным ценам. </span><br>
 <span style="font-size: 12pt;"> </span><span style="font-size: 12pt;"> </span><span style="font-size: 12pt;">
	Для дизайнеров, строителей, архитектурных бюро существуют специальные </span><span style="font-size: 12pt;">предложения. Наша дилерская сеть постоянно расширяется, но мы ценим каждого нашего партнера и рады взаимовыгодному и долгосрочному сотрудничеству.</span>
</p>
<p style="text-align: justify;">
 <span style="font-size: 12pt;"><br>
 </span>
</p>
<p style="text-align: center;">
 <span style="font-size: 12pt;">______________</span>
</p>
<h3 style="text-align: center;"> <b> </b><span style="font-size: 12pt;"><b>КОНТАКТЫ ОТДЕЛА ОПТОВЫХ ПРОДАЖ:</b></span><span style="font-size: 12pt;"> </span> </h3>
 <span style="font-size: 12pt;"> </span>
<p style="text-align: center;">
 <span style="font-size: 12pt;">
	т/ф: +7 (495) 215-21-87 </span> <br>
 <span style="font-size: 12pt;">
	8 (800) 500-82-29, доб. 2 (бесплатные звонки со всех номеров России) </span> <br>
 <span style="font-size: 12pt;"> </span><a href="mailto:opt@planetaplitki.ru"><span style="font-size: 12pt;">opt@planetaplitki.ru</span></a>
</p>
 <br>
 <?$APPLICATION->IncludeComponent(
	"bitrix:form",
	"feedback",
	Array(
		"AJAX_MODE" => "N",
		"AJAX_OPTION_ADDITIONAL" => "",
		"AJAX_OPTION_HISTORY" => "N",
		"AJAX_OPTION_JUMP" => "N",
		"AJAX_OPTION_STYLE" => "Y",
		"CACHE_TIME" => "3600",
		"CACHE_TYPE" => "A",
		"CHAIN_ITEM_LINK" => "",
		"CHAIN_ITEM_TEXT" => "",
		"EDIT_ADDITIONAL" => "N",
		"EDIT_STATUS" => "Y",
		"IGNORE_CUSTOM_TEMPLATE" => "Y",
		"NOT_SHOW_FILTER" => array("",""),
		"NOT_SHOW_TABLE" => array("",""),
		"RESULT_ID" => $_REQUEST[RESULT_9],
		"SEF_MODE" => "N",
		"SHOW_ADDITIONAL" => "Y",
		"SHOW_ANSWER_VALUE" => "N",
		"SHOW_EDIT_PAGE" => "N",
		"SHOW_LIST_PAGE" => "N",
		"SHOW_STATUS" => "Y",
		"SHOW_VIEW_PAGE" => "N",
		"START_PAGE" => "new",
		"SUCCESS_URL" => "",
		"USE_CAPTHCA" => "Y",
		"USE_EXTENDED_ERRORS" => "N",
		"VARIABLE_ALIASES" => Array("action"=>"action"),
		"WEB_FORM_ID" => "12"
	)
);?><br>
 <?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>