<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetTitle("Сотрудничество");
?><div style="line-height: 1.7em;">
	<p style="text-align: center;">
 <strong><span style="font-size: 11pt;">Уважаемые господа!</span></strong><span style="font-size: 11pt;"> </span>
	</p>
	<span style="font-size: 11pt;"> </span>
	<p style="text-align: justify;">
		<span style="font-size: 11pt;">
		Компания «Планета Плитки» приглашает к сотрудничеству всех, кто по роду своей деятельности, связан с керамической плиткой. </span><strong><span style="font-size: 11pt;">Дизайнеры, архитекторы, строители, торговые организации, присоединяйтесь!</span></strong><span style="font-size: 11pt;"> Для каждого из Вас существуют различные схемы сотрудничества, дающие возможность покупать высококачественную продукцию, такую как: керамическая плитка, керамогранит, мозаика, плитка для бассейнов. Обязательно обратите внимание на наше оригинальное направление - керамическая плитка в восточном стиле.</span><br>
		<span style="font-size: 11pt;"> </span><br>
		<span style="font-size: 11pt;">
		Если Вы представляете торговую организацию, то, обратившись в дилерский отдел, Вы имеете возможность: </span><br>
		<span style="font-size: 11pt;">
		Используя наши дилерские цены, успешно конкурировать на рынке керамической плитки. </span><br>
		<span style="font-size: 11pt;">
		Обеспечения рекламной продукцией и выставочным оборудованием. </span><br>
		<span style="font-size: 11pt;">
		Работать по образцам с нашей доставкой до вашего склада (по договоренности). </span><br>
		<span style="font-size: 11pt;">
		Получать рекомендации по наиболее продаваемым коллекциям керамической плитки, информацию о новинках, наличию плитки на складе.</span><br>
		<span style="font-size: 11pt;"> </span><br>
		<span style="font-size: 11pt;"> </span><strong><span style="font-size: 11pt;">Дизайнеров, архитекторов и строителей</span></strong><span style="font-size: 11pt;"> приглашает к сотрудничеству корпоративный отдел:</span><br>
		<span style="font-size: 11pt;"> </span>
	</p>
	<span style="font-size: 11pt;"> </span>
	<ul>
		<li><span style="font-size: 11pt;">Вы будете обеспечены сборным каталогом продукции нашей компании. </span></li>
		<li><span style="font-size: 11pt;">Можете получить консультации по подбору ассортимента и оформлению заказа. </span></li>
		<li><span style="font-size: 11pt;">В одном магазине Вы выбираете плитку из ассортимента более 20 испанских и итальянских фабрик. </span></li>
		<li><span style="font-size: 11pt;">Вам будет предоставлена дисконтная карта, о правилах работы с которой, расскажет менеджер отдела.</span></li>
	</ul>
	<span style="font-size: 11pt;">
	Подробная информация для дизайнеров указана </span><a href="/dizayneram-intererov/"><span style="font-size: 11pt;">здесь</span></a><span style="font-size: 11pt;">.</span><br>
	<span style="font-size: 11pt;"> </span><br>
	<span style="font-size: 11pt;"> </span><strong><span style="font-size: 11pt;">Региональные партнеры:</span></strong><br>
	<span style="font-size: 11pt;"> </span>
	<ul>
		<li><span style="font-size: 11pt;">Вы можете получить каталоги и другую рекламную продукцию по почте. </span></li>
		<li><span style="font-size: 11pt;">Вы можете оформить доставку любой партии товара с нашего склада до указанной Вами транспортной компании. </span></li>
		<li><span style="font-size: 11pt;">Имеете возможность покупать плитку на свой склад или под определенный заказ Вашего клиента по специальным ценам.</span></li>
	</ul>
	<span style="font-size: 11pt;">
	Контактный телефон для региональных партнеров:&nbsp;8 (800) 500-82-29, доб. 2</span><br>
	<span style="font-size: 11pt;"> </span><br>
	<span style="font-size: 11pt;">
	Мы будем рады, если с помощью нашей продукции Вы сумеете воплотить в жизнь свои идеи и творческие замыслы. Надеемся на долгосрочное сотрудничество. </span>
	<p>
		<span style="font-size: 11pt;"> </span>
	</p>
	<span style="font-size: 11pt;"> </span>
</div>
<span style="font-size: 11pt;"> </span> <br><?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>