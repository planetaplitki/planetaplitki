<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetTitle("Оборудование");
?><table cellpadding="5" cellspacing="1">
<tbody>
<tr>
	<td>
		 &nbsp;<img width="360" alt="obor1.jpg" src="/upload/medialibrary/74a/obor1.jpg" height="360" title="obor1.jpg">
	</td>
	<td>
 <b>CТЕНД «АЛЬБЕРО 16-40»</b><br>
 <br>
		 Экспозитор для демонстрации керамической плитки.<br>
		 Принцип — вращение экспозиций вокруг своей оси.
		<hr>
 <b>Размер листов 89x40 см</b><br>
 <b>
		Количество экспозиций - 16 шт.</b>
	</td>
</tr>
<tr>
	<td>
		 &nbsp;<img width="360" alt="obor2.jpg" src="/upload/medialibrary/4b1/obor2.jpg" height="360" title="obor2.jpg">
	</td>
	<td>
 <b>CТЕНД «БРУКС»</b><br>
 <br>
		 Экспозитор для демонстрации керамической плитки.<br>
		 Принцип — книжка (планшеты перелистываются).<br>
		 В одном металлическом планшете – два листа ДСП.
		<hr>
 <b>Размер листов 180x80 см</b><br>
 <b>
		Количество экспозиций - 24 шт.</b><br>
	</td>
</tr>
<tr>
	<td>
		 &nbsp;<img width="360" alt="obor3.jpg" src="/upload/medialibrary/0c2/obor3.jpg" height="360" title="obor3.jpg"><br>
	</td>
	<td>
 <b>C</b><b>ТЕНД «АЛЬБЕРО 8-40»</b><br>
 <br>
		 Экспозитор для демонстрации керамической плитки.<br>
		 Принцип — вращение экспозиций вокруг своей оси.
		<hr>
 <b>Размер листов 180x40 см</b><br>
 <b>
		Количество экспозиций - 8 шт.</b><br>
	</td>
</tr>
<tr>
	<td colspan="1">
		 &nbsp;<img width="360" alt="obor4.jpg" src="/upload/medialibrary/70e/obor4.jpg" height="360" title="obor4.jpg">
	</td>
	<td colspan="1">
 <b>CТЕНД «Техно 24.100»</b><br>
 <br>
		 Экспозитор для демонстрации керамической плитки.<br>
		 Принцип — книжка (планшеты перелистываются).<br>
		 В одном металлическом планшете – два листа ДСП.
		<hr>
 <b>Размер листов&nbsp;&nbsp;1925x1000</b><br>
 <b>
		Количество экспозиций -24 шт.</b><br>
	</td>
</tr>
<tr>
	<td colspan="1">
		 &nbsp;<img width="360" alt="obor5.jpg" src="/upload/medialibrary/1a0/obor5.jpg" height="360" title="obor5.jpg">
	</td>
	<td colspan="1">
 <b>КЕДР 12.26 </b><br>
 <br>
		 Экспозитор “Кедр” способен нести нагрузку полной выклейки керамогранитом.&nbsp;Конструкция разборная.<br>
		 Принцип&nbsp;— книжка (планшеты перелистываются).
		<hr>
 <b>Размер планшета: 1000*1920</b><br>
 <b>
		Ширина – 3030 мм</b><br>
 <b>
		Глубина – 1030 мм</b><br>
 <b>
		Высота – 2380 мм</b><br>
 <br>
 <br>
	</td>
</tr>
</tbody>
</table>
 <br><?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>