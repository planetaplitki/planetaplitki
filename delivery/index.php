<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetTitle("Доставка и оплата");
?>
<?global $USER;?>
<div class="oplata_i_dostavka_page hidden-xs">
    <div class="col-sm-12">
        <?/*<div class="container">*/?>
        <div class="row obj50">
            <ul id="tabs" class="nav nav-tabs" role="tablist">
                <li class="active"><a href="#itme" role="tab" data-toggle="tab">Оплата</a></li>
                <li><a href="#pdf-catalog" role="tab" data-toggle="tab">Доставка</a></li>
            </ul>
            <div class="tab-content">
                <div class="tab-pane fade in active" id="itme">
                    <div class="box-position1">
                        <p class="easy">
                            Уважаемые покупатели! Вы можете оплатить товар наиболее удобным для Вас способом:
                        </p>
                        <div class="row obj50">
                            <div class="col-xs-4 usligi-pict">
                                <img src="<?=SITE_DEFAULT_TEMPLATE_PATH?>/img/oplata_i_dostavka/1.jpg">
                                <p class="text-pict">
                                    Наличными
                                </p>
                            </div>
                            <div class="col-xs-4 usligi-pict">
                                <img src="<?=SITE_DEFAULT_TEMPLATE_PATH?>/img/oplata_i_dostavka/2.jpg">
                                <p class="text-pict">
                                    Банковской картой
                                </p>
                            </div>
                            <div class="col-xs-4 usligi-pict">
                                <img src="<?=SITE_DEFAULT_TEMPLATE_PATH?>/img/oplata_i_dostavka/3.jpg">
                                <p class="text-pict">
                                    On-line
                                </p>
                                <p>
                                    <br>
                                </p>
                                <p>
                                    <br>
                                </p>
                            </div>
                        </div>
                        <div class="row obj50">
                            <p class="easy">
                                <span style="color: #9d0a0f;"><b>Просим Вас обратить внимание, что минимальная сумма заказа в интернет-магазине составляет 3000 руб. Данное условие не распространяется на товары, находящиеся в разделе "Распродажа".</b></span>
                            </p>
                        </div>
                        <div class="row obj50">
                            <p class="t2">
                                Оплата наличными
                            </p>
                            <p class="easy">
                                Мы принимаем к оплате наличные деньги в наших салонах-магазинах расположенных по адресу: <br>
                            </p>
                            <p>
                            </p>
                            <table cellpadding="10" cellspacing="10" align="center" style="width: 100%;">
                                <tbody>
                                    <tr>
                                        <td class="easy">
                                            1. САЛОН-МАГАЗИН "ПЛАНЕТА ПЛИТКИ"<br>
                                            г. Москва, Нахимовский проспект, д. 50<br>
                                            Телефон: +7 (495) 646-16-90, доб. 951, 952, 953, 954, 971, 972<br>
                                            &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;8 (800) 500-82-29, доб. 1<br>
                                            E-mail:<a href="mailto:nahim@planetaplitki.ru">nahim@planetaplitki.ru</a><br>
                                            Skype:zalnaxim<br>
                                            Пн-Вс&nbsp;10:00 - 20:00
                                        </td>
                                        <td class="easy">
                                            2. ТК "ТВОЙ ДОМ", ряд 14<br>
                                            м. Домодедовская, на 24км МКАД, внешняя сторона <br>
                                            Tел./факс: +7 (495)926-37-39 <br>
                                            Skype: tvoyidom <br>
                                            Ежедневно с 10:00 до 20:00
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="easy">
                                            3. ТСК "Каширский Двор", пав. 3-А12 <br>
                                            м. Коломенская, м. Каширская, Каширское шоссе, д. 19, корпус 1 <br>
                                            Tел.: +7 (495) 646-16-90, доб. 940<br>
                                            &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;+7 (985) 648-14-95<br>
                                            E-mail:&nbsp;<a href="https://e.mail.ru/compose/?mailto=mailto%3akadvor1@planetaplitki.ru" target="_blank">kadvor1@planetaplitki.ru</a><br>
                                            Ежедневно с 09:00 до 21:00
                                        </td>
                                        <td class="easy">
                                            4. МТЦ "ГРАНД", 1 этаж Гранд-1 <br>
                                            м. Речной вокзал, м. Планерная, г.Химки, Ленинградское шоссе <br>
                                            Тел.: +7 (495)780-36-12 <br>
                                            Skype: tccgrand <br>
                                            Ежедневно с 10:00 до 21:00
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="easy">
                                            5. ТК "Конструктор", 1-й этаж главного корпуса 25-й км МКАД (внешняя сторона) <br>
                                            Тел.: +7 (495)280-78-57 <br>
                                            Skype: p_konstryktor <br>
                                            Ежедневно с 10:00 до 21:00
                                        </td>
                                    </tr>
                                </tbody>
                            </table>
                            <br>
                            <p class="t2">
                                Оплата банковской картой (Visa, Mastercard)
                            </p>
                            <p class="easy">
                                Оплата банковской картой доступна в салоне-магазине "Планета Плитки" на Нахимовском проспекте, д.&nbsp;50.&nbsp;
                            </p>
                            <p class="t2">
                                Online-оплата банковской картой
                            </p>
                            <p class="easy">
                                &nbsp;- При оформлении заказа в интернет-магазине planetaplitki.ru<br>
                                &nbsp;- Через смс-сообщение, перейдя по ссылке&nbsp;для оплаты<br>
                            </p>
                            <p class="t2">
                                Оплата по безналичному расчету
                            </p>
                            <p class="easy">
                                Юридические и физические лица могут оплатить свой заказ в безналичной форме через любой банк в любой точке России. Для этого необходимо при оформлении заказа прислать реквизиты нашему менеджеру по факсу или на электронный адрес, он подготовит для Вас счет. При этом обязательным является уточнение наличия товара и резервирование его у нашего менеджера до оплаты. Юридические лица оплачивают заказ согласно выставленного счёта в течение 2-х рабочих дней.
                            </p>
                            <p class="easy">
                                Для физических лиц возможна оплата заказа банковским переводом на наш расчётный счёт в ВТБ 24 (ЗАО).
                            </p>
                            <p class="easy">
                                После оплаты заказа, обязательно сообщите нам об этом по телефону +7 (495) 646-16-90 или по электронной почте <a href="mailto:nahim@planetaplitki.ru">nahim@planetaplitki.ru</a>
                            </p>
                            <p class="easy">
                                Мы обязуемся не публиковать и не передавать третьим лицам любые данные, оставленные клиентом при заказе товара.
                            </p>
                        </div>
                        <p class="t2">
                            Договор-оферта
                        </p>
                        <p>
                            <a name="top"></a>
                        </p>
                        <p class="easy">
                            <a href="/upload/documents/Оферта физ. лица ООО ЭЛЬБА.docx">Договор-оферта для физических лиц</a>
                        </p>
                        <p class="easy">
                            <a href="/upload/documents/Оферта юр. лица ООО ЭЛЬБА.docx">Договор-оферта для юридических лиц</a>
                        </p>
                    </div>
                </div>
                <div class="tab-pane" id="pdf-catalog">
                    <div class="box-position1">
                        <p class="easy">
                            Доставка осуществляется до подъезда, без погрузо-разгрузочных работ с 10.00 до 18.00 в будние дни.
                        </p>
                        <table border="1" cellpadding="13" cellspacing="15" width="100%" class="easy">
                            <tbody cellpadding="13" cellspacing="15">
                                <tr>
                                    <td colspan="4" align="center">
                                        Стоимость доставки
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        Зона 1
                                    </td>
                                    <td>
                                        Центр, внутри ТТК до 1000 кг
                                    </td>
                                    <td>
                                        3000 руб.
                                    </td>
                                    <td>
                                        Свыше 1000 кг цена расчетная, уточняйте у менеджера
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        Зона 2
                                    </td>
                                    <td>
                                        Москва от ТТК до МКАД, до 1500 кг
                                    </td>
                                    <td>
                                        2300 руб.
                                    </td>
                                    <td>
                                        Свыше 1500 кг цена расчетная, уточняйте у менеджера
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        Зона 3
                                    </td>
                                    <td>
                                        Доставка от МКАД до МБК (А108), до 1500 кг<br>
                                    </td>
                                    <td>
                                        2300 руб.+40 руб./км
                                    </td>
                                    <td>
                                        Свыше 1500 кг цена расчетная, уточняйте у менеджера
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        Зона 4
                                    </td>
                                    <td>
                                        Доставка за МБК&nbsp;(А108)
                                    </td>
                                    <td>
                                        Цена расчетная
                                    </td>
                                    <td>
                                        Цена расчетная
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                        <br>
                        <p>
                            <img width="965" alt="dostavka-zona.jpg" src="/upload/medialibrary/7e1/7e14337be7b7d51f463037f46c0ff785.jpg" height="400" title="dostavka-zona.jpg"><br>
                        </p>
                        <p class="easy">
                            Доставка по регионам России осуществляется транспортными компаниями. Вы самостоятельно выбираете подходящую Вам транспортную компанию. Наша компания осуществляет доставку до транспортной компании согласно тарифам, указанным выше.
                        </p>
                        <p class="t2">
                            Самовывоз
                        </p>
                        <p class="easy">
                            Возможен самовывоз заказа со склада в г. Подольск или салона-магазина "Планета Плитки" на Нахимовском пр-те 50. Подробности уточняйте у менеджеров.
                        </p>
                    </div>
                </div>
            </div>
        </div>
        <?/*</div>*/?>
    </div>
</div>

<div class="oplata_i_dostavka_page hidden-sm hidden-md hidden-lg">
    <div class="col-sm-12">
        <p class="easy">
            Уважаемые покупатели! Вы можете оплатить товар наиболее удобным для Вас способом:
        </p>
        <div class="row obj50">
            <div class="col-xs-4 usligi-pict">
                <img src="<?=SITE_DEFAULT_TEMPLATE_PATH?>/img/oplata_i_dostavka/1.jpg">
                <p class="text-pict">
                    Наличными
                </p>
            </div>
            <div class="col-xs-4 usligi-pict">
                <img src="<?=SITE_DEFAULT_TEMPLATE_PATH?>/img/oplata_i_dostavka/2.jpg">
                <p class="text-pict">
                    Банковской картой
                </p>
            </div>
            <div class="col-xs-4 usligi-pict">
                <img src="<?=SITE_DEFAULT_TEMPLATE_PATH?>/img/oplata_i_dostavka/3.jpg">
                <p class="text-pict">
                    On-line
                </p>
                <p>
                    <br>
                </p>
                <p>
                    <br>
                </p>
            </div>
        </div>
        <div class="row obj50">
            <p class="easy">
                <span style="color: #9d0a0f;"><b>Просим Вас обратить внимание, что минимальная сумма заказа в интернет-магазине составляет 3000 руб. Данное условие не распространяется на товары, находящиеся в разделе "Распродажа".</b></span>
            </p>
        </div>
        <div class="row obj50">
            <p class="t2">
                Оплата наличными
            </p>
            <p class="easy">
                Мы принимаем к оплате наличные деньги в наших салонах-магазинах расположенных по адресу: <br>
            </p>
            <p>
            </p>
            <table cellpadding="10" cellspacing="10" align="center" style="width: 100%;">
                <tbody>
                    <tr>
                        <td class="easy">
                            1. САЛОН-МАГАЗИН "ПЛАНЕТА ПЛИТКИ"<br>
                            г. Москва, Нахимовский проспект, д. 50<br>
                            Телефон: +7 (495) 646-16-90, доб. 951, 952, 953, 954, 971, 972<br>
                            &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;8 (800) 500-82-29, доб. 1<br>
                            E-mail:<a href="mailto:nahim@planetaplitki.ru">nahim@planetaplitki.ru</a><br>
                            Skype:zalnaxim<br>
                            Пн-Вс&nbsp;10:00 - 20:00
                        </td>
                        <td class="easy">
                            2. ТК "ТВОЙ ДОМ", ряд 14<br>
                            м. Домодедовская, на 24км МКАД, внешняя сторона <br>
                            Tел./факс: +7 (495)926-37-39 <br>
                            Skype: tvoyidom <br>
                            Ежедневно с 10:00 до 20:00
                        </td>
                    </tr>
                    <tr>
                        <td class="easy">
                            3. ТСК "Каширский Двор", пав. 3-А12 <br>
                            м. Коломенская, м. Каширская, Каширское шоссе, д. 19, корпус 1 <br>
                            Tел.: +7 (495) 646-16-90, доб. 940<br>
                            &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;+7 (985) 648-14-95<br>
                            E-mail:&nbsp;<a href="https://e.mail.ru/compose/?mailto=mailto%3akadvor1@planetaplitki.ru" target="_blank">kadvor1@planetaplitki.ru</a><br>
                            Ежедневно с 09:00 до 21:00
                        </td>
                        <td class="easy">
                            4. МТЦ "ГРАНД", 1 этаж Гранд-1 <br>
                            м. Речной вокзал, м. Планерная, г.Химки, Ленинградское шоссе <br>
                            Тел.: +7 (495)780-36-12 <br>
                            Skype: tccgrand <br>
                            Ежедневно с 10:00 до 21:00
                        </td>
                    </tr>
                    <tr>
                        <td class="easy">
                            5. ТК "Конструктор", 1-й этаж главного корпуса 25-й км МКАД (внешняя сторона) <br>
                            Тел.: +7 (495)280-78-57 <br>
                            Skype: p_konstryktor <br>
                            Ежедневно с 10:00 до 21:00
                        </td>
                    </tr>
                </tbody>
            </table>
            <br>
            <p class="t2">
                Оплата банковской картой (Visa, Mastercard)
            </p>
            <p class="easy">
                Оплата банковской картой доступна в салоне-магазине "Планета Плитки" на Нахимовском проспекте, д.&nbsp;50.&nbsp;
            </p>
            <p class="t2">
                Online-оплата банковской картой
            </p>
            <p class="easy">
                &nbsp;- При оформлении заказа в интернет-магазине planetaplitki.ru<br>
                &nbsp;- Через смс-сообщение, перейдя по ссылке&nbsp;для оплаты<br>
            </p>
            <p class="t2">
                Оплата по безналичному расчету
            </p>
            <p class="easy">
                Юридические и физические лица могут оплатить свой заказ в безналичной форме через любой банк в любой точке России. Для этого необходимо при оформлении заказа прислать реквизиты нашему менеджеру по факсу или на электронный адрес, он подготовит для Вас счет. При этом обязательным является уточнение наличия товара и резервирование его у нашего менеджера до оплаты. Юридические лица оплачивают заказ согласно выставленного счёта в течение 2-х рабочих дней.
            </p>
            <p class="easy">
                Для физических лиц возможна оплата заказа банковским переводом на наш расчётный счёт в ВТБ 24 (ЗАО).
            </p>
            <p class="easy">
                После оплаты заказа, обязательно сообщите нам об этом по телефону +7 (495) 646-16-90 или по электронной почте <a href="mailto:nahim@planetaplitki.ru">nahim@planetaplitki.ru</a>
            </p>
            <p class="easy">
                Мы обязуемся не публиковать и не передавать третьим лицам любые данные, оставленные клиентом при заказе товара.
            </p>
        </div>
    </div>
    <hr class="line-block">
    <p class="t2">
        Договор-оферта
    </p>
    <p>
        <a name="top"></a>
    </p>
    <p class="easy">
        <a href="/upload/documents/Оферта физ. лица ООО ЭЛЬБА.docx">Договор-оферта для физических лиц</a>
    </p>
    <p class="easy">
        <a href="/upload/documents/Оферта юр. лица ООО ЭЛЬБА.docx">Договор-оферта для юридических лиц</a>
    </p>
    <hr class="line-block">
    <p class="t2">
        Доставка
    </p>
    <p class="easy">
        Доставка осуществляется до подъезда, без погрузо-разгрузочных работ с 10.00 до 18.00 в будние дни.
    </p>
    <table border="1" cellpadding="13" cellspacing="15" width="100%" class="easy">
        <tbody cellpadding="13" cellspacing="15">
            <tr>
                <td colspan="4" align="center">
                    Стоимость доставки
                </td>
            </tr>
            <tr>
                <td>
                    Зона 1
                </td>
                <td>
                    Центр, внутри ТТК до 1000 кг
                </td>
                <td>
                    3000 руб.
                </td>
                <td>
                    Свыше 1000 кг цена расчетная, уточняйте у менеджера
                </td>
            </tr>
            <tr>
                <td>
                    Зона 2
                </td>
                <td>
                    Москва от ТТК до МКАД, до 1500 кг
                </td>
                <td>
                    2300 руб.
                </td>
                <td>
                    Свыше 1500 кг цена расчетная, уточняйте у менеджера
                </td>
            </tr>
            <tr>
                <td>
                    Зона 3
                </td>
                <td>
                    Доставка от МКАД до МБК (А108), до 1500 кг<br>
                </td>
                <td>
                    2300 руб.+40 руб./км
                </td>
                <td>
                    Свыше 1500 кг цена расчетная, уточняйте у менеджера
                </td>
            </tr>
            <tr>
                <td>
                    Зона 4
                </td>
                <td>
                    Доставка за МБК&nbsp;(А108)
                </td>
                <td>
                    Цена расчетная
                </td>
                <td>
                    Цена расчетная
                </td>
            </tr>
        </tbody>
    </table>
    <br>
    <p>
        <img width="965" alt="dostavka-zona.jpg" src="/upload/medialibrary/7e1/7e14337be7b7d51f463037f46c0ff785.jpg" height="400" title="dostavka-zona.jpg"><br>
    </p>
    <p class="easy">
        Доставка по регионам России осуществляется транспортными компаниями. Вы самостоятельно выбираете подходящую Вам транспортную компанию. Наша компания осуществляет доставку до транспортной компании согласно тарифам, указанным выше.
    </p>
    <p class="t2">
        Самовывоз
    </p>
    <p class="easy">
        Возможен самовывоз заказа со склада в г. Подольск или салона-магазина "Планета Плитки" на Нахимовском пр-те 50. Подробности уточняйте у менеджеров.
    </p>
</div>
<br><?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>