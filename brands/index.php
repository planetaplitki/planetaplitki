<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetPageProperty("description", "Фабрики-производители керамической плитки из Испании, Италии, России, Польши, Китая");
$APPLICATION->SetPageProperty("keywords", "Производители плитки, фабрики керамической плитки");
$APPLICATION->SetPageProperty("title", "Производители плитки Испания, Италия, Россия");
$APPLICATION->SetTitle("Производители");
$APPLICATION->SetPageProperty("pageclass", "main2 manufacturers_page");
?>

<?
$selectCountry = $_REQUEST['select_country'];
$GLOBALS['arrFilter'] = array();
//$GLOBALS['arrFilter']['DEPTH_LEVEL'] = 2;
if( eRU($selectCountry) ){
	$GLOBALS['arrFilter']['SECTION_ID'] = $selectCountry;
}

$sections = getSectionsByUserType();      
$brands = array_keys($sections['BRANDS']);
$GLOBALS['arrFilter']['ID'] = $brands;  
?>

<?$APPLICATION->IncludeComponent(
	"up:iblock.section.list", 
	"brands", 
	array(
		"ID" => "0",
		"SHOW_PARENT_NAME" => "Y",
		"IBLOCK_TYPE" => "1c_catalog",
		"IBLOCK_ID" => "34",
		"SECTION_ID" => "",
		"SECTION_CODE" => "",
		"SECTION_URL" => "",
		"COUNT_ELEMENTS" => "Y",
		"TOP_DEPTH" => "3",
		"SECTION_FIELDS" => array(
			0 => "ID",
			1 => "NAME",
			2 => "PICTURE",
			3 => "SECTION_PAGE_URL",
			4 => "DEPTH_LEVEL",
			5 => "",
		),
		"SECTION_USER_FIELDS" => array(
			0 => "",
			1 => "",
		),
		"ADD_SECTIONS_CHAIN" => "Y",
		"CACHE_TYPE" => "A",
		"CACHE_TIME" => "36000000",
		"CACHE_NOTES" => "",
		"CACHE_GROUPS" => "Y",
		"COMPONENT_TEMPLATE" => "brands",
		"FILTER_NAME" => "arrFilter",
		"SORT_FIELD" => "name",
		"SORT_ORDER" => "asc",
		"SORT_FIELD2" => "id",
		"SORT_ORDER2" => "desc",
		"URL_404" => "/404.php",
		"ELEMENT_CONTROLS" => "N",
		"AJAX_MODE" => "N",
		"AJAX_OPTION_JUMP" => "N",
		"AJAX_OPTION_STYLE" => "Y",
		"AJAX_OPTION_HISTORY" => "N",
		"AJAX_OPTION_ADDITIONAL" => "",
		"CACHE_FILTER" => "Y",
		"PAGER_TEMPLATE" => "modern",
		"DISPLAY_TOP_PAGER" => "N",
		"DISPLAY_BOTTOM_PAGER" => "Y",
		"PAGER_TITLE" => "",
		"PAGER_SHOW_ALWAYS" => "N",
		"PAGER_DESC_NUMBERING" => "N",
		"PAGER_DESC_NUMBERING_CACHE_TIME" => "36000",
		"PAGE_ELEMENT_COUNT" => "500",
		"UF_USER" => USER_TYPE
	),
	false
);?>

<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>