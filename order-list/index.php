<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetTitle("Список заказов");?>

    <div class="lc_left">
        <div class="lc_left_box">
            <a class="lc_left_box_p">Личная информация<span></span></a>
            <ul>
                <li><a>Личные данные</a></li>
            </ul>
        </div>
        <div class="lc_left_box">
            <a class="lc_left_box_p">Заказы<span></span></a>
            <ul>
                <li><a>Состояние заказов</a></li>
                <li><a>Содержание корзины</a></li>
                <li><a>История заказов</a></li>
            </ul>
        </div>
        <div class="lc_left_box">
            <a class="lc_left_box_p">Подписка<span></span></a>
            <ul>
                <li><a>Изменить подписку</a></li>
            </ul>
        </div>
    </div>

    <div class="order_status">
        <div class="order_status__box">
            <div class="order_status__box__left">
                <p class="box__left_title">Заказ №94 от 10.12.2014   10:25:47</p>
                <p class="box__left_easy">
                    Сумма: 35000 р. (не оплачен)<br>
                    Способ оплаты: наличными курьеру<br>
                    Способ доставки: Москва (в пределах МКАД)
                </p>
                <p class="box__left_title2">Состав заказа:</p>
                <p class="box__left_easy">
                    — Ювелирная бижутерия TM Alisa<br>
                    — Ювелирная бижутерия TM Alisa<br>
                    — Ювелирная бижутерия TM Alisa
                </p>
            </div>
            <div class="order_status__box__right">
                <div class="box__right__statusname">Принят, ожидается оплата</div>
                <div class="box__right__links">
                    <a href="#"  class="box__right__link"><span class="repeat"></span>Повторить заказ</a><br>
                    <a href="#"  class="box__right__link"><span class="cancel"></span>Отменить заказ</a>
                </div>
                <a href="#" class="box__right__more">Подробнее<span></span></a>
            </div>
        </div>
        <div class="order_status__box">
            <div class="order_status__box__left">
                <p class="box__left_title">Заказ №94 от 10.12.2014   10:25:47</p>
                <p class="box__left_easy">
                    Сумма: 35000 р. (не оплачен)<br>
                    Способ оплаты: наличными курьеру<br>
                    Способ доставки: Москва (в пределах МКАД)
                </p>
                <p class="box__left_title2">Состав заказа:</p>
                <p class="box__left_easy">
                    — Ювелирная бижутерия TM Alisa<br>
                    — Ювелирная бижутерия TM Alisa<br>
                    — Ювелирная бижутерия TM Alisa
                </p>
            </div>
            <div class="order_status__box__right">
                <div class="box__right__statusname">Принят, ожидается оплата</div>
                <div class="box__right__links">
                    <a href="#"  class="box__right__link"><span class="repeat"></span>Повторить заказ</a><br>
                    <a href="#"  class="box__right__link"><span class="cancel"></span>Отменить заказ</a>
                </div>
                <a href="#" class="box__right__more">Подробнее<span></span></a>
            </div>
        </div>
    </div>

<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>