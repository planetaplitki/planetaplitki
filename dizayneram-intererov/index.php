<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetTitle("Дизайнерам интерьеров");
?><div style="line-height: 1.6em;">
	 Мы рады предложить партнерскую программу, которая заключается в:&nbsp;<br>
	<ul>
		<li>специальной информационной поддержке,&nbsp;</li>
		<li>преимущественных условиях по обслуживанию и контролю ваших заказов, </li>
		<li>возможности оперативно получить ответ на Ваш индивидуальный запрос, резервировать товар и получать информацию о наличии плитки,&nbsp;</li>
		<li>обеспечении образцами и каталогами фабрик-производителей,&nbsp;</li>
		<li>предоставлении гибких цен и выгодных условий,&nbsp;</li>
		<li>размещении фотографий своих лучших проектов на нашем сайте.</li>
	</ul>
</div>
<div style="line-height: 1.6em;">
	<p style="text-align: center;">
	</p>
	<table style="width: 60%;">
	<tbody>
	<tr>
		<td colspan="3">
			<h3><strong>Менеджеры</strong></h3>
 <br>
		</td>
	</tr>
	<tr>
		<td>
			 Гаврикова Екатерина<br>
			 8 (495) 646-16-90,&nbsp;доб. 956<br>
			 8 (800) 500-82-29, доб. 3<br>
 <a href="mailto:gavrikova@planetaplitki.ru">gavrikova@planetaplitki.ru</a>
		</td>
		<td>
 <br>
		</td>
	</tr>
	</tbody>
	</table>
</div>
<p>
 <strong><br>
 </strong>
</p>
<p>
 <strong>Приглашаем к плодотворному сотрудничеству!</strong>
</p><?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>