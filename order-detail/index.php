<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetTitle("Заказ детально");?>

    <div class="lc_left lc_left_order_detail">
        <div class="lc_left_box">
            <a class="lc_left_box_p">Личная информация<span></span></a>
            <ul>
                <li><a>Личные данные</a></li>
            </ul>
        </div>
        <div class="lc_left_box">
            <a class="lc_left_box_p">Заказы<span></span></a>
            <ul>
                <li><a>Состояние заказов</a></li>
                <li><a>Содержание корзины</a></li>
                <li><a>История заказов</a></li>
            </ul>
        </div>
        <div class="lc_left_box">
            <a class="lc_left_box_p">Подписка<span></span></a>
            <ul>
                <li><a>Изменить подписку</a></li>
            </ul>
        </div>
    </div>

    <div class="order_detail">
        <p class="order_detail__title">Заказ № 94 от 18.12.2013 10:22:18</p>
        <p class="order_detail__easy">
            Текущий статус заказа: Принят, ожидается оплата (от 18.12.2013 10:22:18)<br>
            Сумма заказа: 2 950 Р<br>
            Отменен: Нет (<a href="#" class="order_detail__link">Отменить</a>)
        </p>
        <p class="order_detail__title">Данные вашей учетной записи</p>
        <p class="order_detail__easy">
            Учетная запись: Андрей<br>
            Логин: skeicher<br>
            E-Mail адрес: <a href="mailto:skeicher@li.ru" class="order_detail__link">skeicher@li.ru</a>
        </p>
        <p class="order_detail__title">Параметры заказа</p>
        <p class="order_detail__easy">
            Тип плательщика: Физическое лицо<br>
            Личные данные<br>
            Имя:	Андрей<br>
            E-Mail: skeicher@li.ru<br>
            Телефон: 89258597272<br>
            Город: Москва<br>
            Данные для доставки<br>
            Индекс: 100100<br>
            Адрес доставки: Москва, ул. Куршава
        </p>
        <p class="order_detail__title">Оплата и доставка</p>
        <p class="order_detail__easy">
            Платежная система:	Наличные курьеру<br>
            Оплачен: Нет
        </p>
        <p class="order_detail__title">Служба доставки</p>
        <p class="order_detail__easy">
            Служба доставки: Доставка по Москве
        </p>
        <p class="order_detail__title">Содержимое доставки</p>
        <table class="order_detail__table">
            <tr class="order_detail__table__header">
                <td>Заголовочная ячейка</td>
                <td>Заголовочная ячейка</td>
                <td>Заголовочная ячейка</td>
                <td>Заголовочная ячейка</td>
            </tr>
            <tr>
                <td>Ячейка с информацией</td>
                <td>Ячейка с информацией</td>
                <td>Ячейка с информацией</td>
                <td>Ячейка с информацией</td>
            </tr>
            <tr class="order_detail__table__sep">
                <td>Ячейка с информацией</td>
                <td>Ячейка с информацией</td>
                <td>Ячейка с информацией</td>
                <td>Ячейка с информацией</td>
            </tr>
            <tr>
                <td>Ячейка с информацией</td>
                <td>Ячейка с информацией</td>
                <td>Ячейка с информацией</td>
                <td>Ячейка с информацией</td>
            </tr>
        </table>
    </div>

<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>