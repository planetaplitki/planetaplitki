<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetTitle("Реквизиты компании");
?>
<meta name="robots" content="noindex"/>
<table cellpadding="10" style="width: 90%;">
<tbody>
<tr>
	<td>
		 Наименование Клиента
	</td>
	<td>
		 Общество с ограниченной ответственностью "ЭЛЬБА"<br>
		 (ООО «ЭЛЬБА»)
	</td>
</tr>
<tr>
	<td>
		 Юридический адрес
	</td>
	<td>
		 117292, г. Москва,&nbsp;Нахимовский проспект, д.50, этаж 1, помещение I, комнаты 1, 7, 8
	</td>
</tr>
<tr>
	<td>
		 Фактический адрес
	</td>
	<td>
		 117292, г. Москва,&nbsp;Нахимовский проспект, д.50
	</td>
</tr>
<tr>
	<td>
		 Телефон
	</td>
	<td>
		 (495) 646-16-90<br>
		 8&nbsp;800&nbsp;500 82 29
	</td>
</tr>
<tr>
	<td>
		 ИНН/КПП
	</td>
	<td>
		 7728372790/772801001
	</td>
</tr>
<tr>
	<td>
		 ОГРН
	</td>
	<td>
		 1177746581000
	</td>
</tr>
<tr>
	<td>
		 Банк
	</td>
	<td>
		 ПАО "ВТБ 24" г. Москва
	</td>
</tr>
<tr>
	<td>
		 БИК
	</td>
	<td>
		 044525716
	</td>
</tr>
<tr>
	<td>
		 Расчетный счет
	</td>
	<td>
		 40702810200000100292
	</td>
</tr>
<tr>
	<td>
		 Корр. счет
	</td>
	<td>
		 30101810100000000716
	</td>
</tr>
<tr>
	<td>
		 ОКПО
	</td>
	<td>
		 15982658
	</td>
</tr>
<tr>
	<td>
		 ОКТМО
	</td>
	<td>
		 45397000
	</td>
</tr>
<tr>
	<td>
		 ОКВЭД
	</td>
	<td>
		 47.52.7
	</td>
</tr>
</tbody>
</table>
 <br><?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>