<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetTitle("Возврат товара");
?><p>
 <span style="color: #000000; font-size: 11pt;">1. Покупатель вправе возвратить Товар надлежащего качества в соответствии со статьей 25 «Право потребителя на обмен товара надлежащего качества» Закона РФ «О защите прав потребителей». </span><span style="font-size: 11pt;"> </span>
</p>
 <span style="font-size: 11pt;"> </span><span style="color: #000000; font-size: 11pt;"> </span><span style="font-size: 11pt;"> </span>
<p>
 <span style="font-size: 11pt;"> </span><span style="color: #000000; font-size: 11pt;">
	2. Возврат Товара надлежащего качества возможен в случае, если сохранены его товарный вид, потребительские свойства, а также документ, подтверждающий факт и условия покупки Товара. </span><span style="font-size: 11pt;"> </span>
</p>
 <span style="font-size: 11pt;"> </span><span style="color: #000000; font-size: 11pt;"> </span><span style="font-size: 11pt;"> </span>
<p>
 <span style="font-size: 11pt;"> </span><span style="color: #000000; font-size: 11pt;">
	3. Покупатель вправе не позднее 7 (семи) дней после получения Товара известить Продавца о нарушениях условий Договора, касающихся количества, ассортимента, качества, комплектности, упаковки и (или) тары Товара. </span><span style="font-size: 11pt;"> </span>
</p>
 <span style="font-size: 11pt;"> </span><span style="color: #000000; font-size: 11pt;"> </span><span style="font-size: 11pt;"> </span>
<p>
 <span style="font-size: 11pt;"> </span><span style="color: #000000; font-size: 11pt;">
	4. При отказе Покупателя от Товара Продавец должен возвратить ему денежную сумму, уплаченную Покупателем по настоящему Договору, за исключением расходов Продавца на доставку от Покупателя возвращенного Товара, не позднее чем через 10 (десять) дней со дня предъявления Покупателем соответствующего требования. </span><span style="font-size: 11pt;"> </span>
</p>
 <span style="font-size: 11pt;"> </span><span style="color: #000000; font-size: 11pt;"> </span><span style="font-size: 11pt;"> </span>
<p>
 <span style="font-size: 11pt;"> </span><span style="color: #000000; font-size: 11pt;">
	5. Требование Покупателя о возврате денежных средств, переданных за Товар ненадлежащего качества, подлежит удовлетворению в течение 10 (десяти) дней. </span><span style="font-size: 11pt;"> </span>
</p>
 <span style="font-size: 11pt;"> </span><span style="color: #000000; font-size: 11pt;"> </span><span style="font-size: 11pt;"> </span>
<p>
 <span style="font-size: 11pt;"> </span><span style="color: #000000; font-size: 11pt;">
	6. В случае если возврат суммы, уплаченной Покупателем в соответствии с настоящим Договором, осуществляется не одновременно с возвратом Товара Покупателем, возврат указанной суммы осуществляется Продавцом с согласия Покупателя одним из следующих способов: </span><span style="font-size: 11pt;"> </span>
</p>
 <span style="font-size: 11pt;"> </span><span style="color: #000000; font-size: 11pt;"> </span><span style="font-size: 11pt;"> </span>
<p>
 <span style="font-size: 11pt;"> </span><span style="color: #000000; font-size: 11pt;">
	&nbsp; &nbsp; &nbsp; &nbsp; &nbsp;6.1. наличными денежными средствами по месту нахождения Продавца; </span><span style="font-size: 11pt;"> </span>
</p>
 <span style="font-size: 11pt;"> </span><span style="color: #000000; font-size: 11pt;"> </span><span style="font-size: 11pt;"> </span>
<p>
 <span style="font-size: 11pt;"> </span><span style="color: #000000; font-size: 11pt;">
	&nbsp; &nbsp; &nbsp; &nbsp; &nbsp;6.2. путем перечисления соответствующей суммы на банковский или иной счет Покупателя, указанный Покупателем </span><span style="font-size: 11pt;"> </span>
</p>
 <span style="font-size: 11pt;"> </span><span style="color: #000000; font-size: 11pt;"> </span><span style="font-size: 11pt;"> </span>
<p>
 <span style="font-size: 11pt;"> </span><span style="color: #000000; font-size: 11pt;">
	7. Возврат Товара производится на основании письменного обращения Покупателя предоставляемого Продавцу по адресу Продавца: 117292, г. Москва, ул. Нахимовский пр-т, д. 50 и на электронную почту </span><a href="mailto:nahim@planetaplitki.ru"><span style="color: #000000; font-size: 11pt;">nahim@planetaplitki.ru</span></a><span style="color: #000000; font-size: 11pt;">. </span><span style="font-size: 11pt;"> </span>
</p>
 <span style="font-size: 11pt;"> </span><span style="color: #000000; font-size: 11pt;"> </span><span style="font-size: 11pt;"> </span>
<p>
 <span style="font-size: 11pt;"> </span><span style="color: #000000; font-size: 11pt;">
	8. В случае обнаружения Покупателем недостатков Товара и предъявления требования о его замене, Товар подлежит замене в течение 10 (десяти) дней со дня предъявления указанного требования, при необходимости дополнительной проверки качества такого Товара Поставщиком - в течение 20 (двадцати) дней со дня предъявления требования. </span><span style="font-size: 11pt;"> </span>
</p>
 <span style="font-size: 11pt;"> </span><span style="color: #000000; font-size: 11pt;"> </span><span style="font-size: 11pt;"> </span>
<p>
 <span style="font-size: 11pt;"> </span><span style="color: #000000; font-size: 11pt;">
	Если у Поставщика в момент предъявления требования отсутствует необходимый для замены Товар, замена должна быть проведена в течение 2 (двух) месяцев со дня предъявления такого требования. </span><span style="font-size: 11pt;"> </span>
</p>
 <span style="font-size: 11pt;"> </span><span style="color: #000000; font-size: 11pt;"> </span><span style="font-size: 11pt;"> </span>
<p>
 <span style="font-size: 11pt;"> </span><span style="color: #000000; font-size: 11pt;">
	Покупатель вместо предъявления требований вправе отказаться от исполнения договора и потребовать возврата уплаченной за Товар суммы, в таком случае по требованию Поставщика и за его счет Покупатель должен возвратить Поставщику Товар с недостатками.</span>
</p><?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>