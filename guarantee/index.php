<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetPageProperty("title", "Гарантии");
$APPLICATION->SetTitle("");       
?><span style="font-size: 14pt;"><br>
 </span><br>
 <span style="font-size: 14pt;">Клиентам компании "Планета Плитки" гарантируется:<br>
 </span><br>
<h3><b><img width="10" alt="exc3.png" src="/upload/medialibrary/9e8/exc3.png" height="10" title="exc3.png">&nbsp;Докупка плитки в любом необходимом количестве</b> </h3>
<p>
 <span style="font-size: 11pt;">Клиенты нашего магазина имеют возможность докупить недостающий материал из складской программы в любом количестве (от 1 штуки). </span>
</p>
 <span style="font-size: 11pt;"> </span>
<hr class="line-block">
<h3><b><b><img width="10" alt="exc3.png" src="/upload/medialibrary/9e8/exc3.png" height="10" title="exc3.png"></b>&nbsp;Возврат товара </b> </h3>
<p>
 <span style="font-size: 11pt;">Вы имеете право вернуть товар в течении 14 дней с момента его получения.&nbsp;</span>
</p>
<p>
 <span style="font-size: 11pt;">Подробнее о правилах&nbsp;возврата товара читайте в разделе "<a href="/vozvrat/">Возврат товара</a>".&nbsp;</span>
</p>
 <span style="font-size: 11pt;"> </span>
<hr class="line-block">
<h3><b><b><img width="10" alt="exc3.png" src="/upload/medialibrary/9e8/exc3.png" height="10" title="exc3.png"></b>&nbsp;Лучшая цена&nbsp;на складскую программу</b> </h3>
<p>
 <span style="font-size: 11pt;">Мы являемся официальными дистрибьюторами лучших испанских и итальянских фабрик и готовы предложить Вам самую выгодную цену на наш материал.&nbsp;</span>
</p>
<p>
 <span style="font-size: 11pt;">Подробнее о предложении Вы можете узнать <a href="/all-shares/nashli-deshevle/">здесь</a>.</span>
</p>
<hr class="line-block">
<h3><b><b><img width="10" alt="exc3.png" src="/upload/medialibrary/9e8/exc3.png" height="10" title="exc3.png"></b>&nbsp;Возможность получить плитку сразу после покупки</b> </h3>
<p>
 <span style="font-size: 11pt;">Наш складской ассортимент состоит более чем из 150 самых актуальных коллекций. Вы можете совершить покупку и сразу забрать плитку на складе в г. Подольск.</span>
</p>
<hr class="line-block">
<h3><b><b><img width="10" alt="exc3.png" src="/upload/medialibrary/9e8/exc3.png" height="10" title="exc3.png"></b>&nbsp;Наличие самых трендовых коллекций в складском ассортименте</b> </h3>
<p>
 <span style="font-size: 11pt;">Мы посещаем все профильные выставки керамической плитки и отбираем лучшие коллекции испанских и итальянских фабрик, поэтому наш ассортимент постоянно пополняется и обновляется самым актуальным материалом.</span>
</p>
<p>
 <span style="font-size: 11pt;">Ознакомится и выбрать лучшие коллекции Вы можете в разделе "<a href="/catalog/new/">Новые коллекции</a>".&nbsp;</span>
</p>
<hr class="line-block">
<h3><b><b><img width="10" alt="exc3.png" src="/upload/medialibrary/9e8/exc3.png" height="10" title="exc3.png"></b>&nbsp;Гибкая система&nbsp;скидок</b> </h3>
<p>
 <span style="font-size: 11pt;">Индивидуальная гибкая система скидок в зависимости от суммы покупки.</span>
</p>
<p>
 <span style="font-size: 11pt;">О размере возможной скидки Вы можете уточнить</span><span style="font-size: 11pt;">&nbsp;у менеджеров по телефону +7 (495) 646-16-90, доб. 1 или 8 (800) 500-82-29 (Звонок по России бесплатный), а так же в любом из <a href="/contacts/">магазинов "Планета Плитки"</a>.</span>
</p>
<hr>
 <b>
<p style="text-align: center;">
 <br>
</p>
<p style="text-align: center;">
	 СЕРТИФИКАТЫ, ПОДТВЕРЖДАЮЩИЕ ОФИЦИАЛЬНОЕ СОТРУДНИЧЕСТВО С ФАБРИКАМИ
</p>
 </b>
<p>
	 &nbsp; &nbsp; &nbsp; &nbsp;
</p>

<?$APPLICATION->IncludeComponent(
	"bitrix:news.list",
	"gallery_photo",
	Array(
		"ACTIVE_DATE_FORMAT" => "d.m.Y",
		"ADD_SECTIONS_CHAIN" => "N",
		"AJAX_MODE" => "N",
		"AJAX_OPTION_ADDITIONAL" => "",
		"AJAX_OPTION_HISTORY" => "N",
		"AJAX_OPTION_JUMP" => "N",
		"AJAX_OPTION_STYLE" => "N",
		"CACHE_FILTER" => "Y",
		"CACHE_GROUPS" => "Y",
		"CACHE_TIME" => "36000000",
		"CACHE_TYPE" => "A",
		"CHECK_DATES" => "Y",
		"COMPONENT_TEMPLATE" => "",
		"DETAIL_URL" => "",
		"DISPLAY_BOTTOM_PAGER" => "N",
		"DISPLAY_DATE" => "N",
		"DISPLAY_NAME" => "N",
		"DISPLAY_PICTURE" => "N",
		"DISPLAY_PREVIEW_TEXT" => "N",
		"DISPLAY_TOP_PAGER" => "N",
		"FIELD_CODE" => array(0=>"ID",1=>"NAME",2=>"PREVIEW_PICTURE",3=>"DETAIL_PICTURE",),
		"FILTER_NAME" => "",
		"HIDE_LINK_WHEN_NO_DETAIL" => "N",
		"IBLOCK_ID" => "40",
		"IBLOCK_TYPE" => "data",
		"INCLUDE_IBLOCK_INTO_CHAIN" => "N",
		"INCLUDE_SUBSECTIONS" => "Y",
		"MESSAGE_404" => "",
		"NEWS_COUNT" => "9999",
		"PAGER_BASE_LINK_ENABLE" => "N",
		"PAGER_DESC_NUMBERING" => "N",
		"PAGER_DESC_NUMBERING_CACHE_TIME" => "36000",
		"PAGER_SHOW_ALL" => "N",
		"PAGER_SHOW_ALWAYS" => "N",
		"PAGER_TEMPLATE" => "",
		"PAGER_TITLE" => "Новости",
		"PARENT_SECTION" => "",
		"PARENT_SECTION_CODE" => "",
		"PREVIEW_TRUNCATE_LEN" => "150",
		"PROPERTY_CODE" => array(0=>"LINK",1=>"",),
		"SET_BROWSER_TITLE" => "N",
		"SET_LAST_MODIFIED" => "N",
		"SET_META_DESCRIPTION" => "N",
		"SET_META_KEYWORDS" => "N",
		"SET_STATUS_404" => "N",
		"SET_TITLE" => "N",
		"SHOW_404" => "N",
		"SORT_BY1" => "SORT",
		"SORT_BY2" => "ID",
		"SORT_ORDER1" => "ASC",
		"SORT_ORDER2" => "DESC"
	)
);?>
<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>